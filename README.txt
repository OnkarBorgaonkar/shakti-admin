Shakti Warriors

Location:
http://svn.dev.utilivisor.com/tbd/shakti

Database schema:
http://svn.dev.utilivisor.com/tbd/shakti/db/schema_2007-07-12.pdf

Initial schema description:
http://svn.dev.utilivisor.com/tbd/shakti/db/schema.sql

Database data load scripts:
http://svn.dev.utilivisor.com/tbd/propertySearch/db/categories.sql
http://svn.dev.utilivisor.com/tbd/propertySearch/db/human_powers.sql
http://svn.dev.utilivisor.com/tbd/propertySearch/db/ranks.sql
http://svn.dev.utilivisor.com/tbd/propertySearch/db/super_powers.sql
http://svn.dev.utilivisor.com/tbd/propertySearch/db/superheroes.sql
http://svn.dev.utilivisor.com/tbd/propertySearch/db/tasks.sql

This project (shakti) is a complete redevelopment of shaktiwarriors.com, begun on 2007-02-15.
The subversion project "shaktiWarriors" is the pre-existing codebase.

The database is PostgreSQL, the server side scripts are PHP. The frontend uses jQuery-based Javascript.

Hosting Requirements:

PostgreSQL 8.1.3

PHP 5.2.1
Here's a list of configuration particulars to which attention should be paid:

PHP must be compiled with PostgreSQL 8 support (of course). In addition, it must be compiled with PDO (PHP Data Objects) support, and the PostgreSQL PDO driver (pdo_pgsql). Hash support must be enabled, with the SHA256 hashing engine available. JSON support must also be available.

In contrast to the other recent PHP applications written at Tech By Design, Shakti Warriors is not limited by Capital Internet's PHP 4 hosting environment, and many features of PHP 5 are taken advantage of, notable PDO (PHP Data Objects) for persistence. Tom Duffey has some experience with this extension from the Johnsonville "Stories" project.

The live site is hosted here:
http://www.shaktiwarriors.com/

You can access it as
shakti08@shaktiwarriors.com with the password "coclespi". It is deployed by simple svn checkout.

There is a preview site:
http://www.wildeyechicago.com/

You can access it with the same login and password. The administrator of both sites is Corin Martens <cmartens@martenology.com>.

There is a web control panel (useful for phpPgAdmin)
http://www.shaktiwarriors.com/cpanel/

Username: "shakti" password: "cupid".

================================================================================
Frontend
================================================================================

================================================================================
Backend
================================================================================

/config/Config.php:
This static class is used to set the include_path based on the application's installation location using __FILE__. It also sets database connection parameters. It does not automatically detect any differences between production and development environments.

__autoload.php:
You do not need to explicity load files using require() or include() as you might expect. Classes are loaded automatically using PHP5's autoload facility and the include path set above. This imposes a simple restriction on filenames: a class named "User" must be stored in a file named "User.php" in a location listed in the Config class' setIncludePath() method.
http://php.net/autoload

--------------------------------------------------------------------------------
Gotchas
--------------------------------------------------------------------------------

Lane wants to expand the human powers and super powers to allow children to specify their own powers. Ad hoc entry of powers.
