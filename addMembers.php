<?php
    require_once("globals.php");
    require_once("Authenticator.php");
    require_once("isGroupLeader.php");
    
    $userService = new UserService();
    $nonMembers = $userService->find("id NOT IN (SELECT user_id FROM user_groups WHERE group_id = $group->id) ORDER BY login");
    
    $members = $group->getMembers();

    $pageTitle = "Remove a Member";



    include("addMembers.phtml");

?>
