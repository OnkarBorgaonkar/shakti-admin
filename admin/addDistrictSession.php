<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $pageTitle = "Admin Add Session";
    $section = "districts";

    $districtId = $_REQUEST['id'];

    if ($districtId == null) {
        die ("a district must be supplied");
    }

    if ($admin->role != "admin") {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    $sessionService = new SessionService();
    $sessions = $sessionService->getSesssionsNotIn($districtId);

    include("admin/addDistrictSession.phtml");
?>