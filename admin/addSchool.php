<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");
    $pageTitle = "Admin Add School";
	$section = "districts";

    include("admin/addSchool.phtml");
?>