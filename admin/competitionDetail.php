<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    if ($admin->role != "admin") {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    $id = $_GET['id'];

    $competitionService = new CompetitionService();
    $competition = array_pop($competitionService->find("id = $id"));

    if($competition == null)
        die("Competition does not exist");

	$section = "competitions";

    include("admin/competitionDetail.phtml");
?>