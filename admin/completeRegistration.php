<?php
    require_once("../globals.php");

    if(!isset($_GET['key'])) {
        die("invalid request");
    }

    $adminUserService = new AdminUserService();
    $adminUser = array_pop($adminUserService->find("activation_key = '{$_GET['key']}' AND active = false"));

    if ($adminUser == null) {
		include("admin/invalidRegistration.phtml");
        die();
	}

    include("admin/completeRegistration.phtml");
?>