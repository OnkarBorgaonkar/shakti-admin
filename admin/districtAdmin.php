<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $id = $_GET['id'];

    $districtService = new DistrictService();
    $district = array_pop($districtService->find("id = $id"));

    if ($district == null)
        die("district does not exist");

    if (!$district->canEdit($admin)) {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    $pageTitle = "Admin District";
	$section = "districts";

	$schools = $district->getSchools($admin->role != 'admin');

    include("admin/districtAdmin.phtml");
?>