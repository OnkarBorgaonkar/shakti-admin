<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $classroom = new Classroom();

    if (isset($_GET['id'])) {
        $classroomService = new ClassroomService();
        $classroom = array_pop($classroomService->find("id = {$_GET['id']}"));
        if ($classroom == null)
            die("Classroom not found");
    }

    if (isset($_GET['schoolId'])) {
        $classroom->schoolId = $_GET['schoolId'];
    }

    $gradeTypes = $classroom->getGradeTypes();
    $computerTypeResults = $classroom->getComputerTypes();
    $computerTypes = array();
    $otherValue = "";
    foreach ($computerTypeResults as $computerType) {
        $computerTypes[] = $computerType->computerType;
        if ($computerType->computerType == "OTHER") {
            $otherValue = $computerType->otherValue;
        }
    }

    $school = $classroom->getSchool();

    if ($classroom->isPersisted()) {
        if (!$classroom->canEdit($admin)) {
            $_SESSION['errorMessage'] = "You do not have access to this";
            header("location: /admin/login.php");
        }
    }
    else {
        if (!$school->canEdit($admin)) {
            $_SESSION['errorMessage'] = "You do not have access to this";
            header("location: /admin/login.php");
        }
    }

    $classroomDays = $classroom->getDays();

    $district = $school->getDistrict();

    $pageTitle = "Admin Edit Classroom";
    $section = "districts";

    include("admin/editClassroom.phtml");
?>