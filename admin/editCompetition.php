<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    if ($admin->role != "admin") {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    $competition = new Competition();

    if (isset($_GET['id'])) {
        $competitionService = new CompetitionService();
        $competition = array_pop($competitionService->find("id = {$_GET['id']}"));
    }

	$section = "competitions";

    include("admin/editCompetition.phtml");
?>