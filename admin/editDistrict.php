<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $pageTitle = "Admin Edit District";
	$section = "districts";

    $district = new District();

    if (isset($_GET['id'])) {
        $districtService = new DistrictService();
        $district = array_pop($districtService->find("id = {$_GET['id']}"));
    }

    if ($district->isPersisted()) {
        if (!$district->canEdit($admin)) {
            $_SESSION['errorMessage'] = "You do not have access to this";
            header("location: /admin/login.php");
        }
    }
    else if ($admin->role != "admin") {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    include("admin/editDistrict.phtml");
?>