<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $pageTitle = "Admin Edit District - Basic";
	$section = "districts";

    include("admin/editDistrictBasic.phtml");
?>