<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $pageTitle = "Admin Add School Program";
	$section = "districts";

    $program = new Program();

    if (isset($_GET['id'])) {
        $programService = new ProgramService();
        $program = array_pop($programService->find("id = {$_GET['id']}"));
        if ($program == null)
            die("Program not found");
    }

    if (isset($_GET['schoolId']))
        $program->schoolId = $_GET['schoolId'];

    $school = $program->getSchool();
    if (!$school->canEdit($admin)) {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }
    $district = $school->getDistrict();

    include("admin/editProgram.phtml");
?>