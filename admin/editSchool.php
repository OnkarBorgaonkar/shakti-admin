<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $pageTitle = "Admin Edit School";
    $section = "districts";

    $school = new School();

    if (isset($_GET['id'])) {
        $schoolService = new SchoolService();
        $school = array_pop($schoolService->find("id = {$_GET['id']}"));
        if ($school == null)
            die("school not found");
    }

    if (isset($_GET['districtSessionId'])) {
        $school->districtSessionId = $_GET['districtSessionId'];
    }

    $district = $school->getDistrict();

    if ($school->isPersisted()) {
        if (!$school->canEdit($admin)) {
            $_SESSION['errorMessage'] = "You do not have access to this";
            header("location: /admin/login.php");
        }
    }
    else {
        if (!$district->canEdit($admin)) {
            $_SESSION['errorMessage'] = "You do not have access to this";
            header("location: /admin/login.php");
        }
    }

    include("admin/editSchool.phtml");
?>