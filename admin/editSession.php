<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $pageTitle = "Admin Edit Session";
    $section = "districts";

    if ($admin->role != "admin") {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    $session = new Session();

    if (isset($_GET['id'])) {
        $sessionService = new SessionService();
        $session = array_pop($sessionService->find("id = {$_GET['id']}"));
    }


    include("admin/editSession.phtml");
?>