<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");
    $pageTitle = "Admin Home";
	$section = "districts";

    if ($admin->role != "admin") {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    $districtService = new DistrictService();
    $districts = $districtService->find();

    include("admin/index.phtml");
?>