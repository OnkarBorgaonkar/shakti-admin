<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    if (!isset($_GET['week'])) {
        die("invalid request");
    }

    $week = $_GET['week'];

    $lessonPlanService = new LessonPlanService();
    $lessonPlans = $lessonPlanService->find("week <= $week ORDER BY week");

    $nextWeeksLessonPlan = null;

    if ($week < 14) {
        $nextWeek = $week + 1;
        $nextWeeksLessonPlan = array_pop($lessonPlanService->find("week = $nextWeek"));
    }

    $currentLessonPlan = null;

    foreach ($lessonPlans as $i=>$lessonPlan) {
        if ($lessonPlan->week == $week) {
            $currentLessonPlan = $lessonPlan;
            unset($lessonPlans[$i]);
        }
    }

    $webActivityEntryService = new WebActivityEntryService();
    $currentFirstYearWebActivityEntries = $webActivityEntryService->find("week = $week AND academy_year = 1 ORDER BY week");
    $currentSecondYearWebActivityEntries = $webActivityEntryService->find("week = $week AND academy_year = 2 ORDER BY week");

    $weeklyReminderService = new WeeklyReminderService();
    $weeklyReminders = $weeklyReminderService->find("week <= $week ORDER BY week DESC LIMIT 5");
    $weeklyReminders = array_reverse($weeklyReminders);

    $asTimePermitsEntryService = new AsTimePermitsEntryService();
    $asTimePermitsEntries = $asTimePermitsEntryService->find("week <= $week ORDER BY week");

	$section = "lessonPlans";

    include("admin/lessonPlan.phtml");
?>