<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    if ($admin->role != 'admin') {
        header("location: /admin/login.php");
    }

    $competitionService = new CompetitionService();
    $competitions = $competitionService->find("true ORDER BY name");

	$section = "competitions";

    include("admin/manageCompetitions.phtml");
?>