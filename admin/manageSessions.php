<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");
    $pageTitle = "Admin Manage Sessions";
    $section = "districts";

    if ($admin->role != "admin") {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    $sessionService = new SessionService();
    $sessions = $sessionService->find("true");

    include("admin/manageSessions.phtml");
?>