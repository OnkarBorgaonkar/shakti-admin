<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $id = $_GET['id'];

    $classroomService = new ClassroomService();
    $classroom = array_pop($classroomService->find("id = $id"));

    if ($classroom == null)
        die("Classroom doesn't exist");

    if (!$classroom->canEdit($admin)) {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    $school = $classroom->getSchool();
    $district = $school->getDistrict();

    $pageTitle = "Admin Manage Students";
    $section = "districts";

    include("admin/manageStudents.phtml");
?>