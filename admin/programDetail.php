<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $id = $_GET['id'];

    $programService = new ProgramService();
    $program = array_pop($programService->find("id = $id"));

    if ($program == null)
        die("Program does not exist");

    if (!$program->canEdit($admin)) {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    $school = $program->getSchool();
    $district = $school->getDistrict();

    $pageTitle = "Admin Program Detail";
	$section = "districts";

    include("admin/programDetail.phtml");
?>