<?php
    require_once("../globals.php");

    if(!isset($_GET['key'])) {
        die("invalid request");
    }

    $adminUserService = new AdminUserService();
    $adminUser = array_pop($adminUserService->find("activation_key = '{$_GET['key']}' AND active = true"));

    if ($adminUser == null) {
		include("admin/invalidResetPassword.phtml");
        die();
	}

    include("admin/resetPassword.phtml");
?>