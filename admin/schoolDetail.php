<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $id = $_GET['id'];

    $schoolService = new SchoolService();
    $school = array_pop($schoolService->find("id = $id"));

    if($school == null)
        die("School does not exist");

    if (!$school->canEdit($admin)) {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    $district = $school->getDistrict();

    $pageTitle = "Admin School Detail";
	$section = "districts";

	$classrooms = $school->getClassrooms($admin->role != 'admin');

    include("admin/schoolDetail.phtml");
?>