<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    if ($admin->role != "admin") {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    $districtService = new DistrictService();
    $districts = $districtService->find("active = true ORDER BY name");

	$section = "professionalDevelopment";

    include("admin/schoolReport.phtml");
?>