<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    if ($admin->role != "admin") {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

	$section = "professionalDevelopment";

    include("admin/warriorReport.phtml");
?>