<?php
    require_once("../globals.php");
    require_once("AdminAuthenticator.php");

    $pageTitle = "Admin Weekly Lesson Plan";

    if ($admin->role != "classroom" || $admin->classroomId == null) {
        Header("Location:/admin/lessonPlan.php?week=1");
        die();
    }

    $classroomGroupService = new ClassroomGroupService();
    $classroomGroups = $classroomGroupService->find("classroom_id = $admin->classroomId");

    include("admin/weeklyLessonPlan.phtml");
?>