<?php
    require_once("../../globals.php");

    $districtSession = new DistrictSession();
    $districtSession->setAll($_POST['districtSession']);

    $districtSessionService = new DistrictSessionService();
    $duplicate = array_pop($districtSessionService->find("district_id = $districtSession->districtId AND session_id = $districtSession->sessionId"));
    if ($duplicate != null) {
        $message = "You are already signed up for this session";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    $districtSessionService->save($districtSession);

    $_SESSION['successMessage'] = "Session Added";

    $json = Array(
        "success" => true,
        "id"=> $session->id
    );
    echo json_encode($json);
    die();
?>