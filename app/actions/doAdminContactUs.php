<?php
    require_once("../../globals.php");

    $mailService = new MailService();
    $mailService->adminContactUs($_POST['email'], $_POST['name'], $_POST['phone'], $_POST['reason'], $_POST['comments']);

    $json = array (
        "success" => true,
        "systemMessage" => "Email Sent"
    );

    echo json_encode($json);
    die();
?>