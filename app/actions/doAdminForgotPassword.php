<?php
require_once("../../globals.php");

$adminUserService = new AdminUserService();
$adminUser = array_pop($adminUserService->find("email = '{$_POST['email']}'"));

if ($adminUser != null) {

    if (!$adminUser->active) {
        $_SESSION['errorMessage'] = "The account you entered is not active yet. Please finish the registration process. If you need us to resend the email, please click <a href=\"/app/actions/doResendAdminEmail.php?email=" . urlencode($adminUser->email) ."\"/>here</a>";
        header("location: /admin/forgotPassword.php");
        die();
    }

    $adminUser->activationKey = uniqid(null, true);
    $adminUserService->save($adminUser);

    $mailService = new MailService();
    $mailService->forgotPassword($adminUser);

    $_SESSION['successMessage'] = "Email Sent";
    header("location: /admin/forgotPassword.php");
}
else {
    $_SESSION['errorMessage'] = "Email not found";
    header("location: /admin/forgotPassword.php");
}
?>