<?php
require_once("../../globals.php");

$adminUserService = new AdminUserService();
$adminUser = $adminUserService->getAdminUserForLogin($_POST['email']);

if ($adminUser != null) {

    if (!$adminUser->active) {
        $_SESSION['errorMessage'] = "The account you entered is not active yet. Please finish the registration process. If you need us to resend the email, please click <a href=\"/app/actions/doResendAdminEmail.php?email=" . urlencode($adminUser->email) ."\"/>here</a>";
        header("location: /admin/login.php");
        die();
    }

    if ($adminUser->password != $_POST['password']) {
        $_SESSION['errorMessage'] = "The password you entered does not match the username. Please re enter or use the forgot password option.";
        header("location: /admin/login.php");
        die();
    }


    $_SESSION['admin'] = $adminUser;

    if (isset($_SESSION['redirect']) && !preg_match("/(^\/admin\/$)|(^\/admin\/index.php.*$)/", $_SESSION['redirect'])) {
        header("location: {$_SESSION['redirect']}");
        die();
    }

    if ($adminUser->role == "admin") {
        header("location: /admin/index.php");
        die();
    }
    else if ($adminUser->role == "district" && isset($adminUser->districtId)) {
        header("location: /admin/districtAdmin.php?id=$adminUser->districtId");
        die();
    }
    else if ($adminUser->role == "school" && isset($adminUser->schoolId)) {
        header("location: /admin/schoolDetail.php?id=$adminUser->schoolId");
        die();
    }
    else if ($adminUser->role == "classroom" && isset($adminUser->classroomId)) {
        header("location: /admin/manageStudents.php?id=$adminUser->classroomId");
        die();
    }
    else {
        $_SESSION['errorMessage'] = "You are not currently the admin of anything right now";
        header("location: /admin/login.php");
        die();
    }
}
else {
    $_SESSION['errorMessage'] = "<p>Hey! Your email address is not recognized. Please check with the SHAKTI Warriors administrator for your school or school district to have them include you! Once they do that, you'll get an email asking you to complete your setup! Then you can join the fun!!</p><p>Email us if you run into additional problems!</p><p>Gratefully,<br/>SHAKTI.</p>";
    header("location: /admin/login.php");
    die();
}
?>