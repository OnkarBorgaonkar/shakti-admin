<?php
    require_once("../../globals.php");
    require_once("AdminAuthenticator.php");

    unset($_SESSION['admin']);
    session_destroy();

    header("location: /admin/login.php");
?>