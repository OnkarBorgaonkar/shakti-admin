<?php
    require_once("../../globals.php");

    $adminUserService = new AdminUserService();
    $adminUser = array_pop($adminUserService->find("activation_key = '{$_POST['activationKey']}' AND active = true"));
    $adminUser->activationKey = null;
    $adminUser->password = $_POST['password'];

    $adminUserService->save($adminUser);

    $_SESSION['successMessage'] = 'Password is reset. You can now login using your new password';
    header("location: /admin/login.php");
?>