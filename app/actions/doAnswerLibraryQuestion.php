<?php
    require_once "../../globals.php";
    require_once("Authenticator.php");
    
    if (isset($_POST['option'])) {
        $option = $_POST['option'];
        $articleId = $_POST['articleId']; 
        
        $quizQuestionOptionService = new QuizQuestionOptionService();
        $quizQuestionOption = array_pop($quizQuestionOptionService->find("id = $option"));
        
        if ($quizQuestionOption->correct) {
            $libraryAnswerService = new LibraryAnswerService();
            $libraryAnswer = array_pop($libraryAnswerService->find("library_article_id = $articleId"));
            $now = new DateTime();
            $libraryAnswer->completedOn = $now->format("Y-m-d H:i:s");
            $libraryAnswerService->save($libraryAnswer);
            
            $libraryArticleService = new LibraryArticleService();
            $article = array_pop($libraryArticleService->find("id = $articleId"));
            
            $libraryResponseService = new LibraryResponseService();
            $libraryResponse = array_pop($libraryResponseService->find("user_id = $user->id AND library_id = $article->libraryId"));
            
            if($libraryResponse->completedAt == null && $libraryResponse->hasReadAllArticles()){
                $now = new DateTime();
                $libraryResponse->completedAt = $now->format("Y-m-d H:i:s");
                $libraryResponseService->save($libraryResponse);
                $user->recalculateRank();
            }
            
            $message = urlencode("congratulations, you got the answer correct.");
            header("location: /library.php?systemMessage=$message");
        }
        else {
            $message = urlencode("Incorrect, please reread the article and then try Again.");
            header("location: /library.php?systemMessage=$message");
        }
    }
    
?>