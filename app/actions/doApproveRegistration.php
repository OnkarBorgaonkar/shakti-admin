<?php
require_once("../../globals.php");

$guardianService = new GuardianService();
$guardian = array_pop($guardianService->find("id = " . $_GET['guardianId']));
$guardian->approvedOn = date("Y-m-d");
$guardianService->save($guardian);

$systemMessage = urlencode("We have received your approval for your child to join the academy. Thank you.");
header("location: /index.php?systemMessage=$systemMessage");	
?>
