<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $assessmentResponse = new AssessmentResponse();
    $assessmentResponse->setAll($_POST['assessmentResponse']);

    $assessmentResponseService = new AssessmentResponseService();
    try
    {
        $assessmentResponseService->save($assessmentResponse);
    }
    catch(PDOException $e){

        $humanReadableMessage = "An Error occurred";
        $systemMessage = urlencode($humanReadableMessage);

        echo $e->getMessage();
        header("location: /assessment.php?systemMessage=$systemMessage");
        die();
    }

    $assessmentAnswerService = new AssessmentAnswerService();

    foreach ($_POST['answers'] as $answer){
        $assessmentAnswer = new AssessmentAnswer();
        $assessmentAnswer->setAll($answer);
        $assessmentAnswer->assessmentResponseId = $assessmentResponse->id;

        try
        {
            $assessmentAnswerService->save($assessmentAnswer);
        }
        catch(PDOException $e){

            $humanReadableMessage = "An Error occurred";
            $systemMessage = urlencode($humanReadableMessage);

            echo $e->getMessage();
            header("location: /assessment.php?systemMessage=$systemMessage");
            die();
        }

    }

    $humanReadableMessage = "Your stats have been recorded.  Thank you!";
    $systemMessage = urlencode($humanReadableMessage);

    header("location: /welcome.php?systemMessage=$systemMessage");

?>
