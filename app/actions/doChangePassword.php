<?php
    require_once("../../globals.php");
    require_once("AdminAuthenticator.php");
    
    $userId = $_POST['userId'];
    $password = $_POST['password'];

    $userService = new UserService();
    $editUser = array_pop($userService->find("id = $userId"));
    $editUser->password = $password;
    $editUser->initializePassword();
    $userService->save($editUser);
?>