<?php
    require_once("../../globals.php");
    
    if ($_SESSION['user']->role == 'admin') {
	    $userId = $_POST['userId'];
	    $password = $_POST['password'];

	    $userService = new UserService();
	    $editUser = array_pop($userService->find("id = $userId"));
	    $editUser->password = $password;
	    $editUser->initializePassword();
	    $userService->save($editUser);

	    $systemMessage = urlencode("Password has been changed.");
   		header("location: /warriors_list.php?systemMessage=$systemMessage");
	}
?>