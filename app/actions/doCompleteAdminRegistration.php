<?php
    require_once("../../globals.php");

    $adminUserService = new AdminUserService();
    $adminUser = array_pop($adminUserService->find("activation_key = '{$_POST['activationKey']}' AND active = false"));
    $adminUser->activationKey = null;
    $adminUser->password = $_POST['password'];
    $adminUser->active = true;

    $adminUserService->save($adminUser);

    $_SESSION['successMessage'] = 'Congratulations, your account is now active. Please login to start using your account';
    header("location: /admin/login.php");
?>