<?php
    require_once("../../globals.php");

    $district = new District();
    $district->setAll($_POST['school']);
    $district->contractStartDate = '2012-01-01';
    $district->contractEndDate = '2013-01-01';
    $district->name = $district->name . " Placeholder District";
    $district->passcode = uniqid("", true);

    $districtService = new DistrictService();
    $districtName = pg_escape_string($district->name);
    $where = "name = '$districtName'";
    if (isset($district->id))
        $where .= " AND id != $district->id";

    $duplicateName = array_pop($districtService->find($where));
    if ($duplicateName != null) {
        $humanReadableMessage = "This Name Has Already Been Used";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/district/createDistrict.php?systemMessage=$systemMessage");
        die();
    }
    $passcode = pg_escape_string($district->passcode);
    $where = "passcode = '$passcode'";
    if (isset($district->id))
        $where .= " AND id != $district->id";

    $duplicatePassword = array_pop($districtService->find($where));
    if ($duplicatePassword != null) {
        $humanReadableMessage = "This Password Has Already Been Used";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/district/createDistrict.php?systemMessage=$systemMessage");
        die();
    }

    try
    {
        $districtService->save($district);
    }
    catch(PDOException $e){

        echo $e->getMessage();
        die();

        $humanReadableMessage = "An Error Occured";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/district/createDistrict.php?systemMessage=$systemMessage");
        die();
    }


    $school = new School();
    $school->setAll($_POST['school']);
    $school->districtId = $district->id;

    $schoolService = new SchoolService();
    $schoolName = pg_escape_string($school->name);
    $where = "name = '$schoolName'";

    $duplicateName = array_pop($schoolService->find($where));
    if ($duplicateName != null) {
        $humanReadableMessage = "This Name Has Already Been Used";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/district/createSchool.php?systemMessage=$systemMessage");
        die();
    }
    $passcode = pg_escape_string($school->passcode);
    $where = "passcode = '$passcode'";

    $duplicatePassword = array_pop($schoolService->find($where));
    if ($duplicatePassword != null) {
        $humanReadableMessage = "This Password Has Already Been Used";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/district/createSchool.php?systemMessage=$systemMessage");
        die();
    }

    try
    {
        $schoolService->save($school);
    }
    catch(PDOException $e){

        echo $e->getMessage();
        die();

        $humanReadableMessage = "An Error Occured";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/district/createSchool.php?systemMessage=$systemMessage");
        die();
    }


    $systemMessage = urlencode("District Created.");
    header("location: /index.php?systemMessage=$systemMessage");
    die();
?>