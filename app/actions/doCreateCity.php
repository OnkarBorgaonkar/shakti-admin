<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    if ($user->role != "admin")
        header("location: /welcome.php");
    
    $city = new City();
    $city->setAll($_POST);
    
    $cityService = new CityService();
    
    try
    {
        $cityService->save($city);
    }
    catch(PDOException $e){
    
        $humanReadableMessage = "An Error Occured";
        $systemMessage = urlencode($humanReadableMessage);
        echo $e->getMessage();
        die();
        header("location: /admin/city/createCity.php?systemMessage=$systemMessage");
        die();
    }
    
    $systemMessage = urlencode("City Created.");
    header("location: /admin/city/index.php?systemMessage=$systemMessage");
    die();
?>