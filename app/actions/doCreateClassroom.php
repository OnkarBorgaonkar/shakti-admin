<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    if ($user->role != "admin")
        header("location: /welcome.php");
    
    $classroom = new Classroom();
    $classroom->setAll($_POST['classroom']);
    if (!isset($_POST['classroom']['active']))
        $classroom->active = false;
    
    $classroomService = new ClassroomService();
    
    $classroomName = pg_escape_string($classroom->name);
    $where = "name = '$classroomName'";
    if (isset($classroom->id))
        $where .= " AND id != $classroom->id";
    $duplicateName = array_pop($classroomService->find($where));
    
    if ($duplicateName != null) {
        $humanReadableMessage = "A Classroom with this name already exists";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/classroom/createClassroom.php?systemMessage=$systemMessage&schoolId=$classroom->schoolId");
        die();
    }
    
    $passcode = pg_escape_string($classroom->passcode);
    $where = "passcode = '$passcode'";
    if (isset($classroom->id))
        $where .= " AND id != $classroom->id";
    $duplicateName = array_pop($classroomService->find($where));
    
    if ($duplicateName != null) {
        $humanReadableMessage = "A Classroom with this passcode already exists";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/classroom/createClassroom.php?systemMessage=$systemMessage&schoolId=$classroom->schoolId");
        die();
    }
    
    
    try
    {
        $classroomService->save($classroom);
    }
    catch(PDOException $e){
    
        $humanReadableMessage = "An Error Occured";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/classroom/createClassroom.php?systemMessage=$systemMessage&schoolId=$classroom->schoolId");
        die();
    }
    
    $classroomGradeService = new ClassroomGradeService();
    $classroomGradeService->deleteWhere("classroom_id = $classroom->id");
    
    $gradeTypes = $_POST['gradeTypes'];
    foreach ($gradeTypes as $gradeType) {
        $classroomGrade = new ClassroomGrade();
        $classroomGrade->classroomId = $classroom->id;
        $classroomGrade->gradeType = $gradeType;
        $classroomGradeService->save($classroomGrade);
    }
    
    $systemMessage = urlencode("Classroom Updated.");
    header("location: /admin/classroom/index.php?systemMessage=$systemMessage&schoolId=$classroom->schoolId");
    die();
?>