<?php
    require_once("../../globals.php");
	require_once("Authenticator.php");
	
	if ($user->role != "admin")
	    header("location: /welcome.php");
	
	$competition = new Competition();
	$competition->setAll($_POST);
	
	$competitionService = new CompetitionService();
	
	$existingCompetition = array_pop($competitionService->find("passcode = '$competition->passcode'"));
	if ($competition->id == null && $existingCompetition != null) {
	    $humanReadableMessage = "A competition Already Exists with that Passcode";
	    $systemMessage = urlencode($humanReadableMessage);
	    header("location: /admin/competition/createCompetition.php?systemMessage=$systemMessage");
	    die();
	}
	
	try
	{
	    $competitionService->save($competition);
	}
	catch(PDOException $e){
	
	    $humanReadableMessage = "An Error Occured";
	    $systemMessage = urlencode($humanReadableMessage);	   
	    header("location: /admin/competition/createCompetition.php?systemMessage=$systemMessage");
	    die();
	}
	
	$systemMessage = urlencode("Competition Created.");
	header("location: /admin/competition/index.php?systemMessage=$systemMessage");
	die();
?>