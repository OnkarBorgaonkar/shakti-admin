<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $childHeroId = $user->getChildHeroId();

    $childHeroCustomTaskService = new ChildHeroCustomTaskService();
    $task = array_pop($childHeroCustomTaskService->getRemainingCustomTasks($childHeroId));

    $childHeroCustomTask = new ChildHeroCustomTask();
    $childHeroCustomTask->childHeroId = $childHeroId;
    $childHeroCustomTask->task_id = $task->id;
    $childHeroCustomTask->name = "Activity Name";

    $childHeroCustomTaskService->save($childHeroCustomTask);

    echo json_encode($childHeroCustomTask);

?>