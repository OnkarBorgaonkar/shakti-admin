<?php
    require_once("../../globals.php");

    $group = new Group();
    $group->setAll($_POST['group']);

    $isNew = !isset($group->id);

	$groupService = new GroupService();

    if ($groupService->updateRequiresApproval($group)) {
        if (strtolower($_POST['approvalPassword']) != strtolower(Config::$approvalPassword)) {
            $humanReadableMessage = "Incorrect Approval Password";
            $systemMessage = urlencode($humanReadableMessage);

            $location = "location: /createGroup.php?systemMessage=$systemMessage";
            if (!$isNew)
                $location .= "&groupId=$group->id";
            header($location);
            die();
        }
    }

    $webPurifyService = new WebPurifyService();

    if ($webPurifyService->checkForProfanity($group->name)) {
        $message = "You have some inappropriate content in your Group Name. Please remove it.";
        $systemMessage = urlencode($message);
        $location = "location: /createGroup.php?systemMessage=$systemMessage";
        if (!$isNew)
            $location .= "&groupId=$group->id";
        header($location);
        die();
    }

    if ($webPurifyService->checkForProfanity($group->backstory)) {
        $message = "You have some inappropriate content in your Group Back Story. Please remove it.";
        $systemMessage = urlencode($message);
        $location = "location: /createGroup.php?systemMessage=$systemMessage";
        if (!$isNew)
            $location .= "&groupId=$group->id";
        header($location);
        die();
    }



    if($_FILES['file']['size'] != 0){
        $group->setFileData($_FILES['file']);
    }

    $userService = new UserService();
    $leader = array_pop($userService->find("id = $group->leaderId"));

    try
    {
        $groupService->save($group);
    }
    catch(PDOException $e){

        $humanReadableMessage = "Group Name Already In Use";
        $systemMessage = urlencode($humanReadableMessage);

       header("location: /createGroup.php?systemMessage=$systemMessage");
       die();
    }

    if ($_FILES['file']['size'] != 0){
        ImageFileService::instance()->resizeAndStore($_FILES['file']['tmp_name'], $group, "thumb", 100);
    }

    if (!$group->isMember($leader)){
        $group->addUser($group->leaderId);
    }


    $systemMessage = urlencode("Your Team has been Successfully Created.");
    header("location: /groupDetails.php?systemMessage=$systemMessage&groupId=$group->id");

?>
