<?php
    require_once("../../globals.php");

    $groupGoal = new GroupGoal();
    $groupGoal->setAll($_POST);

    $groupGoalService = new GroupGoalService();

    try
    {
        $groupGoalService->save($groupGoal);
    }
    catch(PDOException $e){

        $humanReadableMessage = "An Error Occured";
        $systemMessage = urlencode($humanReadableMessage);       
        header("location: /createGroupGoal.php?systemMessage=$systemMessage");
        die();
    }

    $systemMessage = urlencode("Your Goal has been Successfully Created.");
    header("location: /groupDetails.php?systemMessage=$systemMessage&groupId=$groupGoal->groupId");

?>
