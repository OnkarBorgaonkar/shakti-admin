<?php
    require_once("../../globals.php");
	require_once("Authenticator.php");
	
	if ($user->role != "admin")
	    header("location: /welcome.php");
	
	$school = new School();
	$school->setAll($_POST);
	
	$geocodeService = new GeocodeService();
	$latLng = $geocodeService->GeocodeAddress($school->getFullAddress());
	
	if ($latLng != null) {
	    $school->latitude = $latLng['latitude'];
	    $school->longitude = $latLng['longitude'];
	}
	else {
        $humanReadableMessage = "The address could not be geocoded.";
	    $systemMessage = urlencode($humanReadableMessage);
	    header("location: /admin/school/createSchool.php?systemMessage=$systemMessage&districtId=$school->districtId");
	    die();
	}
	
	$schoolService = new SchoolService();
	
	$passcode = pg_escape_string($school->passcode);
	$where = "passcode = '$passcode'";
	if (isset($school->id))
	    $where .= " AND id != $school->id";	
	$duplicateName = array_pop($schoolService->find($where));
	
	if ($duplicateName != null) {
	    $humanReadableMessage = "A School with this passcode already exists";
	    $systemMessage = urlencode($humanReadableMessage);
	    header("location: /admin/school/createSchool.php?systemMessage=$systemMessage&districtId=$school->districtId");
	    die();
	}
	
	try
	{
	    $schoolService->save($school);
	}
	catch(PDOException $e){
	
	    echo $e->getMessage();
	    die();
	    $humanReadableMessage = "An Error Occured";
	    $systemMessage = urlencode($humanReadableMessage);
	    header("location: /admin/school/createSchool.php?systemMessage=$systemMessage&districtId=$school->districtId");
	    die();
	}
	
	$systemMessage = urlencode("School Updated.");
	header("location: /admin/school/index.php?systemMessage=$systemMessage&districtId=$school->districtId");
	die();
?>