<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $groupId = $_REQUEST['groupId'];

    $groupService = new GroupService();
    $group = $groupService->find("id = $groupId");

    if($group != null && $group->leaderId == $user->id){
        $groupGoalService = new GroupGoalService();
        $groupGoalService->deleteWhere("group_id = $group->id");

        $humanReadableMessage = "Goal Deleted";
        $systemMessage = urlencode($humanReadableMessage);

        header("location: /groupDetails.php?groupId=$group->id&systemMessage=$systemMessage");
        die();
    }
    else{
        $humanReadableMessage = "You are not the Team leader";
        $systemMessage = urlencode($humanReadableMessage);

        header("location: /groupDetails.php?groupId=$group->id&systemMessage=$systemMessage");
    }
?>
