<?php
require_once("../../globals.php");

$userService = new UserService();
$userService->deleteWhere("id = " . $_GET['userId']);

$systemMessage = urlencode("Your child has been removed from our system.");
header("location: /index.php?systemMessage=$systemMessage");	
?>