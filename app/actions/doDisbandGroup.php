<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    require_once("isGroupLeader.php");

    $userGroupService = new UserGroupService();
    $userGroupService->deleteWhere("group_id = $group->id");

    $groupMessageService = new GroupMessageService();
    $groupMessageService->deleteWhere("group_id = $group->id");

    $groupInvitationService = new GroupInvitationService();
    $groupInvitationService->deleteWhere("group_id = $group->id");

    $groupRequestService = new GroupRequestService();
    $groupRequestService->deleteWhere("group_id = $group->id");
    
    $groupJoinPointsService = new GroupJoinPointsService();
    $groupJoinPointsService->findBySql("UPDATE group_join_points SET group_id = NULL WHERE group_id = $group->id");
    

    $groupService = new GroupService();
    $groupService->delete($group);

    $humanReadableMessage = "\"$group->name\" has been deleted";
    $systemMessage = urlencode($humanReadableMessage);


    header("location: /welcome.php?systemMessage=$systemMessage");

?>
