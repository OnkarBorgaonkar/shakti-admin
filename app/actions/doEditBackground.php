<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    $backgroundId = $_POST['backgroundId'];
    $user->backgroundImage = $backgroundId;
    $userService = new UserService();
    $userService->save($user);
    
    $humanReadableMessage = "Background Image Changed";
    $systemMessage = urlencode($humanReadableMessage);
    header("location: /welcome.php?systemMessage=$systemMessage");
    die();
?>