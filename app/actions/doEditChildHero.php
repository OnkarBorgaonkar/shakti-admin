<?php
require_once("../../globals.php");

$childHero = new ChildHero();
$childHero->setAll($_POST['childHero']);

$childHeroService = new ChildHeroService();
$webPurifyService = new WebPurifyService();

if ($childHeroService->updateRequiresApproval($childHero)) {
    if (strtolower($_POST['approvalPassword']) != strtolower(Config::$approvalPassword)) {
        $message = "Incorrect Approval Password";
        $systemMessage = urlencode($message);
        header("location: /editSuperHero.php?systemMessage=$systemMessage");
        die();
    }
}

if ($webPurifyService->checkForProfanity($childHero->superHeroOutfit)) {
    $message = "You have some inappropriate content in your super hero outfit. Please remove it.";
    $systemMessage = urlencode($message);
    header("location: /editSuperHero.php?systemMessage=$systemMessage");
    die();
}

if ($webPurifyService->checkForProfanity($childHero->backstory)) {
    $message = "You have some inappropriate content in your backstory. Please remove it.";
    $systemMessage = urlencode($message);
    header("location: /editSuperHero.php?systemMessage=$systemMessage");
    die();
}

if ($webPurifyService->checkForProfanity($childHero->planToDoGood)) {
    $message = "You have some inappropriate content in the \"Plan to do Good\". Please remove it.";
    $systemMessage = urlencode($message);
    header("location: /editSuperHero.php?systemMessage=$systemMessage");
    die();
}

try
{
    $childHeroService->save($childHero);
}
catch(PDOException $e)
{
    Logger::log($e);
    $systemMessage = urlencode("An error occurred saving your changes.");

    header("location: /editSuperHero.php?systemMessage=$systemMessage");
    die();
}

$childHeroSuperPowerService = new ChildHeroSuperPowerService();
$childHeroSuperPowerService->deleteWhere("child_hero_id=$childHero->id");

$childHeroSuperPower = new ChildHeroSuperPower();
$childHeroSuperPower->childHeroId = $childHero->id;
$childHeroSuperPower->superPowerId = $_POST['superPowers']['superHeroPower1'];
$childHeroSuperPowerService->save($childHeroSuperPower);

$childHeroSuperPower = new ChildHeroSuperPower();
$childHeroSuperPower->childHeroId = $childHero->id;
$childHeroSuperPower->superPowerId = $_POST['superPowers']['superHeroPower2'];
$childHeroSuperPowerService->save($childHeroSuperPower);

$childHeroHumanPowerService = new ChildHeroHumanPowerService();
$childHeroHumanPowerService->deleteWhere("child_hero_id=$childHero->id");

$childHeroHumanPower = new ChildHeroHumanPower();
$childHeroHumanPower->childHeroId = $childHero->id;
$childHeroHumanPower->humanPowerId = $_POST['humanPowers']['humanPower1'];
$childHeroHumanPowerService->save($childHeroHumanPower);

$childHeroHumanPower = new ChildHeroHumanPower();
$childHeroHumanPower->childHeroId = $childHero->id;
$childHeroHumanPower->humanPowerId = $_POST['humanPowers']['humanPower2'];
$childHeroHumanPowerService->save($childHeroHumanPower);

$childHeroHumanPower = new childHeroHumanPower();
$childHeroHumanPower->childHeroId = $childHero->id;
$childHeroHumanPower->humanPowerId = $_POST['humanPowers']['humanPower3'];
$childHeroHumanPowerService->save($childHeroHumanPower);

$systemMessage = urlencode("Changes saved.");
header("location: /editSuperHero.php?systemMessage=$systemMessage");
?>
