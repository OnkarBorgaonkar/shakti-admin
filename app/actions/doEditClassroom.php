<?php
    require_once("../../globals.php");

    $classroom = new Classroom();
    $classroom->setAll($_POST['classroom']);
    $classroom->name = $_POST['admin']['prefix'] . " " . $_POST['admin']['lastName'];

    $classroomService = new ClassroomService();
    $classroomName = pg_escape_string($classroom->name);
    $where = "name = '$classroomName' AND school_id = $classroom->schoolId";
    if (isset($classroom->id))
        $where .= " AND id != $classroom->id";

    $duplicateName = array_pop($classroomService->find($where));
    if ($duplicateName != null) {
        $message = "This Name Has Already Been Used";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    $adminEmail = $_POST['admin']['email'];
    $adminUserService = new AdminUserService();
    $adminUser = $adminUserService->getAdminUserByEmail($adminEmail);

    if ($adminUser != null) {
        if ($adminUser->role == "admin") {
            $message = "$adminEmail is an Admin User and cannot be the manager of a classroom";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
        else if ($adminUser->classroomId != $classroom->id && $classroom->isPersisted() && $adminUser->classroomId != null) {
            $existingClassroom = array_pop($classroomSerivce->find("id = $adminUser->classroomId"));
            $message = "$adminEmail is already the admin of the $existingClassroom->name classroom";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
        else if ($adminUser->districtId != null) {
            $districtService = new DistrictService();
            $existingDistrict = array_pop($districtService->find("id = $adminUser->districtId"));
            $message = "$adminEmail is already the admin of the $existingDistrict->name district";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
        else if ($adminUser->schoolId != null) {
            $schoolService = new SchoolService();
            $existingSchool = array_pop($schoolService->find("id = $adminUser->schoolId"));
            $message = "$adminEmail is already the admin of the $existingSchool->name school";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
    }

    if ($classroom->id == null) {
        $classroomGroup = new ClassroomGroup();
        $classroomGroup->name = "Group 1";
        $today = new DateTime();
        $classroomGroup->startDate = $today->format("Y-m-d");
        $classroomGroup->startWeek = 1;
        $classroomGroup->currentWeek = 1;
        $classroomGroup->done = false;
        $classroomGroup->updated = false;
    }


    $classroomService->save($classroom);

    if ($classroomGroup != null) {
        $classroomGroup->classroomId = $classroom->id;
        $classroomGroupService = new ClassroomGroupService();
        $classroomGroupService->save($classroomGroup);
    }

    $classroomDayService = new ClassroomDayService();
    $classroomDayService->deleteWhere("classroom_id = $classroom->id");

    if (isset($_POST['classroomDays'])) {
        $classroomDays = $_POST['classroomDays'];
        foreach ($classroomDays as $day) {
            $classroomDay = new ClassroomDay();
            $classroomDay->classroomId = $classroom->id;
            $classroomDay->day = $day;
            $classroomDayService->save($classroomDay);
        }
    }

    if ($adminUser != null && $adminUser->classroomId == $classroom->id) {
        $adminUser->prefix = $_POST['admin']['prefix'];
        $adminUser->firstName = $_POST['admin']['firstName'];
        $adminUser->lastName = $_POST['admin']['lastName'];
        $adminUser->workPhone = $_POST['admin']['workPhone'];
        $adminUser->cellPhone = $_POST['admin']['cellPhone'];
        unset($adminUser->districtId);
        unset($adminUser->schoolId);
        unset($adminUser->classroomId);
        unset($adminUser->role);
        $adminUserService->save($adminUser);
    }
    else if ($adminUser == null) {
        $adminUser = new AdminUser();
        $adminUser->setAll($_POST['admin']);
        $adminUser->active = false;
        $adminUser->activationKey = uniqid(null, true);
        $adminUser->password = $adminUser->activationKey;

        $adminUserService = new AdminUserService();
        $adminUserService->save($adminUser);

        $mailService = new MailService();
        $mailService->newAdminAccount($adminUser, "Classroom", $classroom->name);
    }

    $adminUserRole = new AdminUserRole();
    $adminUserRole->adminUserId = $adminUser->id;
    $adminUserRole->role = "classroom";

    $adminUserRoleService = new AdminUserRoleService();
    $adminUserRoleService->deleteWhere("admin_user_id = $adminUser->id");
    $adminUserRoleService->save($adminUserRole);

    $adminUserClassroom = new AdminUserClassroom();
    $adminUserClassroom->adminUserId = $adminUser->id;
    $adminUserClassroom->classroomId = $classroom->id;

    $adminUserClassroomService = new AdminUserClassroomService();
    $adminUserClassroomService->deleteWhere("classroom_id = $classroom->id");
    $adminUserClassroomService->save($adminUserClassroom);





    $classroomGradeService = new ClassroomGradeService();
    $classroomGradeService->deleteWhere("classroom_id = $classroom->id");

    if (isset($_POST['gradeTypes'])) {
        $gradeTypes = $_POST['gradeTypes'];
        foreach ($gradeTypes as $gradeType) {
            $classroomGrade = new ClassroomGrade();
            $classroomGrade->classroomId = $classroom->id;
            $classroomGrade->gradeType = $gradeType;
            $classroomGradeService->save($classroomGrade);
        }
    }

    $classroomComputerTypeService = new ClassroomComputerTypeService();
    $classroomComputerTypeService->deleteWhere("classroom_id = $classroom->id");

    if (isset($_POST['computerTypes'])) {
        $computerTypes = $_POST['computerTypes'];
        foreach ($computerTypes as $computerType) {
            $classroomComputerType = new ClassroomComputerType();
            $classroomComputerType->classroomId = $classroom->id;
            $classroomComputerType->computerType = $computerType;
            if ($computerType == "OTHER")
                $classroomComputerType->otherValue = $_POST['otherValue'];
            $classroomComputerTypeService->save($classroomComputerType);
        }
    }

    $_SESSION['successMessage'] = "Classroom Updated";

    $json = Array(
        "success" => true,
        "id"=> $classroom->id
    );
    echo json_encode($json);
    die();
?>