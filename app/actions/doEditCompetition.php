<?php
    require_once("../../globals.php");
    require_once("AdminAuthenticator.php");

    if ($admin->role != "admin")
        header("location: /admin/login.php");

    $competition = new Competition();
    $competition->setAll($_POST['competition']);

    $competitionService = new CompetitionService();

    $existingCompetition = array_pop($competitionService->find("passcode = '$competition->passcode'"));
    if ($competition->id == null && $existingCompetition != null) {
        $message = "A competition Already Exists with that Passcode";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    try
    {
        $competitionService->save($competition);
    }
    catch(PDOException $e){

        $message = "An Error Occured";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    $_SESSION['successMessage'] = "Competition Updated";

    $json = Array(
        "success" => true,
        "id"=> $competition->id
    );
    echo json_encode($json);
    die();
?>