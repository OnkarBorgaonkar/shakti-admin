<?php
    require_once("../../globals.php");

    $district = new District();
    $district->setAll($_POST['district']);
    if (!isset($district->joinDate))
        $district->joinDate = date("Y-m-d");

    $districtService = new DistrictService();
    $districtName = pg_escape_string($district->name);
    $where = "name = '$districtName'";
    if (isset($district->id))
        $where .= " AND id != $district->id";

    $duplicateName = array_pop($districtService->find($where));
    if ($duplicateName != null) {
        $message = "This Name Has Already Been Used";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    $adminEmail = $_POST['admin']['email'];
    $adminUserService = new AdminUserService();
    $adminUser = $adminUserService->getAdminUserByEmail($adminEmail);

    if ($adminUser != null) {
        if ($adminUser->role == "admin") {
            $message = "$adminEmail is an Admin User and cannot be the manager of a district";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
        else if ($adminUser->districtId != $district->id && $district->isPersisted() && $adminUser->districtId != null) {
            $existingDistrict = array_pop($districtService->find("id = $adminUser->districtId"));
            $message = "$adminEmail is already the admin of the $existingDistrict->name district";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
        else if ($adminUser->schoolId != null) {
            $schoolService = new SchoolService();
            $existingSchool = array_pop($schoolService->find("id = $adminUser->schoolId"));
            $message = "$adminEmail is already the admin of the $existingSchool->name school";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();

        }
        else if ($adminUser->classroomId != null) {
            $classroomSerivce = new ClassroomService();
            $existingClassroom = array_pop($classroomSerivce->find("id = $adminUser->classroomId"));
            $message = "$adminEmail is already the admin of the $existingClassroom->name classroom";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
    }

    $districtService->save($district);

    if ($adminUser != null && $adminUser->districtId == $district->id) {
        $adminUser->prefix = $_POST['admin']['prefix'];
        $adminUser->firstName = $_POST['admin']['firstName'];
        $adminUser->lastName = $_POST['admin']['lastName'];
        $adminUser->workPhone = $_POST['admin']['workPhone'];
        $adminUser->cellPhone = $_POST['admin']['cellPhone'];
        unset($adminUser->districtId);
        unset($adminUser->schoolId);
        unset($adminUser->classroomId);
        unset($adminUser->role);
        $adminUserService->save($adminUser);
    }
    else if ($adminUser == null) {
        $adminUser = new AdminUser();
        $adminUser->setAll($_POST['admin']);
        $adminUser->active = false;
        $adminUser->activationKey = uniqid(null, true);
        $adminUser->password = $adminUser->activationKey;

        $adminUserService = new AdminUserService();
        $adminUserService->save($adminUser);

        $mailService = new MailService();
        $mailService->newAdminAccount($adminUser, "District", $district->name);
    }

    $adminUserRole = new AdminUserRole();
    $adminUserRole->adminUserId = $adminUser->id;
    $adminUserRole->role = "district";

    $adminUserRoleService = new AdminUserRoleService();
    $adminUserRoleService->deleteWhere("admin_user_id = $adminUser->id");
    $adminUserRoleService->save($adminUserRole);

    $adminUserDistrict = new AdminUserDistrict();
    $adminUserDistrict->adminUserId = $adminUser->id;
    $adminUserDistrict->districtId = $district->id;

    $adminUserDistrictService = new AdminUserDistrictService();
    $adminUserDistrictService->deleteWhere("district_id = $district->id");
    $adminUserDistrictService->save($adminUserDistrict);

    $_SESSION['successMessage'] = "District Updated";

    $json = Array(
        "success" => true,
        "id"=> $district->id
    );
    echo json_encode($json);
    die();
?>