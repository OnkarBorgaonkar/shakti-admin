<?php
require_once("../../globals.php");
require_once("Authenticator.php");

if (empty($_POST['user']['password']))
{
    unset($_POST['user']['password']);
    $user->setAll($_POST['user']);
}
else
{
    $user->setAll($_POST['user']);
    $user->initializePassword();
}
$_SESSION['user'] = $user;

$child = new Child();
$child->setAll($_POST['child']);
$_SESSION['child'] = $child;

$userService = new UserService();
try
{
    $userService->save($user);
}
catch(PDOException $e)
{
    $errorMessages = array(
        'SQLSTATE[23505]: Unique violation: 7 ERROR:  duplicate key violates unique constraint "users_email_key"' => "That email address is already in use.",
        'SQLSTATE[23505]: Unique violation: 7 ERROR:  duplicate key violates unique constraint "users_login_key"' => "That login is already in use."
    );

    $message = $e->getMessage();
    $humanReadableMessage = $errorMessages[$message];
    $systemMessage = urlencode($humanReadableMessage);

    header("location: /editProfile.php?systemMessage=$systemMessage");
    die();
}

if (!empty($_POST['guardian']['email']))
{
    $guardian = new Guardian();
    $guardian->setAll($_POST['guardian']);
    $guardian->notificationCount = 0;

    $guardianService = new GuardianService();
    $guardianService->save($guardian);
    $guardianService->notify($guardian);
}

$child->userId = $user->id;
$childService = new ChildService();
$childService->save($child);

$systemMessage = urlencode("Changes saved.");
header("location: /editProfile.php?systemMessage=$systemMessage");
?>
