<?php
    require_once("../../globals.php");

    $program = new Program();
    $program->setAll($_POST['program']);

    $programService = new ProgramService();
    $programName = pg_escape_string($program->name);
    $where = "name = '$programName' AND school_id = $program->schoolId";
    if (isset($program->id))
        $where .= " AND id != $program->id";

    $duplicateName = array_pop($programService->find($where));
    if ($duplicateName != null) {
        $message = "This Name Has Already Been Used";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    $startDate = strtotime($program->startDate);
    $endDate = strtotime($program->endDate);

    if ($endDate <= $startDate) {
        $message = "The Session end date must be after the Session start date.";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    if (isset($_POST['weeksNotInSession'])) {
        $notInSessionDates = array();
        foreach ($_POST['weeksNotInSession'] as $weekNotInSession) {
            $mondayTime = strtotime("Monday this week", strtotime($weekNotInSession));
            if ($mondayTime > $endDate || $mondayTime < $startDate) {
                $message = "Not in session weeks must be within the start and end weeks.";
                $json = Array(
                    "success" => false,
                    "systemMessage" => $message
                );
                echo json_encode($json);
                die();
            }

            $monday = date("Y-m-d", $mondayTime);
            if (in_array($monday, $notInSessionDates)) {
                $message = "Two or more not in session dates occur in the same week. Please correct this.";
                $json = Array(
                    "success" => false,
                    "systemMessage" => $message
                );
                echo json_encode($json);
                die();
            }
            else {
                $notInSessionDates[] = $monday;
            }
        }
    }

    $programService->save($program);

    $programNotInSessionDateService = new ProgramNotInSessionDateService();
    $programNotInSessionDateService->deleteWhere("program_id = $program->id");

    if (isset($notInSessionDates)) {
        foreach ($notInSessionDates as $weekNotInSession) {
            if (trim($weekNotInSession) != '') {
                $programNotInSessionDate = new ProgramNotInSessionDate();
                $programNotInSessionDate->programId = $program->id;
                $programNotInSessionDate->startDate = $weekNotInSession;

                $programNotInSessionDateService->save($programNotInSessionDate);
            }
        }
    }

    $_SESSION['successMessage'] = "Session Updated";

    $json = Array(
        "success" => true,
        "id"=> $program->id
    );
    echo json_encode($json);
    die();
?>