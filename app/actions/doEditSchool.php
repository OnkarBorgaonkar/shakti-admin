<?php
    require_once("../../globals.php");

    $school = new School();
    $school->setAll($_POST['school']);
    if (!isset($school->joinDate))
        $school->joinDate = date("Y-m-d");

    $schoolService = new SchoolService();

    $adminEmail = $_POST['admin']['email'];
    $adminUserService = new AdminUserService();
    $adminUser = $adminUserService->getAdminUserByEmail($adminEmail);

    if ($adminUser != null) {
        if ($adminUser->role == "admin") {
            $message = "$adminEmail is an Admin User and cannot be the manager of a school";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
        else if ($adminUser->schoolId != $school->id && $school->isPersisted() && $adminUser->schoolId != null) {
            $existingSchool = array_pop($schoolService->find("id = $adminUser->schoolId"));
            $message = "$adminEmail is already the admin of the $existingSchool->name school";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
        else if($adminUser->districtId != null) {
            $districtService = new DistrictService();
            $existingDistrict = array_pop($districtService->find("id = $adminUser->districtId"));
            $message = "$adminEmail is already the admin of the $existingDistrict->name district";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
        else if ($adminUser->classroomId != null) {
            $classroomSerivce = new ClassroomService();
            $existingClassroom = array_pop($classroomSerivce->find("id = $adminUser->classroomId"));
            $message = "$adminEmail is already the admin of the $existingClassroom->name classroom";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
    }

    $startDate = strtotime($school->startDate);
    $endDate = strtotime($school->endDate);

    if ($endDate <= $startDate) {
        $message = "The Session end date must be after the Session start date.";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    if (isset($_POST['weeksNotInSession'])) {
        $notInSessionDates = array();
        foreach ($_POST['weeksNotInSession'] as $weekNotInSession) {
            $mondayTime = strtotime("Monday this week", strtotime($weekNotInSession));
            if ($mondayTime > $endDate || $mondayTime < $startDate) {
                $message = "Not in session weeks must be within the start and end weeks.";
                $json = Array(
                    "success" => false,
                    "systemMessage" => $message
                );
                echo json_encode($json);
                die();
            }

            $monday = date("Y-m-d", $mondayTime);
            if (in_array($monday, $notInSessionDates)) {
                $message = "Two or more not in session dates occur in the same week. Please correct this.";
                $json = Array(
                    "success" => false,
                    "systemMessage" => $message
                );
                echo json_encode($json);
                die();
            }
            else {
                $notInSessionDates[] = $monday;
            }
        }
    }


    $schoolService->save($school);

    $schoolNotInSessionDateService = new SchoolNotInSessionDateService();
    $schoolNotInSessionDateService->deleteWhere("school_id = $school->id");


    if (isset($notInSessionDates)) {
        foreach ($notInSessionDates as $weekNotInSession) {
            if (trim($weekNotInSession) != '') {
                $schoolNotInSessionDate = new SchoolNotInSessionDate();
                $schoolNotInSessionDate->schoolId = $school->id;
                $schoolNotInSessionDate->startDate = $weekNotInSession;

                $schoolNotInSessionDateService->save($schoolNotInSessionDate);
            }
        }
    }

    if ($adminUser != null && $adminUser->schoolId == $school->id) {
        $adminUser->prefix = $_POST['admin']['prefix'];
        $adminUser->firstName = $_POST['admin']['firstName'];
        $adminUser->lastName = $_POST['admin']['lastName'];
        $adminUser->workPhone = $_POST['admin']['workPhone'];
        $adminUser->cellPhone = $_POST['admin']['cellPhone'];
        unset($adminUser->districtId);
        unset($adminUser->schoolId);
        unset($adminUser->classroomId);
        unset($adminUser->role);
        $adminUserService->save($adminUser);
    }
    else if ($adminUser == null) {
        $adminUser = new AdminUser();
        $adminUser->setAll($_POST['admin']);
        $adminUser->active = false;
        $adminUser->activationKey = uniqid(null, true);
        $adminUser->password = $adminUser->activationKey;

        $adminUserService = new AdminUserService();
        $adminUserService->save($adminUser);

        $mailService = new MailService();
        $mailService->newSchoolAdminAccount($adminUser, $school->name);
    }

    $adminUserRole = new AdminUserRole();
    $adminUserRole->adminUserId = $adminUser->id;
    $adminUserRole->role = "school";

    $adminUserRoleService = new AdminUserRoleService();
    $adminUserRoleService->deleteWhere("admin_user_id = $adminUser->id");
    $adminUserRoleService->save($adminUserRole);

    $adminUserSchool = new AdminUserSchool();
    $adminUserSchool->adminUserId = $adminUser->id;
    $adminUserSchool->schoolId = $school->id;

    $adminUserSchoolService = new AdminUserSchoolService();
    $adminUserSchoolService->deleteWhere("school_id = $school->id");
    $adminUserSchoolService->save($adminUserSchool);

    $_SESSION['successMessage'] = "School Updated";

    $json = Array(
        "success" => true,
        "id"=> $school->id
    );
    echo json_encode($json);
    die();
?>