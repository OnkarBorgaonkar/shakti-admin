<?php
    require_once("../../globals.php");

    $session = new Session();
    $session->setAll($_POST['session']);

    $sessionService = new SessionService();
    $sessionName = pg_escape_string($session->name);
    $where = "name = '$sessionName'";
    if (isset($session->id))
        $where .= " AND id != $session->id";

    $duplicateName = array_pop($sessionService->find($where));
    if ($duplicateName != null) {
        $message = "This Name Has Already Been Used";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    $sessionService->save($session);

    $_SESSION['successMessage'] = "Session Updated";

    $json = Array(
        "success" => true,
        "id"=> $session->id
    );
    echo json_encode($json);
    die();
?>