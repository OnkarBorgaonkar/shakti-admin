<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    if ($user->role != "admin")
        header("location: /welcome.php");
    
    $libraryId = $_POST['libraryId'];
    
    $filename = "libraryId-{$libraryId}_" . date("m-d-Y") . ".csv";
    
    header("Content-type: text/csv");
    header("Content-disposition: attachment; filename=$filename");
    
    $out = fopen("php://output", "w+");
    
    fputcsv($out, array("Article ID", "Subject ID", "Article Name", "Points", "Order", "article Type", "URL", "pages"));
    
    $libraryArticleService = new LibraryArticleService();
    $articles = $libraryArticleService->find("library_id = $libraryId ORDER BY subject_id, \"order\"");
    
    foreach($articles as $article) {
        fputcsv($out, array($article->id, $article->subjectId, $article->name, $article->points, $article->order, $article->articleType, $article->url, $article->pages));
    }
    
?>