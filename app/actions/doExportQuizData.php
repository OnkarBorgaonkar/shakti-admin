<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    if ($user->role != "admin")
        header("location: /welcome.php");
    
    $quizId = $_POST['quizId'];
    
    $filename = "quizid-{$quizId}_" . date("m-d-Y") . ".csv";
    
    header("Content-type: text/csv");
    header("Content-disposition: attachment; filename=$filename");
    
    $out = fopen("php://output", "w+");
    
    fputcsv($out, array("Question ID", "Library Article ID", "Subject ID", "Points", "Question"));
    fputcsv($out, array("", "", "Answer", "Correct/Incorrect", "Order"));
    
    $quizQuestionService = new QuizQuestionService();
    $quizQuestions = $quizQuestionService->find("quiz_id = $quizId ORDER BY subject_id, points");
    
    foreach($quizQuestions as $quizQuestion) {
    	$libaryArticleId = $quizQuestion->libraryArticleId == null ? "-1" : $quizQuestion->libraryArticleId;
        fputcsv($out, array($quizQuestion->id, $libaryArticleId, $quizQuestion->subjectId, $quizQuestion->points, $quizQuestion->question));
        
        $options = $quizQuestion->getOptions();
        foreach ($options as $option) {
            $isCorrect = $option->correct ? "TRUE" : "FALSE";
            fputcsv($out, array("", "", $option->option, $isCorrect, $option->order));
            
        }
        
    }
    
?>