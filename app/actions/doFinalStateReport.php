<?php
    require_once("../../globals.php");
    require_once("AdminAuthenticator.php");

    set_time_limit(900);

    if ($admin->role != "admin") {
        header("location: /admin/login.php");
    }

    $childService = new ChildService();
    $children = $childService->getStateSignupCount($_POST['startDate'], $_POST['endDate']);

    $filename = "final-state-report_" . date("m-d-Y") . ".csv";

    header("Content-type: text/csv");
    header("Content-disposition: attachment; filename=$filename");

    $out = fopen("php://output", "w+");

    fputcsv($out, array("State", "Sign Ups"));

    foreach ($children as $child) {
        fputcsv($out, array($child->st, $child->count));
    }

    die();

?>