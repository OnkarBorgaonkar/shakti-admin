<?php
    require_once "../../globals.php";
    require_once("Authenticator.php");

    $articleId = $_REQUEST['articleId'];

    $libraryArticleService = new LibraryArticleService();
    $article = array_pop($libraryArticleService->find("id = $articleId"));

    if($article == null){
        $humanReadableMessage = "Article Does Not exist";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /index.php?systemMessage=$systemMessage");
        die();
    }

    $libraryResponseService = new LibraryResponseService();
    $libraryResponse = $user->getCurrentLibraryResponse();

    $libraryAnswerService = new LibraryAnswerService();
    $libraryAnswer = array_pop($libraryAnswerService->find("library_response_id = $libraryResponse->id AND library_article_id = $article->id"));
    if($libraryAnswer == null){
        $libraryAnswer = new LibraryAnswer();
        $libraryAnswer->libraryResponseId = $libraryResponse->id;
        $libraryAnswer->libraryArticleId = $article->id;
        $libraryAnswerService->save($libraryAnswer);

        $userPointsEntry = new UserPointsEntry();
        $userPointsEntry->pointType = "LIBRARY_ARTICLE";
        $userPointsEntry->points = $article->points;
        $userPointsEntry->activityId = $libraryAnswer->id;

        $userPointsEntryService = new UserPointsEntryService();
        $userPointsEntryService->saveEntry($userPointsEntry, $user);
        $user->recalculateRank();
    }

    if($libraryResponse->completedAt == null && $libraryResponse->hasReadAllArticles()) {
        $now = new DateTime();
        $libraryResponse->completedAt = $now->format("Y-m-d H:i:s");
        $libraryResponseService->save($libraryResponse);
        $user->recalculateRank();
    }

    header("location: /library.php");
    die();

?>