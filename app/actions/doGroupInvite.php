<?php
    require_once("../../globals.php");
    $groupInvitation = new GroupInvitation();

    $groupInvitation->setAll($_POST);

    $groupInvitationService = new GroupInvitationService();

    $invites = $groupInvitationService->find("recipient_id = $groupInvitation->recipientId AND group_id= $groupInvitation->groupId");

    if (sizeof($invites) != 0){
        $humanReadableMessage = "This Member has already been Invited to this Team";
        $systemMessage = urlencode($humanReadableMessage);

        header("location: /groupInvite.php?userId=$groupInvitation->recipientId&systemMessage=$systemMessage");
        die();
    }
    
    $userGroupService = new UserGroupService();
    $userGroup = array_pop($userGroupService->find("group_id = $groupInvitation->groupId AND user_id = $groupInvitation->recipientId"));
    
    if ($userGroup != null) {
        $humanReadableMessage = "This user already belongs to that Team.";
        $systemMessage = urlencode($humanReadableMessage);
        
        header("location: /groupInvite.php?userId=$groupInvitation->recipientId&systemMessage=$systemMessage");
        die();
    }
    
    $groupService = new GroupService();
    $group = array_pop($groupService->find("id = $groupInvitation->groupId"));
    
    if ($group->typeName == "CLASS_ROOM" && $groupService->isUserInAClassroom($groupInvitation->recipientId)) {
        $humanReadableMessage = "This user already belongs to a classroom team";
         $systemMessage = urlencode($humanReadableMessage);
        
        header("location: /groupInvite.php?userId=$groupInvitation->recipientId&systemMessage=$systemMessage");
        die();
    }
    
    try
    {
        $groupInvitationService->save($groupInvitation);
    }
    catch(PDOException $e){

        $humanReadableMessage = "An error has occured while Sending your Request";
        $systemMessage = urlencode($humanReadableMessage);

        header("location: /groupInvite.php?userId=$groupInvitation->recipientId&systemMessage=$systemMessage");
        die();
    }

    $systemMessage = urlencode("Your Team Invite has been Sent.");
    header("location: /warriors_list.php?systemMessage=$systemMessage");

?>
