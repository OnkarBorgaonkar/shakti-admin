<?php
    require_once("../../globals.php");
    $groupRequest = new GroupRequest();

    $groupRequest->setAll($_POST);

    $groupRequestService = new GroupRequestService();
    try
    {
        $groupRequestService->save($groupRequest);
    }
    catch(PDOException $e){

        $humanReadableMessage = "An error has occurred while Sending your Request";
        $systemMessage = urlencode($humanReadableMessage);
        //echo  $e->getMessage();
        header("location: /groupRequest.php?groupId=$groupInvitation->groupId&systemMessage=$systemMessage");
        die();
    }

    $systemMessage = urlencode("Your Team Request has been Sent.");
    header("location: /groupRequest.php?systemMessage=$systemMessage&groupId$groupInvitation->groupId");

?>
