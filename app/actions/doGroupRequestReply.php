<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $response = $_REQUEST['response'];
    $groupId = $_REQUEST['groupId'];
    $userId = $_REQUEST['userId'];

    $userService = new UserService();
    $otherUser = $userService->find("id = $userId");

    if ($response != null && $groupId != null && $userId != null){

        if($response == 'accept'){
            $userGroup = new UserGroup();
            $userGroup->userId = $userId;
            $userGroup->groupId = $groupId;

            $userGroupService = new UserGroupService();
            $userGroupService->save($userGroup);

            $groupRequestService = new GroupRequestService();
            $groupRequestService->deleteWhere("sender_id = $userId AND group_id = $groupId");

            $groupJoinPointsService = new GroupJoinPointsService();

            $groupJoinPoints = new GroupJoinPoints();
            $groupJoinPoints->groupId = $groupId;
            $groupJoinPoints->userId = $userId;
            $groupJoinPoints->points = 10;
            $groupJoinPointsService->save($groupJoinPoints);

            $userPointsEntry = new UserPointsEntry();
            $userPointsEntry->pointType = "RECRUITING";
            $userPointsEntry->points = 10;
            $userPointsEntry->activityId = $groupJoinPoints->id;
            $userPointsEntryService->saveEntry($userPointsEntry, $otherUser);

            $groupJoinPoints = new GroupJoinPoints();
            $groupJoinPoints->groupId = $groupId;
            $groupJoinPoints->userId = $user->id;
            $groupJoinPoints->points = 10;
            $groupJoinPointsService->save($groupJoinPoints);

            $userPointsEntry = new UserPointsEntry();
            $userPointsEntry->pointType = "RECRUITING";
            $userPointsEntry->points = 10;
            $userPointsEntry->activityId = $groupJoinPoints->id;
            $userPointsEntryService->saveEntry($userPointsEntry, $user);

            $user->recalculateRank();

            header("location: /groupDetails.php?groupId=$groupId&systemMessage=Approved");
        }
        else if ($response == 'deny'){
            $groupRequestService = new GroupRequestService();
            $groupRequestService->deleteWhere("sender_id = $userId AND group_id = $groupId");

            header("location: /welcome.php?systemMessage=Denied");
        }
    }
    else {

    }
?>
