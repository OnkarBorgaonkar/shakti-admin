<?php 
    require_once("../../globals.php");

    $jsonString = "";  
    $jsonData = fopen('php://input' , 'rb'); 
    while (!feof($jsonData)) { 
        $jsonString .= fread($jsonData, 4096); 
    }
    fclose($jsonData);
    
    
    $json = json_decode($jsonString);
    
    
    $items = $json->{'line_items'};
    var_dump($items);
    
    foreach ($items as $item) {
        if ($item->{'product_id'} == "110429302") {
            $shopifyOrder = new ShopifyOrder();
            $shopifyOrder->email = $json->email;
            $shopifyOrder->orderNumber = $json->{'order_number'};
            $shopifyOrder->productId = $item->{'product_id'};
            $shopifyOrder->quantity = $item->quantity;
            $shopifyOrder->activationKey = uniqid($json->email);
            $shopifyOrder->created = $json->{'created_at'};
            
            $shopifyOrderService = new ShopifyOrderService();
            $shopifyOrderService->save($shopifyOrder);
            
            $mailService = new MailService();
            $mailService->shopifyOrderCreatedEmail($shopifyOrder);
            
        }
    }

    header('HTTP/1.0 200 OK');
    exit();
    
?>