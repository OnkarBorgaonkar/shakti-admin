<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $response = $_REQUEST['response'];
    $senderId = $_REQUEST['senderId'];
    $recipientId = $_REQUEST['recipientId'];
    
    if ($response == 'accept') {
        $heroCornerUser = new HeroCornerUser();
        $heroCornerUser->userId = $senderId;
        $heroCornerUser->heroId = $recipientId;
        $heroCornerUserService = new HeroCornerUserService();
        $heroCornerUserService->save($heroCornerUser);
    }
    
    $heroCornerInviteService = new HeroCornerInviteService();
    $heroCornerInviteService->deleteWhere("sender_id = $senderId AND recipient_id = $recipientId");
    
    header("location: /welcome.php?systemMessage=Responded to request");
    
    
?>