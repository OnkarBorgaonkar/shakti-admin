<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    if ($user->role != "admin")
        header("location: /welcome.php");

    $libraryId = $_POST['libraryId'];

    if (!isset($_FILES['file']['tmp_name'])) {
        $message = urlencode("Please select a file");
        header("locaton: /admin/importQuizData.php?systemMessage=$message");
    }

    $handle = fopen($_FILES['file']['tmp_name'], "r");
    $header1 = fgetcsv($handle);

    $libraryArticleService = new LibraryArticleService();

    while (($data = fgetcsv($handle)) !== false) {
        list($articleId, $subjectId, $name, $points, $order, $articleType, $url, $pages) = $data;
        $libraryArticle = new LibraryArticle();
        if (!empty($articleId))
            $libraryArticle->id = $articleId;

        $libraryArticle->libraryId = $libraryId;
        $libraryArticle->subjectId = $subjectId;
        $libraryArticle->name = $name;
        $libraryArticle->points = $points;
        $libraryArticle->order = $order;
        $libraryArticle->articleType = $articleType;
        $libraryArticle->url = $url;
        $libraryArticle->pages = $pages;

        $libraryArticleService->save($libraryArticle);
    }
    $message = urlencode("Library Updated");
    header("location: /admin/index.php?systemMessage=$message");

?>