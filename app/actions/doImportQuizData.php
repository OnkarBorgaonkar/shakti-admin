<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    if ($user->role != "admin")
        header("location: /welcome.php");

    $quizId = $_POST['quizId'];

    if (!isset($_FILES['file']['tmp_name'])) {
        $message = urlencode("Please select a file");
        header("locaton: /admin/importQuizData.php?systemMessage=$message");
    }

    $handle = fopen($_FILES['file']['tmp_name'], "r");
    $header1 = fgetcsv($handle);
    $header2 = fgetcsv($handle);
    $currentQuestionId = -1;

    $quizQuestionService = new QuizQuestionService();
    $quizQuestionOptionService = new QuizQuestionOptionService();

    while (($data = fgetcsv($handle)) !== false) {
        if (!empty($data[1])) {
            $quizQuestion = new QuizQuestion();
            list($questionId, $libraryArticleId, $subjectId, $points, $question) = $data;
            if (!empty($questionId))
                $quizQuestion->id = $questionId;

            $quizQuestion->quizId = $quizId;
            $quizQuestion->subjectId = $subjectId;
            $quizQuestion->points = $points;
            $quizQuestion->question = $question;
            $quizQuestion->order = $points / 10;
            $quizQuestion->libraryArticleId = $libraryArticleId != -1 ? $libraryArticleId : null;

            $quizQuestionService->save($quizQuestion);
            $currentQuestionId = $quizQuestion->id;
            $quizQuestionOptionService->deleteWhere("quiz_question_id = $quizQuestion->id");
        }
        else {
            $quizQuestionOption = new QuizQuestionOption();
            list($nothing1, $nothing2, $option, $correct, $order) = $data;
            
            $quizQuestionOption->quizQuestionId = $currentQuestionId;
            $quizQuestionOption->option = $option;
            $quizQuestionOption->correct = $correct;
            $quizQuestionOption->order = $order;

            $quizQuestionOptionService->save($quizQuestionOption);
        }
    }
    $message = urlencode("Quiz Updated");
    header("location: /admin/index.php?systemMessage=$message");

?>