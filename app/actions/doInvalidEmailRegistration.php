<?php
require_once("../../globals.php");

$guardianService = new GuardianService();
$guardian = array_pop($guardianService->find("id = " . $_GET['guardianId']));
$guardian->email = "";
$guardian->invalidEmail = true;
$guardianService->save($guardian);

$systemMessage = urlencode("Your email has been removed from our system.");
header("location: /index.php?systemMessage=$systemMessage");	
?>