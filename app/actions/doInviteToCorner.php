<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    $heroCornerInvite = new HeroCornerInvite();
    $heroCornerInvite->setAll($_POST);
    
    $heroCornerInviteService = new HeroCornerInviteService();
    $invite = array_pop($heroCornerInviteService->find("sender_id = $heroCornerInvite->senderId AND recipient_id = $heroCornerInvite->recipientId"));
    if ($invite != null) {
        $humanReadableMessage = "You have already invited that Hero to your corner";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /searchUser.php?systemMessage=$systemMessage");
        die();
    }
    
    $heroCornerUserService = new HeroCornerUserService();
    $heroCornerUser = array_pop($heroCornerUserService->find("user_id = $heroCornerInvite->senderId AND hero_id = $heroCornerInvite->recipientId"));
    if ($heroCornerUser != null) {
        $humanReadableMessage = "The Hero is already in your corner";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /searchUser.php?systemMessage=$systemMessage");
        die();
    }
    
    $heroCornerUsers = $heroCornerUserService->find("user_id = $heroCornerInvite->senderId");
    if (sizeof($heroCornerUsers) >= 4) {
        $humanReadableMessage = "Your corner is full, please remove someone before adding someone new";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /searchUser.php?systemMessage=$systemMessage");
        die();
    }
    
    $heroCornerInviteService->save($heroCornerInvite);
    
    $humanReadableMessage = "Invite Sent";
    $systemMessage = urlencode($humanReadableMessage);
    header("location: /searchUser.php?systemMessage=$systemMessage");
    die();
?>