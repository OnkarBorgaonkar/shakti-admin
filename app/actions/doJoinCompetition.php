<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    
    $competitionSchool = new CompetitionSchool();
    $competitionSchool->setAll($_POST['competitionSchool']);
    
    $competitionService = new CompetitionService();
    $competition = array_pop($competitionService->find("id = $competitionSchool->competitionId"));
    
    if($competition->passcode != $_POST['passcode']) {
        $humanReadableMessage = "Invalide passcode for the competition";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/school/joinCompetition.php?systemMessage=$systemMessage&schoolId=$competitionSchool->schoolId");
        die();
    }
       
    $competitionSchoolService = new CompetitionSchoolService();
    
    $alreadyInCompetition = array_pop($competitionSchoolService->find("competition_id = $competitionSchool->competitionId AND school_id = $competitionSchool->schoolId"));
    if ($alreadyInCompetition != null) {
        $humanReadableMessage = "This school is Already in a competition";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/school/joinCompetition.php?systemMessage=$systemMessage&schoolId=$competitionSchool->schoolId");
        die();
    }
    
    try
    {
        $competitionSchoolService->save($competitionSchool);
    }
    catch(PDOException $e){
        $humanReadableMessage = "An Error Occured";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /admin/school/joinCompetition.php?systemMessage=$systemMessage&schoolId=$competitionSchool->schoolId");
        die();
    }
    
    $schoolService = new SchoolService();
    $school = array_pop($schoolService->find("id = $competitionSchool->schoolId"));
    
    $systemMessage = urlencode("Competition Joined");
    header("location: /admin/school/index.php?systemMessage=$systemMessage&districtId=$school->districtId");
    die();
?>