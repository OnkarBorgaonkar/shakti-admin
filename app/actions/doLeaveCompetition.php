<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    $competitionSchoolService = new CompetitionSchoolService();
    $competitionSchoolService->deleteWhere("competition_id = {$_GET['competitionId']} AND school_id = {$_GET['schoolId']}");
    
    $schoolService = new SchoolService();
    $school = array_pop($schoolService->find("id = {$_GET['schoolId']}"));
    
    $humanReadableMessage = "School Removed From Competition";
    $systemMessage = urlencode($humanReadableMessage);
    header("location: /admin/school/index.php?systemMessage=$systemMessage&districtId=$school->districtId");
    die();
?>