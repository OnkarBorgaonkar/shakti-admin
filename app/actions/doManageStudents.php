<?php
    require_once("../../globals.php");

    $id = $_POST['id'];
    $userIds = $_POST['ids'];
    $studentIds = $_POST['studentIds'];

    $userClassroomService = new UserClassroomService();
    $userClassroomService->deleteWhere("classroom_id = $id");

    $childService = new ChildService();


    foreach ($userIds as $i=>$userId) {
        $studentId = trim($studentIds[$i]);
        if ($studentId != "") {
            $child = array_pop($childService->find("user_id = $userId"));
            $child->studentId = $studentId;
            $childService->save($child);
        }
        $userClassroom = new UserClassroom();
        $userClassroom->userId = $userId;
        $userClassroom->classroomId = $id;
        $userClassroomService->save($userClassroom);
    }

    $_SESSION['successMessage'] = "Classroom Updated Successfully";

    header("location: /admin/manageStudents.php?id=$id");

?>