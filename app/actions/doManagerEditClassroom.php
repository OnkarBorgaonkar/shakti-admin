<?php
    require_once("../../globals.php");

    $role = isset($_SESSION['manager']['role']) ? $_SESSION['manager']['role'] : null;
    if ($role == null || ($role != "district" && $role != "school" && $role != "classroom")) {
        $humanReadableMessage = "You do not have access to do this";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /index.php?systemMessage=$systemMessage");
        die();
    }
    
    $classroom = new Classroom();
    $classroom->setAll($_POST['classroom']);
    if (!isset($_POST['classroom']['active']))
        $classroom->active = false;
    
    $classroomService = new ClassroomService();
    
    $classroomName = pg_escape_string($classroom->name);
    $where = "name = '$classroomName'";
    if (isset($classroom->id))
        $where .= " AND id != $classroom->id";
    $duplicateName = array_pop($classroomService->find($where));
    
    if ($duplicateName != null) {
        $humanReadableMessage = "A Classroom with this name already exists";
        $systemMessage = urlencode($humanReadableMessage);
        $id = isset($classroom->id) ? "&classroomId=$classroom->id" : "";
        header("location: /manager/classroom/editClassroom.php?systemMessage=$systemMessage&schoolId=$classroom->schoolId" . $id);
        die("test");
    }
    
    $passcode = pg_escape_string($classroom->passcode);
    $where = "passcode = '$passcode'";
    if (isset($classroom->id))
        $where .= " AND id != $classroom->id";
    $duplicateName = array_pop($classroomService->find($where));
    
    if ($duplicateName != null) {
        $humanReadableMessage = "A Classroom with this passcode already exists";
        $systemMessage = urlencode($humanReadableMessage);
        isset($classroom->id) ? "&classroomId=$classroom->id" : "";
        header("location: /manager/classroom/editClassroom.php?systemMessage=$systemMessage&schoolId=$classroom->schoolId"  . $id);
        die();
    }
    
    try
    {
        $classroomService->save($classroom);
    }
    catch(PDOException $e){
        $humanReadableMessage = "An Error Occured";
        $systemMessage = urlencode($humanReadableMessage);
        isset($classroom->id) ? "&classroomId=$classroom->id" : "";
        header("location: /manager/classroom/editClassroom.php?systemMessage=$systemMessage&schoolId=$classroom->schoolId"  . $id);
        die();
    }
    
    $classroomGradeService = new ClassroomGradeService();
    $classroomGradeService->deleteWhere("classroom_id = $classroom->id");
    
    $gradeTypes = $_POST['gradeTypes'];
    foreach ($gradeTypes as $gradeType) {
        $classroomGrade = new ClassroomGrade();
        $classroomGrade->classroomId = $classroom->id;
        $classroomGrade->gradeType = $gradeType;
        $classroomGradeService->save($classroomGrade);
    }
    
    $systemMessage = urlencode("Classroom Updated.");
    header("location: /manager/classroom/index.php?systemMessage=$systemMessage&classroomId=$classroom->id");
    die();
?>