<?php
    require_once("../../globals.php");
    
    if (!isset($_SESSION['manager']['role']) && $_SESSION['manager']['role'] != 'district') {
        $humanReadableMessage = "You do not have access to do this";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /index.php?systemMessage=$systemMessage");
        die();
    }
    
    $districtService = new DistrictService(); 
    $districtId = $_POST['id'];
    
    $district = array_pop($districtService->find("id = $districtId"));
    if ($district == null) {
        $humanReadableMessage = "Cannot find District";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /index.php?systemMessage=$systemMessage");
        die();
    }
    $district->setAll($_POST);    
    
    $districtName = pg_escape_string($district->name);
    $where = "name = '$districtName'";
    if (isset($district->id))
        $where .= " AND id != $district->id";
    
    $duplicateName = array_pop($districtService->find($where));
    if ($duplicateName != null) {
        $humanReadableMessage = "This Name Has Already Been Used";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /manager/district/editDistrict.php?systemMessage=$systemMessage&districtId=$districtId");
        die();
    }
    
    $passcode = pg_escape_string($district->passcode);
    $where = "passcode = '$passcode'";
    if (isset($district->id))
        $where .= " AND id != $district->id";
    
    $duplicatePassword = array_pop($districtService->find($where));
    if ($duplicatePassword != null) {
        $humanReadableMessage = "This Password Has Already Been Used";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /manager/district/editDistrict.php?systemMessage=$systemMessage&districtId=$districtId");
        die();
    }
    
    try
    {
        $districtService->save($district);
    }
    catch(PDOException $e){
    
        $humanReadableMessage = "An Error Occured";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /manager/district/editDistrict.php?systemMessage=$systemMessage&districtId=$districtId");
        die();
    }
    
    $systemMessage = urlencode("District Updated.");
    header("location: /manager/district/index.php?systemMessage=$systemMessage&districtId=$districtId");
    die();
?>