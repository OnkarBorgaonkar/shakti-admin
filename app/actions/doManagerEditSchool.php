<?php
    require_once("../../globals.php");
    
    $role = isset($_SESSION['manager']['role']) ? $_SESSION['manager']['role'] : null;
    if ($role == null || ($role != "district" && $role != "school")) {
        $humanReadableMessage = "You do not have access to do this";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /index.php?systemMessage=$systemMessage");
        die();
    }
    
    $school = new School();
    $school->setAll($_POST);
    
    $geocodeService = new GeocodeService();
    $latLng = $geocodeService->GeocodeAddress($school->getFullAddress());
    
    if ($latLng != null) {
        $school->latitude = $latLng['latitude'];
        $school->longitude = $latLng['longitude'];
    }
    else {
        $humanReadableMessage = "The address could not be geocoded.";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /manager/school/editSchool.php?systemMessage=$systemMessage&districtId=$school->districtId" . isset($school->id) ? "&schoolId=$school->id" : "");
        die();
    }
    
    $schoolService = new SchoolService();
    
    $schoolName = pg_escape_string($school->name);
    $where = "name = '$schoolName'";
    if (isset($school->id))
        $where .= " AND id != $school->id"; 
    $duplicateName = array_pop($schoolService->find($where));
    
    if ($duplicateName != null) {
        $humanReadableMessage = "A School with this name already exists";
        $systemMessage = urlencode($humanReadableMessage);
        $id = isset($school->id) ? "&schoolId=$school->id" : "";
        header("location: /manager/school/editSchool.php?systemMessage=$systemMessage&districtId=$school->districtId" . $id);
        die();
    }
    
    $passcode = pg_escape_string($school->passcode);
    $where = "passcode = '$passcode'";
    if (isset($school->id))
        $where .= " AND id != $school->id"; 
    $duplicateName = array_pop($schoolService->find($where));
    
    if ($duplicateName != null) {
        $humanReadableMessage = "A School with this passcode already exists";
        $systemMessage = urlencode($humanReadableMessage);
        $id = isset($school->id) ? "&schoolId=$school->id" : "";
        header("location: /manager/school/editSchool.php?systemMessage=$systemMessage&districtId=$school->districtId" . $id);
        die();
    }
    
    try
    {
        $schoolService->save($school);
    }
    catch(PDOException $e){
    
        echo $e->getMessage();
        die();
        $humanReadableMessage = "An Error Occured";
        $systemMessage = urlencode($humanReadableMessage);
        $id = isset($school->id) ? "&schoolId=$school->id" : "";
        header("location: /manager/school/editSchool.php?systemMessage=$systemMessage&districtId=$school->districtId" . $id);
        die();
    }
    
    $systemMessage = urlencode("School Updated.");
    header("location: /manager/school/index.php?systemMessage=$systemMessage&schoolId=$school->id");
    die();
?>