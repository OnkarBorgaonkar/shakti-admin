<?php
    require_once("../../globals.php");
    
    $role = isset($_SESSION['manager']['role']) ? $_SESSION['manager']['role'] : null;
    if ($role == null || ($role != "district")) {
        $humanReadableMessage = "You do not have access to do this";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /index.php?systemMessage=$systemMessage");
        die();
    }
    
    $competitionSchoolService = new CompetitionSchoolService();
    $competitionSchoolService->deleteWhere("competition_id = {$_GET['competitionId']} AND school_id = {$_GET['schoolId']}");
    
    $schoolService = new SchoolService();
    $school = array_pop($schoolService->find("id = {$_GET['schoolId']}"));
    
    $humanReadableMessage = "School Removed From Competition";
    $systemMessage = urlencode($humanReadableMessage);
    header("location: /manager/district/index.php?systemMessage=$systemMessage&districtId=$school->districtId");
    die();
?>