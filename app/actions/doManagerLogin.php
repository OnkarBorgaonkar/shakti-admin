<?php 
    require_once("../../globals.php");
    
    $managerType = $_POST['managerType'];
    $passcode = $_POST['password'];
    
    if ($managerType == "district") {
        $districtService = new DistrictService();         
        $district = $districtService->findDistrictByPasscode($passcode);
        if ($district != null) {
            $_SESSION['manager']['role'] = 'district';
            $_SESSION['manager']['districtId'] = $district->id;
            
            header("location: /manager/district/index.php?districtId=$district->id");
            die();            
        }
        else {
            $systemMessage = urlencode("Incorrect Passcode");
            header("location: /manager/login.php?systemMessage=$systemMessage");
        }
    }
    else if ($managerType == "school") {
        $schoolService = new SchoolService();         
        $school = $schoolService->findSchoolByPasscode($passcode);
        if ($school != null) {
            $_SESSION['manager']['role'] = 'school';
            $_SESSION['manager']['schoolId'] = $school->id;
            
            header("location: /manager/school/index.php?schoolId=$school->id");
            die();            
        }
        else {
            $systemMessage = urlencode("Incorrect Passcode");
            header("location: /manager/login.php?systemMessage=$systemMessage");
        }
    }
    else if ($managerType == "classroom") {
        $classroomService = new ClassroomService();         
        $classroom = $classroomService->findClassroomByPasscode($passcode);
        if ($classroom != null) {
            $_SESSION['manager']['role'] = 'classroom';
            $_SESSION['manager']['classroomId'] = $classroom->id;
            
            header("location: /manager/classroom/index.php?classroomId=$classroom->id");
            die();            
        }
        else {
            $systemMessage = urlencode("Incorrect Passcode");
            header("location: /manager/login.php?systemMessage=$systemMessage");
        }
    }
?>