<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $checkPointId = $_POST['id'];
    $answer = $_POST['answer'];

    $orbCheckpointService = new OrbCheckpointService();
    $orbCheckpoint = array_pop($orbCheckpointService->find("id = $checkPointId"));

    // Remove exclamation points from input and answer to allow both of these examples to be valid answers: "I did it" or "I did it!"
    if (str_replace("!", "", strtolower($orbCheckpoint->answer)) != str_replace("!", "", strtolower($answer))) {
        $message = "Incorrect Answer";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    $userOrbCheckpointService = new UserOrbCheckpointService();
    $userOrbCheckpoint = array_pop($userOrbCheckpointService->find("user_id = $user->id AND orb_checkpoint_id = $checkPointId"));

    if ($userOrbCheckpoint == null) {

        $userOrbCheckpoint = new UserOrbCheckpoint();
        $userOrbCheckpoint->userId = $user->id;
        $userOrbCheckpoint->orbCheckpointId = $checkPointId;

        $userOrbCheckpointService->save($userOrbCheckpoint);

        $userPointsEntryService = new UserPointsEntryService();

        $userPointsEntry = new UserPointsEntry();
        $userPointsEntry->pointType = "ORB_CHECKPOINT";
        $userPointsEntry->points = $orbCheckpoint->points;
        $userPointsEntry->activityId = $userOrbCheckpoint->id;
        $userPointsEntry->userId = $user->id;

        $userPointsEntryService->save($userPointsEntry);
    }

    $message = "Correct";
    $json = Array(
        "success" => true,
        "systemMessage" => $message
    );
    echo json_encode($json);
    die();
?>