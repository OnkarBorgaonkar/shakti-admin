<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $orbCheckpointId = $_POST['id'];

    $userOrbCheckpointService = new UserOrbCheckpointService();
    $userOrbCheckpoint = array_pop($userOrbCheckpointService->find("user_id = $user->id AND orb_checkpoint_id = $orbCheckpointId"));

    if ($userOrbCheckpoint == null) {

        $userOrbCheckpoint = new UserOrbCheckpoint();
        $userOrbCheckpoint->userId = $user->id;
        $userOrbCheckpoint->orbCheckpointId = $orbCheckpointId;

        $userOrbCheckpointService->save($userOrbCheckpoint);

        $orbCheckpoinService = new OrbCheckpointService();
        $orbCheckpoint = array_pop($orbCheckpoinService->find("id = $orbCheckpointId"));

        $userPointsEntryService = new UserPointsEntryService();

        $userPointsEntry = new UserPointsEntry();
        $userPointsEntry->pointType = "ORB_CHECKPOINT";
        $userPointsEntry->points = $orbCheckpoint->points;
        $userPointsEntry->activityId = $userOrbCheckpoint->id;
        $userPointsEntry->userId = $user->id;

        $userPointsEntryService->save($userPointsEntry);
    }

    $message = "Correct";
    $json = Array(
        "success" => true,
        "systemMessage" => $message
    );
    echo json_encode($json);
    die();
?>