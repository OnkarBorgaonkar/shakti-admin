<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $orbCheckpointId = $_POST['id'];
    $optionId = $_POST['answer'];

    $orbCheckpointOptionService = new OrbCheckpointOptionService();
    $option = array_pop($orbCheckpointOptionService->find("id = $optionId"));

    if ($option == null) {
        $message = "Not a valid option";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    if (!$option->correct) {
        $message = "Incorrect Answer";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    $userOrbCheckpointService = new UserOrbCheckpointService();
    $userOrbCheckpoint = array_pop($userOrbCheckpointService->find("user_id = $user->id AND orb_checkpoint_id = $orbCheckpointId"));

    if ($userOrbCheckpoint == null) {

        $userOrbCheckpoint = new UserOrbCheckpoint();
        $userOrbCheckpoint->userId = $user->id;
        $userOrbCheckpoint->orbCheckpointId = $orbCheckpointId;

        $userOrbCheckpointService->save($userOrbCheckpoint);

        $orbCheckpoinService = new OrbCheckpointService();
        $orbCheckpoint = array_pop($orbCheckpoinService->find("id = $orbCheckpointId"));

        $userPointsEntryService = new UserPointsEntryService();

        $userPointsEntry = new UserPointsEntry();
        $userPointsEntry->pointType = "ORB_CHECKPOINT";
        $userPointsEntry->points = $orbCheckpoint->points;
        $userPointsEntry->activityId = $userOrbCheckpoint->id;
        $userPointsEntry->userId = $user->id;

        $userPointsEntryService->save($userPointsEntry);
    }

    $message = "Correct";
    $json = Array(
        "success" => true,
        "systemMessage" => $message
    );
    echo json_encode($json);
    die();
?>