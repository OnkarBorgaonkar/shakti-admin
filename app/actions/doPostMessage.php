<?php
    require_once("../../globals.php");

    $groupMessage = new GroupMessage();
    $groupMessage->setAll($_POST);

    $groupMessageService = new GroupMessageService();
    try
    {
        $groupMessageService->save($groupMessage);
    }
    catch(PDOException $e){

        $humanReadableMessage = "An Error Occured While Posting Your Message";
        $systemMessage = urlencode($humanReadableMessage);

        echo $e->getMessage();
        header("location: /messageBoard.php?systemMessage=$systemMessage");
        die();
    }

    $systemMessage = urlencode("Your Message Has Been Posted.");
    header("location: /messageBoard.php?systemMessage=$systemMessage&groupId=$groupMessage->groupId");

?>
