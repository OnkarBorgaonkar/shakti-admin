<?php
    require_once "../../globals.php";
    require_once("Authenticator.php");

    $articleId = $_REQUEST['articleId'];

    $libraryArticleService = new LibraryArticleService();
    $article = array_pop($libraryArticleService->find("id = $articleId"));

//     if($article == null){
//         $humanReadableMessage = "Article Does Not exist";
//         $systemMessage = urlencode($humanReadableMessage);
//         header("location: /index.php?systemMessage=$systemMessage");
//         die();
//     }

//     $libraryResponseService = new LibraryResponseService();
//     $libraryResponse = array_pop($libraryResponseService->find("user_id = $user->id AND library_id = $article->libraryId"));
//     if($libraryResponse == null){
//         $libraryResponse = new LibraryResponse();
//         $libraryResponse->userId = $user->id;
//         $libraryResponse->libraryId = $article->libraryId;
//         $libraryResponseService->save($libraryResponse);
//     }

//     $libraryAnswerService = new LibraryAnswerService();
//     $libraryAnswer = array_pop($libraryAnswerService->find("library_response_id = $libraryResponse->id AND library_article_id = $article->id"));
//     if($libraryAnswer == null){
//         $libraryAnswer = new LibraryAnswer();
//         $libraryAnswer->libraryResponseId = $libraryResponse->id;
//         $libraryAnswer->libraryArticleId = $article->id;
//         $libraryAnswerService->save($libraryAnswer);
//     }
    
//     if($libraryResponse->completedAt == null && $libraryResponse->hasReadAllArticles()){
//         $now = new DateTime();
//         $libraryResponse->completedAt = $now->format("Y-m-d H:i:s");
//         $libraryResponseService->save($libraryResponse);
//         $user->recalculateRank();
//     }

    if ($article->articleType == 'PDF'){
        header('Content-type: application/pdf');
        header("location: " . $article->getFilePath());
        die();
    }
    else{
        header("location: " . $article->url);
        die();
    }

?>
