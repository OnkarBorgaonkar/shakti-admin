<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    $heroId = $_GET['heroId'];

    $heroCornerUserService = new HeroCornerUserService();
    $heroCornerUserService->deleteWhere("user_id = $user->id AND hero_id = $heroId");
    
    $humanReadableMessage = "Hero removed from corner";
    $systemMessage = urlencode($humanReadableMessage);
    
    header("location: /welcome.php?systemMessage=$systemMessage");
?>