<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $groupId = $_REQUEST['groupId'];

    $groupService = new GroupService();
    $group = array_pop($groupService->find("id = $groupId"));

    if($group == null){
        $humanReadableMessage = "Group Not Found";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /index.php?systemMessage=$systemMessage");
        die();
    }

    if($group->leaderId != $user->id){
        $humanReadableMessage = "You Are Not the Leader of the Team";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /groupDetails.php?groupId=$group->id&systemMessage=$systemMessage");
        die();
    }

    $userId = $_REQUEST['memberId'];
    if ($group->isMemberById($userId)){
        $group->removeMember($userId);
        $humanReadableMessage = "Member Removed";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /groupDetails.php?groupId=$group->id&systemMessage=$systemMessage");
    }
    else{
        $humanReadableMessage = "Member Was not apart of the group";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /groupDetails.php?groupId=$group->id&systemMessage=$systemMessage");
    }
?>
