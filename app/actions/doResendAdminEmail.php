<?php
    require_once("../../globals.php");

    $email = $_GET['email'];

    $adminUserService = new AdminUserService();
    $adminUser = $adminUserService->getAdminUserByEmail($email);

    if ($adminUser->role == 'district') {
        $districtService = new DistrictService();
        $district = array_pop($districtService->find("id = $adminUser->districtId"));

        $mailService = new MailService();
        $mailService->newAdminAccount($adminUser, "District", $district->name);
    }
    else if ($adminUser->role == 'school') {
        $schoolService = new SchoolService();
        $school = array_pop($schoolService->find("id = $adminUser->schoolId"));

        $mailService = new MailService();
        $mailService->newSchoolAdminAccount($adminUser, $school->name);
    }
    else if ($adminUser->role == 'classroom') {
        $classroomService = new ClassroomService();
        $classroom = array_pop($classroomService->find("id = $adminUser->classroomId"));

        $mailService = new MailService();
        $mailService->newAdminAccount($adminUser, "Classroom", $classroom->name);
    }

    $_SESSION['errorMessage'] = "The email has been resent.";
    header("location: /admin/login.php");
    die();

?>