<?php
    require_once("../../globals.php");

    if (isset($_POST['show']) && $_POST['show'] != ""){
        if ($_POST['show'] == 'user'){
            //user search
            $orderBy = 'points';
            if($_POST['order'] == 'name'){
                $orderBy = 'login';
            }

            $search = null;
            if (isset($_POST['search'])){
                $search = $_POST['search'];
            }
            header("location: /searchUser.php?search=$search&orderBy=$orderBy");
        }
        else {
            //user group
            $orderBy = 'points';
            if($_POST['order'] == 'name'){
                $orderBy = 'name';
            }

            $search = null;
            if (isset($_POST['search'])){
                $search = $_POST['search'];
            }
            header("location: /searchGroup.php?search=$search&orderBy=$orderBy");
        }
    }
    else {
        header("location: /warriors_list.php");
    }

?>
