<?php
require_once("../../globals.php");
require_once('recaptchalib.php');

$resp = recaptcha_check_answer (Config::$recaptchaPrivateKey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);

if (!$resp->is_valid) {
    $systemMessage = "Invalid Captcha. Please try again";
    $systemMessage = urlencode($systemMessage);
    header("location: /contact.php?systemMessage=$systemMessage");
    die();
}

$to      = Config::$contactRecipient;
$subject = 'Contact Us';
$message = "Name: " . $_POST['name'] . "\r\n";
$message .= "Phone: " . $_POST['phone'] . "\r\n";
$message .= $_POST['message'];
$headers = 'From: ' . $_POST['email'] . "\r\n" .
   'Reply-To: ' . $_POST['email'] . "\r\n" .
   'X-Mailer: PHP/' . phpversion();

if (mail($to, $subject, $message, $headers))
    $systemMessage = "Your message has been sent.";
else
    $systemMessage = "There was an error sending your message.";
$systemMessage = urlencode($systemMessage);

header("location: /contact.php?systemMessage=$systemMessage");
?>
