<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    $userService = new UserService();
    
    if (isset($_REQUEST['userId'])) {
        $tmpUser = array_pop($userService->find("id = {$_REQUEST['userId']}"));
        if ($tmpUser != null) {
            $user = $tmpUser;
        }
    }

    $user->fileType = "png";
    $user->contentType = "image/png";    
    $userService->save($user);
    
    FileService::instance()->storeFromStream($user, "php://input");
    
    ImageFileService::instance()->resizeAndStore($user->getOriginalStoragePath(), $user, "thumb", 150);

	$humanReadableMessage = "Your avatar has been saved";
	$systemMessage = urlencode($humanReadableMessage);

	header("Location: /welcome.php?systemMessage=$systemMessage");

?>
