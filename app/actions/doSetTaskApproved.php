<?php
require_once("../../globals.php");
require_once("Authenticator.php");

extract($_POST);
// Test the server environment to see if magic quotes are enabled, unescape if so:
if (get_magic_quotes_gpc())
    $value = stripslashes($value);
$value = json_decode($value);
$childHeroTaskId = $value->id;
$dayOffset = $value->dayOffset;
$secondsOffset = $dayOffset * 24 * 60 * 60;
$completedDate = date("Y-m-d", strtotime($baseDate) + $secondsOffset);

$childHeroId = $user->getChildHeroId();

$childHeroTaskService = new ChildHeroTaskService();
$childHeroTasks = $childHeroTaskService->find("task_id=$childHeroTaskId AND completed_at='$completedDate' AND child_hero_id=$childHeroId");
$childHeroTask = array_pop($childHeroTasks);
$childHeroTask->approved = ("true" == $checked) ? true : false;
var_dump($childHeroTask->approved);

$userPointsEntryService = new UserPointsEntryService();
if($childHeroTask->approved) {

    $taskService = new TaskService();
    $sql = "
            SELECT
                tasks.*, categories.id AS category_id
            FROM
                categories INNER JOIN tasks ON categories.name = tasks.category
            WHERE
                tasks.id = $childHeroTask->taskId
        ";

    $task = array_pop($taskService->findBySql($sql));

    $userPointsEntry = new UserPointsEntry();
    $userPointsEntry->pointType = "DAILY_TRAINING";
    $userPointsEntry->points = $task->points;
    $userPointsEntry->activityId = $childHeroTask->id;
    $userPointsEntry->categoryId = $task->categoryId;

    $userPointsEntryService->saveEntry($userPointsEntry, $user);
}
else {
    $userPointsEntryService->deleteEntry("DAILY_TRAINING", $childHeroTask->id);
}

if ($childHeroTaskService->save($childHeroTask))
    $user->recalculateRank();
   print "Success";
?>
