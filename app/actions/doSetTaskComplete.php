<?php
require_once("../../globals.php");
require_once("Authenticator.php");

extract($_POST);
// Test the server environment to see if magic quotes are enabled, unescape if so:
if (get_magic_quotes_gpc())
    $value = stripslashes($value);
$value = json_decode($value);
$taskId = $value->id;
$dayOffset = $value->dayOffset;
$secondsOffset = $dayOffset * 24 * 60 * 60;
$completedDate = strtotime($baseDate) + $secondsOffset;
$checkDate = strtotime(date("Y-m-d", strtotime($user->getChild()->createdAt)) . " -2 weeks");

if ($completedDate >= $checkDate) {
    $childHeroId = $user->getChildHeroId();

    $childHeroTask = new ChildHeroTask();
    $childHeroTask->taskId = $taskId;
    $childHeroTask->childHeroId = $childHeroId;
    $childHeroTask->completedAt = date("Y-m-d", $completedDate);
    $childHeroTask->approved = $approved == "true" ? true : false;
    $childHeroTaskService = new ChildHeroTaskService();
    $userPointsEntryService = new UserPointsEntryService();

    $taskService = new TaskService();
    $sql = "
        SELECT
            tasks.*, categories.id AS category_id
        FROM
            categories INNER JOIN tasks ON categories.name = tasks.category
        WHERE
            tasks.id = $childHeroTask->taskId
    ";

    $task = array_pop($taskService->findBySql($sql));

    if ("true" == $checked){

        $childHeroTaskService->save($childHeroTask);


        if ($childHeroTask->approved) {
            $userPointsEntry = new UserPointsEntry();
            $userPointsEntry->pointType = "DAILY_TRAINING";
            $userPointsEntry->points = $task->points;
            $userPointsEntry->activityId = $childHeroTask->id;
            $userPointsEntry->categoryId = $task->categoryId;

            $userPointsEntryService->saveEntry($userPointsEntry, $user);
            $user->recalculateRank();
        }

    }
    else{
        $sql = "child_hero_id = $childHeroTask->childHeroId
                AND completed_at = '$childHeroTask->completedAt'
                AND task_id = $childHeroTask->taskId";

        $childHeroTask = array_pop($childHeroTaskService->find($sql));

        echo $childHeroTask->id;
        if ($childHeroTask->approved)
            $userPointsEntryService->deleteEntry("DAILY_TRAINING", $childHeroTask->id, $user);
        $childHeroTaskService->delete($childHeroTask);

        $user->recalculateRank();
    }
}
?>
