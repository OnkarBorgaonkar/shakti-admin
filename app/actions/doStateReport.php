<?php
    require_once("../../globals.php");
    require_once("AdminAuthenticator.php");

    set_time_limit(900);

    if ($admin->role != "admin") {
        header("location: /admin/login.php");
    }

    $checkDate = new DateTime($_POST['startDate']);
    $endDate = new DateTime();
    if (!empty($_POST['endDate']))
        $endDate = new DateTime($_POST['endDate']);

    $resultMap = array();

    while ($checkDate <= $endDate) {
        $dateString = $checkDate->format("Y-m-d");

        $childService = new ChildService();
        $children = $childService->getStateSignupCount($dateString, $dateString);

        foreach ($children as $child) {
            if (!array_key_exists($child->st, $resultMap)) {
                $resultMap[$child->st] = array();
            }
            $resultMap[$child->st][$dateString] = $child->count;
        }

        $checkDate->add(new DateInterval("P1D"));
    }

    $filename = "state-report_" . date("m-d-Y") . ".csv";

    header("Content-type: text/csv");
    header("Content-disposition: attachment; filename=$filename");

    $out = fopen("php://output", "w+");

    ksort($resultMap);
    $labels = array_keys($resultMap);
    array_unshift($labels, "Date");
    fputcsv($out, $labels);

    $states = array_keys($resultMap);
    $checkDate = new DateTime($_POST['startDate']);

    while ($checkDate <= $endDate) {
        $row = array();
        $dateString = $checkDate->format("Y-m-d");
        $row[] = $dateString;

        foreach ($states as $state) {
            if (array_key_exists($dateString, $resultMap[$state])) {
                $row[] = $resultMap[$state][$dateString];
            }
            else {
                $row[] = " ";
            }
        }

        fputcsv($out, $row);
        $checkDate->add(new DateInterval("P1D"));
    }

    die();

?>