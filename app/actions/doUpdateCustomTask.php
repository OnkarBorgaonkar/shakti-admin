<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    if (isset($_REQUEST['id']) && isset($_REQUEST['name'])) {
        $id = $_REQUEST['id'];
        $name = $_REQUEST['name'];

        $webPurifyService = new WebPurifyService();
        if ($webPurifyService->checkForProfanity($name)) {
            $message = "The name contains profanity. Please change the name.";
            $json = array("success" => false, "message" => $message);
            echo json_encode($json);
            die();
        }


        $childHeroId = $user->getChildHeroId();

        $childHeroCustomTaskService = new ChildHeroCustomTaskService();
        $childHeroCustomTask = array_pop($childHeroCustomTaskService->find("id = $id AND child_hero_id = $childHeroId"));

        if ($childHeroCustomTask != null) {
            $childHeroCustomTask->name = $name;
            $childHeroCustomTaskService->save($childHeroCustomTask);
        }

        $message = "Success";
        $json = array("success" => true, "message" => message);
        echo json_encode($json);
        die();
    }

?>