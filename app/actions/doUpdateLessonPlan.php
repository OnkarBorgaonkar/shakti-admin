<?php
    require_once("../../globals.php");
    require_once("AdminAuthenticator.php");

    if ($admin->role != "classroom") {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
        die();
    }

    $classroomGroupId = $_REQUEST['id'];
    $action = $_REQUEST['action'];

    if ($classroomGroupId == null || $action == null)
        die("invalid request");

    $classroomGroupService = new ClassroomGroupService();
    $classroomGroup = array_pop($classroomGroupService->find("id = $classroomGroupId"));

    if ($classroomGroup->classroomId != $admin->classroomId) {
        $_SESSION['errorMessage'] = "You do not have access to this";
        header("location: /admin/login.php");
    }

    if (!$classroomGroup->updated && !$classroomGroup->done) {
        $classroomGroupHistoryEntry = new ClassroomGroupHistoryEntry();
        $classroomGroupHistoryEntry->classroomGroupId = $classroomGroup->id;
        $classroomGroupHistoryEntry->adminUserId = $admin->id;
        $classroomGroupHistoryEntry->adminUserId = $admin->id;
        $now = new DateTime();
        $classroomGroupHistoryEntry->created = $now->format("Y-m-d H:i:s");
        if ($action == "yes") {
            if ($classroomGroup->currentWeek < 14) {
                $classroomGroup->currentWeek++;
            }
            else {
                $classroomGroup->done = true;
            }
            $classroomGroup->updated = true;
            $classroomGroupHistoryEntry->action = "YES";
        }
        else if ($action == "no") {
            $classroomGroup->updated = true;
            $classroomGroupHistoryEntry->action = "NO";
        }
        else if ($action == "done") {
            $classroomGroup->updated = true;
            $classroomGroup->done = true;
            $classroomGroupHistoryentry->action = "DONE";
        }
        $classroomGroupService->save($classroomGroup);

        if ($classroomGroupHistoryEntry->action != null) {
            $classroomGroupHistoryEntryService = new ClassroomGroupHistoryEntryService();
            $classroomGroupHistoryEntryService->save($classroomGroupHistoryEntry);
        }
    }

    header("Location:/admin/lessonPlan.php?week=$classroomGroup->currentWeek");
    die();
?>
