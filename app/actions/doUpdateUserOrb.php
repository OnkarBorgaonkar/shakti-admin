<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $orbId = $_POST['orbId'];
    $status = $_POST['status'];

    $userOrbService = new UserOrbService();
    $userOrb = array_pop($userOrbService->find("user_id = $user->id AND orb_id = $orbId"));

    $now = new DateTime();
    if ($status == "started") {
        if ($userOrb == null) {
            $userOrb = new UserOrb();
            $userOrb->userId = $user->id;
            $userOrb->orbId = $orbId;
            $userOrb->startDate = $now->format("Y-m-d H:i:s");
        }
    }
    else {
        $userOrb->completedDate = $now->format("Y-m-d H:i:s");
    }

    $userOrbService->save($userOrb);

    if ($status == "completed") {
        $orbService = new OrbService();
        $orb = array_pop($orbService->find("id = $orbId"));

        $userPointsEntry = new UserPointsEntry();
        $userPointsEntry->pointType = "ORB";
        $userPointsEntry->points = $orb->points;
        $userPointsEntry->activityId = $userOrb->id;
        $userPointsEntry->userId = $user->id;

        $userPointEntryService = new UserPointsEntryService();
        $userPointEntryService->save($userPointsEntry);
    }


    $message = "Correct";
    $json = Array(
        "success" => true,
        "systemMessage" => $message
    );
    echo json_encode($json);
    die();
?>