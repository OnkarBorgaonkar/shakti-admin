<?php
    require_once("../../globals.php");
    require_once("AdminAuthenticator.php");

    set_time_limit(900);

    if ($admin->role != "admin") {
        header("location: /admin/login.php");
    }

    $userService = new UserService();
    $sql = "
        SELECT
            users.*, children.created_at
        FROM
            users INNER JOIN children ON users.id = children.user_id
    ";

    if (!empty($_POST['signUpDate'])) {
        $sql .= "
            WHERE
                children.created_at >= '" . $_POST['signUpDate'] . "'
        ";
    }

    $sql .= " ORDER BY users.login";

    $users = $userService->findBySql($sql);

    $filename = "warrior-report_" . date("m-d-Y") . ".csv";

    header("Content-type: text/csv");
    header("Content-disposition: attachment; filename=$filename");

    $out = fopen("php://output", "w+");

    fputcsv($out, array("User", "Task Points", "Quiz Points", "Library Points", "Group Join Points", "Total Points", "Current Rank", "Last Assessment"));

    foreach ($users as $currentUser) {
        $userData = array(
            $currentUser->login,
            $currentUser->getTaskPoints(),
            $currentUser->getQuizPoints(),
            $currentUser->getLibraryPoints(),
            $currentUser->getGroupJoinPoints(),
            $currentUser->getTotalPoints(),
            $currentUser->getRankName(),
            $currentUser->getLastAssessment()
        );
        fputcsv($out, $userData);
    }

?>