<?php
    require_once("../../../globals.php");

    $user = new User();
    $user->setAll($_POST['user']);
    $user->active = true;
    $user->initializePassword();
    $user->login = strtolower(trim($user->login));
    $user->rankId = 1;
    $user->backgroundImage = 1;

    $webPurifyService = new WebPurifyService();
    $userService = new UserService();

    if ($userService->updateRequiresApproval($user)) {
        if (strtolower($_POST['approvalPassword']) != strtolower(Config::$approvalPassword)) {
            $message = "Incorrect Approval Password";
            $json = Array(
                "success" => false,
                "systemMessage" => $message
            );
            echo json_encode($json);
            die();
        }
    }

    if ($webPurifyService->checkForProfanity($user->login)) {
        $message = "The Super Hero name is not acceptable, please choose another";
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    $child = new Child();
    $child->setAll($_POST['child']);
    $child->academyYear = 1;


    $existingUser = array_pop($userService->find("email LIKE '$user->email' OR trim(lower(login)) LIKE trim(lower('$user->login'))"));

    if($existingUser != null){
        $message = "The Super Hero name is already in use";
        if($user->email != '' && $existingUser->email == $user->email){
            $message = "That email is already in use";
        }
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    try
    {
        $userService->save($user);
    }
    catch(PDOException $e)
    {
        $errorMessages = array(
            'SQLSTATE[23505]: Unique violation: 7 ERROR:  duplicate key violates unique constraint "users_email_key"' => "That email address is already in use.",
            'SQLSTATE[23505]: Unique violation: 7 ERROR:  duplicate key violates unique constraint "users_login_key"' => "That login is already in use."
        );

        $message = $e->getMessage();
        $json = Array(
            "success" => false,
            "systemMessage" => $message
        );
        echo json_encode($json);
        die();
    }

    $_SESSION['user'] = $user;
    $_SESSION['child'] = $child;


    if (!empty($_POST['guardian']['email']))
    {
        $guardian = new Guardian();
        $guardian->setAll($_POST['guardian']);
        $guardian->notificationCount = 0;

        $guardianService = new GuardianService();
        $guardianService->save($guardian);
        $guardianService->notify($guardian);
    }

    $child->userId = $user->id;
    $childService = new ChildService();
    $childService->save($child);

    $childHero = new ChildHero();

    // We set the child hero only so that we can later edit the hero's height, weight, etc.
    $childHero->childId = $child->id;
    $childHero->color = "blue"; // Hard code color as we aren't using it for Summer of Learning - Old Code: $_POST['hero']['color'];
    $childHeroService = new ChildHeroService();
    $childHeroService->save($childHero);

    $academyYearChangeService = new AcademyYearChangeService();
    $academyYearChange = new AcademyYearChange();
    $academyYearChange->userId = $user->id;
    $academyYearChange->academyYear = 1;

    $academyYearChangeService->save($academyYearChange);

    $message = "Account Created Successfully";
    $json = Array(
        "success" => true,
        "systemMessage" => $message
    );
    echo json_encode($json);
    die();
?>
