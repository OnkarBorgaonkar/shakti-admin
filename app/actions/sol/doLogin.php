<?php
require_once("../../../globals.php");

$user = new User();
$user->setAll($_POST);

$userService = new UserService();
$savedUser = array_pop($userService->find("login='" . $user->login . "' AND active = true"));

$user->salt = $savedUser->salt;

if ($savedUser->password == $user->getHashedPassword())
{
    $child = $savedUser->getChild();
    if ($child->academyYear == 1) {
        $academyYearChangeService = new AcademyYearChangeService();
        $academyYearChange = array_pop($academyYearChangeService->find("user_id = $savedUser->id AND academy_year = 1"));
        $changeDate = strtotime($academyYearChange->created);

        $today = time();
        $year = date("Y", $today);
        $checkDate = strtotime("9/1/" . $year);
        if ($today < $checkDate) {
            $checkDate = strtotime("-1 year", $checkDate);
        }


        if ($changeDate < $checkDate) {
            $childService = new ChildService();
            $child->academyYear = 2;
            $childService->save($child);

            $academyYearChange = new AcademyYearChange();
            $academyYearChange->userId = $savedUser->id;
            $academyYearChange->academyYear = 2;
            var_dump($academyYearChange);
            $academyYearChangeService->save($academyYearChange);
        }
    }

    $_SESSION['user'] = $savedUser;
    header("location: /summerOfLearning/training.php");
}
else
{
    $systemMessage = urlencode("Incorrect Login or Password");
    header("location: /summerOfLearning/index.php?systemMessage=$systemMessage");
}
?>