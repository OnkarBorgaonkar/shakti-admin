<?php
    require_once("../../../globals.php");
    require_once("Authenticator.php");

    unset($_SESSION['user']);
    session_destroy();

    header("location: /summerOfLearning/");
?>