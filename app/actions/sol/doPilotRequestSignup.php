<?php
    require_once("../../../globals.php");

    $pilotRequest = new PilotRequest();
    $pilotRequest->setAll($_POST);

    $pilotRequestService = new PilotRequestService();
    $pilotRequestService->save($pilotRequest);

    $mailService = new MailService();
    $mailService->pilotRequest($pilotRequest);

    $message = "Request Sent";
    $json = Array(
        "success" => true,
        "systemMessage" => $message
    );
    echo json_encode($json);
    die();
?>