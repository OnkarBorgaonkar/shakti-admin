<?php
class AdminUser extends BaseDomainObject {

    public function hasAdminAccess() {
        return $this->role == "admin";
    }

    public function hasDistrictAccess() {
        return $this->hasAdminAccess() || $this->role == "district";
    }

    public function hasSchoolAccess() {
        return $this->hasDistrictAccess() || $this->role == "school";
    }

    public function hasClassroomAccess() {
        return $this->hasSchoolAccess() || $this->role == "classroom";
    }
}
?>