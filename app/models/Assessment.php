<?php
class Assessment extends BaseDomainObject
{
    public function getQuestions(){
        
        $assessmentQuestionCategoryService = new AssessmentQuestionCategoryService();
        $assessmentQuestionCategories = $assessmentQuestionCategoryService->find("assessment_id=".$this->id." ORDER BY \"order\"");

        $returnQuestions = Array();
        $assessmentQuestionService = new AssessmentQuestionService();

        foreach ($assessmentQuestionCategories as $assessmentQuestionCategory){
            $questions = $assessmentQuestionService->find("assessment_question_category_id = $assessmentQuestionCategory->id $category->id ORDER BY \"order\"");


            $returnQuestions[$assessmentQuestionCategory->name] = $questions;
        }

        return $returnQuestions;
    }

    public function getAssessmentQuestionCategories(){
        $assessmentQuestionCategoryService = new AssessmentQuestionCategoryService();
        return $assessmentQuestionCategoryService->find("assessment_id=".$this->id." ORDER BY \"order\"");
    }

}
?>
