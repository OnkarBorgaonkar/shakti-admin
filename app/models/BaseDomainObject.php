<?php
class BaseDomainObject
{
    public function __construct()
    {
    }

	public function isPersisted() {
		return is_numeric($this->id) && $this->id > 0;
	}

    public function setAll($map)
    {
        foreach ($map as $key => $value)
            if($key != 'x' && $key != 'y')
                $this->$key = $value;
    }

    public function __set($name, $value) {
        $fieldName = Inflector::variablize($name);
        if (get_magic_quotes_gpc())
            $value = stripslashes($value);
        $this->$fieldName = $value;
    }

    public function __get($name) {
        return $this->$name;
    }
}
?>
