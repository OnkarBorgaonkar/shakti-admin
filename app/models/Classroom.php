<?php
class Classroom extends SecuredDomainObject {

    public function getSchool() {
        $schoolService = new SchoolService();
        return array_pop($schoolService->find("id = $this->schoolId"));
    }

    public function getClassroomGroups() {
        $classroomGroupService = new ClassroomGroupService();
        return $classroomGroupService->find("classroom_id = $this->id");
    }

    public function getTotalPoints($startDate = null, $endDate = null) {
        $userPointsEntryService = new UserPointsEntryService();
        $sql = "
            SELECT
                COALESCE(SUM(user_points_entries.points), 0) as points
            FROM
                user_classrooms INNER JOIN user_points_entries ON user_classrooms.user_id = user_points_entries.user_id
            WHERE
                user_classrooms.classroom_id = $this->id
        ";

        if ($startDate != null)
            $sql .= " AND user_points_entries.created >= '$startDate' ";
        if ($endDate != null)
            $sql .= " AND user_points_entries.created <= '$endDate' ";

        $points = array_pop($userPointsEntryService->findBySql($sql));
        return $points->points;
    }

    public function getMembers() {
        $userService = new UserService();
        $sql = "
            SELECT
                users.*
            FROM
                users INNER JOIN user_classrooms ON users.id = user_classrooms.user_id
            WHERE
                user_classrooms.classroom_id = $this->id
            ORDER BY
                users.login
        ";
        $members = $userService->findBySql($sql);
        return $members;
    }

    public function getGradeTypes() {
        if (!$this->isPersisted())
            return array();
        $classroomGradeService = new ClassroomGradeService();
        $results = $classroomGradeService->findBySql("SELECT grade_type FROM classroom_grades WHERE classroom_id = $this->id");
        $gradeTypes = array();
        foreach ($results as $result)
            $gradeTypes[] = $result->gradeType;
        return $gradeTypes;
    }

    public function getComputerTypes() {
        if (!$this->isPersisted())
            return array();
        $classroomComputerTypeService = new ClassroomComputerTypeService();
        $results = $classroomComputerTypeService->find("classroom_id = $this->id");
        return $results;
    }

    public function getDays() {
        if (!$this->isPersisted())
            return array();
        $classroomDayService = new ClassroomDayService();
        $results = $classroomDayService->find("classroom_id = $this->id");
        $days = array();
        foreach ($results as $result)
            $days[] = $result->day;
        return $days;
    }

    public function getAdmin() {
        if (!$this->isPersisted())
            return null;

        $adminUserService = new AdminUserService();
        $sql = "
            SELECT
                admin_users.*
            FROM
                admin_users INNER JOIN admin_user_classrooms ON admin_users.id = admin_user_classrooms.admin_user_id
            WHERE
                admin_user_classrooms.classroom_id = $this->id
        ";
        $adminUser = array_pop($adminUserService->findBySql($sql));
        return $adminUser;
    }

    protected function getParent() {
        return $this->getSchool();
    }

    public function canEdit($adminUser) {
        if ($adminUser->role == "admin")
            return true;
        else if ($adminUser->role == "classroom" && $adminUser->classroomId == $this->id)
            return true;
        else
            return $this->getParent()->canEdit($adminUser);
    }

}
?>