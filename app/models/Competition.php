<?php
class Competition extends BaseDomainObject {
    
    public function getSchools() {
        $schoolService = new SchoolService();
        $schools = $schoolService->getSchoolsFromCompetition($this->id);        
        return $schools;
    }

    public function getTeams() {
        $groupService = new GroupService();
        $teams = $groupService->getTeamsInCompetition($this->id);
        return $teams;
    }
    
    public function getClassrooms() {
        $classroomService = new ClassroomService();
        $classrooms = $classroomService->getClassroomsInCompetition($this->id);
        return $classrooms;
    }
    
    public function getStudents($schoolId = null) {
        $userService = new UserService();
        $students = $userService->getUsersInCompetition($this->id, $schoolId);
        return $students;
    }
    
    public function clearStandingsData() {
        $competitionSchoolStandingService = new CompetitionSchoolStandingService();
        $competitionSchoolStandingService->deleteWhere("competition_id = $this->id");
        
        $competitionUserStandingService = new CompetitionUserStandingService();
        $competitionUserStandingService->deleteWhere("competition_id = $this->id");
        
        $competitionUserCategoryStandingService = new CompetitionUserCategoryStandingService();
        $competitionUserCategoryStandingService->deleteWhere("competition_id = $this->id");
    }
    
    public function getCompetitionSchoolStandings() {
        $schoolService = new SchoolService();
        $schools = $schoolService->getCompetitionSchoolStandingsByCompetitionId($this->id);
        return $schools;
    }
    
    public function getTopStandings($limit = 15) {
        $userService = new UserService();
        $users = $userService->getTopUsersInCompetition($this->id, null, $limit);
        return $users;
    }
    
    public function getTopCategoryStandings($categoryId) {
        $userService = new UserService();
        $users = $userService->getTopUsersInCompetitionForCategory($categoryId, $this->id);
        return $users;
    }
    
    public function getStatus() {
        $now = time();
        $start = strtotime($this->startDate);
        $end = strtotime($this->endDate);
        
        if ($now < $start)
            return "Pending";
        if ($now > $end)
            return "Completed";
        else
            return "Active";
    }
    
}
?>