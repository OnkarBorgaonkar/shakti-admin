<?php
class District extends SecuredDomainObject {

    public function getSchools($active = false) {
        $districtSession = $this->getCurrentlyActiveDistrictSession();
        if ($districtSession == null)
            return Array();

        $schoolService = new SchoolService();
        if ($active)
            return $schoolService->find("district_session_id = $districtSession->id AND active = true ORDER BY state, city, name");
        else
            return $schoolService->find("district_session_id = $districtSession->id ORDER BY state, city, name");
    }

    public function getAdmin() {
        if (!$this->isPersisted())
            return null;

        $adminUserService = new AdminUserService();
        $sql = "
            SELECT
                admin_users.*
            FROM
                admin_users INNER JOIN admin_user_districts ON admin_users.id = admin_user_districts.admin_user_id
            WHERE
                admin_user_districts.district_id = $this->id
        ";
        $adminUser = array_pop($adminUserService->findBySql($sql));
        return $adminUser;
    }

    protected function getParent() {
        return null;
    }

    public function canEdit($adminUser) {
        if ($adminUser->role == "admin")
            return true;
        else if ($adminUser->role == "district" && $adminUser->districtId == $this->id)
            return true;
        else
            return false;
    }

    public function getCurrentlyActiveDistrictSession() {

        $today = new DateTime();
        $today = $today->format("Y-m-d");

        $districtSessionService = new DistrictSessionService();
        $districtSession = array_pop($districtSessionService->find("district_id = $this->id AND start_date <= '$today' AND end_date >= '$today'"));
        return $districtSession;

    }

}
?>