<?php

/**
 * Base class for our domain model objects that have a file association.
 */
abstract class FileAwareDomainObject extends BaseDomainObject {

    public function getFileGroup() {
        return strtolower(get_class($this));
    }
    
    public function getOriginalFilePath() {
        return $this->getFilePath("original");
    }
    
    public function getFilePath($name = "original") {
        return "/data/" . $this->getFileGroup() . "/" . $this->id . "/$name." . $this->fileType;        
    }

    public function getOriginalStoragePath() {
        return Config::$fileStoragePath . "/" . $this->getFileGroup() . "/" . $this->id . "/original." .$this->fileType;        
    }

    public function setFileData($fileData){
        $this->contentType = $fileData['type'];
        $this->fileName = $fileData['name'];
        $this->fileType = $this->getFileExtension($fileData['name']);
    }

    protected function getFileExtension($filename) {
        $name = pathinfo($filename);
        return $name['extension'];
    }

}

?>
