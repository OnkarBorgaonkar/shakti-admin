<?php
class Group extends FileAwareDomainObject
{
    private $groupMembers;

    public function addUser($userId){
        $userGroup = new UserGroup();
        $userGroup->userId = $userId;
        $userGroup->groupId = $this->id;

        $userGroupService = new UserGroupService();
        $userGroupService->save($userGroup);
    }

    public function getMembers(){
        if($this->groupMembers == null){
            $userService = new UserService();
            $id = $this->id;
            $sql = "
                SELECT users.*
                FROM users INNER JOIN user_groups ON (users.id = user_groups.user_id)
                WHERE users.active = true AND user_groups.group_id = $id
                ORDER BY users.login
            ";
            $this->groupMembers = $userService->findBySql($sql);
        }
        return $this->groupMembers;
    }

    public function getLeader(){
        $userService = new UserService();
        $id = $this->leaderId;
        return array_pop($userService->find("id = $id"));
    }

    public function getTotalPoints($startDate = null, $endDate = null){
        if ($this->groupMembers == null){
            $this->getMembers();
        }

        $totalPoints = 0;

        foreach ($this->groupMembers as $user){
            $totalPoints += $user->getTotalPoints($startDate, $endDate);
        }

        return $totalPoints;
    }

    public function isMember($user){
        return $this->isMemberById($user->id);
    }

    public function removeMember($userId){
        $userGroupService = new UserGroupService();
        $userGroupService->deleteWhere("group_id = $this->id AND user_id = $userId");
    }

    public function isMemberById($userId){
        $userGroupService = new UserGroupService();
        $member = $userGroupService->find("user_id = $userId AND group_id = $this->id");
        return sizeof($member) != 0 ? true : false;
    }

    public function getGoal(){
        $groupGoalService = new GroupGoalService();
        $sql = "
            SELECT *
            FROM groups INNER JOIN group_goals ON groups.id = group_goals.group_id
            WHERE groups.id = $this->id
        ";

        return array_pop($groupGoalService->findBySql($sql));
    }

    public function getMessages(){
        $groupMessageService = new GroupMessageService();
        $sql = "
            SELECT *
            FROM group_messages INNER JOIN users ON group_messages.sender_id = users.id
            WHERE group_messages.group_id = $this->id
            ORDER BY group_messages.sent_on DESC
        ";
        return $groupMessageService->findBySql($sql);

    }

    public function getClassroomInformation() {
        $classRoomInformationService = new ClassroomInformationService();
        $classroomInformation = array_pop($classRoomInformationService->find("group_id = $this->id"));
        return $classroomInformation;
    }

    public function hasImage(){
        return isset($this->fileType);
    }

    public function getThumbFilePath() {
        return "/data/" . $this->getFileGroup() . "/" . $this->id . "/thumb." .$this->fileType;
    }

    public function getCompetitions() {
        $competitionService = new CompetitionService();
        $competitions = $competitionService->getCompetitionsByGroupId($this->id);
        return $competitions;
    }

    public function inCompetition() {
        return $this->getCompetitions() != null;
    }

    public function getSchool() {
        $schoolService = new SchoolService();
        $school = $schoolService->getSchoolByGroupId($this->id);
        return $school;
    }

    public function getType() {
        switch($this->typeName) {
            case "FRIENDS":
                return "Friends";
                break;
            case "SCHOOL":
                return "School";
                break;
            case "FAMILY":
                return "Family";
                break;
            case "CHURCH":
                return "Church";
                break;
            case "SPORTS_TEAM":
                return "Sports Team";
                break;
            case "CLASS_ROOM":
                return "Classroom Competition";
                break;
        }
    }

}
?>
