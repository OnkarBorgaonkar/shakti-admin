<?php
class GroupGoal extends BaseDomainObject
{
    public function getCurrentPoints($members){
        $points = 0;

        foreach($members as $member){
            $points += $member->getTotalPoints($this->startsOn, $this->endsOn);
        }

        return $points;
    }
}
?>
