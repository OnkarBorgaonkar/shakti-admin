<?php
class GroupInvitation extends BaseDomainObject{

    public function getSender(){
        $userService = new UserService();
        return array_pop($userService->find("id = $this->senderId"));
    }

    public function getGroup(){
        $groupService = new GroupService();
        return array_pop($groupService->find("id = $this->groupId"));
    }
}
?>
