<?php
class GroupRequest extends BaseDomainObject{

    public function getSender(){
        $userService = new UserService();
        return array_pop($userService->find("id = $this->senderId"));
    }

    public function getGroup(){
        $groupService = new GroupService();
        $group = array_pop($groupService->find("id = $this->groupId"));
        return $group;
    }
}
?>
