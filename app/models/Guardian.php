<?php
/**
* Guardian, aka "Parent." We use the name "Guardian" because "Parent" is disallowed for class
* names in PHP and we're automatically mapping to tables, services, etc.
*/
class Guardian extends BaseDomainObject
{
}
?>