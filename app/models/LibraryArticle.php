<?php
class LibraryArticle extends FileAwareDomainObject{

    public function getFilePath() {
        return "/data/" . $this->getFileGroup() . "/" . $this->id . "/article.pdf";
    }
    
    public function showLibraryQuestion($userId) {
        $libraryAnswerService = new LibraryAnswerService();        
        
        $sql = "
        	SELECT *
        	FROM library_answers INNER JOIN library_responses ON library_answers.library_response_id = library_responses.id
        	WHERE library_responses.user_id = $userId AND library_answers.library_article_id = $this->id
        ";
        
        $libraryAnswer = array_pop($libraryAnswerService->findBySql($sql));        
        if ($libraryAnswer != null && $libraryAnswer->completedOn == null)
            return true;
        
        return false;        
    }
    

}
?>
