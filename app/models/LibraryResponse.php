<?php
class LibraryResponse extends BaseDomainObject{

    public function hasReadAllArticles(){
        $libraryArticleService = new LibraryArticleService();
        $sql = "
            SELECT *
            FROM
                library_articles
            WHERE
                library_articles.library_id = $this->libraryId
                AND library_articles.id NOT IN (
                    SELECT library_answers.library_article_id
                    FROM library_answers
                    WHERE library_answers.library_response_id = $this->id
                    AND library_answers.created_on IS NOT NULL
                )
        ";

        $libraryArticles = $libraryArticleService->findBySql($sql);

        return sizeof($libraryArticles) == 0;
    }

}
?>
