<?php
class Orb Extends BaseDomainObject {

    public function getImageName() {
        return preg_replace("/\W/", "", ucwords(strtolower($this->name)));
    }

    public function getCheckpoints() {
        $orbCheckpointService = new OrbCheckpointService();
        $checkpoints = $orbCheckpointService->find("orb_id = $this->id");
        return $checkpoints;
    }

    public function getCheckPointsForUser($userId) {
        $orbCheckpointService = new OrbCheckpointService();
        $sql = "
            SELECT
                orb_checkpoints.*,
                EXISTS (
                    SELECT *
                    FROM user_orb_checkpoints
                    WHERE user_orb_checkpoints.orb_checkpoint_id = orb_checkpoints.id AND user_orb_checkpoints.user_id = $userId
                ) as completed
            FROM
                orb_checkpoints
            WHERE
                orb_checkpoints.orb_id = $this->id
            ORDER BY \"order\"
        ";
        $checkpoints = $orbCheckpointService->findBySql($sql);
        return $checkpoints;
    }
}
?>