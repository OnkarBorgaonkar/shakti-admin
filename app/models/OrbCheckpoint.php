<?php
class OrbCheckpoint extends BaseDomainObject {

    public function getOptions() {
        $orbCheckpointOptionService = new OrbCheckpointOptionService();
        $options = $orbCheckpointOptionService->find("orb_checkpoint_id = $this->id");
        return $options;
    }
}
?>