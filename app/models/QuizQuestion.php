<?php
class QuizQuestion extends BaseDomainObject
{
    public function getLibraryArticle(){
        $libraryArticleService = new LibraryArticleService();
        $libraryArticle = array_pop($libraryArticleService->find("id = $this->libraryArticleId"));
        return $libraryArticle;
    }
    
    public function getOptions() {
        $quizQuestionOptionService = new QuizQuestionOptionService();
        $quizQuestionOptions = $quizQuestionOptionService->find("quiz_question_id = $this->id ORDER BY \"order\"");
        return $quizQuestionOptions;
    }
}
?>
