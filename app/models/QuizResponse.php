<?php
class QuizResponse extends BaseDomainObject
{

    public function getCurrentPoints(){
        $quizResponseService = new QuizResponseService();
        $sql = "
            SELECT COALESCE(SUM(points), 0) AS points
            FROM
                quiz_questions INNER JOIN quiz_answers ON quiz_questions.id = quiz_answers.quiz_question_id
                INNER JOIN quiz_responses ON quiz_answers.quiz_response_id = quiz_responses.id
            WHERE
                quiz_responses.id = $this->id
        ";
        $points = array_pop($quizResponseService->findBySql($sql));
        return $points->points;
    }
}
?>
