<?php
class School extends SecuredDomainObject {

    private $cityState;

    public function getCityState() {
        $cityService = new CityService();
        $this->cityState = array_pop($cityService->find("id = $this->cityId"));
    }

    public function getDistrict() {
        $districtService = new DistrictService();
        $sql = "
            SELECT
                districts.*
            FROM
                districts INNER JOIN district_sessions ON districts.id = district_sessions.district_id
            WHERE
                district_sessions.id = $this->districtSessionId
        ";
        return array_pop($districtService->findBySql($sql));
    }

    public function getCity() {
        return $this->city;
    }

    public function getState() {
        return $this->state;
    }

    public function getClassrooms($active = false) {
        $classroomService = new ClassroomService();

		if ($active)
			return $classroomService->find("school_id = $this->id AND active = true ORDER BY name");
		else
			return $classroomService->find("school_id = $this->id ORDER BY name");
    }

    public function getTotalPoints($startDate = null, $endDate = null) {
        $GroupService = new GroupService();
    }

    public function getFullAddress() {
        return $this->address . ", " . $this->city . ", " . $this->state . " " . $this->zip;
    }

	public function getStudentsFromCompetition($competitionId) {
		$userService = new UserService();
		$users = $userService->getUsersInCompetition($competitionId, $this->id);
		return $users;
	}

    public function getTopStandingsFromCompetition($competitionId, $limit = 15) {
        $userService = new UserService();
        $users = $userService->getTopUsersInCompetition($competitionId, $this->id, $limit);
        return $users;
    }

    public function getTopCategoryStandingsForCompetition($categoryId, $competitionId) {
        $userService = new UserService();
        $users = $userService->getTopUsersInCompetitionForCategory($categoryId, $competitionId, $this->id);
        return $users;
    }

    public function getStudents() {
        $userService = new UserService();
        $users = $userService->getUsersInSchool($this->id);
        return $users;
    }

    public function getCompetition() {
        $competitionService = new CompetitionService();
        $sql = "
            SELECT
                competitions.*
            FROM
                competitions INNER JOIN competition_schools ON competitions.id = competition_schools.competition_id
            WHERE
                competition_schools.school_id = $this->id
        ";
        $competition = array_pop($competitionService->findBySql($sql));
        return $competition;
    }

    public function getWeeksNotInSession() {
        if (!$this->isPersisted())
            return array();
        $schoolNotInSessionDateService = new SchoolNotInSessionDateService();
        return $schoolNotInSessionDateService->find("school_id = $this->id ORDER BY start_date");
    }

    public function getAdmin() {
        if (!$this->isPersisted())
            return null;

        $adminUserService = new AdminUserService();
        $sql = "
            SELECT
                admin_users.*
            FROM
                admin_users INNER JOIN admin_user_schools ON admin_users.id = admin_user_schools.admin_user_id
            WHERE
                admin_user_schools.school_id = $this->id
        ";
        $adminUser = array_pop($adminUserService->findBySql($sql));
        return $adminUser;
    }

    public function isSetup() {
        $programs = $this->getPrograms();
        if (sizeof($programs) == 0)
            return false;

        foreach ($programs as $program) {
            if ($program->getStatusName() != "Completed") {
                $classrooms = $program->getClassrooms();
                if (sizeof($classrooms) > 0)
                    return true;
            }
        }

        return false;
    }

    protected function getParent() {
        return $this->getDistrict();
    }

    public function canEdit($adminUser) {
        if ($adminUser->role == "admin")
            return true;
        else if ($adminUser->role == "school" && $adminUser->schoolId == $this->id)
            return true;
        else
            return $this->getParent()->canEdit($adminUser);
    }
}
?>