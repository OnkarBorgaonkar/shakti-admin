<?php
abstract class SecuredDomainObject extends BaseDomainObject {
    protected abstract function getParent();
    public abstract function canEdit($adminUser);
}
?>