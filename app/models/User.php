<?php
class User extends FileAwareDomainObject
{
    private $rank;

    public function getHashedPassword()
    {
            return hash("sha256", $this->password . $this->salt);
    }

    public function initializePassword()
    {
        $this->salt = hash("sha256", uniqid(rand()));
        $this->password = $this->getHashedPassword();
    }

    public function getRankName()
    {
        $rank = $this->getRank();
        return $rank->name;
    }

    public function getRankImage() {
        return str_replace(" ", "", strtolower($this->getRankName())) . "Orb.png";
    }

    public function getAvatarImage() {
        if (file_exists($this->getOriginalStoragePath()))
            return $this->getFilePath("thumb");
        else
            return "/public/img/backgrounds/noWarriorDesign.png";
    }

    public function setRank($rank){ $this->rank = $rank; }

    public function recalculateRank() {
        $childHeroRankService = new ChildHeroRankService();

        $pointsRank = $childHeroRankService->getRankIdFromPoints($this->getTotalPoints());
        $quizRank = $childHeroRankService->getRankIdFromQuiz($this->id);
        $libraryRank = $childHeroRankService->getRankIdFromLibrary($this->id);

        $ranks = array($pointsRank, $quizRank, $libraryRank);

        $rankId = min($ranks);

        $rank = array_pop($childHeroRankService->find("id = $rankId"));
        $this->rank = $rank;
        $this->rankId = $rankId;

        $userService = new UserService();
        $userService->save($this);
    }

    public function getRank(){
        if($this->rank == null){

            $childHeroRankService = new ChildHeroRankService();
            $rank = array_pop($childHeroRankService->find("id = $this->rankId"));

            $this->rank = $rank;
        }

        return $this->rank;

    }

    public function getNextRank()
    {
        $childHeroRankService = new ChildHeroRankService();
        return $childHeroRankService->getNextRankForPoints($this->getTotalPoints());
    }

    public function getTotalPoints($startDate = null, $endDate = null)
    {
        $calculatePoints = false;
        if (isset($_SESSION['user']) && $_SESSION['user'] == $this)
            $calculatePoints = true;
        if ($startDate == null && $endDate == null && !$calculatePoints) {
            return $this->totalPoints;
        }
        else {
            $userPointsEntryService = new UserPointsEntryService();
            return $userPointsEntryService->getPointsForUser($this->id, null, $startDate, $endDate);
        }
    }

    public function getTaskPoints($startDate = null, $endDate = null, $categoryId = null) {
        $userPointsEntryService = new UserPointsEntryService();
        return $userPointsEntryService->getPointsForUser($this->id, "DAILY_TRAINING", $startDate, $endDate, $categoryId);
    }

    public function getQuizPoints($startDate = null, $endDate = null) {
        $userPointsEntryService = new UserPointsEntryService();
        return $userPointsEntryService->getPointsForUser($this->id, "ECHO_BOARD", $startDate, $endDate);
    }

    public function getLibraryPoints($startDate = null, $endDate = null) {
        $userPointsEntryService = new UserPointsEntryService();
        return $userPointsEntryService->getPointsForUser($this->id, "LIBRARY_ARTICLE", $startDate, $endDate);
    }

    public function getGroupJoinPoints($startDate = null, $endDate = null) {
        $userPointsEntryService = new UserPointsEntryService();
        return $userPointsEntryService->getPointsForUser($this->id, "RECRUITING_POINTS", $startDate, $endDate);
    }

    public function getOrbPoints($startDate = null, $endDate = null) {
        $userPointsEntryService = new UserPointsEntryService();
        $orbCheckpointPoints = $userPointsEntryService->getPointsForUser($this->id, "ORB_CHECKPOINT", $startDate, $endDate);
        $orbPoints = $userPointsEntryService->getPointsForUser($this->id, "ORB", $startDate, $endDate);
        return $orbCheckpointPoints + $orbPoints;
    }

    public function getChild()
    {
        $childService = new ChildService();
        $id = $this->id;
        return(array_pop($childService->find("user_id = $id")));
    }

    public function getChildHeroId()
    {
        $childHeroService = new ChildHeroService();
        return $childHeroService->getChildHeroIdByUserId($this->id);
    }

    public function getChildHero()
    {
        $childHeroService = new ChildHeroService();
        return $childHeroService->findByUserId($this->id);
    }

    public function guardianEmailIsInvalid()
    {

    }

    public function getTeams(){
        $groupService = new GroupService();
        $sql = "
            SELECT groups.*
            FROM groups INNER JOIN user_groups ON groups.id = user_groups.group_id
            WHERE user_groups.user_id = $this->id
            ORDER BY groups.name
        ";

        return $groupService->findBySql($sql);
    }

    public function getLeaderTeams(){
        $groupService = new GroupService();
        $sql = "
            SELECT groups.*
            FROM groups
            WHERE groups.leader_id = $this->id
            ORDER BY groups.name
        ";

        return $groupService->findBySql($sql);
    }

    public function needToTakeAssessment(){

        if ($this->needToTakeInitialAssessment())
            return array("assessmentType" => "INITIAL", "academyYear" => $this->getChild()->academyYear);

        if ($this->needToTakeFinalAssessment())
            return array("assessmentType" => "FINAL", "academyYear" => $this->getChild()->academyYear);

        if ($school == null && $this->needToTakeHeroAssessment())
            return array("assessmentType" => "HERO", "academyYear" => $this->getChild()->academyYear);

        return false;
    }

    private function needToTakeInitialAssessment() {
        $academyYear = $this->getChild()->academyYear;
        $assessmentResponseService = new AssessmentResponseService();
        $assessmentResponse = array_pop($assessmentResponseService->find("user_id = $this->id AND assessment_type = 'INITIAL' AND academy_year = $academyYear AND assessment_id = (SELECT id FROM assessments WHERE active = true)"));

        if ($assessmentResponse == null) {
            $academyYearChangeServce = new AcademyYearChangeService();
            $academyYearChange = array_pop($academyYearChangeServce->find("user_id = $this->id AND academy_year = $academyYear"));

            $change = strtotime($academyYearChange->created);
            $checkDate = strtotime("-1 days");

            if ($change <= $checkDate) {
                return true;
            }
        }

        return false;
    }

    private function needToTakeFinalAssessment() {
        $academyYear = $this->getChild()->academyYear;
        $assessmentResponseService = new AssessmentResponseService();
        $assessmentResponse = array_pop($assessmentResponseService->find("user_id = $this->id AND assessment_type = 'FINAL' AND academy_year = $academyYear"));

        if ($assessmentResponse == null) {

            $classrooms = $this->getClassrooms();
			
			$today = strtotime("now");

			foreach ($classrooms as $classroom) {
				$school = $classroom->getSchool();

				$lowerCheck = strtotime("$school->endDate -10 days");
				$uppercheck = strtotime("$school->endDate +10 days");

				if ($today <= $uppercheck && $today >= $lowerCheck) {
					return true;
				}
			}
        }
        return false;
    }

    private function needToTakeHeroAssessment() {

        if ($this->rankId >= 3) {
            $academyYear = $this->getChild()->academyYear;
            $assessmentResponseService = new AssessmentResponseService();
            $assessmentResponse = array_pop($assessmentResponseService->find("user_id = $this->id AND assessment_type = 'HERO'"));

            if ($assessmentResponse == null) {
                $lastAssessment = array_pop($assessmentResponseService->find("user_id = $this->id ORDER BY completed_at DESC LIMIT 1"));

                if ($lastAssessment == null) {
                    return true;
                }
                else {
                    $lastAssessmentDate = strtotime($lastAssessment->completedAt);
                    $checkDate = strtotime("-6 weeks");

                    if ($lastAssessmentDate <= $checkDate) {
                        return true;
                    }
                }
            }

        }
        return false;
    }

    public function getNeededAssessment(){
        $rank = $this->getRank();
        $assessmentService = new AssessmentService();
        return array_pop($assessmentService->find("active = TRUE"));
    }

    public function getLastAssessment() {
        $assessmentService = new AssessmentService();
        $sql = "
            SELECT *
            FROM
                assessments INNER JOIN assessment_responses ON assessments.id = assessment_responses.assessment_id
            WHERE
                assessment_responses.user_id = $this->id
            ORDER BY
                assessment_responses.completed_at

        ";

        $assessment = array_pop($assessmentService->findBySql($sql));

        return $assessment == null ? 0 : $assessment->rankId;
    }

    public function getNextRankInfo(){
        $currentRank = $this->rank;
        $currentRankId = $currentRank->id;

        $childHeroRankService = new ChildHeroRankService();
        $nextRankId = $currentRank->id + 1;


        $nextRank = array_pop($childHeroRankService->find("id = $nextRankId"));

        $pointsLeft = $nextRank->lowerBound - $this->getTotalPoints();

        $quizResponseService = new QuizResponseService();
        $sql = "
            SELECT *
            FROM quizzes INNER JOIN quiz_responses ON quizzes.id = quiz_responses.quiz_id
            WHERE quizzes.rank_id = $currentRankId AND quiz_responses.user_id = $this->id AND quiz_responses.completed_at IS NOT NULL
        ";
        $quiz = $quizResponseService->findBySql($sql);
        $needQuiz = sizeof($quiz) == 0;

        $libraryResponseService = new LibraryResponseService();
        $sql = "
            SELECT *
            FROM libraries INNER JOIN library_responses ON libraries.id = library_responses.library_id
            WHERE libraries.rank_id = $currentRankId AND library_responses.user_id = $this->id AND library_responses.completed_at IS NOT NULL

        ";

        $libraryResponse = $libraryResponseService->findBySql($sql);
        $needLibrary = sizeof($libraryResponse) == 0;

        $nextRankInfo = array(
            "nextRank" => $nextRank,
            "pointsNeeded" => $pointsLeft,
            "needQuiz" => $needQuiz,
            "needLibrary" => $needLibrary
        );

        return $nextRankInfo;
    }

public function getCurrentQuiz() {
        $quizService = new QuizService();
        $year = $this->getChild()->academyYear;
        $rank = $this->rankId;
        $level = $this->getCurrentLevel();

        $sql = "
            SELECT
                quizzes.*, quiz_responses.completed_at
            FROM
                quizzes LEFT JOIN quiz_responses ON quizzes.id = quiz_responses.quiz_id AND quiz_responses.user_id = $this->id
            WHERE
                academy_year <= $year
                AND rank_id <= $rank
                AND level <= $level
            ORDER BY
                academy_year, rank_id, level;
        ";
        $quizzes = $quizService->findBySql($sql);
        $currentQuiz = null;
        foreach ($quizzes as $quiz) {
            if ($quiz->completedAt == null) {
                $currentQuiz = $quiz;
                break;
            }
        }

        if ($currentQuiz == null)
            $currentQuiz = array_pop($quizzes);

        return $currentQuiz;
    }

    public function getCurrentQuizResponse(){
        $quiz = $this->getCurrentQuiz();

        $quizResponseService = new QuizResponseService();
        $quizResponse = array_pop($quizResponseService->find("quiz_id = $quiz->id AND user_id = $this->id"));

        if($quizResponse == null){
            $quizResponse = new QuizResponse();
            $quizResponse->quizId = $quiz->id;
            $quizResponse->userId = $this->id;

            $quizResponseService->save($quizResponse);
        }

        return $quizResponse;
    }

    public function getCurrentLibrary() {
        $libraryService = new LibraryService();
        $year = $this->getChild()->academyYear;
        $rank = $this->rankId;
        $level = $this->getCurrentLevel();

        $sql = "
            SELECT
                libraries.*, library_responses.completed_at
            FROM
                libraries LEFT JOIN library_responses ON libraries.id = library_responses.library_id AND library_responses.user_id = $this->id
            WHERE
                academy_year <= $year
                AND rank_id <= $rank
                AND level <= $level
            ORDER BY
                academy_year, rank_id, level;
        ";

        $libraries = $libraryService->findBySql($sql);
        $currentLibrary = null;
        foreach ($libraries as $library) {
            if ($library->completedAt == null) {
                $currentLibrary = $library;
                break;
            }
        }

        if ($currentLibrary == null)
            $currentLibrary = array_pop($libraries);

        return $currentLibrary;
    }

    public function getCurrentLibraryResponse() {
        $library = $this->getCurrentLibrary();

        $libraryResponseService = new LibraryResponseService();
        $libraryResponse = array_pop($libraryResponseService->find("library_id = $library->id AND user_id = $this->id"));

        if($libraryResponse == null){
            $libraryResponse = new LibraryResponse();
            $libraryResponse->libraryId = $library->id;
            $libraryResponse->userId = $this->id;

            $libraryResponseService->save($libraryResponse);
        }

        return $libraryResponse;
    }


    public function getCurrentLibraryData() {
        $libraryResponse = $this->getCurrentLibraryResponse();
        $currentLibrary = $this->getCurrentLibrary();

        $returnData = array();

        $subjectService = new SubjectService();
        $subjects = $subjectService->find("true");

        $libraryArticleService = new LibraryArticleService();
        $libraryAnswerService = new LibraryAnswerService();
        foreach ($subjects as $subject){
            $articles = $libraryArticleService->getArticlesBySubject($subject->id, $currentLibrary->id);

            $points = 0;
            if ($libraryResponse != null){
                $points = $libraryAnswerService->getPointsForSubject($subject->id, $libraryResponse->id);
            }

            $returnData[] = array(
                "subject" => $subject,
                "articles" => $articles,
                "subjectPoints" => $points
            );
        }

        return $returnData;

    }

    public function getCurrentLibraryAnswers(){
        $libraryResponse = $this->getCurrentLibraryResponse();
        $libraryAnswerService = new LibraryAnswerService();
        $libraryAnswers = $libraryAnswerService->find("library_response_id = $libraryResponse->id");
        return $libraryAnswers;
    }

    public function getGroupRequestsForGroups(){
        $groupRequestService = new GroupRequestService();
        $sql = "
            SELECT group_requests.*, groups.name as group_name
            FROM group_requests INNER JOIN groups ON group_requests.group_id = groups.id
            WHERE groups.leader_id = $this->id
        ";
        $groupRequests = $groupRequestService->findBySql($sql);
        return $groupRequests;
    }

    public function getGaugeData() {
        $date = date("Y-m-d", strtotime("-2 weeks"));
        $childHeroId = $this->getChildHeroId();
        $childHeroTaskService = new ChildHeroTaskService();
        $sql = "
            SELECT (
                SELECT COUNT(*)
                FROM
                    child_hero_tasks INNER JOIN tasks ON child_hero_tasks.task_id = tasks.id
                WHERE
                    tasks.category = 'Hero''s Body'
                    AND child_hero_tasks.child_hero_id = $childHeroId
                    AND child_hero_tasks.completed_at >= '$date'
            )::real / (
                (
                    SELECT COUNT (*)
                    FROM tasks
                    WHERE tasks.category = 'Hero''s Body'
                )*14
            ) AS \"body\",
            (
                SELECT COUNT(*)
                FROM
                    child_hero_tasks INNER JOIN tasks ON child_hero_tasks.task_id = tasks.id
                WHERE
                    tasks.category = 'Hero''s Mind'
                    AND child_hero_tasks.child_hero_id = $childHeroId
                    AND child_hero_tasks.completed_at >= '$date'
            )::real / (
                (
                    SELECT COUNT (*)
                    FROM tasks
                    WHERE tasks.category = 'Hero''s Mind'
                )*14
            ) AS \"mind\",
            (
                SELECT COUNT(*)
                FROM
                    child_hero_tasks INNER JOIN tasks ON child_hero_tasks.task_id = tasks.id
                WHERE
                    tasks.category = 'Hero''s Heart'
                    AND child_hero_tasks.child_hero_id = $childHeroId
                    AND child_hero_tasks.completed_at >= '$date'
            )::real / (
                (
                    SELECT COUNT (*)
                    FROM tasks
                    WHERE tasks.category = 'Hero''s Heart'
                )*14
            ) AS \"heart\",
            (
                SELECT COUNT(*)
                FROM
                    child_hero_tasks INNER JOIN tasks ON child_hero_tasks.task_id = tasks.id
                WHERE
                    tasks.category = 'Hero''s Community'
                    AND child_hero_tasks.child_hero_id = $childHeroId
                    AND child_hero_tasks.completed_at >= '$date'
            )::real / (
                (
                    SELECT COUNT (*)
                    FROM tasks
                    WHERE tasks.category = 'Hero''s Community'
                )*14
            ) AS \"community\"
        ";
        

        $data = array_pop($childHeroTaskService->findBySql($sql));

        return $data;
    }

    public function getCurrentLevel() {
        $quizLevel = $this->getQuizLevel();
        $libraryLevel = $this->getLibraryLevel();

        return $quizLevel < $libraryLevel ? $quizLevel : $libraryLevel;
    }

    public function getQuizLevel() {
        $quizService = new QuizService();
        $year = $this->getChild()->academyYear;
        $sql = "
            SELECT
                quizzes.*
            FROM
                quizzes INNER JOIN quiz_responses ON quizzes.id = quiz_responses.quiz_id
            WHERE
                quizzes.academy_year = $year
                AND quiz_responses.user_id = $this->id
            ORDER BY level DESC
            LIMIT 1
        ";
        $quiz = array_pop($quizService->findBySql($sql));
        return $quiz != null ? $quiz->level : 1;
    }

    public function getLibraryLevel() {
        $libraryService = new LibraryService();
        $year = $this->getChild()->academyYear;
        $sql = "
            SELECT
                libraries.*
            FROM
                libraries INNER JOIN library_responses ON libraries.id = library_responses.library_id
            WHERE
                libraries.academy_year = $year
                AND library_responses.user_id = $this->id
            ORDER BY level DESC
            LIMIT 1
        ";
        $library = array_pop($libraryService->findBySql($sql));
        return $library != null ? $library->level : 1;
    }

    public function getHeroesInCorner() {
        $userService = new UserService();
        $sql = "
            SELECT
                users.*
            FROM
                users INNER JOIN hero_corner_users ON users.id = hero_corner_users.hero_id
            WHERE
                hero_corner_users.user_id = $this->id
        ";
        $users = $userService->findBySql($sql);
        return $users;
    }

    public function getReadArticles() {
        $academyYear = $this->getChild()->academyYear;
        $libraryArticleService = new LibraryArticleService();
        $sql = "
            SELECT
                library_articles.*
            FROM
                libraries INNER JOIN library_articles ON libraries.id = library_articles.library_id
                INNER JOIN library_answers ON library_articles.id = library_answers.library_article_id
                INNER JOIN library_responses ON library_answers.library_response_id = library_responses.id
            WHERE
                libraries.academy_year = $academyYear
                AND library_responses.user_id = $this->id
        ";
        $libraryArticles = $libraryArticleService->findBySql($sql);
        return $libraryArticles;
    }

    public function getAvatarStyle() {
        $heroString = $this->getChildHero()->heroMachineString;
        if (strstr($heroString, "*m1*") !== false || strstr($heroString, "*f1*") !== false)
            return "Short";
        else
            return "Tall";

    }

    public function getOrbs() {
        $orbService = new OrbService();
        $orbs = $orbService->getOrbsByUserId($this->id);
        return $orbs;
    }
    
    public function getOrbsByProgramId($programId) {
        $orbService = new OrbService();
        $orbs = $orbService->getOrbsByProgramId($this->id, $programId);
        return $orbs;
    }

    public function getSchool() {
        $schoolService = new SchoolService();
        $sql = "
            SELECT
                schools.*
            FROM
                schools INNER JOIN classrooms ON schools.id = classrooms.school_id
                INNER JOIN user_classrooms ON classrooms.id = user_classrooms.classroom_id
            WHERE
                user_classrooms.user_id = $this->id
			ORDER BY
				schools.end_date DESC,
				schools.name

        ";
        $school = array_pop($schoolService->findBySql($sql));
        return $school;
    }

    public function getClassrooms() {
        $classroomService = new ClassroomService();
        $sql = "
            SELECT
                classrooms.*
            FROM
                classrooms INNER JOIN user_classrooms ON classrooms.id = user_classrooms.classroom_id
            WHERE
                user_classrooms.user_id = $this->id
        ";

		return $classroomService->findBySql($sql);
    }
}
?>
