<?php
    require_once("../../globals.php");

    $userPointsEntryService = new UserPointsEntryService();

    $userService = new UserService();
    $users = $userService->find("TRUE");

    foreach ($users as $user) {
        set_time_limit(300);

        //daily training points
        $childHeroTaskService = new ChildHeroTaskService();
        $sql = "
            SELECT
                  child_hero_tasks.*, tasks.points, categories.id AS category_id
              FROM
                  categories INNER JOIN tasks ON categories.name = tasks.category
                  INNER JOIN child_hero_tasks ON tasks.id = child_hero_tasks.task_id
                  INNER JOIN child_heroes ON child_hero_tasks.child_hero_id = child_heroes.id
                  INNER JOIN children ON child_heroes.child_id = children.id
                  INNER JOIN users ON children.user_id = users.id
            WHERE
                users.id = $user->id
                and child_hero_tasks.approved = 'true'
        ";
        $childHeroTasks = $childHeroTaskService->findBySql($sql);

        foreach ($childHeroTasks as $childHeroTask) {
            //echo "$childHeroTask->categoryId - $childHeroTask->points<br/>";
            $userPointsEntry = new UserPointsEntry();
            $userPointsEntry->pointType = "DAILY_TRAINING";
            $userPointsEntry->points = $childHeroTask->points;
            $userPointsEntry->activityId = $childHeroTask->id;
            $userPointsEntry->categoryId = $childHeroTask->categoryId;
            $userPointsEntry->created = $childHeroTask->completedAt;

            $userPointsEntryService->saveEntry($userPointsEntry, $user);
        }

        //echo board points
        $quizAnswerService = new QuizAnswerService();
        $sql = "
            SELECT
                quiz_answers.*, quiz_questions.points
            FROM
                quiz_responses INNER JOIN quiz_answers ON quiz_responses.id = quiz_answers.quiz_response_id
                INNER JOIN quiz_questions ON quiz_answers.quiz_question_id = quiz_questions.id
            WHERE
                quiz_responses.user_id = $user->id
        ";
        $quizAnswers = $quizAnswerService->findBySql($sql);

        foreach ($quizAnswers as $quizAnswer) {
            //echo "$quizAnswer->id - $quizAnswer->points<br/>";
            $userPointsEntry = new UserPointsEntry();
            $userPointsEntry->pointType = "ECHO_BOARD";
            $userPointsEntry->points = $quizAnswer->points;
            $userPointsEntry->activityId = $quizAnswer->id;
            $userPointsEntry->created = $quizAnswer->createdOn;

            $userPointsEntryService->saveEntry($userPointsEntry, $user);
        }

        //library points
        $libraryAnswerService = new LibraryAnswerService();
        $sql = "
            SELECT
                library_answers.*, library_articles.points
            FROM
                library_responses INNER JOIN library_answers ON library_responses.id = library_answers.library_response_id
                INNER JOIN library_articles ON library_answers.library_article_id = library_articles.id
            WHERE
                library_responses.user_id = $user->id
        ";
        $libraryAnswers = $libraryAnswerService->findBySql($sql);

        foreach ($libraryAnswers as $libraryAnswer) {
            //echo "$libraryAnswer->id - $libraryAnswer->points<br/>";
            $userPointsEntry = new UserPointsEntry();
            $userPointsEntry->pointType = "LIBRARY_ARTICLE";
            $userPointsEntry->points = $libraryAnswer->points;
            $userPointsEntry->activityId = $libraryAnswer->id;
            $userPointsEntry->created = $libraryAnswer->createdOn;

            $userPointsEntryService->saveEntry($userPointsEntry, $user);
        }

        //recruiting points
        $groupJoinPointsService = new GroupJoinPointsService();
        $groupJoinPointsEntries = $groupJoinPointsService->find("user_id = $user->id");

        foreach ($groupJoinPointsEntries as $groupJoinPointsEntry) {
            //echo "$groupJoinPointsEntry->id - $groupJoinPointsEntry->points<br/>";
            $userPointsEntry = new UserPointsEntry();
            $userPointsEntry->pointType = "RECRUITING_POINTS";
            $userPointsEntry->points = $groupJoinPointsEntry->points;
            $userPointsEntry->activityId = $groupJoinPointsEntry->id;
            $userPointsEntry->created = $groupJoinPointsEntry->createdOn;

            $userPointsEntryService->saveEntry($userPointsEntry, $user);
        }

    }

?>