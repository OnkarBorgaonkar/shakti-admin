<?php
    ini_set("memory_limit","128M"); 
    require_once("../../globals.php");
    
    $userService = new UserService();    
    $users = $userService->find("TRUE");
    
    foreach ($users as $user) {
        set_time_limit(300);
        $user->recalculateRank();
    }
?>
