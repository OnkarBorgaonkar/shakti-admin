<?php
class AdminUserService extends BaseService {

    public function getAdminUserByEmail($email) {
        $sql = "
            SELECT
                admin_users.*,
                (
                    SELECT
                        district_id
                    FROM
                        admin_user_districts
                    WHERE
                        admin_user_districts.admin_user_id = admin_users.id
                ) as district_id,
                (
                    SELECT
                        school_id
                    FROM
                        admin_user_schools
                    WHERE
                        admin_user_schools.admin_user_id = admin_users.id
                ) as school_id,
                (
                    SELECT
                        classroom_id
                    FROM
                        admin_user_classrooms
                    WHERE
                        admin_user_classrooms.admin_user_id = admin_users.id
                ) as classroom_id,
                (
                    SELECT
                        role
                    FROM
                        admin_user_roles
                    WHERE
                        admin_user_roles.admin_user_id = admin_users.id
                    LIMIT 1
                ) as role
            FROM
                admin_users
            WHERE
                admin_users.email = '$email'
        ";

        return array_pop($this->findBySql($sql));
    }

    public function getAdminUserForLogin($email) {
        $sql = "
        SELECT
            admin_users.*,
            (
                SELECT
                    district_id
                FROM
                    admin_user_districts
                WHERE
                    admin_user_districts.admin_user_id = admin_users.id
            ) as district_id,
            (
                SELECT
                    school_id
                FROM
                    admin_user_schools
                WHERE
                    admin_user_schools.admin_user_id = admin_users.id
            ) as school_id,
            (
                SELECT
                    classroom_id
                FROM
                    admin_user_classrooms
                WHERE
                    admin_user_classrooms.admin_user_id = admin_users.id
            ) as classroom_id,
            (
                SELECT
                    role
                FROM
                    admin_user_roles
                WHERE
                    admin_user_roles.admin_user_id = admin_users.id
                LIMIT 1
            ) as role
        FROM
            admin_users
        WHERE
            admin_users.email = '$email'
        ";

        return array_pop($this->findBySql($sql));
    }

}
?>