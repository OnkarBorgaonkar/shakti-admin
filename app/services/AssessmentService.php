<?php
class AssessmentService extends BaseService
{
    public function getAverageData($userId, $categoryId, $rankId = null){

        $sql = "
            SELECT
                COALESCE(AVG(assessment_answers.answer), 0) AS average
            FROM
                assessments INNER JOIN assessment_responses ON assessments.id = assessment_responses.assessment_id
                INNER JOIN assessment_answers ON assessment_responses.id = assessment_answers.assessment_response_id
                INNER JOIN assessment_questions ON assessment_answers.assessment_question_id = assessment_questions.id
            WHERE
                assessment_responses.user_id = $userId
                AND assessment_questions.category_id = $categoryId
        ";
        if ($rankId != null){
            $sql .= " AND assessments.rank_id = $rankId";
        }
        else{
            $sql .= "
             AND assessments.rank_id = (
                SELECT MAX(rank_id)
                FROM assessments INNER JOIN assessment_responses ON assessments.id = assessment_responses.assessment_id
                WHERE assessment_responses.user_id = $userId
            )";
        }

        $average = array_pop($this->findBySql($sql));

        return $average->average;
    }
}
?>
