<?php
require_once("Inflector.php");

class BaseService
{

    private static $dbh = null;

    public function find($whereClause = null)
    {
        $dbh = self::getDbh();

        if (!empty($whereClause))
            $whereClause = " WHERE " . $whereClause;

        $sql = "SELECT * FROM " . $this->getTableName() . $whereClause;
        //Logger::log(__METHOD__ . " " . $sql);
        $stmt = $dbh->prepare($sql);
        $stmt->execute();

        $results = array();
        while ($entry = $stmt->fetchObject($this->getClassName())) {
            $results[] = $entry;
        }
        $stmt->closeCursor();
        return $results;
    }

    public function findBySql($sql)
    {
        $dbh = self::getDbh();

        //Logger::log(__METHOD__ . " " . $sql);
        $stmt = $dbh->prepare($sql);
        $stmt->execute();

        $results = array();
        while ($entry = $stmt->fetchObject($this->getClassName())) {
            $results[] = $entry;
        }
        $stmt->closeCursor();
        return $results;
    }

    protected function getTableName()
    {
        return Inflector::tableize($this->getClassName());
    }

    private function getClassName()
    {
        return substr(get_class($this), 0, -(strlen("Service")));
    }

    public function save(&$domainObject) {
        if (isset($domainObject->id))
            $this->update($domainObject);
        else
            $this->insert($domainObject);
    }

    function insert(&$domainObject)
    {
        $dbh = $this->getDbh();
        $inTransaction = $dbh->inTransaction();
        try
        {
            if (!$inTransaction) {
                $dbh->beginTransaction();
            }
            $sql = $this->getInsertSql($domainObject);
            Logger::log(__METHOD__ . " " . $sql);
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            if (!$inTransaction) {
                $dbh->commit();
            }
            $sequenceName = $this->getTableName() . "_id_seq";
            $domainObject->id = $dbh->lastInsertId($sequenceName);
        }
        catch (Exception $e)
        {
            /**
            * TODO: this is checking to see if the error code is the code for a PDOException
            * which is thrown if the above call to:
            * $sequenceName = $this->getTableName() . "_id_seq";
            * $domainObject->id = $dbh->lastInsertId($sequenceName);
            *
            * ...fails because we're attempting to save a domainObject with no primary key
            * (e.g. a ChildHumanPower). This needs to be made more flexible.
            */
            if ("42P01" != $e->errorInfo[0])
            {
                $dbh->rollBack();
                throw $e;
            }
        }
    }

    function update($domainObject)
    {
        $dbh = $this->getDbh();
        $inTransaction = $dbh->inTransaction();
        try
        {
            if (!$inTransaction) {
                $dbh->beginTransaction();
            }
            $sql = $this->getUpdateSql($domainObject);
            //Logger::log(__METHOD__ . " " . $sql);
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
        }
        catch (Exception $e)
        {
            $dbh->rollBack();
            throw $e;
        }
        if (!$inTransaction) {
            $dbh->commit();
        }
    }

    function delete($domainObject)
    {
        $this->deleteWhere("id = $domainObject->id");
    }

    function deleteWhere($whereClause)
    {
        $dbh = $this->getDbh();
        $inTransaction = $dbh->inTransaction();
        try
        {
            if (!$inTransaction) {
                $dbh->beginTransaction();
            }
            $sql = "DELETE from " . $this->getTableName() . "
                    WHERE " . $whereClause;
            //Logger::log(__METHOD__ . " " . $sql);
            $stmt = $dbh->prepare($sql);
            $stmt->execute();
            if (!$inTransaction) {
                $dbh->commit();
            }
        }
        catch (Exception $e)
        {
            $dbh->rollBack();
            throw $e;
        }
    }

    private function getInsertSql($domainObject)
    {
        $fieldNames = array_keys(get_object_vars($domainObject));
        foreach ($fieldNames as $name)
            $underscoredFieldNames[] = Inflector::underscore($name);

        $buffer = "(" . join(', ', $underscoredFieldNames) . ") SELECT ";

        $escapedValues = array_map(array($this, "escapeString"), array_values(get_object_vars($domainObject)));
        $buffer .= join(', ', $escapedValues);

        return "INSERT INTO " . $this->getTableName() . $buffer;
    }

    private function getUpdateSql($domainObject)
    {
        foreach (get_object_vars($domainObject) as $key => $value)
        {
            $key = Inflector::underscore($key);
            $result[] = "$key=" . $this->escapeString($value);
        }
        return "UPDATE " . $this->getTableName() . " SET " . join(', ', $result) . " WHERE id=" . $domainObject->id;
    }

    protected function escapeString($value)
    {
        $dbh = $this->getDbh();
        if (is_bool($value)) {
            if ($value)
                return $dbh->quote("true");
            else
                return $dbh->quote("false");
        }
        if (is_numeric($value) || !empty($value))
            return $dbh->quote($value);
        else
            return 'null';
    }

    protected static function getDbh() {
        if (self::$dbh == null) {
            $dbHost = Config::$databaseHost;
            $dbPort = Config::$databasePort;
            $dbName = Config::$databaseName;
            $dbType = Config::$databaseType;
            try
            {

                $dbh = new PDO("pgsql:host=$dbHost;port=$dbPort;dbname=$dbName", Config::$databaseUser, Config::$databasePass);
            }
            catch(PDOException $e)
            {
                print $e->getMessage();
            }
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$dbh = $dbh;
        }

        return self::$dbh;
    }
}
?>
