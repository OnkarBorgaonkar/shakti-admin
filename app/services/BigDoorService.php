<?php
class BigDoorService extends BaseService{

    public function getBigDoorData($userId) {

        $appKey = Config::$bigDoorAppKey;
        $url = "http://loyalty.bigdoor.com/api/publisher/{$appKey}/end_user/{$userId}";
        var_dump($url);
        $response = file_get_contents($url);
        var_dump($response);
    }

    public function importBigdoorData() {

        $this->getDbh()->beginTransaction();

        $orbService = new OrbService();
        $orbCheckpointService = new OrbCheckpointService();
        $orbCheckpointOptionService = new OrbCheckpointOptionService();
        $userStoryResponseService = new UserStoryResponseService();

        $appKey = Config::$bigDoorAppKey;

        $url = "http://loyalty.bigdoor.com/api/publisher/$appKey/named_level_collection?attribute_friendly_id=bdm-quest&max_records=50&order_by=-created";
        $quests = json_decode(file_get_contents($url))[0];

        foreach ($quests as $quest) {
            $checkpoints = $quest->named_levels;
            foreach ($checkpoints as $checkpoint) {
                $url = "http://loyalty.bigdoor.com/api/publisher/$appKey/named_level/$checkpoint->id";
                $checkpointData = json_decode(file_get_contents($url))[0];
                $attributes = $checkpointData->attributes;
                if (sizeof($attributes) > 1) {

                    $orb = array_pop($orbService->find("big_door_quest_id = $checkpointData->named_level_collection_id"));
                    $orbCheckpoint = array_pop($orbCheckpointService->find("big_door_checkpoint_id = $checkpointData->id"));

                    if ($orbCheckpoint == null)
                        $orbCheckpoint = new OrbCheckpoint();

                    $orbCheckpoint->bigDoorCheckpointId = $checkpointData->id;
                    $orbCheckpoint->name = $checkpointData->pub_title;
                    $orbCheckpoint->orbId = $orb->id;
                    $orbCheckpoint->active = true;
                    $orbCheckpoint->order = $checkpointData->threshold;
                    $orbCheckpoint->points = 150;

                    if(!isset($orbCheckpoint->id)) {
                        $orbCheckpoint->description = $checkpointData->end_user_description;
                    }

                    $checkpointType = $attributes[1]->friendly_id;
                    if ($checkpointType == "bdm-checkpoint-quiz") {
                        $orbCheckpoint->type = "MULTIPLE_CHOICE";
                        $orbCheckpointService->save($orbCheckpoint);
                        $options = json_decode($checkpointData->pub_description);
                        $count = 0;
                        foreach ($options->choices as $choice) {
                            $orbCheckpointOption = new OrbCheckpointOption();
                            $orbCheckpointOption->orbCheckpointId = $orbCheckpoint->id;
                            $orbCheckpointOption->name = $choice;
                            $orbCheckpointOption->correct = $count == $options->correct_choice;
                            $orbCheckpointOption->order = $count;
                            $orbCheckpointOptionService->save($orbCheckpointOption);
                            $count++;
                        }
                    }
                    else if ($checkpointType == "bdm-checkpoint-page") {
                        $orbCheckpoint->type = "LINK";
                        $orbCheckpoint->url = $checkpointData->urls[0]->url;
                        $orbCheckpoint->seconds = 30;
                        $orbCheckpointService->save($orbCheckpoint);
                    }
                    else if ($checkpointType == "bdm-checkpoint-fill-in-the-blank") {
                        $orbCheckpoint->type = "FILL_IN_THE_BLANK";
                        $answerData = json_decode($checkpointData->pub_description);
                        $orbCheckpoint->answer = $answerData->correct_submission;
                        $orbCheckpointService->save($orbCheckpoint);
                    }
                    else if ($checkpointType == "bdm-checkpoint-event") {
                        $orbCheckpoint->type = "STORY";
                        $orbCheckpointService->save($orbCheckpoint);
                    }
                }
            }
        }

//         $userStoryResponses = $userStoryResponseService->find("true");
//         foreach ($userStoryResponses as $userStoryResponse) {
//             $orbCheckpoint = array_pop($orbCheckpointService->find("big_door_checkpoint_id = $userStoryResponse->bigDoorCheckpointId"));
//             var_dump($orbCheckpoint);
//             $userStoryResponse->orbCheckpointId = $orbCheckpoint->id;
//             var_dump($userStoryResponse);
//             $userStoryResponseService->save($userStoryResponse);
//         }

        $this->getDbh()->commit();
    }
}
?>