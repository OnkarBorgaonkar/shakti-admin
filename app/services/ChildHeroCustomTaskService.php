<?php

class ChildHeroCustomTaskService extends BaseService
{
    public function getRemainingCustomTasks($childHeroId)
    {        
        $sql = "
        	SELECT * 
        	FROM tasks
        	WHERE 
        		tasks.category = 'Custom'
        		AND tasks.id NOT IN (
        			SELECT child_hero_custom_tasks.task_id
        			FROM child_hero_custom_tasks
        			WHERE child_hero_id = $childHeroId
    			)
        ";
        
        $childHeroCustomTasks = $this->findBySql($sql);
        
        return $childHeroCustomTasks;
    }
}

?>