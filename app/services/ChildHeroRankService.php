<?php
class ChildHeroRankService extends BaseService
{
    public function getRankNameForPoints($points)
    {
        $whereClause = "lower_bound <= $points ORDER BY lower_bound DESC LIMIT 1";

        $ranks = $this->find($whereClause);
        $rank = array_pop($ranks);
        return $rank->name;
    }

    public function getRankFromPoints($points)
    {
        $whereClause = "lower_bound <= $points ORDER BY lower_bound DESC LIMIT 1";

        $ranks = $this->find($whereClause);
        $rank = array_pop($ranks);
        return $rank;
    }

    public function getRankIdFromPoints($points)
    {
        $whereClause = "lower_bound <= $points ORDER BY lower_bound DESC LIMIT 1";

        $ranks = $this->find($whereClause);
        $rank = array_pop($ranks);
        return $rank->id;
    }

    public function getNextRankForPoints($points)
    {
        $whereClause = "lower_bound > $points ORDER BY lower_bound LIMIT 1";

        $ranks = $this->find($whereClause);
        $rank = array_pop($ranks);
        return $rank;
    }

    public function getRankIdFromQuiz($userId){
                $sql = "
            SELECT
                child_hero_ranks.id,
                child_hero_ranks.name,
                child_hero_ranks.lower_bound
            FROM
                child_hero_ranks INNER JOIN quizzes ON child_hero_ranks.id = quizzes.rank_id
                INNER JOIN quiz_responses ON quizzes.id = quiz_responses.quiz_id
            WHERE
                quiz_responses.user_id = $userId
                AND quiz_responses.completed_at IS NOT NULL
            ORDER BY child_hero_ranks.lower_bound DESC
            LIMIT 1
        ";

        $rank = array_pop($this->findBySql($sql));
        return $rank == null ? 1 : $rank->id + 1;
    }

    public function getRankIdFromLibrary($userId) {
        $sql = "
            SELECT
                child_hero_ranks.id,
                child_hero_ranks.name,
                child_hero_ranks.lower_bound
            FROM
                child_hero_ranks INNER JOIN libraries ON child_hero_ranks.id = libraries.rank_id
                INNER JOIN library_responses ON libraries.id = library_responses.library_id
            WHERE
                library_responses.user_id = $userId
                AND library_responses.completed_at IS NOT NULL
            ORDER BY child_hero_ranks.lower_bound DESC
            LIMIT 1
        ";

        $rank = array_pop($this->findBySql($sql));
        return $rank == null ? 1 : $rank->id + 1;
    }

}
?>
