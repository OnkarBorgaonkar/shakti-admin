<?php
class ChildHeroService extends BaseService
{
	public function findByUserId($userId)
	{
		$sql = "SELECT child_heroes.*
				FROM children, child_heroes
				WHERE children.user_id = $userId
				AND children.id = child_heroes.child_id";
				
		$childHeroes = $this->findBySql($sql);
		return array_pop($childHeroes);
	}
	
	public function getChildHeroIdByUserId($userId)
	{
		$childHero = $this->findByUserId($userId);
		return $childHero->id;
	}

	public function updateRequiresApproval($updated) {
		if (Config::$approvalRequired) {
			if ($updated->isPersisted()) {
				$original = array_pop($this->find("id = {$updated->id}"));

				return ($updated->superHeroOutfit != '' && $updated->superHeroOutfit != $original->superHeroOutfit) ||
					   ($updated->backstory       != '' && $updated->backstory       != $original->backstory)       ||
					   ($updated->planToDoGood    != '' && $updated->planToDoGood    != $original->planToDoGood)
				;
			}
			else {
				return $updated->superHeroOutfit != '' || $updated->backstory != '' || $updated->planToDoGood;
			}
		}
		else {
			return false;
		}
	}
}
?>