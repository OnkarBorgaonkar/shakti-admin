<?php
class ChildHeroTaskService extends BaseService
{
    /**
    * Get all tasks a child hero has completed regardless of time.
    */
    public function getPointsForChildHeroId($childHeroId, $startDate = null, $endDate = null, $category = null)
    {
        $tableName = $this->getTableName();
        // TODO: Possible: we could find the points for the week directly in SQL here
        $sql = "SELECT COALESCE(SUM(tasks.points), 0) as points
                 FROM tasks INNER JOIN $tableName ON ($tableName.task_id = tasks.id)
                 WHERE
                    $tableName.child_hero_id = $childHeroId
                    AND approved = 'true'
        ";
        
        if ($category != null) {
            $category = pg_escape_string($category);
            $sql .= " AND tasks.category = '$category'";
        }
        
        if($startDate != null){
            $sql .= " AND $tableName.completed_at >= '$startDate'";
        }

        if($endDate != null){
            $sql .= " AND $tableName.completed_at <= '$endDate'";
        }

        $points = array_pop($this->findBySql($sql));
        return $points->points;


    }

    public function findForChildHeroAndWeek($childHeroId, $weekStartDate)
    {
        $oneWeekInSeconds = 6 * 24 * 60 * 60;
        $weekEndDate = date("Y-m-d", (strtotime($weekStartDate) + $oneWeekInSeconds));

        $tableName = $this->getTableName();
        // TODO: Possible: we could find the points for the week directly in SQL here
        $sql = "SELECT task_id, tasks.points as points, tasks.category as category, $tableName.approved as approved, $tableName.completed_at - '$weekStartDate' as dayoffset
                 FROM tasks, $tableName
                 WHERE $tableName.task_id = tasks.id
                AND $tableName.child_hero_id = $childHeroId
                AND completed_at >= '$weekStartDate'
                AND completed_at <= '$weekEndDate'";

        return $this->findBySql($sql);
    }


    /**
    * This method is used when the user unchecks a box on the track your points
    * page — since we don't have a child_hero_task.id at that point, the id isn't set
    * on the incoming $childHeroTask, so we can't just call BaseService::delete().
    */
    public function deleteChildHeroTask($childHeroTask)
    {
        $sql = "child_hero_id = $childHeroTask->childHeroId
                AND completed_at = '$childHeroTask->completedAt'
                AND task_id = $childHeroTask->taskId";
        $this->deleteWhere($sql);
    }
}
?>
