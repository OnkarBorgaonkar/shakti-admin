<?php
class ChildService extends BaseService
{
    public function getStateSignupCount($startDate = null, $endDate = null) {

        $whereClause = "";
        if (!empty($startDate)) {
            $startDate = new DateTime($startDate);
            $whereClause .= "AND children.created_at >= '{$startDate->format("Y-m-d")}'";
        }
        if (!empty($endDate)) {
            $endDate = new DateTime($endDate);
            $endDate->add(new DateInterval("P1D"));
            $whereClause .= "AND children.created_at < '{$endDate->format("Y-m-d")}'";
        }

        $sql = "
            SELECT
                count(*),
                upper(state) as st
            FROM
                children
            WHERE
                state != ''
                $whereClause
            GROUP BY
                st
            ORDER BY
                st
        ";

        $children = $this->findBySql($sql);
        return $children;
    }
}
?>