<?php
class ClassroomService extends BaseService {

    public function getClassroomsInCompetition($competitionId) {
        $sql = "
            SELECT
                classrooms.*
            FROM
                classrooms INNER JOIN competition_schools ON classrooms.school_id = competition_schools.school_id
            WHERE
                competition_schools.competition_id = $competitionId
        ";
        return $this->findBySql($sql);
    }

    public function findClassroomByPasscode($passcode) {
        $passcode = $this->escapeString($passcode);
        return array_pop($this->find("lower(passcode) = lower($passcode)"));
    }

    public function getClassroomByUserId($userId) {
        $sql = "
                    SELECT
                        classrooms.*
                    FROM
                        classrooms INNER JOIN user_classrooms ON classrooms.id = user_classrooms.classroom_id
                    WHERE
                        user_classrooms.user_id = $userId
                ";
        return array_pop($this->findBySql($sql));
    }

}
?>