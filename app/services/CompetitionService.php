<?php 
class CompetitionService extends BaseService {
    
    public function calculateCurrentStandings()  {
        
        $dbh = $this->getDbh();        
        $dbh->beginTransaction();
        
        $categoryService = new CategoryService();
        $categories = $categoryService->find();
        
        $competitionUserStandingService = new CompetitionUserStandingService();
        $competitionSchoolStandingService = new CompetitionSchoolStandingService();
        $competitionUserCategoryStandingService = new CompetitionUserCategoryStandingService();
        
        $now = new DateTime();
        $today = $now->format("Y-m-d");
        $competitions = $this->find("start_date <= '$today' AND end_date >= '$today'");
        
        foreach ($competitions as $competition) {

            $competition->clearStandingsData();
            
            $startDate = $competition->startDate;
            $endDate = $competition->endDate;            
            $classrooms = $competition->getClassrooms();
            $schoolData = array();
            
            foreach ($classrooms as $classroom) {
                $schoolId = $classroom->schoolId;
                $schoolName = $classroom->getSchool()->name;
                
                $members = $classroom->getMembers();
        
                $classroomPoints = $classroom->getTotalPoints($startDate, $endDate);
                $classroomSize = sizeof($members);
        
                $schoolData[$schoolId]["schoolPoints"] += $classroomPoints;
                $schoolData[$schoolId]["schoolSize"] += $classroomSize;
        
                foreach ($members as $groupMember) {
                    $memberPoints = $groupMember->getTotalPoints($startDate, $endDate);
                    $rankName = $groupMember->getRankName();
                    
                    $competitionUserStanding = new CompetitionUserStanding();
                    $competitionUserStanding->userId = $groupMember->id;
                    $competitionUserStanding->competitionId = $competition->id;
                    $competitionUserStanding->schoolId = $schoolId;
                    $competitionUserStanding->totalPoints = $memberPoints;
                    $competitionUserStanding->schoolName = $schoolName;
                    $competitionUserStanding->rankName = $rankName;
                           
                    $competitionUserStandingService->save($competitionUserStanding);
                    
                    foreach ($categories as $category) {
                        $points = $groupMember->getTaskPoints($startDate, $endDate, $category->id);
                        
                        $competitionUserCategoryStanding = new CompetitionUserCategoryStanding();
                        $competitionUserCategoryStanding->competitionId = $competition->id;
                        $competitionUserCategoryStanding->userId = $groupMember->id;
                        $competitionUserCategoryStanding->schoolId = $schoolId;
                        $competitionUserCategoryStanding->categoryId = $category->id;
                        $competitionUserCategoryStanding->totalPoints = $points;
                        $competitionUserCategoryStanding->schoolName = $schoolName;
                        $competitionUserCategoryStanding->rankName = $rankName;
                        
                        $competitionUserCategoryStandingService->save($competitionUserCategoryStanding);
                    }
                }
            }
        
            foreach ($schoolData as $schoolId => $data) {
                $competitionSchoolStanding = new CompetitionSchoolStanding();
                $competitionSchoolStanding->competitionId = $competition->id;
                $competitionSchoolStanding->schoolId = $schoolId;
                $competitionSchoolStanding->averagePoints = $data['schoolPoints'] / $data['schoolSize'];
                $competitionSchoolStanding->totalPoints = $data['schoolPoints'];
        
                $competitionSchoolStandingService->save($competitionSchoolStanding);
            }
        }
        
        $dbh->commit();
    }
    
    public function getCompetitionsByGroupId($groupId) {
        $sql = "
        	SELECT
        		competitions.*
    		FROM
    			competitions INNER JOIN competition_teams ON competitions.id = competition_teams.competition_id
			WHERE
				competition_teams.group_id = $groupId	
			ORDER BY start_date DESC		
        ";        
        $competitions = $this->findBySql($sql);
        
        return $competitions;
    }
    
}
?>