<?php 
class DistrictService extends BaseService {
    
    public function findDistrictByPasscode($passcode) {
        $passcode = $this->escapeString($passcode);
        return array_pop($this->find("lower(passcode) = lower($passcode)"));
    }
    
}
?>