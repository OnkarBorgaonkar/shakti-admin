<?php

class FileService {

    private static $instance = null;

    private function __construct() {}

    public static function instance() {
        if (!isset(self::$instance))
            self::$instance = new FileService();

        return self::$instance;
    }

    /**
     * Copies source file into our file repository using the supplied FileAwareDomainObject.
     */
    public function store($source, $fileAwareDomainObject, $fileName = "original") {
        if (!$fileAwareDomainObject instanceof FileAwareDomainObject || !$fileAwareDomainObject->id > 0)
            return;

        $this->ensurePathExists($fileAwareDomainObject->getFileGroup(), $fileAwareDomainObject->id);
        copy($source, $this->getPath($fileAwareDomainObject->getFileGroup(), $fileAwareDomainObject->id) . "/" . $fileName . "." . $fileAwareDomainObject->fileType);
    }
    
    public function storeFromStream($fileAwareDomainObject, $inputStream, $fileName = "original") {
        $imageData = fopen($inputStream, "r");       
        
        $this->ensurePathExists($fileAwareDomainObject->getFileGroup(), $fileAwareDomainObject->id);
        $fp = fopen($this->getPath($fileAwareDomainObject->getFileGroup(), $fileAwareDomainObject->id) . "/" . $fileName . "." . $fileAwareDomainObject->fileType, "w");        
        
        while ($data = fread($imageData, 1024))
            fwrite($fp, $data);
        
        fclose($fp);
        fclose($imageData);
    }

    /**
     * Remove a directory and its files from our file repository with the supplied FileAwareDomainObject
     */
    public function remove($fileAwareDomainObject) {
        if (!$fileAwareDomainObject instanceof FileAwareDomainObject || $fileAwareDomainObject->getId() == null )
            return;

        $path = $this->getPath($fileAwareDomainObject->getFileGroup(), $fileAwareDomainObject->getId());

        if (is_dir($path))
            $handle = opendir($path);

        if (!$handle)
            return;

        while($file = readdir($handle))
            if ($file != "."  && $file != "..")
                unlink($path."/".$file);

        closedir($handle);

        rmdir($path);
    }

    protected function getPath($group, $id = null) {
        $path = Config::$fileStoragePath . "/$group";
        if ($id > 0) $path .= "/$id";
        return $path;
    }

    protected function ensurePathExists($group, $id) {
        $groupDir = $this->getPath($group);
        if (!file_exists($groupDir)) mkdir($groupDir);

        $idDir = $this->getPath($group, $id);
        if (!file_exists($idDir)) mkdir($idDir);
    }

    public function exists($fileGroup, $id, $fileName, $fileType) {
        return file_exists(Config::$fileStoragePath . "/" . $fileGroup . "/" . $id . "/" . $fileName . "." . $fileType);
    }
}

?>
