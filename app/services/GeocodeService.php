<?php 
class GeocodeService {
    
    public function GeocodeAddress($address) {
        $url = Config::$geocodeUrl . "&address=" . urlencode($address);
        
        $json = file_get_contents($url);        
        $data = json_decode($json);
        
        if ($data != null && $data->status == "OK") {
            $firstMatch = $data->results[0];
            
            $latitude = $firstMatch->geometry->location->lat;
            $longitude = $firstMatch->geometry->location->lng;
            
            $latLng = array (
                "latitude" => $latitude,
                "longitude" => $longitude
            );
            
            return $latLng;
        }
        else {            
            return null;
        }
        
    }
}
?>