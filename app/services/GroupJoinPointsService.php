<?php
class GroupJoinPointsService extends BaseService
{
    public function getPointsByUserId($userId, $startDate = null, $endDate = null){
        $sql = "
                SELECT COALESCE(SUM(points), 0) AS points
                FROM
                    group_join_points
                WHERE
                    group_join_points.user_id = $userId
            ";
    
        if($startDate != null){
            $sql .= " AND group_join_points.created_on >= '$startDate'";
        }
    
        if($endDate != null){
            $sql .= " AND group_join_points.created_on <= '$endDate'";
        }
    
        $points = array_pop($this->findBySql($sql));
    
    
        return $points->points;
    }
}
?>
