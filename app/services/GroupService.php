<?php
class GroupService extends BaseService{

    public function searchGroups($search = null, $orderby = 'points', $orderDirection = 'DESC', $limit = 6, $offset = 0){
        $sql = "
             SELECT
                groups.id,
                groups.name,
                groups.location,
                groups.color,
                (                    
                	SELECT COALESCE(SUM(points), 0)
                    FROM
                        user_groups INNER JOIN user_points_entries ON user_groups.user_id = user_points_entries.user_id
                     WHERE
                        user_groups.group_id = groups.id
                    
                ) AS points
            FROM
                groups ";
        if($search != null){
           $sql .= "WHERE LOWER(name) LIKE LOWER('%$search%') ";
        }
        $sql .= "ORDER BY $orderby $orderDirection
            LIMIT $limit OFFSET $offset
        ";
        return $this->findBySql($sql);
    }
    
    public function getGroupsInSchool($schoolId) {
        $sql = "
        	SELECT
        		groups.*
    		FROM 
    			groups INNER JOIN classroom_information ON groups.id = classroom_information.group_id
    			INNER JOIN schools ON classroom_information.school_id = schools.id
			WHERE
				schools.id = $schoolId
        ";        
        $school = $this->findBySql($sql);
        return $schools;
    }
    
    public function getTeamsInCompetition($competitionId) {
        $sql= "
        	SELECT 
        		groups.*
    		FROM
    			groups INNER JOIN competition_teams ON groups.id = competition_teams.group_id
			WHERE
				competition_teams.competition_id = $competitionId
        ";
        $teams = $this->findBySql($sql);
        return $teams;
    }
    
    function isUserInAClassroom($userId) {
        $sql = "
            	SELECT *
            	FROM
            		groups INNER JOIN user_groups ON groups.id = user_groups.group_id
        		WHERE        			 
        			user_groups.user_id = $userId
        			AND groups.type_name = 'CLASS_ROOM'
            ";
    
        $group = array_pop($this->findBySql($sql));
    
        return $group != null;
    }

	function updateRequiresApproval($updated) {
        if (Config::$approvalRequired) {
			if ($updated->isPersisted()) {
				$original = array_pop($this->find("id = {$updated->id}"));

				return ($updated->name      != '' && $updated->name      != $original->name) ||
					   ($updated->backstory != '' && $updated->backstory != $original->backstory)
				;
			}
			else {
				return true;
			}
        }
        else {
            return false;
        }
	}
}
?>
