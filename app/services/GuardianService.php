<?php
class GuardianService extends BaseService
{
	private function sendEmailNotification($guardian)
	{
		$to      = $guardian->email;
		$subject = 'Your child has registered at shaktiwarriors.com.';
		$headers = 	"From: info@shaktiwarriors.com\r\n" .
		   		 	"Reply-To: info@shaktiwarriors.com";

		//TODO: pass this as a parameter
		$firstName = $_POST['child']['firstName'];
		$userId = $_SESSION['user']->id;
		$guardianId = $guardian->id;
		
		$hostname = $_SERVER['SERVER_NAME'];
		ob_start();
		include("notification.phtml");
		$message = ob_get_contents();
		ob_end_clean();

		return mail($to, $subject, $message, $headers);
	}
	
	public function notify($guardian)
	{
		if ($this->sendEmailNotification($guardian))
		{
			$guardian->notifiedOn = date("Y-m-d");
			$guardian->notificationCount++;
			$this->save($guardian);
			Logger::log(__METHOD__ . " Email sent to $guardian->email");
		}
		else
			Logger::log(__METHOD__ . " Error sending email to $guardian->email");
	}
}
?>