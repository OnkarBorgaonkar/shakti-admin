<?php
class HumanPowerService extends BaseService
{
	public function findForChildHeroId($childHeroId)
	{
		$tableName = $this->getTableName();
		// TODO: Possible: we could find the points for the week directly in SQL here
		$sql = "SELECT *
			 	FROM child_hero_human_powers, $tableName
	 			WHERE $tableName.id = child_hero_human_powers.human_power_id
				AND child_hero_human_powers.child_hero_id = $childHeroId";
		return $this->findBySql($sql);
	}
}
?>