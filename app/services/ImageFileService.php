<?php

/**
 * Extension of the FileService specifically for handling image files.
 */
class ImageFileService extends FileService {

    private static $instance = null;

    private function __construct() {}

    public static function instance() {
        if (!isset(self::$instance))
            self::$instance = new ImageFileService();

        return self::$instance;
    }

    /**
     * This method resizes a source image and stores it into our file repository.
     *
     * The source image is not affected by this method and the resulting image we
     * store has the supplied maximum width, unless the original width is smaller than
     * the supplied value.
     */
    public function resizeAndStore($source, $fileAwareDomainObject, $fileName = "thumb", $maxWidth = null, $maxHeight = null, $density = null) {
        if (!$fileAwareDomainObject instanceof FileAwareDomainObject || !$fileAwareDomainObject->id > 0)
            return;

        if ($maxWidth == null && $maxHeight == null)
            throw new Exception("Invalid width ($maxWidth) or  height ($maxHeight)");

        $tmpName = tempnam("/tmp", "SHAKTI");

        $density = $density != null ? "-density $density" : "";
        $resize = "-resize {$maxWidth}x{$maxHeight}\>";

        $cmd = Config::$convert . " $source $resize $density $tmpName";

        exec($cmd);

        try {
            parent::store($tmpName, $fileAwareDomainObject, $fileName);
        }
        catch (Exception $e) {
            @unlink($tmpName);
            throw $e;
        }
    }
}

?>
