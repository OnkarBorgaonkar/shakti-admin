<?php
class LessonPlanService extends BaseService {

    public function sendOutWeeklyEmail($date, $test = false) {
        $districtService = new DistrictService();
        $classroomGroupService = new ClassroomGroupService();
        $mailService = new MailService();

        $today = new DateTime(date("Y-m-d", strtotime("next monday")));

        $districts = $districtService->find("active = true");
        foreach ($districts as $district) {
            $schools = $district->getSchools();
            foreach ($schools as $school) {
                $schoolStartDate = new DateTime($school->startDate);
                $schoolEndDate = new DateTime($school->endDate);

                if ($school->active && $today >= $schoolStartDate && $today <= $schoolEndDate && $this->schoolIsInSession($school, $today)) {
                    $classrooms = $school->getClassrooms();
                    foreach ($classrooms as $classroom) {
                        if ($classroom->active) {
                            $classroomGroups = $classroom->getClassroomGroups();
                            $emailed = false;
                            foreach ($classroomGroups as $classroomGroup) {
                                $classroomGroupStartDate = new DateTime($classroomGroup->startDate);
                                if (!$classroomGroup->done && $today >= $classroomStartDate) {
                                    if (!$emailed) {
                                        $classroomAdmin = $classroom->getAdmin();
                                        $mailService->sessionEmail($classroomAdmin, Config::$bccClassroomEmail, $test);
                                        $emailed = true;
                                    }
                                    $classroomGroup->updated = false;
                                    $classroomGroupService->save($classroomGroup);
                                }
                            }
                        }
                    }
                    $schoolAdmin = $school->getAdmin();
                    if (sizeof($classrooms) > 0) {
                        $mailService->schoolSessionEmail($schoolAdmin, Config::$bccSchoolEmail, $test);
                    }
                    else {
                        $mailService->schoolNoClassrooms($schoolAdmin, Config::$bccSchoolEmail, $test);
                    }
                }
            }
        }
    }

    private function schoolIsInSession($school, $today) {
        $weeksNotInSession = $school->getWeeksNotInSession();
        if ($weeksNotInSession != null && sizeof($weeksNotInSession) > 0) {
            foreach ($weeksNotInSession as $weekNotInSession) {
                $date = new DateTime($weekNotInSession->startDate);
                if ($date == $today) {
                    return false;
                }
            }
        }
        return true;
    }
}
?>