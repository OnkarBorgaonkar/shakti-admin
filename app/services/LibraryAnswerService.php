<?php
class LibraryAnswerService extends BaseService{

    public function getPointsForSubject($subjectId, $libraryResponseId){
        $sql = "
            SELECT COALESCE(SUM(library_articles.points), 0) AS points
            FROM
                library_articles INNER JOIN library_answers ON library_articles.id = library_answers.library_article_id
                INNER JOIN library_responses ON library_answers.library_response_id = library_responses.id
            WHERE
                library_responses.id = $libraryResponseId
                AND library_articles.subject_id = $subjectId
                AND library_answers.created_on IS NOT NULL
        ";

        $points = array_pop($this->findBySql($sql));
        return $points->points;
    }

}
?>
