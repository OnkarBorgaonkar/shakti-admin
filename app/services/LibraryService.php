<?php
class LibraryService extends BaseService{

    public function getPointsByUserId($userId, $startDate = null, $endDate = null){
        $sql = "
            SELECT COALESCE(SUM(library_articles.points), 0) as points
            FROM
                library_articles INNER JOIN library_answers ON library_articles.id = library_answers.library_article_id
                INNER JOIN library_responses ON library_answers.library_response_id = library_responses.id
            WHERE
                library_responses.user_id = $userId
                AND library_answers.created_on IS NOT NULL
        ";

        if($startDate != null){
            $sql .= " AND library_answers.created_on >= '$startDate'";
        }

        if($endDate != null){
            $sql .= " AND library_answers.created_on <= '$endDate'";
        }

        $points = array_pop($this->findBySql($sql));


        return $points->points;
    }

}
?>
