<?php
class MailService {

    function shopifyOrderCreatedEmail($shopifyOrder) {
        $to = $shopifyOrder->email;
        $from = 'Shakti Registration <noreply@shaktiwarriors.com>';
        $subject = "Finish Shakti Curriculum registration.";
        $version = phpversion();
        $registrationUrl = Config::$baseUrl . "/completeCurriculumRegistration.php?id=$shopifyOrder->id&activationKey=$shopifyOrder->activationKey";
        $headers =
        "From: $from\r\n".
        "Reply-To: $from\r\n".
        "Content-type: text/html; charset=iso-8859-1 \r\n".
        "X-Mailer: PHP/$version"
        ;

        $htmlTemplateFile = Config::$fileStoragePath . "/../mail/shopify-registration.html";
        $htmlTemplate = file_get_contents($htmlTemplateFile);

        $search = array (
            "{baseUrl}",
            "{registrationUrl}"
        );

        $replace = array (
            Config::$baseUrl,
            $registrationUrl
        );

        $message = str_replace($search, $replace, $htmlTemplate);

        mail($to, $subject, $message, $headers);
    }

    function newAdminAccount($adminUser, $accountType, $accountName) {
        $to = $adminUser->email;
        $from = 'Shakti Registration <noreply@shaktiwarriors.com>';
        $subject = "Finish Shakti Admin registration.";
        $version = phpversion();

        $headers =
        "From: $from\r\n".
        "Reply-To: $from\r\n".
        "Content-type: text/html; charset=iso-8859-1 \r\n".
        "X-Mailer: PHP/$version"
        ;

        $htmlTemplateFile = Config::$fileStoragePath . "/../mail/new-admin-account.html";
        $htmlTemplate = file_get_contents($htmlTemplateFile);

        $search = array (
            "{baseUrl}",
            "{name}",
            "{accountType}",
            "{accountName}",
            "{key}"
        );

        $replace = array (
            Config::$baseUrl,
            ucfirst($adminUser->firstName),
            $accountType,
            $accountName,
            $adminUser->activationKey
        );

        $message = str_replace($search, $replace, $htmlTemplate);

        mail($to, $subject, $message, $headers);
    }

    function newSchoolAdminAccount($adminUser, $accountName) {
        $to = $adminUser->email;
        $from = 'Shakti Registration <noreply@shaktiwarriors.com>';
        $subject = "Finish Shakti Admin registration.";
        $version = phpversion();

        $headers =
        "From: $from\r\n".
        "Reply-To: $from\r\n".
        "Content-type: text/html; charset=iso-8859-1 \r\n".
        "X-Mailer: PHP/$version"
        ;

        $htmlTemplateFile = Config::$fileStoragePath . "/../mail/new-admin-account-school.html";
        $htmlTemplate = file_get_contents($htmlTemplateFile);

        $search = array (
            "{baseUrl}",
            "{name}",
            "{accountName}",
            "{key}"
        );

        $replace = array (
            Config::$baseUrl,
            ucfirst($adminUser->firstName),
            $accountName,
            $adminUser->activationKey
        );

        $message = str_replace($search, $replace, $htmlTemplate);

        mail($to, $subject, $message, $headers);
    }

    function forgotPassword($adminUser) {
        $to = $adminUser->email;
        $from = 'Shakti Warriors <noreply@shaktiwarriors.com>';
        $subject = "Forgot Password.";
        $version = phpversion();

        $headers =
        "From: $from\r\n".
        "Reply-To: $from\r\n".
        "Content-type: text/html; charset=iso-8859-1 \r\n".
        "X-Mailer: PHP/$version"
        ;

        $htmlTemplateFile = Config::$fileStoragePath . "/../mail/admin-forgot-password.html";
        $htmlTemplate = file_get_contents($htmlTemplateFile);

        $search = array (
            "{baseUrl}",
            "{name}",
            "{key}"
        );

        $replace = array (
            Config::$baseUrl,
            ucfirst($adminUser->firstName),
            $adminUser->activationKey
        );

        $message = str_replace($search, $replace, $htmlTemplate);

        mail($to, $subject, $message, $headers);
    }

    function sessionEmail($adminUser, $bcc="", $test = false) {
        $to = $test ? Config::$testEmail : $adminUser->email;
        $from = 'SHAKTI Warriors <noreply@shaktiwarriors.com>';
        $subject = "SHAKTI Warriors: Weekly Framework Session" . ($test ? " [" . $adminUser->email . "]" : "");
        $version = phpversion();

        $headers =
        "From: $from\r\n".
        "Reply-To: $from\r\n".
        "Bcc: $bcc\r\n".
        "Content-type: text/html; charset=iso-8859-1 \r\n".
        "X-Mailer: PHP/$version"
        ;

        $htmlTemplateFile = Config::$fileStoragePath . "/../mail/admin-weekly-lesson-plan.html";
        $htmlTemplate = file_get_contents($htmlTemplateFile);

        $search = array (
            "{baseUrl}"
        );

        $replace = array (
            Config::$baseUrl
        );

        $message = str_replace($search, $replace, $htmlTemplate);

        mail($to, $subject, $message, $headers);
    }

    function schoolSessionEmail($adminUser, $bcc="", $test = false) {
        $to = $test ? Config::$testEmail : $adminUser->email;
        $from = 'SHAKTI Warriors <noreply@shaktiwarriors.com>';
        $subject = "SHAKTI Warriors: Weekly Framework Session" . ($test ? " [" . $adminUser->email . "]" : "");
        $version = phpversion();

        $headers =
        "From: $from\r\n".
        "Reply-To: $from\r\n".
        "Bcc: $bcc\r\n".
        "Content-type: text/html; charset=iso-8859-1 \r\n".
        "X-Mailer: PHP/$version"
        ;

        $htmlTemplateFile = Config::$fileStoragePath . "/../mail/admin-school-weekly-lesson-plan.html";
        $htmlTemplate = file_get_contents($htmlTemplateFile);

        $search = array (
            "{baseUrl}"
        );

        $replace = array (
            Config::$baseUrl
        );

        $message = str_replace($search, $replace, $htmlTemplate);

        mail($to, $subject, $message, $headers);
    }

    function schoolNoClassrooms($adminUser, $bcc="", $test = false) {
        $to = $test ? Config::$testEmail : $adminUser->email;
        $from = 'SHAKTI Warriors <noreply@shaktiwarriors.com>';
        $subject = "SHAKTI Warriors: Setup Classrooms" . ($test ? " [" . $adminUser->email . "]" : "");
        $version = phpversion();

        $headers =
        "From: $from\r\n".
        "Reply-To: $from\r\n".
        "Bcc: $bcc\r\n".
        "Content-type: text/html; charset=iso-8859-1 \r\n".
        "X-Mailer: PHP/$version"
        ;

        $htmlTemplateFile = Config::$fileStoragePath . "/../mail/admin-school-no-classrooms.html";
        $htmlTemplate = file_get_contents($htmlTemplateFile);

        $search = array (
            "{baseUrl}"
        );

        $replace = array (
            Config::$baseUrl
        );

        $message = str_replace($search, $replace, $htmlTemplate);

        mail($to, $subject, $message, $headers);
    }

    function adminContactUs($email, $name, $phone, $reason, $comments) {
        // TODO: Find a less dumb way of doing this
        if ($reason == "Ask a question regarding Lesson Plans") {
            $to = Config::$contactLessonRecipient;
        } else {
            $to = Config::$contactRecipient;
        }
        $from = "$name <$email>";
        $subject = "Admin Contact Form";
        $version = phpversion();

        $headers =
        "From: $from\r\n".
        "Reply-To: $from\r\n".
        "X-Mailer: PHP/$version"
        ;

        $htmlTemplateFile = Config::$fileStoragePath . "/../mail/admin-contact-us.txt";
        $htmlTemplate = file_get_contents($htmlTemplateFile);

        $search = array (
            "{name}",
            "{phone}",
            "{reason}",
            "{comments}"
        );

        $replace = array (
            $name,
            $phone,
            $reason,
            $comments
        );

        $message = str_replace($search, $replace, $htmlTemplate);

        mail($to, $subject, $message, $headers);
    }

    function pilotRequest($pilotRequest) {

        $from = "$pilotRequest->firstName $pilotRequest->lastName <$pilotRequest->email>";
        $subject = "Pilot Program Request";
        $version = phpversion();

        $headers =
        "From: $from\r\n".
        "Reply-To: $from\r\n".
        "X-Mailer: PHP/$version"
        ;

        $htmlTemplateFile = Config::$fileStoragePath . "/../mail/pilot-request.txt";
        $htmlTemplate = file_get_contents($htmlTemplateFile);

        $search = array (
            "{name}",
            "{email}",
            "{phone}",
            "{location}",
            "{level}"
        );

        $replace = array (
            $pilotRequest->firstName . " " . $pilotRequest->lastName,
            $pilotRequest->email,
            $pilotRequest->phone,
            $pilotRequest->city . ", " . $pilotRequest->state,
            $pilotRequest->level
        );

        $message = str_replace($search, $replace, $htmlTemplate);

        $to = Config::$contactRecipient;

        mail($to, $subject, $message, $headers);
    }

    function adminGenericEmail($from, $to, $subject, $content) {
        $version = phpversion();

        $headers =
        "From: $from\r\n".
        "Reply-To: $from\r\n".
        "BCC: $from\r\n".
        "Content-type: text/html; charset=iso-8859-1 \r\n".
        "X-Mailer: PHP/$version"
        ;

        $htmlTemplateFile = Config::$fileStoragePath . "/../mail/admin-generic.html";
        $htmlTemplate = file_get_contents($htmlTemplateFile);

        $search = array (
            "{baseUrl}",
            "{content}"
        );

        $replace = array (
            Config::$baseUrl,
            nl2br($content)
        );

        $message = str_replace($search, $replace, $htmlTemplate);

        mail($to, $subject, $message, $headers);
    }
}
?>