<?php
class OrbService extends BaseService {

    public function getOrbsByUserId($userId) {
        $sql = "
            SELECT
                orbs.*,
                EXISTS(SELECT * FROM user_orbs WHERE user_orbs.orb_id = orbs.id AND user_orbs.user_id = $userId AND user_orbs.start_date IS NOT NULL AND user_orbs.completed_date IS NULL) as started,
                EXISTS(SELECT * FROM user_orbs WHERE user_orbs.orb_id = orbs.id AND user_orbs.user_id = $userId AND user_orbs.completed_date IS NOT NULL) as completed
            FROM
                users JOIN children ON users.id = children.user_id
                JOIN orbs ON children.academy_year = orbs.academy_year
            WHERE
                orbs.active = true
                AND users.id = $userId
            ORDER BY \"order\"
        ";
        $orbs = $this->findBySql($sql);
        return $orbs;
    }
    
    public function getOrbsByProgramId($userId, $programId) {
        $sql = "
            SELECT
                orbs.*,
                EXISTS(SELECT * FROM user_orbs WHERE user_orbs.orb_id = orbs.id AND user_orbs.user_id = $userId AND user_orbs.start_date IS NOT NULL AND user_orbs.completed_date IS NULL) as started,
                EXISTS(SELECT * FROM user_orbs WHERE user_orbs.orb_id = orbs.id AND user_orbs.user_id = $userId AND user_orbs.completed_date IS NOT NULL) as completed
            FROM
                orbs
            WHERE
                orbs.active = true
                AND orbs.program_id = $programId
            ORDER BY \"order\"
        ";
        $orbs = $this->findBySql($sql);
        return $orbs;
    }

}
?>