<?php
class QuizQuestionService extends BaseService
{
    public function getNotAnsweredQuestions($quizResponse){
        $sql = "
            SELECT quiz_questions.id
            FROM quiz_questions
            WHERE
                quiz_questions.quiz_id = $quizResponse->quizId
                AND quiz_questions.id NOT IN (
                    SELECT quiz_answers.quiz_question_id
                    FROM quiz_answers
                    WHERE quiz_answers.quiz_response_id = $quizResponse->id
                )
        ";
        $quizQuestions = $this->findBySql($sql);
        return $quizQuestions;
    }

    public function getAnsweredQuestions($quizResponse){
        $sql = "
            SELECT quiz_questions.id
            FROM quiz_questions INNER JOIN quiz_answers ON quiz_questions.id = quiz_answers.quiz_question_id
            WHERE quiz_answers.quiz_response_id = $quizResponse->id;
        ";
        $quizQuestions = $this->findBySql($sql);
        return $quizQuestions;
    }
}
?>
