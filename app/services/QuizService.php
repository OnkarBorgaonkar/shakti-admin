<?php
class QuizService extends BaseService
{
    public function getPointsByUserId($userId, $startDate = null, $endDate = null){
        $sql = "
            SELECT COALESCE(SUM(points), 0) AS points
            FROM
                quiz_questions INNER JOIN quiz_answers ON quiz_questions.id = quiz_answers.quiz_question_id
                INNER JOIN quiz_responses ON quiz_answers.quiz_response_id = quiz_responses.id
            WHERE
                quiz_responses.user_id = $userId
        ";

        if($startDate != null){
            $sql .= " AND quiz_answers.created_on >= '$startDate'";
        }

        if($endDate != null){
            $sql .= " AND quiz_answers.created_on <= '$endDate'";
        }

        $points = array_pop($this->findBySql($sql));


        return $points->points;
    }
}
?>
