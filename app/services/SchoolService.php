<?php
class SchoolService extends BaseService {

    public function getSchoolsFromCompetition($competitionId) {

        $sql = "
            SELECT
                schools.*
            FROM
                schools INNER JOIN competition_schools ON schools.id = competition_schools.school_id
            WHERE
                competition_schools.competition_id = $competitionId
            ORDER BY
                schools.name
        ";

        $schools = $this->findBySql($sql);
        return $schools;
    }

    public function getCompetitionSchoolStandingsByCompetitionId($competitionId) {
        $sql = "
                SELECT
                    schools.*,
                    competition_school_standings.competition_id,
                    competition_school_standings.school_id,
                    competition_school_standings.average_points,
                    competition_school_standings.total_points,
                    competition_school_standings.created as \"lastUpdated\"
                FROM
                    schools INNER JOIN competition_school_standings ON schools.id = competition_school_standings.school_id
                WHERE
                    competition_school_standings.competition_id = $competitionId
                ORDER BY
                    average_points DESC, schools.name
            ";

        $competitionSchoolStandings = $this->findBySql($sql);
        return $competitionSchoolStandings;
    }

    public function getSchoolByGroupId($groupId) {
        $sql = "
            SELECT
                schools.*
            FROM
                schools INNER JOIN classroom_information ON schools.id = classroom_information.school_id
                INNER JOIN groups ON classroom_information.group_id = groups.id
            WHERE
                groups.id = $groupId
        ";

        $school = array_pop($this->findBySql($sql));
        return $school;
    }

    public function findSchoolByPasscode($passcode) {
        $passcode = $this->escapeString($passcode);
        return array_pop($this->find("lower(passcode) = lower($passcode)"));
    }
}
?>