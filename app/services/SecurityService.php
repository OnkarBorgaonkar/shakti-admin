<?php
class SecurityService
{
	static function authenticate()
	{
		session_start();
		if (!isset($_SESSION['user']))
			self::redirectToLogin();
	}

	static function redirectToLogin()
	{
		header("location: /login.php");
	}
}
?>