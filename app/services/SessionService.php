<?php
class SessionService extends BaseService {

    public function getSesssionsNotIn($districtId) {
        $sql = "
            SELECT
                sessions.*
            FROM
                sessions
            WHERE
                sessions.id NOT IN (SELECT session_id FROM district_sessions WHERE district_sessions.district_id = $districtId)
            ORDER BY
                sessions.name

        ";
        return $this->findBySql($sql);
    }

    public function getCurrentSessionForUser($adminUser) {
        if ($adminUser->role == 'district' && $adminUser->districtId != null)
            return $this->getCurrentSessionForDistrict($adminUser->districtId);
        else if ($adminUser->role == 'school' && $adminUser->schoolId != null)
            return $this->getCurrentSessionForSchool($adminUser->schoolId);
        else if ($adminUser->role == 'classroom' && $adminUser->classroomId != null)
            return $this->getCurrentSessionForClassroom($adminUser->classroomId);
        return null;
    }

    public function getCurrentSessionForDistrict($districtId) {
        $today = new DateTime();
        $today = $today->format("Y-m-d");
        $sql = "
            SELECT
                sessions.*
            FROM
                sessions INNER JOIN district_sessions ON sessions.id = district_sessions.session_id
            WHERE
                district_sessions.start_date <= '$today'
                AND district_sessions.end_date >= '$today'
                AND district_sessions.district_id = $districtId;
        ";
        return array_pop($this->findBySql($sql));
    }

    public function getCurrentSessionForSchool($schoolId) {
        $today = new DateTime();
        $today = $today->format("Y-m-d");
        $sql = "
            SELECT
                sessions.*
            FROM
                sessions INNER JOIN district_sessions ON sessions.id = district_sessions.session_id
                INNER JOIN schools ON district_sessions.id = schools.district_session_id
            WHERE
                district_sessions.start_date <= '$today'
                AND district_sessions.end_date >= '$today'
                AND schools.id = $schoolId;
        ";
        return array_pop($this->findBySql($sql));
    }

    public function getCurrentSessionForClassroom($classroomId) {
        $today = new DateTime();
        $today = $today->format("Y-m-d");
        $sql = "
            SELECT
                sessions.*
            FROM
                sessions INNER JOIN district_sessions ON sessions.id = district_sessions.session_id
                INNER JOIN schools ON district_sessions.id = schools.district_session_id
                INNER JOIN classrooms ON schools.id = classrooms.school_id
            WHERE
                district_sessions.start_date <= '$today'
                AND district_sessions.end_date >= '$today'
                AND classrooms.id = $classroomId;
        ";
        return array_pop($this->findBySql($sql));
    }
}
?>