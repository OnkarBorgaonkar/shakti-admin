<?php
class SuperPowerService extends BaseService
{
	public function findForChildHeroId($childHeroId)
	{
		$tableName = $this->getTableName();
		// TODO: Possible: we could find the points for the week directly in SQL here
		$sql = "SELECT *
			 	FROM child_hero_super_powers, $tableName
	 			WHERE $tableName.id = child_hero_super_powers.super_power_id
				AND child_hero_super_powers.child_hero_id = $childHeroId";
		return $this->findBySql($sql);
	}
}
?>