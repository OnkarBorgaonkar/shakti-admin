<?php

class TaskService extends BaseService
{
    public function findByCategoryName($name)
    {
        $name = $this->escapeString($name);
        return $this->find("category = $name");
    }

    /**
    * Note: this function filters out the Extra Credit category tasks, so that the resulting
    * list is suitable for use on the "Track Your Points" page.
    */
    public function findAllByCategories()
    {
        $categoryService = new CategoryService();
        $categories = $categoryService->find('true ORDER BY "order"');
        foreach ($categories as $category)
            if ($category->name != 'Extra Credit' &&  $category->name != 'Custom')
                $results[$category->name] = $this->findByCategoryName($category->name);
        return $results;
    }
}

?>
