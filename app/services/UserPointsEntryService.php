<?php
class UserPointsEntryService extends BaseService {

    public function saveEntry($userPointsEntry, $currentUser) {
        $dbh = $this->getDbh();
        $dbh->beginTransaction();
        try {
            $userPointsEntry->userId = $currentUser->id;
            $updateUser = $userPointsEntry->id == null;
            $this->save($userPointsEntry);
            if ($updateUser) {
                $currentUser->totalPoints += $userPointsEntry->points;
                $userService = new UserService();
                $userService->save($currentUser);
            }
            $dbh->commit();
        }
        catch (Exception $e) {
            $dbh->rollBack();
            throw $e;
        }
    }

    public function deleteEntry($pointType, $activityId, $currentUser) {
        $dbh = $this->getDbh();
        $dbh->beginTransaction();
        try {
            $userPointsEntry = array_pop($this->find("point_type = '$pointType' AND activity_id = $activityId AND user_id = {$currentUser->id}"));

            $currentUser->totalPoints -= $userPointsEntry->points;
            $userService = new UserService();
            $userService->save($currentUser);

            $this->delete($userPointsEntry);
            $dbh->commit();
        }
        catch (Exception $e) {
            $dbh->rollBack();
            throw $e;
        }
    }

    public function getPointsForUser($userId, $pointType = null, $startDate = null, $endDate = null, $categoryId = null) {
        $sql = "
            SELECT
                COALESCE(SUM(user_points_entries.points), 0) as points
            FROM
                user_points_entries
            WHERE
                user_points_entries.user_id = $userId
        ";
        if ($pointType != null) {
            $sql .= " AND user_points_entries.point_type = '$pointType' ";
        }
        if($startDate != null){
            $sql .= " AND user_points_entries.created >= '$startDate' ";
        }
        if($endDate != null){
            $sql .= " AND user_points_entries.created <= '$endDate' ";
        }
        if ($categoryId != null) {
            $sql .= " AND user_points_entries.category_id = $categoryId ";
        }
        $points = array_pop($this->findBySql($sql));
        return (int)$points->points;
    }

}
?>