<?php
class UserService extends BaseService{

    public function searchUsers($search = null, $orderby = 'points', $orderDirection = "DESC", $limit = 6, $offset = 0){
        $sql = "
            SELECT
                users.*, users.total_points AS points
            FROM
                users
            WHERE
                users.active = true
        ";
        if($search != null){
           $sql .= "AND LOWER(login) LIKE LOWER('%$search%') ";
        }
        $sql .= "ORDER BY $orderby $orderDirection
            LIMIT $limit OFFSET $offset
        ";

        return $this->findbySql($sql);
    }

    public function getUsersInCompetition($competitionId, $schoolId = null) {
		$limit = null;
		return $this->getTopUsersInCompetition($competitionId, $schoolId, $limit);
    }

    public function getTopUsersInCompetition($competitionId, $schoolId = null, $limit = 15) {
        $sql = "
            SELECT
                users.*,
                competition_user_standings.competition_id,
                competition_user_standings.school_id,
                competition_user_standings.total_points,
                competition_user_standings.school_name,
                competition_user_standings.rank_name
            FROM
                users INNER JOIN competition_user_standings ON users.id = competition_user_standings.user_id
            WHERE
                competition_user_standings.competition_id = $competitionId
        ";

        if ($schoolId != null) {
            $sql .= " AND competition_user_standings.school_id = $schoolId";
        }

        $sql .= " ORDER BY competition_user_standings.total_points DESC, users.login";

		if ($limit != null) {
			$sql .= " LIMIT $limit";
		}

        $users = $this->findBySql($sql);
        return $users;
    }

    public function getTopUsersInCompetitionForCategory($categoryId, $competitionId, $schoolId = null) {
        $sql = "
                SELECT
                    users.*,
                    competition_user_category_standings.competition_id,
                    competition_user_category_standings.category_id,
                    competition_user_category_standings.total_points,
                    competition_user_category_standings.school_id,
                    competition_user_category_standings.rank_name,
                    competition_user_category_standings.school_name
                FROM
                    users INNER JOIN competition_user_category_standings ON users.id = competition_user_category_standings.user_id
                WHERE
                    competition_user_category_standings.competition_id = $competitionId
                    AND competition_user_category_standings.category_id = $categoryId
            ";

        if ($schoolId != null) {
            $sql .= " AND competition_user_category_standings.school_id = $schoolId";
        }

        $sql .= "
            ORDER BY competition_user_category_standings.total_points DESC, users.login
            LIMIT 3
        ";

        $users = $this->findBySql($sql);
        return $users;
    }

    public function getUsersInSchool($schoolId) {
        $sql = "
            SELECT
                users.*
            FROM
                users INNER JOIN user_classrooms ON users.id = user_classrooms.user_id
                INNER JOIN classrooms ON user_classrooms.classroom_id = classrooms.id
            WHERE
                classrooms.school_id = $schoolId
        ";

        $users = $this->findBySql($sql);
        return $users;
    }

    public function updateRequiresApproval($updated) {
        return Config::$approvalRequired;
    }
}
?>
