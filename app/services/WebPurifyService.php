<?php 
class WebPurifyService {
    private $baseUrl = "http://api1.webpurify.com/services/rest/";
    private $apiKey = "d8611238aac15fb8d8c3e3f38296f82b";
    
    public function checkForProfanity($text) {
        $method = "webpurify.live.check";
        $text = urldecode($text);
        $url = "$this->baseUrl?api_key=$this->apiKey&method=$method&text=$text";
    
        $response = simplexml_load_file($url,'SimpleXMLElement', LIBXML_NOCDATA);
    
        return $response->found == 0 ? false : true;
    }
}
?>