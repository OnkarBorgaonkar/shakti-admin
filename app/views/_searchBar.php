<div class="searchWrap">
    <form method="post" action="/app/actions/doSearch.php">
        <img src="/public/img/headers/search.png">
        <fieldset>
            <div class="fieldWrap">
                <select name="show">
                    <option value="user" <?= $category == "user" ? 'selected="selected"' : ''?>>Warriors</option>
                    <option value="group" <?= $category == "group" ? 'selected="selected"' : ''?>>Teams</option>
                </select>
            </div>
            <div class="fieldWrap">
                <select name="order">
                    <option value="name" <?= $orderBy == "login" || $orderBy == "name" ? 'selected="selected"' : ''?>>Name</option>
                    <option value="points" <?= $orderBy == "points" ? 'selected="selected"' : ''?>>Points</option>
                </select>
            </div>
            <div class="fieldWrap">
                <input autocomplete="off" type="text" name="search" value="<?=$search?>"/>
            </div>
            <div class="fieldWrap">
                <input autocomplete="off" type="image" src="/public/img/global/pixel.gif" value="" class="submitButton" />
            </div>
        </fieldset>
    </form>
</div>
