<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="stylesheet" type="text/css" href="/public/css/global.css" />
        <script type="text/javascript">
        $(document).ready(function() {
            $("#story-form").submit(function() {
                var bigDoorCheckpointId = $("#bigDoorCheckpointId").val();
                var title = $("#bigDoorTitle").val();
                var description = $("#bigDoorDescription").val();
                var response = $("#response").val();

                $.ajax({
                    url: "/json/profanityCheck.php",
                    type: "POST",
                    data: {text: response},
                    dataType: "json",
                    success: function(json) {
                        if (!json.result) {
                            $.ajax({
                                url: "/app/actions/doCompleteBigDoorStory.php",
                                type: "POST",
                                data: {
                                    bigDoorCheckpointId: bigDoorCheckpointId, title: title, description: description, response: response},
                                    success: function() {
                                        BDM.trigger("tell_us");
                                        $.fancybox.close();
                                    }
                    
                            });  
                        }
                        else {
                            $("#story-error").html("Your response contains profanity. Please fix and re submit");
                        }                          
                    }
                })
        
                return false;
            });
        });
        </script>
    </head>
    <body style="background:none;">
        <div id="bigdoor-story">
            <img style="vertical-align: bottom;display:block;" src="/public/img/dialogs/dialogTop.png" />
                    <div class="ddWrap">
                        <form id="story-form">
                            <div class="formInner">
                                <div id="story-error"></div>
                                <input type="hidden" name="bigDoorCheckpointId" id="bigDoorCheckpointId" value="<?=$_GET['checkpointId']?>"/>
                                <input type="hidden" name="title" id="bigDoorTitle" value="<?=urlencode($_GET['title'])?>"/>
                                <input type="hidden" name="description" id="bigDoorDescription" value="<?=urlencode($_GET['description'])?>"/>
                                <div id="bigDoorDescription2">
                                    <?=urldecode($_GET['description'])?>
                                </div>
                                <div class="textAreaWrap">
                                    <textarea name="response" id="response"></textarea>
                                </div>
                                <div class="submitWrap">
                                    <input id="submitStory" type="image" src="/public/img/dialogs/pixel.gif" value="Submit"/>
                                </div>
                            </div>
                        </form>
                    </div>
            <img style="vertical-align: bottom;display:block;" src="/public/img/dialogs/dialogBottom.png" />
        </div>
    </body>
</html>