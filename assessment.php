<?php
    require_once("globals.php");
    require_once("Authenticator.php");
    $pageTitle = "Take Assessment";

    $assessment = $user->getNeededAssessment();

    $questions = $assessment->getQuestions();

    $assessmentQuestionCategories = $assessment->getAssessmentQuestionCategories();

    $assessmentData = $user->needToTakeAssessment();

    include("assessment.phtml");
?>
