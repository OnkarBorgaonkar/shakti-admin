<?php
    require_once("globals.php");

    if ($_SESSION['user']->role == 'admin') {

        if (!isset($_GET['userId'])) {
            $message = urlencode("No User Selected");
            header("location: /welcome.php?systemMessage=$message");
            die();
        }

        $userService = new UserService();
        $editUser = array_pop($userService->find("id = {$_GET['userId']}"));

        if ($editUser == null) {
            $message = urlencode("User not found");
            header("location: /welcome.php?systemMessage=$message");
            die();
        }

        $pageTitle = "Change Password";
        include("changePassword.phtml");

    }
?>