<?php
	require_once("globals.php");
	$competitionId = $_REQUEST['competitionId'];
	$competitionService = new CompetitionService();
	$competition = array_pop($competitionService->find("id = $competitionId"));
	
	if ($competition == null) {
	    die("competition Not found");
	}
	
	$schools = $competition->getCompetitionSchoolStandings();
	
	if (sizeof($schools) > 0) {
 	    $lastUpdated = new DateTime($schools[0]->lastUpdated);
 	    $lastUpdated = $lastUpdated->format("m/d/Y");
	}
	
	$pageTitle = "Competition Map";
	include("competitionMap.phtml");
?>