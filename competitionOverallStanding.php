<?php
    require_once("globals.php");
    $competitionId = $_REQUEST['competitionId'];
    $competitionService = new CompetitionService();
    $competition = array_pop($competitionService->find("id = $competitionId")); 
    
    if ($competition == null) {
        die("competition Not found");
    }
    
    $schoolId = $_REQUEST['schoolId'];
    $schoolService = new SchoolService();
    $school = array_pop($schoolService->find("id = $schoolId"));
    
    if ($school == null) {
        die("school not found");
    }
    
    $studentCount = count($competition->getStudents());
    $limit = round($studentCount * .75, 0);
    
    $topStandings = $competition->getTopStandings($limit);
    
    $bodyStandings = $competition->getTopCategoryStandings(1);
    $mindStandings = $competition->getTopCategoryStandings(2);
    $heartStandings = $competition->getTopCategoryStandings(3);
    $communityStandings = $competition->getTopCategoryStandings(4);
    
    $pageTitle = "Competition Standings";
    include("competitionOverallStanding.phtml");
?>