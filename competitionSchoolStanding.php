<?php
	require_once("globals.php");
	$competitionId = $_REQUEST['competitionId'];
	$competitionService = new CompetitionService();
	$competition = array_pop($competitionService->find("id = $competitionId"));	
	
	if ($competition == null) {
	    die("competition Not found");
	}
	
	$schoolId = $_REQUEST['schoolId'];
	$schoolService = new SchoolService();
	$school = array_pop($schoolService->find("id = $schoolId"));
	
	if ($school == null) {
	    die("school not found");
	}
	
	$studentCount = count($competition->getStudents());
	
	$limit = round($studentCount * .75, 0);
	
	$topStandings = $school->getTopStandingsFromCompetition($competition->id, $limit);
	
	$bodyStandings = $school->getTopCategoryStandingsForCompetition(1, $competitionId);
	$mindStandings = $school->getTopCategoryStandingsForCompetition(2, $competitionId);
	$heartStandings = $school->getTopCategoryStandingsForCompetition(3, $competitionId);
	$communityStandings = $school->getTopCategoryStandingsForCompetition(4, $competitionId);
	
	$pageTitle = "Competition Standings";
	include("competitionSchoolStanding.phtml");
?>