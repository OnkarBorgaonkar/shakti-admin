<?php
class Config {

    public static $dev = false;

    // Database connection info
    public static $databaseName = 'shakti';
    public static $databaseHost = 'localhost';
    public static $databasePort = '5432';
    public static $databaseUser = 'shakti';
    public static $databaseType = 'pgsql';
    public static $databasePass = '';

    public static $convert = "/usr/bin/convert";

    public static $dirPath = "";
    public static $fileStoragePath = "";
    public static $baseUrl = "";

    public static $recaptchaPublicKey = "6LcfgMUSAAAAAPR11eRPlARVSMpxPP2tqIpxuWZ5";
    public static $recaptchaPrivateKey = "6LcfgMUSAAAAANHqPDi-P51L_MVxTgup6BQ9qdnG";

    public static $geocodeUrl = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false";

    public static $googleMapApiKey = "AIzaSyBXkZXP_cAg5GC0GamDfnSutD46Ft1NCIo";

    public static $bigDoorAppKey = "d6b0ad2425f643d0a86f5f1b5340c770";
    public static $bigDoorSecretKey = "2dd51aa582654e7081263db96d2b0444";

    public static $contactRecipient = "info@shaktiwarriors.com";
    public static $contactLessonRecipient = "curriculum_support@shaktiwarriors.com";

    public static $bccSchoolEmail = "lane@lanejabaay.com, rosieyoga@mac.com";
    public static $bccClassroomEmail = "lane@lanejabaay.com, rosieyoga@mac.com";
	public static $testEmail = "rmanrique@trillitech.com";

    public static $approvalRequired = false;
    public static $approvalPassword = "password";

    public static $finalAssessmentDate = null;

    public static $lessFilePath = "/public/admin/styles/less/main.less";
    public static $compliledCSSFilePath = "/public/admin/styles/css/main.css";

    public static function init() {
        Config::setDirPath();
        Config::setIncludePath();
        Config::setFileStoragePath();
        Config::setBaseUrl();
        Config::autoCompileLess();

    @include "Config.local.php";

        if (!file_exists(Config::$fileStoragePath) || !is_dir(Config::$fileStoragePath) || !is_writable(Config::$fileStoragePath))
            throw new Exception("Invalid file storage path or permissions error: " . Config::$fileStoragePath);
    }

    protected static function setDirPath() {
        Config::$dirPath = dirname(__FILE__) . "/..";
    }


    /**
     * Sets the path to the folder where we store files uploaded to the system
     */
    protected static function setFileStoragePath() {
        $installationPath = Config::$dirPath;
        $filePath = $installationPath . "/data";
        Config::$fileStoragePath = $filePath;
    }

    /**
    * Set PHP include path based on __FILE__
    */
    public static function setIncludePath()
    {
        $installationPath = dirname(dirname(__FILE__));
        $installationRelativePaths = array(
            ".",
            "/app/models",
            "/app/services",
            "/app/views",
            "/lib/"
        );
        $includePath =  join(":$installationPath", $installationRelativePaths);
        ini_set("include_path", $includePath);
    }

    protected static function setBaseUrl() {
        $hostUrl = $_SERVER['HTTP_HOST'];
        $url = "http://" . $hostUrl;
        Config::$baseUrl = $url;
    }

    protected static function autoCompileLess() {
//         if (Config::$lessFilePath != "" && Config::$compliledCSSFilePath != "" && file_exists(Config::$dirPath . Config::$lessFilePath)) {
//             // load the cache
//             $cacheFileName = Config::$dirPath . Config::$lessFilePath . ".cache";
//             if (file_exists($cacheFileName)) {
//                 $cache = unserialize(file_get_contents($cacheFileName));
//             }
//             else {
//                 $cache = Config::$dirPath . Config::$lessFilePath;
//             }

//             $less = new lessc;

//             $newCache = $less->cachedCompile($cache);
//             if (!is_array($cache) || $newCache['updated'] > $cache['updated']) {
//                 file_put_contents($cacheFileName, serialize($newCache));
//                 file_put_contents(Config::$dirPath . Config::$compliledCSSFilePath, $newCache['compiled']);
//             }
//         }
    }
}

?>
