<?php 
    require_once("globals.php");
    
    if (isset($_GET['userId'])) {
        $userId = $_GET['userId'];
        $userService = new UserService();
        $user = array_pop($userService->find("id = $userId"));        
        if ($user != null) {
            $childHero = $user->getChildHero();
            if ($childHero->heroMachineString != null) {
                header("location: /public/swf/heromachine-saver/saveCharacter.php?userId=$userId&childHeroId=$childHero->id&heroMachineString=$childHero->heroMachineString");
            }
        }
    }
?>