<?php
    require_once("globals.php");
    require_once("Authenticator.php");

    $group = new Group();
    $group->leaderId = $user->id;

    if(isset($_REQUEST['groupId'])){
        require_once("isGroupLeader.php");
        $groupId = $_REQUEST['groupId'];
        $groupService = new GroupService();
        $result = array_pop($groupService->find("id = $groupId"));

        if ($result != null && $result->leaderId == $user->id){
            $group = $result;
        }
    }
    
    $schoolService = new SchoolService();
    $schools = $schoolService->find("true ORDER BY name");

    $pageTitle = "Create Group";
    include("createGroup.phtml");
?>
