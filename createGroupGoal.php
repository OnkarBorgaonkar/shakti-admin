<?php
    require_once("globals.php");
    require_once("Authenticator.php");
    require_once("isGroupLeader.php");

    $pageTitle = "Create Group Goal";

   $groupId = $_GET['groupId'];

   $groupGoalService = new GroupGoalService();

   $goal = new GroupGoal();

    if ($_REQUEST['mode'] == 'new'){
        $existingGoal = array_pop($groupGoalService->find("group_id = $groupId"));
        if($existingGoal != null){
           $groupGoalService->delete($existingGoal);
        }
        $goal->groupId = $groupId;
    }
    else {
        $groupGoal = array_pop($groupGoalService->find("group_id = $groupId"));
        if($groupGoal == null){
            $goal->groupId = $groupId;
        }
        else{
            $goal = $groupGoal;
        }
    }

    include("createGroupGoal.phtml");
?>
