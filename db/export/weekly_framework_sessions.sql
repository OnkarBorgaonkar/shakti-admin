-- Lesson Plans
SELECT 'Lesson Plan ID', 'Week', 'Description', 'Things to Remember', 'Preparation';
SELECT id, week, description, things_to_remember, preperation FROM lesson_plans ORDER BY week;

-- Weekly Reminders
SELECT '';
SELECT 'Weekly Reminder ID', 'Week', 'Description';
SELECT id, week, description FROM weekly_reminders ORDER BY week;

-- Weekly Messages
SELECT '';
SELECT 'Weekly Message ID', 'Week', 'Superhero', 'Message';
SELECT weekly_messages.id, weekly_messages.week, superheroes.name, weekly_messages.message FROM weekly_messages JOIN superheroes ON weekly_messages.superhero_id = superheroes.id ORDER BY week;

-- Web Activity Entries
SELECT '';
SELECT 'Web Activity ID', 'Week', 'Academy Year', 'Name', 'Description';
SELECT id, week, academy_year, name, description FROM web_activity_entries ORDER BY academy_year, week;

-- As Time Permits
SELECT '';
SELECT 'As Time Permits ID', 'Week', 'Description';
SELECT id, week, description FROM as_time_permits_entries ORDER BY week;