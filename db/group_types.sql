BEGIN;

INSERT INTO group_types (name) VALUES ('FRIENDS');
INSERT INTO group_types (name) VALUES ('SCHOOL');
INSERT INTO group_types (name) VALUES ('FAMILY');
INSERT INTO group_types (name) VALUES ('CHURCH');
INSERT INTO group_types (name) VALUES ('SPORTS_TEAM');

COMMIT;