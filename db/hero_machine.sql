COPY(
SELECT
    users.id, (
        (
            SELECT COALESCE(SUM(points), 0)
            FROM
                children INNER JOIN child_heroes ON children.id = child_heroes.child_id
                INNER JOIN child_hero_tasks ON child_heroes.id = child_hero_tasks.child_hero_id
                INNER JOIN tasks ON child_hero_tasks.task_id = tasks.id
            WHERE
                children.user_id = users.id
                AND child_hero_tasks.approved = true
        ) + (
            SELECT COALESCE(SUM(points), 0)
            FROM
                quiz_questions INNER JOIN quiz_answers ON quiz_questions.id = quiz_answers.quiz_question_id
                INNER JOIN quiz_responses ON quiz_answers.quiz_response_id = quiz_responses.id
            WHERE
                quiz_responses.user_id = users.id
        ) + (

            SELECT COALESCE(SUM(library_articles.points), 0) as points
            FROM
                library_articles INNER JOIN library_answers ON library_articles.id = library_answers.library_article_id
                INNER JOIN library_responses ON library_answers.library_response_id = library_responses.id
            WHERE
                library_responses.user_id = users.id
                AND library_answers.completed_on IS NOT NULL
        ) + (
        	SELECT COALESCE(SUM(points), 0) AS points
            FROM
                group_join_points
            WHERE
                group_join_points.user_id = users.id
        )
    ) AS points
FROM
    users INNER JOIN children ON users.id = children.user_id
    INNER JOIN child_heroes ON children.id = child_heroes.Child_id
WHERE
	child_heroes.hero_machine_string IS NOT NULL
ORDER BY
	points DESC
) TO STDOUT;