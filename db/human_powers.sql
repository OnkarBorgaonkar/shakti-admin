INSERT INTO human_powers (name, description) SELECT 'Compassion', 'A deep awareness and sympathy for another''s suffering.';
INSERT INTO human_powers (name, description) SELECT 'Courageous', 'Being afraid and acting anyway';
INSERT INTO human_powers (name, description) SELECT 'Integrity', 'Honoring your word. Morally sound.';
INSERT INTO human_powers (name, description) SELECT 'Forgiveness', 'To overlook others'' percieved shortcomings or faults';