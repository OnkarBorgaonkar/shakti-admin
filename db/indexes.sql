BEGIN;

CREATE INDEX child_heroes_child_id_idx ON child_heroes (child_id);
CREATE INDEX child_hero_tasks_child_hero_id_idx ON child_hero_tasks (child_hero_id);
CREATE INDEX child_hero_tasks_task_id_idx ON child_hero_tasks (task_id);
CREATE INDEX children_user_id_idx ON children (user_id);

CREATE INDEX quiz_answers_question_id_idx ON quiz_answers (quiz_question_id);
CREATE INDEX quiz_answers_response_id_idx ON quiz_answers (quiz_response_id);

COMMIT;