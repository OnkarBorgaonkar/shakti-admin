--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: libraries_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('libraries_id_seq', 4, true);


--
-- Data for Name: libraries; Type: TABLE DATA; Schema: public; Owner: -
--

COPY libraries (id, rank_id) FROM stdin;
1	1
2	2
3	3
4	4
\.


--
-- PostgreSQL database dump complete
--

