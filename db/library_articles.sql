SELECT pg_catalog.setval('library_articles_id_seq', 49, true);


--
-- Data for Name: library_articles; Type: TABLE DATA; Schema: public; Owner: -
--

COPY library_articles (id, library_id, subject_id, name, points, "order", article_type, url) FROM stdin;
25	4	3	Cyber Safety 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22003&cat_id=20069
42	1	6	Environmental Sustainability 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=59587&cat_id=115
12	3	2	Nutrition 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=42970&cat_id=20512
16	3	2	Nutrition 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=34326&cat_id=20512
13	4	2	Nutrition 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10256&cat_id=20512
17	4	2	Nutrition 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10618&cat_id=20512
19	2	3	Cyber Safety 1	150	1	PDF	\N
20	3	3	Cyber Safety 1	150	1	PDF	\N
21	4	3	Cyber Safety 1	150	1	PDF	\N
23	2	3	Cyber Safety 2	150	2	PDF	\N
26	1	4	Non Violence 1	150	1	PDF	\N
27	2	4	Non Violence 1	150	1	PDF	\N
30	1	4	Non Violence 2	150	2	PDF	\N
34	1	5	Mind Up 1	150	1	PDF	\N
35	2	5	Mind Up 1	150	1	PDF	\N
36	3	5	Mind Up 1	150	1	PDF	\N
37	4	5	Mind Up 1	150	1	PDF	\N
38	1	5	Mind Up 2	150	2	PDF	\N
39	2	5	Mind Up 2	150	2	PDF	\N
40	3	5	Mind Up 2	150	2	PDF	\N
41	4	5	Mind Up 2	150	2	PDF	\N
43	2	6	Environmental Sustainability 1	150	1	PDF	\N
44	3	6	Environmental Sustainability 1	150	1	PDF	\N
45	4	6	Environmental Sustainability 1	150	1	PDF	\N
46	1	6	Environmental Sustainability 2	150	2	PDF	\N
47	2	6	Environmental Sustainability 2	150	2	PDF	\N
48	3	6	Environmental Sustainability 2	150	2	PDF	\N
49	4	6	Environmental Sustainability 2	150	2	PDF	\N
10	1	2	Nutrition 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10255&cat_id=20512
14	1	2	Nutrition 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10262&cat_id=20512
11	2	2	Nutrition 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=74783&cat_id=20512
15	2	2	Nutrition 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=20520&cat_id=20512
1	1	1	Fitness 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=35919&cat_id=20514
5	1	1	Fitness 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10269&cat_id=120
2	2	1	Fitness 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10267&cat_id=118
6	2	1	Fitness 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=20262&cat_id=120
3	3	1	Fitness 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=76817&cat_id=20071
7	3	1	Fitness 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=62242&cat_id=120
4	4	1	Fitness 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22521&cat_id=20512
8	4	1	Fitness 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22514&cat_id=115
31	2	4	Non Violence 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22066&cat_id=20068
28	3	4	Non Violence 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22021&cat_id=20069
32	3	4	Non Violence 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22400&cat_id=20070
29	4	4	Non Violence 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=40305&cat_id=20069
33	4	4	Non Violence 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=43408&cat_id=20070
18	1	3	Cyber Safety 1	150	1	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10623&cat_id=115
22	1	3	Cyber Safety 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=61559&cat_id=115
24	3	3	Cyber Safety 2	150	2	URL	http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22505&cat_id=20070
\.


--
-- PostgreSQL database dump complete
--

