BEGIN;

DELETE FROM assessment_answers;
DELETE FROM assessment_responses;
DELETE FROM assessment_questions;
DELETE FROM assessments;

ALTER TABLE assessments DROP COLUMN rank_id;
ALTER TABLE assessments ADD COLUMN name CHARACTER VARYING NOT NULL UNIQUE;

ALTER TABLE districts ADD COLUMN assessment_date DATE;
ALTER TABLE schools ADD COLUMN assessment_date DATE;

CREATE TABLE assessment_types (
    name CHARACTER VARYING PRIMARY KEY
);

INSERT INTO assessment_types (name) VALUES('INITIAL'),('FINAL'),('HERO');

ALTER TABLE assessment_responses ADD COLUMN assessment_type CHARACTER VARYING NOT NULL REFERENCES assessment_types(name);
ALTER TABLE assessment_responses ADD COLUMN academy_year INTEGER NOT NULL;

INSERT INTO assessments (name) VALUES('Main Assessment');

INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'My classmates are friendly', 3, 1);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'When you work in a group, you learn from other kids.', 3, 2);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'There are many ways to solve problems other than fighting.', 3, 3);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'I try not to make fun of other kids.', 3, 4);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'My classmates treat me with respect. ', 3, 5);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'I like to talk to my parents.', 3, 6);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'I try my best to be friendly with all the kids in my classroom.', 3, 7);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'You can learn a lot by helping others. ', 3, 8);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'When I disagree with someone, I talk about my ideas, listen, and we solve the problem.', 3, 9);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Kids make fun of me. ', 3, 10);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'I want to develop my special talents.', 2, 1);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'It''s O.K. to say "No" to friends sometimes.', 2, 2);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'I am proud of the things I do.', 2, 3);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'I would like to make my own decisions.', 2, 4);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'It feels stupid to give other people compliments.', 2, 5);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'I make decisions every day.', 2, 6);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'My parents make all my decisions for me. ', 2, 7);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'I have many responsibilities.', 2, 8);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'I know several skills for making good decisions. ', 2, 9);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'I would like to argue less with other kids. ', 2, 10);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'It''s always risky to smoke cigarettes.', 1, 1);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Medicines are drugs.', 1, 2);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'You must have written rules to make good decisions.', 1, 3);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'The way I treat my body changes how my body works and feels.', 1, 4);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Other people cannot always tell me what the best decision is for me.', 1, 5);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'How I eat now will affect me as an adult.', 1, 6);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Children who drink may become alcoholics much faster than adults.', 1, 7);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Being healthy is mostly a matter of luck.', 1, 8);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'It''s always risky to use drugs and alcohol.', 1, 9);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Someday, I will smoke cigarettes or drink alcohol.', 1, 10);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Good decisions are those which are easiest to make.', 4, 1);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'I know ways to show people that I am listening to them.', 4, 2);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Kids should be allowed to make some classroom rules.', 4, 3);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Ads on T.V. speak the truth.', 4, 4);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Everyone in my class makes a contribution to the class.', 4, 5);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Working with a small group can be difficult, but it is worth it.', 4, 6);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Kids my own age might want me to do harmful things.', 4, 7);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'Kids get along better when there are school rules.', 4, 8);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'When two people disagree, they should fight it out until one person gives in.', 4, 9);
INSERT INTO assessment_questions(assessment_id, question, category_id, "order") VALUES((SELECT id FROM assessments WHERE name = 'Main Assessment'), 'When you are working with others, you can''t always expect to get your way.', 4, 10);


COMMIT;

