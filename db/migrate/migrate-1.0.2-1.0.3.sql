BEGIN;

CREATE INDEX child_hero_tasks_gauge_idx ON child_hero_tasks (child_hero_id, completed_at);

CREATE TABLE group_join_points(
	id SERIAL PRIMARY KEY,
	group_id INTEGER REFERENCES groups(id),
	user_id INTEGER NOT NULL REFERENCES users(id),	
	points INTEGER NOT NULL,
	created_on TIMESTAMP DEFAULT NOW()
);

INSERT INTO categories (name, "order") VALUES('Custom', 6);

INSERT INTO tasks (name, category, points) VALUES('Custom 1', 'Custom', 10);
INSERT INTO tasks (name, category, points) VALUES('Custom 2', 'Custom', 10);
INSERT INTO tasks (name, category, points) VALUES('Custom 3', 'Custom', 10);
INSERT INTO tasks (name, category, points) VALUES('Custom 4', 'Custom', 10);

CREATE TABLE child_hero_custom_tasks (
	id SERIAL PRIMARY KEY,
	child_hero_id INTEGER NOT NULL REFERENCES child_heroes(id),
	task_id INTEGER NOT NULL REFERENCES tasks(id),
	name CHARACTER VARYING NOT NULL
);



ALTER TABLE library_answers ADD COLUMN completed_on TIMESTAMP;

DELETE FROM assessment_answers;
DELETE FROM assessment_responses;
DELETE FROM assessment_questions;


ALTER TABLE categories DROP CONSTRAINT categories_pkey CASCADE;
ALTER TABLE categories ADD CONSTRAINT categories_name_unique UNIQUE(name);
ALTER TABLE tasks ADD CONSTRAINT tasks_categories_fkey FOREIGN KEY (category) REFERENCES categories(name);
ALTER TABLE categories ADD COLUMN id SERIAL PRIMARY KEY;

ALTER TABLE assessment_questions DROP COLUMN subject_id;
ALTER TABLE assessment_questions ADD COLUMN category_id INTEGER REFERENCES categories(id);

ALTER TABLE users ADD COLUMN content_type CHARACTER VARYING;
ALTER TABLE users ADD COLUMN file_type CHARACTER VARYING;

INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Going to bed on time?', '1', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Spending more time exercising or playing sports?', '2', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Eating fruits and vegetables?', '3', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Eating less junk food?', '4', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Keeping yourself safe?', '5', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Spending less time playing video games?', '6', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Spending less time watching TV?', '7', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Nearly always telling the truth?', '1', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Completing your homework on-time?', '2', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Taking a "time-in" several times a day?', '3', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Making it to school on-time?', '4', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Staying out of trouble?', '5', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Checking your homework?', '6', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Becoming a better reader?', '7', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Becoming a better writer?', '8', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Doing better in math?', '9', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Doing more creative activities like drawing, painting and arts & crafts?', '10', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Doing more things that are creative?', '11', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Learning to do new things?', '12', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Performing in plays and public speaking?', '13', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Being kind to those around you?', '1', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Writing in your journal?', '2', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Showing compassion for those less fortunate than you?', '3', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Doing good deeds?', '4', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Speaking to others respectfully?', '5', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Not yelling too much?', '6', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Letting your parents know about your accomplishments?', '7', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Letting your parents know about problems you are having?', '8', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Letting your teachers know about nice things happening at home?', '9', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Letting your teachers know about problems happening at home?', '10', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Getting along better with your parents?', '11', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Getting along better with your friends?', '12', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Telling adults about bullies or cyber-bullying?', '1', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Completing your chores?', '2', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Helping out more in your classroom and school?', '3', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Supporting your team mates and class mates?', '4', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Doing less or no bullying?', '5', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Doing less or no cyber-bullying?', '6', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Being more safe on the internet?', '7', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Saving more water and energy around your house?', '8', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Doing more group activities like team sports, bands, dance or singing?', '9', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('1', 'Getting along better with your brothers, sisters and cousins?', '10', '4');

INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Going to bed on time?', '1', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Spending more time exercising or playing sports?', '2', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Eating fruits and vegetables?', '3', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Eating less junk food?', '4', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Keeping yourself safe?', '5', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Spending less time playing video games?', '6', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Spending less time watching TV?', '7', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Nearly always telling the truth?', '1', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Completing your homework on-time?', '2', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Taking a "time-in" several times a day?', '3', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Making it to school on-time?', '4', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Staying out of trouble?', '5', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Checking your homework?', '6', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Becoming a better reader?', '7', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Becoming a better writer?', '8', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Doing better in math?', '9', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Doing more creative activities like drawing, painting and arts & crafts?', '10', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Doing more things that are creative?', '11', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Learning to do new things?', '12', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Performing in plays and public speaking?', '13', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Being kind to those around you?', '1', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Writing in your journal?', '2', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Showing compassion for those less fortunate than you?', '3', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Doing good deeds?', '4', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Speaking to others respectfully?', '5', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Not yelling too much?', '6', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Letting your parents know about your accomplishments?', '7', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Letting your parents know about problems you are having?', '8', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Letting your teachers know about nice things happening at home?', '9', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Letting your teachers know about problems happening at home?', '10', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Getting along better with your parents?', '11', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Getting along better with your friends?', '12', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Telling adults about bullies or cyber-bullying?', '1', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Completing your chores?', '2', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Helping out more in your classroom and school?', '3', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Supporting your team mates and class mates?', '4', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Doing less or no bullying?', '5', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Doing less or no cyber-bullying?', '6', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Being more safe on the internet?', '7', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Saving more water and energy around your house?', '8', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Doing more group activities like team sports, bands, dance or singing?', '9', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('2', 'Getting along better with your brothers, sisters and cousins?', '10', '4');

INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Going to bed on time?', '1', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Spending more time exercising or playing sports?', '2', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Eating fruits and vegetables?', '3', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Eating less junk food?', '4', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Keeping yourself safe?', '5', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Spending less time playing video games?', '6', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Spending less time watching TV?', '7', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Nearly always telling the truth?', '1', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Completing your homework on-time?', '2', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Taking a "time-in" several times a day?', '3', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Making it to school on-time?', '4', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Staying out of trouble?', '5', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Checking your homework?', '6', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Becoming a better reader?', '7', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Becoming a better writer?', '8', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Doing better in math?', '9', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Doing more creative activities like drawing, painting and arts & crafts?', '10', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Doing more things that are creative?', '11', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Learning to do new things?', '12', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Performing in plays and public speaking?', '13', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Being kind to those around you?', '1', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Writing in your journal?', '2', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Showing compassion for those less fortunate than you?', '3', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Doing good deeds?', '4', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Speaking to others respectfully?', '5', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Not yelling too much?', '6', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Letting your parents know about your accomplishments?', '7', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Letting your parents know about problems you are having?', '8', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Letting your teachers know about nice things happening at home?', '9', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Letting your teachers know about problems happening at home?', '10', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Getting along better with your parents?', '11', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Getting along better with your friends?', '12', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Telling adults about bullies or cyber-bullying?', '1', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Completing your chores?', '2', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Helping out more in your classroom and school?', '3', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Supporting your team mates and class mates?', '4', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Doing less or no bullying?', '5', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Doing less or no cyber-bullying?', '6', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Being more safe on the internet?', '7', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Saving more water and energy around your house?', '8', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Doing more group activities like team sports, bands, dance or singing?', '9', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('3', 'Getting along better with your brothers, sisters and cousins?', '10', '4');

INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Going to bed on time?', '1', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Spending more time exercising or playing sports?', '2', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Eating fruits and vegetables?', '3', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Eating less junk food?', '4', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Keeping yourself safe?', '5', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Spending less time playing video games?', '6', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Spending less time watching TV?', '7', '1');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Nearly always telling the truth?', '1', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Completing your homework on-time?', '2', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Taking a "time-in" several times a day?', '3', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Making it to school on-time?', '4', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Staying out of trouble?', '5', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Checking your homework?', '6', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Becoming a better reader?', '7', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Becoming a better writer?', '8', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Doing better in math?', '9', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Doing more creative activities like drawing, painting and arts & crafts?', '10', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Doing more things that are creative?', '11', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Learning to do new things?', '12', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Performing in plays and public speaking?', '13', '2');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Being kind to those around you?', '1', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Writing in your journal?', '2', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Showing compassion for those less fortunate than you?', '3', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Doing good deeds?', '4', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Speaking to others respectfully?', '5', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Not yelling too much?', '6', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Letting your parents know about your accomplishments?', '7', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Letting your parents know about problems you are having?', '8', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Letting your teachers know about nice things happening at home?', '9', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Letting your teachers know about problems happening at home?', '10', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Getting along better with your parents?', '11', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Getting along better with your friends?', '12', '3');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Telling adults about bullies or cyber-bullying?', '1', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Completing your chores?', '2', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Helping out more in your classroom and school?', '3', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Supporting your team mates and class mates?', '4', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Doing less or no bullying?', '5', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Doing less or no cyber-bullying?', '6', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Being more safe on the internet?', '7', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Saving more water and energy around your house?', '8', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Doing more group activities like team sports, bands, dance or singing?', '9', '4');
INSERT INTO assessment_questions (assessment_id, question, "order", category_id) VALUES('4', 'Getting along better with your brothers, sisters and cousins?', '10', '4');

UPDATE library_articles SET article_type = 'PDF', url = NULL WHERE id = 23;

UPDATE quiz_questions SET library_article_id = 11 WHERE id = 191;
UPDATE quiz_questions SET library_article_id = 6 WHERE id = 213;
UPDATE quiz_questions SET library_article_id = 6 WHERE id = 214;
UPDATE quiz_questions SET library_article_id = 6 WHERE id = 215;
UPDATE quiz_questions SET library_article_id = 20 WHERE id = 276;
UPDATE quiz_questions SET library_article_id = 20 WHERE id = 277;
UPDATE quiz_questions SET library_article_id = 20 WHERE id = 278;
UPDATE quiz_questions SET library_article_id = 24 WHERE id = 279;
UPDATE quiz_questions SET library_article_id = 24 WHERE id = 280;

DELETE FROM roles;
INSERT INTO roles (name) VALUES('user'),('admin');
UPDATE users SET role = 'user';


UPDATE  tasks SET name = 'Redeemed a SW coupon this week' WHERE id = 119;
UPDATE  tasks SET name = 'Read with your parents this week' WHERE id = 102;
INSERT INTO tasks (name, category, points) VALUES('Reviewed your homework with your parents this week', 'Extra Credit', 10);

COMMIT;

ANALYZE;