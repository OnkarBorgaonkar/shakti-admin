BEGIN;

CREATE TABLE competitions (
	id SERIAL PRIMARY KEY,
	name CHARACTER VARYING NOT NULL,
	description CHARACTER VARYING NOT NULL,
	passcode CHARACTER VARYING NOT NULL,
	start_date DATE NOT NULL,
	end_date DATE NOT NULL
);

CREATE TABLE cities (
	id SERIAL PRIMARY KEY,
	name CHARACTER VARYING NOT NULL,
	state CHARACTER VARYING NOT NULL
);

CREATE TABLE schools (
	id SERIAL PRIMARY KEY,
	name CHARACTER VARYING NOT NULL,
	neighborhood_name CHARACTER VARYING NOT NULL,
	lead_name CHARACTER VARYING NOT NULL,
	lead_phone CHARACTER VARYING NOT NULL,
	lead_email CHARACTER VARYING NOT NULL,
	address CHARACTER VARYING NOT NULL,	
	zip CHARACTER VARYING NOT NULL,
	latitude REAL,
	longitude REAL,
	city_id INTEGER NOT NULL REFERENCES cities(id)
);

CREATE TABLE classroom_information (
	id SERIAL PRIMARY KEY,
	school_id INTEGER NOT NULL REFERENCES schools(id),
	group_id INTEGER NOT NULL UNIQUE REFERENCES groups(id),
	grade CHARACTER VARYING NOT NULL,
	coach_first_name CHARACTER VARYING NOT NULL,
	coach_last_name CHARACTER VARYING NOT NULL,
	coach_email CHARACTER VARYING NOT NULL,
	coach_phone CHARACTER VARYING NOT NULL
);

CREATE TABLE competition_teams (
	competition_id INTEGER NOT NULL REFERENCES competitions(id),
	group_id INTEGER NOT NULL REFERENCES groups(id),
	UNIQUE (competition_id, group_id)
);

CREATE TABLE competition_school_standings (
	id SERIAL PRIMARY KEY,
	competition_id INTEGER NOT NULL REFERENCES competitions(id),
	school_id INTEGER NOT NULL REFERENCES schools(id), 
	average_points DOUBLE PRECISION NOT NULL, 
	total_points DOUBLE PRECISION NOT NULL, 
	created TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE competition_user_standings (
	id SERIAL PRIMARY KEY,
	user_id INTEGER NOT NULL REFERENCES users(id),
	competition_id INTEGER NOT NULL REFERENCES competitions(id),	
	school_id INTEGER NOT NULL REFERENCES schools(id),
	total_points DOUBLE PRECISION NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE competition_user_category_standings (
	id SERIAL PRIMARY KEY,
	competition_id INTEGER NOT NULL REFERENCES competitions(id),
	user_id INTEGER NOT NULL REFERENCES users(id), 
	school_id INTEGER NOT NULL REFERENCES schools(id),
	category_id INTEGER NOT NULL REFERENCES categories(id),
	total_points DOUBLE PRECISION NOT NULL, 
	created TIMESTAMP NOT NULL DEFAULT now()
);

ALTER TABLE group_invitations ALTER COLUMN message DROP NOT NULL;
ALTER TABLE group_requests ALTER COLUMN message DROP NOT NULL;
ALTER TABLE groups ALTER COLUMN backstory DROP NOT NULL;


INSERT INTO group_types values ('CLASS_ROOM');
