BEGIN;

DELETE FROM competition_user_standings;
DELETE FROM competition_user_category_standings;

ALTER TABLE competition_user_standings ADD COLUMN rank_name CHARACTER VARYING NOT NULL;
ALTER TABLE competition_user_standings ADD COLUMN school_name CHARACTER VARYING NOT NULL;

ALTER TABLE competition_user_category_standings ADD COLUMN rank_name CHARACTER VARYING NOT NULL;
ALTER TABLE competition_user_category_standings ADD COLUMN school_name CHARACTER VARYING NOT NULL;

COMMIT;