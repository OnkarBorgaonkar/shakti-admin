BEGIN;
--
-- content updates
--

--UPDATE library_articles SET url = 'http://kidshealth.org/kid/nutrition/food/pyramid.html#cat119' WHERE url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=74783&cat_id=20512';

UPDATE tasks SET name = 'Got 9 hours of sleep last night?' WHERE name = 'In bed on time?';
UPDATE tasks SET name = 'Exercised 30 minutes today?' WHERE name = 'Exercised 20 minutes today?';
INSERT INTO tasks (name, category, points) VALUES('Check this box if you exercised 60 minutes or more today.', 'Hero''s Body', 10);
UPDATE tasks SET name = 'Ate 5+ helpings of fruits and/or vegetables today?' WHERE name = 'Ate 3-5 fruits and/or vegetables today?';
INSERT INTO tasks (name, category, points) VALUES('Check this box if you ate more fruits and vegetables than grains and proteins.', 'Hero''s Body', 10);
UPDATE tasks SET name = 'Showed compassion for someone today?' WHERE name = 'Showed compassion for someone less fortunate than you?';
UPDATE tasks SET name = 'Read with your parents or guardian this week' WHERE name = 'Read with your parents this week';
UPDATE tasks SET name = 'Helped out with yard work today' WHERE name = 'Raked you or your neighbors leaves';

UPDATE child_hero_tasks SET task_id = (SELECT id FROM tasks WHERE name = 'Helped out with yard work today') WHERE task_id = (SELECT id FROM tasks WHERE name = 'Mowed you or your neighbors lawn');
DELETE FROM tasks WHERE name = 'Mowed you or your neighbors lawn';
UPDATE child_hero_tasks SET task_id = (SELECT id FROM tasks WHERE name = 'Helped out with yard work today') WHERE task_id = (SELECT id FROM tasks WHERE name = 'Shoveled you or your neighbors snow');
DELETE FROM Tasks WHERE name = 'Shoveled you or your neighbors snow';









--
--schema updates
--

ALTER TABLE library_articles ADD COLUMN pages REAL NOT NULL DEFAULT 2;
ALTER TABLE library_articles ALTER COLUMN pages DROP DEFAULT;


CREATE TABLE point_types(
	point_type CHARACTER VARYING NOT NULL UNIQUE
);

INSERT INTO point_types(point_type) VALUES('ECHO_BOARD');
INSERT INTO point_types(point_type) VALUES('DAILY_TRAINING');
INSERT INTO point_types(point_type) VALUES('LIBRARY_ARTICLE');
INSERT INTO point_types(point_type) VALUES('RECRUITING');

CREATE TABLE user_points_entries (
	id SERIAL PRIMARY KEY,
	user_id INTEGER NOT NULL REFERENCES users(id),
	point_type CHARACTER VARYING NOT NULL REFERENCES point_types(point_type),
	points REAL NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT NOW(),
	activity_id INTEGER NOT NULL,
	category_id INTEGER REFERENCES categories(id)	
);

CREATE INDEX user_points_entries_user_id_index ON user_points_entries (user_id);
CREATE INDEX user_points_entries_point_type_index ON user_points_entries (point_type);
CREATE INDEX user_points_entries_created_index ON user_points_entries (created);

ALTER TABLE users ADD COLUMN total_points REAL NOT NULL DEFAULT 0;
ALTER TABLE users ADD COLUMN rank_id INTEGER NOT NULL DEFAULT 1 REFERENCES child_hero_ranks(id);

DELETE FROM child_hero_tasks cht1 USING child_hero_tasks cht2 WHERE cht1.child_hero_id = cht2.child_hero_id AND cht1.task_id = cht2.task_id AND cht1.completed_at = cht2.completed_at AND cht1.id < cht2.id;
ALTER TABLE child_hero_tasks ADD CONSTRAINT child_hero_id_task_id_completed_at UNIQUE(child_hero_id, task_id, completed_at);

CREATE TABLE districts (
	id SERIAL PRIMARY KEY,
	name CHARACTER VARYING NOT NULL UNIQUE,
	contact_prefix CHARACTER VARYING,
	contact_first_name CHARACTER VARYING,
	contact_last_name CHARACTER VARYING,
	contact_super_hero_name CHARACTER VARYING,
	contact_email CHARACTER VARYING,
	contact_work_phone CHARACTER VARYING,
	contact_cell_phone CHARACTER VARYING,
	contract_users INTEGER NOT NULL,
	contract_start_date DATE NOT NULL,
	contract_end_date DATE NOT NULL,
	passcode CHARACTER VARYING NOT NULL UNIQUE
);

INSERT INTO districts (name, contract_users, contract_start_date, contract_end_date, passcode) VALUES('LA School District', 1000, now(), now(), 'laschoolsrule!');

ALTER TABLE schools ADD COLUMN contact_prefix CHARACTER VARYING;
ALTER TABLE schools ADD COLUMN contact_last_name CHARACTER VARYING;
ALTER TABLE schools ADD COLUMN contact_super_hero_name CHARACTER VARYING;
ALTER TABLE schools ADD COLUMN city CHARACTER VARYING;
ALTER TABLE schools ADD COLUMN state CHARACTER VARYING;
ALTER TABLE schools RENAME COLUMN lead_name TO contact_first_name;
ALTER TABLE schools ALTER COLUMN contact_first_name DROP NOT NULL;
ALTER TABLE schools RENAME COLUMN lead_phone TO contact_work_phone;
ALTER TABLE schools ALTER COLUMN contact_work_phone DROP NOT NULL;
ALTER TABLE schools ADD COLUMN contact_cell_phone CHARACTER VARYING;
ALTER TABLE schools RENAME COLUMN lead_email TO contact_email;
ALTER TABLE schools ALTER COLUMN contact_email DROP NOT NULL;
ALTER TABLE schools DROP COLUMN neighborhood_name;
ALTER TABLE schools ADD COLUMN district_id INTEGER REFERENCES districts(id);
ALTER TABLE schools ADD COLUMN passcode CHARACTER VARYING UNIQUE NOT NULL DEFAULT substring(md5(random()::text),1 , 10);
ALTER TABLE schools ALTER COLUMN passcode DROP DEFAULT;

UPDATE schools SET district_id = (SELECT id FROM districts WHERE passcode = 'laschoolsrule!');
ALTER TABLE schools ALTER COLUMN district_id SET NOT NULL;

UPDATE schools SET city = (SELECT cities.name FROM cities INNER JOIN schools as schools2 ON cities.id = schools2.city_id WHERE schools2.id = schools.id), state = (SELECT cities.state FROM cities INNER JOIN schools as schools2 ON cities.id = schools2.city_id WHERE schools2.id = schools.id);
ALTER TABLE schools ALTER COLUMN city SET NOT NULL;
ALTER TABLE schools ALTER COLUMN state SET NOT NULL;

ALTER TABLE schools DROP COLUMN city_id;

CREATE TABLE grade_types (
	name CHARACTER VARYING NOT NULL UNIQUE
);

INSERT INTO grade_types VALUES('BELOW_4TH'),('4TH'),('5TH'),('6TH'),('ABOVE_6TH');

CREATE TABLE classrooms (
	id SERIAL PRIMARY KEY,
	school_id INTEGER NOT NULL REFERENCES schools(id),
	name CHARACTER VARYING NOT NULL,
	contact_prefix CHARACTER VARYING,
	contact_first_name CHARACTER VARYING,
	contact_last_name CHARACTER VARYING,
	contact_super_hero_name CHARACTER VARYING,
	contact_email CHARACTER VARYING,
	contact_work_phone CHARACTER VARYING,
	contact_cell_phone CHARACTER VARYING,
	grade_type CHARACTER VARYING NOT NULL REFERENCES grade_types(name),
	active BOOLEAN NOT NULL,
	passcode CHARACTER VARYING NOT NULL UNIQUE,
	group_id INTEGER REFERENCES groups(id) NOT NULL
);

CREATE TABLE classroom_grades (
	classroom_id INTEGER NOT NULL REFERENCES classrooms(id),
	grade_type CHARACTER VARYING NOT NULL REFERENCES grade_types(name)
);


INSERT INTO classrooms (school_id, name, contact_first_name, contact_last_name, contact_email, contact_work_phone, grade_type, active, passcode, group_id)
SELECT 
	classroom_information.school_id,
	groups.name,
	classroom_information.coach_first_name,
	classroom_information.coach_last_name,
	classroom_information.coach_email,
	classroom_information.coach_phone,
	'5TH',
	'true',
	substring(md5(random()::text), 1, 10),
	groups.id
FROM
	groups INNER JOIN classroom_information ON groups.id = classroom_information.group_id
WHERE
	groups.type_name = 'CLASS_ROOM'
;


CREATE TABLE competition_schools (
	competition_id INTEGER NOT NULL REFERENCES competitions(id),
	school_id INTEGER NOT NULL REFERENCES schools(id)
);

INSERT INTO competition_schools (competition_id, school_id)
SELECT 
	competition_teams.competition_id,
	classroom_information.school_id
FROM
	competition_teams INNER JOIN groups ON competition_teams.group_id = groups.id
	INNER JOIN classroom_information ON groups.id = classroom_information.group_id
;

CREATE TABLE user_classrooms (
	user_id INTEGER NOT NULL REFERENCES users(id),
	classroom_id INTEGER NOT NULL REFERENCES classrooms(id),
	UNIQUE(user_id, classroom_id)
);

INSERT INTO user_classrooms (user_id, classroom_id)
SELECT
	user_groups.user_id,
	classrooms.id
FROM 
	user_groups INNER JOIN classrooms ON user_groups.group_id = classrooms.group_id
;

ALTER TABLE classrooms DROP COLUMN group_id;

ALTER TABLE children ADD COLUMN student_id CHARACTER VARYING;









DELETE FROM group_goals WHERE group_id IN (SELECT id FROM groups WHERE type_name = 'CLASS_ROOM');
DELETE FROM group_invitations WHERE group_id IN (SELECT id FROM groups WHERE type_name = 'CLASS_ROOM');
DELETE FROM group_join_points WHERE group_id IN (SELECT id FROM groups WHERE type_name = 'CLASS_ROOM');
DELETE FROM group_messages WHERE group_id IN (SELECT id FROM groups WHERE type_name = 'CLASS_ROOM');
DELETE FROM group_requests WHERE group_id IN (SELECT id FROM groups WHERE type_name = 'CLASS_ROOM');
DELETE FROM user_groups WHERE group_id IN (SELECT id FROM groups WHERE type_name = 'CLASS_ROOM');
DROP TABLE classroom_information;
DROP TABLE competition_teams;
DELETE FROM groups WHERE type_name = 'CLASS_ROOM';
DELETE FROM group_types WHERE name = 'CLASS_ROOM';
DROP TABLE cities;