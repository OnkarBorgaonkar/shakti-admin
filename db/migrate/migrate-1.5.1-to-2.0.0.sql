BEGIN;

ALTER TABLE quiz_answers DROP COLUMN quiz_question_option_id ;

ALTER TABLE users ADD COLUMN background_image INTEGER NOT NULL DEFAULT 1;
ALTER TABLE users ALTER COLUMN background_image DROP DEFAULT;

ALTER TABLE children ADD COLUMN academy_year INTEGER NOT NULL DEFAULT 1;
ALTER TABLE children ALTER COLUMN academy_year DROP DEFAULT;

CREATE TABLE hero_corner_users (
    user_id INTEGER NOT NULL REFERENCES users(id),
    hero_id INTEGER NOT NULL REFERENCES users(id),
    UNIQUE (user_id, hero_id)
);

CREATE TABLE hero_corner_invites (
    sender_id INTEGER NOT NULL REFERENCES users(id),
    recipient_id INTEGER NOT NULL REFERENCES users(id),
    message TEXT
);

ALTER TABLE quizzes ADD COLUMN academy_year INTEGER NOT NULL DEFAULT 1;
ALTER TABLE quizzes ALTER COLUMN academy_year DROP DEFAULT;
ALTER TABLE quizzes ADD COLUMN level INTEGER DEFAULT 1;
ALTER TABLE quizzes ALTER COLUMN level DROP DEFAULT;

ALTER TABLE libraries ADD COLUMN academy_year INTEGER NOT NULL DEFAULT 1;
ALTER TABLE libraries ALTER COLUMN academy_year DROP DEFAULT;
ALTER TABLE libraries ADD COLUMN level INTEGER DEFAULT 1;
ALTER TABLE libraries ALTER COLUMN level DROP DEFAULT;

CREATE TABLE academy_year_changes (
    user_id INTEGER NOT NULL REFERENCES users(id),
    academy_year INTEGER NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT NOW()
);

INSERT INTO academy_year_changes(user_id, academy_year, created) SELECT users.id, 1, children.created_at FROM users INNER JOIN children ON users.id = children.user_id;


CREATE TABLE orbs (
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING NOT NULL,
    "order" INTEGER NOT NULL,
    description CHARACTER VARYING,
    long_description TEXT,
    big_door_quest_id INTEGER NOT NULL UNIQUE,
    active BOOLEAN NOT NULL,
    academy_year INTEGER NOT NULL
);


CREATE TABLE user_orbs (
    id SERIAL PRIMARY KEY,
    orb_id INTEGER NOT NULL REFERENCES orbs(id),
    user_id INTEGER NOT NULL REFERENCES users(id),
    status CHARACTER VARYING NOT NULL,
    start_date TIMESTAMP,
    completed_date TIMESTAMP,
    big_door_level_id INTEGER NOT NULL,
    UNIQUE(orb_id, user_id)
);

CREATE TABLE user_story_responses (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL REFERENCES users(id),
    big_door_checkpoint_id INTEGER NOT NULL,
    title CHARACTER VARYING NOT NULL,
    description CHARACTER VARYING NOT NULL,
    response CHARACTER VARYING NOT NULL,
    created TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE big_door_checkpoints (
    id SERIAL PRIMARY KEY,
    big_door_checkpoint_id INTEGER NOT NULL,
    description TEXT
);

CREATE TABLE shopify_orders (
    id SERIAL NOT NULL,
    email CHARACTER VARYING NOT NULL,
    order_number CHARACTER VARYING NOT NULL,
    product_id CHARACTER VARYING NOT NULL,
    quantity INTEGER NOT NULL,
    activation_key CHARACTER VARYING NOT NULL,
    created TIMESTAMP NOT NULL,
    completed TIMESTAMP
);

ALTER TABLE districts DROP COLUMN contact_super_hero_name;
ALTER TABLE districts ADD COLUMN contact_role CHARACTER VARYING;
ALTER TABLE districts ADD COLUMN organization_type CHARACTER VARYING;
ALTER TABLE districts ADD COLUMN address CHARACTER VARYING;
ALTER TABLE districts ADD COLUMN city CHARACTER VARYING;
ALTER TABLE districts ADD COLUMN state CHARACTER VARYING;
ALTER TABLE districts ADD COLUMN zip CHARACTER VARYING;
ALTER TABLE districts ADD COLUMN number_of_schools CHARACTER VARYING;
ALTER TABLE districts RENAME COLUMN contract_users TO number_of_students;
ALTER TABLE districts ADD COLUMN number_of_students_in_program INTEGER;
ALTER TABLE districts ADD COLUMN interest CHARACTER VARYING;
ALTER TABLE districts ADD COLUMN sel_program BOOLEAN;
ALTER TABLE districts ADD COLUMN internet_access BOOLEAN;

ALTER TABLE schools ADD COLUMN contact_role CHARACTER VARYING;
ALTER TABLE schools ADD COLUMN organization_type CHARACTER VARYING;
ALTER TABLE schools ADD COLUMN number_of_students INTEGER;
ALTER TABLE schools ADD COLUMN number_of_students_in_program INTEGER;
ALTER TABLE schools ADD COLUMN interest CHARACTER VARYING;
ALTER TABLE schools ADD COLUMN sel_program BOOLEAN;
ALTER TABLE schools ADD COLUMN internet_access BOOLEAN;

--
--quiz data
--
--INSERT INTO quizzes (rank_id, academy_year, level) VALUES(4,2,1);
--INSERT INTO quizzes (rank_id, academy_year, level) VALUES(4,2,2);
--INSERT INTO quizzes (rank_id, academy_year, level) VALUES(4,2,3);
--INSERT INTO quizzes (rank_id, academy_year, level) VALUES(4,2,4);
--
----
---- library data
----
--INSERT INTO libraries (rank_id, academy_year, level) VALUES(4,2,1);
--INSERT INTO libraries (rank_id, academy_year, level) VALUES(4,2,2);
--INSERT INTO libraries (rank_id, academy_year, level) VALUES(4,2,3);
--INSERT INTO libraries (rank_id, academy_year, level) VALUES(4,2,4);


--
--orb data
--

INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Orb Tracker', 1, 'Be prepared orb', 'Preparation is important. Earning this orb will help you earn the other orbs. Take time to create your Orb Book. Use this book to keep notes about what you are learning. Remember, if you loose this one you can always create another one.', 5346001, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('The Ambassador Orb', 2, 'This Orb is all about being friendly to kids in your school.', 'Wouldn''t it be great if your school was always a friendly place for new students? Earning this orb will encourage you to meet new students, learn about them and include them in different school activities. This is what an ambassador does. C''mon! You can do it!', 5502000, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Mentor / Mentee Orb', 3, 'This orb teach you to ask for help with any of your subjects in school.', 'We all need help with school work sometimes. This orb is about learning to ask for help when you need it. It will require you to learn what a mentor is and then find someone to mentor you in a school subject. For example, math, science, reading, etc. This will make them your "Mentor" and you their "Mentee" (the person being mentored). ', 5512000, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Rope of Hope Orb', 4, 'The orb is all about HOPE!', 'Hope is a powerful emotion. It helps us believe that anything is possible! To earn this orb you are going to have to learn about the idea of HOPE. You will also have to learn about people who are Hopeful.', 5520000, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Who''s your Hero? Orb', 5, 'This orb helps you to learn about your family members.', 'Every now and then we need to stop and think about who are our real-life heroes or the role-models in our daily lives. Why do we look up to them? Have we picked them because they have positive qualities? This orb will help you decide what qualities make someone a powerful and positive role model.', 5518000, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Safety Orb', 6, 'This orb teaches you to safeguard yourself.', 'This orb encourages you to learn how to protect yourself from danger. To earn this orb, you will have to talk to your teachers, read articles about safety, and practice being safe in your home.', 5350000, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Word Warriors Orb', 7, 'This orb is all about words!', 'Reading opens you up to new places, new worlds, and new ideas. Reading, just like playing, painting, music and art, is a part of life. If you read books with your friends, these "new worlds" will be yours together! To earn this orb you have to have to read 3 books. And then tell us what you learn about these stories, and the power of words!', 5530000, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Green Machine Orb', 8, 'This is the "take care of the earth" orb!', 'This orb will help you learn ways to take care of the Earth - from turning off the water at home while you''re brushing your teeth to picking up litter in your school. You can do this! And our beautiful Earth needs your help!', 5532000, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('The THINKER Orb', 9, 'This is the "user your brain" orb!', 'This orb is going to make you THINK! To earn this orb, you are going to have to ask yourself and others, mind-boggling questions. You will need to be prepared to write answers down in your Orb Book.', 5534000, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Get Your Move On Orb', 10, 'This orb will get you up and moving!', 'Sometimes we all need someone or some thing to motivate us. Use this orb to help you to move your body in a healthy direction!', 5560000, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Learn This! Orb', 11, 'This orb will help you to stay focused on something you need to learn!', 'Did you know that there is a skill for learning? Learning does not always come easy for people. Many people have to work hard to learn how to "learn". This orb helps you to learn how to LEARN.', 5562000, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Attitude of Gratitude Orb', 12, 'This is the Positive Mental Attitude (PMA) orb!', 'Sure it is easy to get upset or feel down sometimes. But did you know that you can change that? Did you know that you have the power to change the way you feel? You can begin by learning about having an Attitude of GRATITUDE!', 5334001, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Give it up Orb', 13, 'This orb will require you to "give it up"!', 'This is going to be a tough orb to earn. You are going to have to "give up" something that you enjoy doing. This orb is all about disciplining yourself. And learning that you don''t always get everything you want, when you want it, and that is ok. So use this orb to practice saying NO to yourself and being ok with it. Remember, Warriors are very disciplined people! They know how and when to say NO!', 5564000, true, 2);
INSERT INTO orbs (name, "order", description, long_description, big_door_quest_id, active, academy_year) VALUES('Home Sweet Home Orb', 14, 'This orb is all about your family!', 'This orb is all about getting to know your family members better. And, for them to get to know you better.', 5568000, true, 2);


--
--big door checkpoing data
--

INSERT INTO big_door_checkpoints (big_door_checkpoint_id, description) VALUES('7830002', '<div class="checkpointWrap">Decorate your Orb Book. Spend at least 15 minutes decorating the front page of your Orb Book. You must include the following on the front of your book (or the first page).</br>
<ol>
<li>ORB BOOK</li>
<li>Your Name</li>
<li>Your Teacher''s Name</li>
<li>Your Super Hero''s Name</li>
<li>Your Super Human Powers</li>
<li>Your Super Hero Powers</li>
</ol>
</br>
When you have finished decorating your Orb Book type "Complete" in the space below.</div>
');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8184000', '<div class="checkpointWrap">In order to get to know them, ask this person some questions. Make sure to write these questions and answers in your Orb Book. Then you can tell us online.</br>

Questions for you to ask:
</br>
<ol>
<li>Where are you from?</li>
<li>Do you have any brothers or sisters? If yes, how old are they and what are their names?</li>
<li>Are you new to the area? If so, why did your family move here? If not, how long have you been here?</li>
<li>What is one thing you like about this area? (or school?)</li>
<li>What do you like to do for fun? What are some of your hobbies?</li>
</ol>
</br>
Also come up with a few of your own questions that you would like to know about this person.
</br>
Tell us what you learned about them.</div>
');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8204000', '<div class="checkpointWrap">In order to get to know them, ask this second person some questions. Make sure to write these questions and answers in your Orb Book so you''ll remember them later when you tell us online.</br>
Questions for you to ask:
</br>
<ol>"
<li>Where are you from?</li>
<li>Do you have any brothers or sisters? If yes, how old are they and what are their names?</li>
<li>Are you new to the area? If so, why did your family move here? If not, how long have you been here?</li>
<li>What is one thing you like about this area? (or school?)</li>
<li>What do you like to do for fun? What are some of your hobbies?</li>
</ol>
</br>
Also, come up with a few of your own questions that you would like to know about this person.
</br>
Then tell us what you learned about them. </div>
');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8212000', '<div class="checkpointWrap">In order to get to know them, ask this person some questions about themselves. Write these questions and answers in your Orb Book, so you can input your answers later.</br>

Questions:
</br>
<ol>
<li>Where are you from?</li>
<li>Do you have any brothers or sisters? If yes, how old are they and what are their names?</li>
<li>Are you new to the area? If so, why did your family move here? If not, how long have you been here?</li>
<li>What is one thing you like about this area? (or school?)</li>
<li>What do you like to do for fun? What are some of your hobbies?</li>
</ol>
</br>
Come up with a few of your own questions that you would like to know about this person.
</br>
Then tell us what you learned about them.</div>
');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8232000', '<div class="checkpointWrap">Then, ask the first person on your list if they would be willing to be your mentor. Explain to them what subject you want support with. Let them know that you would like to meet with them for 30 minutes each week for the next 3 weeks. You should plan to meet at the same place, same time and same day if possible. If this person is unable to be your mentor, it''s ok. Just select the next person on your list and repeat this process.  Keep asking until you have found your mentor. Then, in the space provided, tell us your mentor''s name and what days of the week and times you plan to meet. (For example, we will meet on Monday''s, after school, from 3:00 to 3:30).</div>');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8220001', '<div class="checkpointWrap">Tell us your definition of a role model. Also, tell us 3 of your favorite "famous" role models. Then, tell us 3 of your favorite role models that are "close to home". (These could be  family members, neighbors, school teachers, or others in your community. ) Then, write their names down in your Orb Book.</div>');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8208002', '<div class="checkpointWrap">Now, go back to two of the family members whose definition of a role model you agree with the MOST. Ask them to tell you who their role model was when they were your age. Then, write the family member''s name and their role model''s name down in your Orb Book so that you remember later. Once you have completed this checkpoint, type "Complete" in the space provided.</div>');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8226001', '<div class="checkpointWrap">Schedule some time to interview these two family members so that you can ask them the following questions. Make sure you write these questions and their answers in your Orb Book:</br>
<ol>
<li>When you were a child, why did you pick this role model?</li>
<li>Do you have a role model today (now that you''re older)?</li>
<li>Is it the same role model as when you were my age? If yes, then explain why.</li>
<li>If your role model is different now that you are older, who is your new role model? Why did you pick this role model now that you are older?</li>
<li>Do you think that all children my age should have a role model? Why or why not?</li>
</ol>
</br>
Once you have completed this checkpoint, type "Complete" in the space provided.</div>
');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8296000', '<div class="checkpointWrap">Set up a meeting with the other Word Warriors in your book club. Together, decide what topics you are all interested in reading about. And decide how long you should give yourselves to read each book. For example: We choose books about Aliens and we think it will take two weeks to read each book. Choose the specific books you are going to read. Tell us the titles of each book you are going to read.</div>');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8298000', '<div class="checkpointWrap">About book 1. In the space provided, tell us about book 1.</br>
<ul>
<li>What was the story about?</li>
<li>Was there a lesson that you learned from reading this book?</li>
<li>Did this book help you? Did this book scare you? Did this book make you laugh? etc.</li>
<li>Did you like this book?</li>
<li>Would you tell someone else to read this book? If yes, why? If not, why not?</li>
<li>What did you learn about the other Word Warriors in your book club after reading this book?</li>
</ul>
</div>
');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8300000', '<div class="checkpointWrap">About book 2. In the space provided, tell us about book 2.</br>
<ul>
<li>What was the story really about?</li>
<li>Was there a lesson that you learned from reading this book?</li>
<li>Did this book help you? Did this book scare you? Did this book make you laugh? etc.</li>
<li>Did you like this book?</li>
<li>Would you tell someone else to read this book? If yes, why? If not, why not?</li>
<li>What did you learn about the other Word Warriors in your book club after reading this book?<li>
</ul>
</div>
');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8302000', '<div class="checkpointWrap">About book 3. In the space provided, tell us about book 3.</br>
<ul>
<li>What was the story really about?</li>
<li>Was there a lesson that you learned from reading this book?</li>
<li>Did this book help you? Did this book scare you? Did this book make you laugh? etc.</li>
<li>Did you like this book? </li>
<li>Would you tell someone else to read this book? If yes, why? If not, why not?</li>
<li>What did you learn about the other Word Warriors in your book club after reading this book?</li>
<ul>
</div>
');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8310000', '<div class="checkpointWrap">Week 1: For 7 days, check off 3 things that you did from the GREEN MACHINE checklist. Do at least 3 things each day for all 7 days.</br>

If you forget to do something one day - that''s ok. But you won''t be able to count that day in your quest.
</br>
When you have finished being "green" for 7 days, type "Complete" in the space provided.</div>
');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8310001', '<div class="checkpointWrap">Week 3: For 7 days, check off 3 things that you did from the GREEN MACHINE checklist. Do at least 3 things each day for all 7 days.</br>

If you forget to do something one day - that''s ok. But you won''t be able to count that day in your quest.
</br>
When you have finished being "green" for 7 days, type "Complete" in the space provided.</div>
');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8384000', '<div class="checkpointWrap">Week 7: Reuse the same plastic water bottle for 2 days. Then throw it away. Repeat this step every 2 days for one whole week. Note: if you reuse a water bottle every 2 days, after a week you would have used 4 bottles. When you have done this, type "Complete" in the space provided.</div>');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8390000', '<div class="checkpointWrap">Question 1: What do you think would happen if you saved half of all the money you received (as a gift, or your allowance, etc.) from now until when you graduated high school? How much money do you think you would have? (Take a guess.) What would you be able to buy with this amount of money? In the space provided, tell us what you think.</div>');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8406000', '<div class="checkpointWrap">Checkpoint 2: Do 25 pushups. (You can do them with your knees on the floor or using straight legs with your knees off the floor). Make sure you are using proper form. If you are unsure of your form, ask someone. You don''t have to do them all at one time.  But complete them all by the end of the day. Once you have completed this checkpoint, type "Complete" in the space provided. Then wait for the next day to start your next fitness move.</div>');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8318002', '<div class="checkpointWrap">It''s time to learn how to LEARN. It''s time to write an Action Plan for the subject you are having a hard time with. For the next week, ask others to help you write your plan. Talk to teachers, friends, counselors who can help you. Make sure you trust them. Write your plan down in your Orb Book. These actions can be things like: Studying an extra 30 minutes each night. Or, meeting with a tutor. Or, looking up information you need to learn on the internet. Or, going to the library to read a book that might help you. Or, writing down all of the questions you have about this subject. Create at least 5 different actions that might help you LEARN this challenging subject! When you''ve completed this Action Plan, type "Complete" in the space provided.</div>');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8368003', '<div class="checkpointWrap">Week 1. Spend ONE WHOLE WEEK taking at least two of these actions EVERY DAY. This should make your most challenging subject easier to LEARN. If your ACTION PLAN includes the weekend, then take action on the weekend! Keep your Orb Book close by. Write down all your steps so that you''ll remember later! When you''ve completed doing your actions this week, type the word "Complete" in the space provided.</div>');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8282002', '<div class="checkpointWrap">Week 2. Spend ANOTHER WHOLE WEEK learning. Take at least two of these actions EVERY DAY. Remember, If your ACTION PLAN includes the weekend, then do these actions on the weekend! Keep your Orb Book close by. Write down all your steps so that you''ll remember later! When you''ve completed doing your actions this week, type the word "Complete" in the space provided.</div>');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8330002', '<div class="checkpointWrap">This is going to be a tough orb to earn. You are going to have to "give up" something that you enjoy doing. This orb is all about disciplining yourself.  And learning that you don''t always get everything you want, when you want it - AND that is ok. So use this orb to practice saying NO to yourself and being ok with it. Remember, Warriors are very disciplined people! They know how and when to say NO!</div>');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8404001', '<div class="checkpointWrap">Now comes the hard part. Take a moment and review your list. Pick one item from your list that you know you should "give up" for 7 days because you''d be a stronger person if you did. Do not pick something just because it is easy. Pick something that will really challenge yourself. Are you prepared to give this one thing up for 7 days? And we mean give it up completely. Tell us your answer Yes or No?</div>');

INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8396002', '<div class="checkpointWrap">Have a family sit-down dinner this week. Take time to help prepare the meal. Or, you can help set the table. During dinner ask your family members questions about themselves. Questions like: What did they want to be when they grew up? What was their favorite childhood memory? Who was their favorite friend, and why? Did they have a favorite pet? Remember, this is time for you to learn about them. After you have completed this checkpoint, in the space provided, tell us what you learned.</div>');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8422001', '<div class="checkpointWrap">Look in newspapers, magazines, on television, on the Internet, etc., to find examples of people that talk about being "grateful". Does this for 3 days. Write these examples down in your Orb Book. (For example: I saw a story on the news about a family. The family lost their home in a storm. But, the mom said that she was just "grateful that her family survived!"). When finished, type "Complete" in the space provided.</div>');
INSERT INTO big_door_checkpoints(big_door_checkpoint_id, description) VALUES('8322002', '<div class="checkpointWrap">Review the list of people that you are grateful for. Find a way to tell them that you are grateful for them (For example, you can tell them in person, over the phone, via text, etc.) Tell each of them the things you listed that you''re grateful for about them each time you speak with them. Tell us how did they respond to your gratitude?</div>');

--
-- library data
--

UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=35919&cat_id=20514', pages = 3 WHERE name = 'Fitness 1' AND library_id = 1;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22662&cat_id=20185', pages = 3 WHERE name = 'Fitness 2' AND library_id = 1;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10255&cat_id=20512', pages = 2 WHERE name = 'Nutrition 1' AND library_id = 1;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=42970&cat_id=20512', pages = 1 WHERE name = 'Nutrition 2' AND library_id = 1;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10267&cat_id=118', pages = 4 WHERE name = 'Fitness 1' AND library_id = 2;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=20262&cat_id=120', pages = 4 WHERE name = 'Fitness 2' AND library_id = 2;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10258&cat_id=20512', pages = 2 WHERE name = 'Nutrition 1' AND library_id = 2;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=20520&cat_id=20512', pages = 2 WHERE name = 'Nutrition 2' AND library_id = 2;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=76817&cat_id=20071', pages = 3 WHERE name = 'Fitness 1' AND library_id = 3;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10262&cat_id=20512', pages = 1 WHERE name = 'Nutrition 1' AND library_id = 3;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=34326&cat_id=20512', pages = 2 WHERE name = 'Nutrition 2' AND library_id = 3;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10256&cat_id=20512', pages = 3 WHERE name = 'Nutrition 1' AND library_id = 4;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10618&cat_id=20512', pages = 2 WHERE name = 'Nutrition 2' AND library_id = 4;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22421&cat_id=20070', pages = 3 WHERE name = 'Cyber Safety 2' AND library_id = 2;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 2 WHERE name = 'Environmental Sustainability 1' AND library_id = 3;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 2 WHERE name = 'Environmental Sustainability 2' AND library_id = 3;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 2 WHERE name = 'Cyber Safety 1' AND library_id = 4;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 2 WHERE name = 'Cyber Safety 2' AND library_id = 4;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22505&cat_id=20070', pages = 1 WHERE name = 'Cyber Safety 1' AND library_id = 1;
UPDATE library_articles SET article_type = 'URL', url = NULL, pages = 3 WHERE name = 'Cyber Safety 2' AND library_id = 1;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 2 WHERE name = 'Non Violence 1' AND library_id = 1;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 2 WHERE name = 'Non Violence 2' AND library_id = 1;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 1 WHERE name = 'Mind Up 1' AND library_id = 1;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 1 WHERE name = 'Mind Up 2' AND library_id = 1;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=59587&cat_id=115', pages = 3 WHERE name = 'Environmental Sustainability 1' AND library_id = 1;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 1 WHERE name = 'Environmental Sustainability 2' AND library_id = 1;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22314&cat_id=20071', pages = 2 WHERE name = 'Cyber Safety 1' AND library_id = 2;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 1 WHERE name = 'Non Violence 1' AND library_id = 2;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22066&cat_id=20068', pages = 1 WHERE name = 'Non Violence 2' AND library_id = 2;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 1 WHERE name = 'Mind Up 1' AND library_id = 2;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 2 WHERE name = 'Mind Up 2' AND library_id = 2;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 1 WHERE name = 'Environmental Sustainability 1' AND library_id = 2;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 1 WHERE name = 'Environmental Sustainability 2' AND library_id = 2;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=62242&cat_id=120', pages = 1 WHERE name = 'Fitness 2' AND library_id = 3;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=10623&cat_id=115', pages = 2 WHERE name = 'Cyber Safety 1' AND library_id = 3;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=61559&cat_id=115', pages = 3 WHERE name = 'Cyber Safety 2' AND library_id = 3;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22021&cat_id=20069', pages = 2 WHERE name = 'Non Violence 1' AND library_id = 3;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22400&cat_id=20070', pages = 4 WHERE name = 'Non Violence 2' AND library_id = 3;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 1 WHERE name = 'Mind Up 1' AND library_id = 3;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 1 WHERE name = 'Mind Up 2' AND library_id = 3;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22521&cat_id=20512', pages = 2 WHERE name = 'Fitness 1' AND library_id = 4;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=22514&cat_id=115', pages = 3 WHERE name = 'Fitness 2' AND library_id = 4;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=40305&cat_id=20069', pages = 4 WHERE name = 'Non Violence 1' AND library_id = 4;
UPDATE library_articles SET article_type = 'URL', url = 'http://kidshealth.org/PageManager.jsp?lic=444&dn=HealthyHeroes&article_set=43408&cat_id=20070', pages = 3 WHERE name = 'Non Violence 2' AND library_id = 4;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 2 WHERE name = 'Mind Up 1' AND library_id = 4;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 1 WHERE name = 'Mind Up 2' AND library_id = 4;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 2 WHERE name = 'Environmental Sustainability 1' AND library_id = 4;
UPDATE library_articles SET article_type = 'PDF', url = NULL, pages = 2 WHERE name = 'Environmental Sustainability 2' AND library_id = 4;


