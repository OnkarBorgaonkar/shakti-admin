BEGIN;

INSERT INTO point_types VALUES ('ORB'), ('ORB_CHECKPOINT');

ALTER TABLE orbs ADD COLUMN points INTEGER NOT NULL DEFAULT 500;
ALTER TABLE orbs ALTER COLUMN points DROP DEFAULT;

CREATE TABLE orb_checkpoint_types (
    name CHARACTER VARYING PRIMARY KEY
);

INSERT INTO orb_checkpoint_types (name) VALUES('FILL_IN_THE_BLANK'),('STORY'),('MULTIPLE_CHOICE'),('LINK');

ALTER TABLE big_door_checkpoints RENAME TO orb_checkpoints;
ALTER TABLE big_door_checkpoints_id_seq RENAME TO orb_checkpoints_id_seq;

ALTER TABLE orb_checkpoints ADD COLUMN orb_id INTEGER NOT NULL REFERENCES orbs(id) DEFAULT 1;
ALTER TABLE orb_checkpoints ALTER COLUMN orb_id DROP DEFAULT;
ALTER TABLE orb_checkpoints ADD COLUMN name CHARACTER VARYING NOT NULL DEFAULT 'fake name';
ALTER TABLE orb_checkpoints ALTER COLUMN name DROP DEFAULT;
ALTER TABLE orb_checkpoints ADD COLUMN "type" CHARACTER VARYING DEFAULT 'FILL_IN_THE_BLANK';
ALTER TABLE orb_checkpoints ALTER COLUMN "type" DROP DEFAULT;
ALTER TABLE orb_checkpoints ADD COLUMN active BOOLEAN DEFAULT true;
ALTER TABLE orb_checkpoints ALTER COLUMN active DROP DEFAULT;
ALTER TABLE orb_checkpoints ADD COLUMN "order" INTEGER DEFAULT 0;
ALTER TABLE orb_checkpoints ALTER COLUMN "order" DROP DEFAULT;
ALTER TABLE orb_checkpoints ADD COLUMN url CHARACTER VARYING;
ALTER TABLE orb_checkpoints ADD COLUMN seconds INTEGER;
ALTER TABLE orb_checkpoints ADD COLUMN answer CHARACTER VARYING;
ALTER TABLE orb_checkpoints ADD COLUMN points INTEGER NOT NULL DEFAULT 150;
ALTER TABLE orb_checkpoints ALTER COLUMN points DROP DEFAULT;

CREATE TABLE orb_checkpoint_options(
    id SERIAL PRIMARY KEY,
    orb_checkpoint_id INTEGER NOT NULL REFERENCES orb_checkpoints (id),
    name CHARACTER VARYING NOT NULL,
    correct BOOLEAN NOT NULL,
    "order" INTEGER NOT NULL
);

CREATE TABLE user_orb_checkpoints (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL REFERENCES users (id),
    orb_checkpoint_id INTEGER NOT NULL REFERENCES orb_checkpoints (id),
    completed_at TIMESTAMP NOT NULL DEFAULT now()
);

ALTER TABLE user_story_responses ADD COLUMN orb_checkpoint_id INTEGER NOT NULL REFERENCES orb_checkpoints (id) DEFAULT 1;
ALTER TABLE user_story_responses ALTER COLUMN orb_checkpoint_id DROP DEFAULT;

COMMIT;