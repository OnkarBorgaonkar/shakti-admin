BEGIN;

CREATE TABLE admin_users (
    id SERIAL PRIMARY KEY,
    prefix CHARACTER VARYING NOT NULL,
    email CHARACTER VARYING NOT NULL UNIQUE,
    first_name CHARACTER VARYING NOT NULL,
    last_name CHARACTER VARYING NOT NULL,
    password CHARACTER VARYING,
    active BOOLEAN NOT NULL,
    activation_key CHARACTER VARYING,
    created TIMESTAMP NOT NULL DEFAULT now()
);

INSERT INTO roles VALUES('district'),('school'),('classroom');

CREATE TABLE admin_user_roles (
    admin_user_id INTEGER NOT NULL REFERENCES admin_users (id),
    role CHARACTER VARYING NOT NULL REFERENCES roles (name),
    UNIQUE (admin_user_id, role)
);

CREATE TABLE admin_user_districts (
    admin_user_id INTEGER NOT NULL REFERENCES admin_users (id),
    district_id INTEGER NOT NULL REFERENCES districts (id)
);

CREATE TABLE admin_user_schools (
    admin_user_id INTEGER NOT NULL REFERENCES admin_users (id),
    school_id INTEGER NOT NULL REFERENCES schools (id)
);

CREATE TABLE admin_user_classrooms (
    admin_user_id INTEGER NOT NULL REFERENCES admin_users (id),
    classroom_id INTEGER NOT NULL REFERENCES classrooms (id)
);

CREATE TABLE programs (
    id SERIAL PRIMARY KEY,
    school_id INTEGER NOT NULL REFERENCES schools (id),
    name CHARACTER VARYING NOT NULL,
    start_date DATE NOT NULL,
    end_date DATE NOT NULL,
    computers INTEGER NOT NULL,
    located_in CHARACTER VARYING NOT NULL,
    computer_type CHARACTER VARYING NOT NULL
);

CREATE TABLE program_not_in_session_dates (
    id SERIAL PRIMARY KEY,
    program_id INTEGER NOT NULL REFERENCES programs (id),
    start_date DATE NOT NULL
);

CREATE TABLE lesson_plans (
    id SERIAL PRIMARY KEY,
    week INTEGER NOT NULL,
    description TEXT NOT NULL,
    things_to_remember TEXT NOT NULL,
    preperation TEXT NOT NULL
);

CREATE TABLE web_activity_entries (
    id SERIAL PRIMARY KEY,
    week INTEGER NOT NULL,
    academy_year INTEGER NOT NULL,
    name CHARACTER VARYING NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE weekly_reminders (
    id SERIAL PRIMARY KEY,
    week INTEGER NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE as_time_permits_entries (
    id SERIAL PRIMARY KEY,
    week INTEGER NOT NULL,
    description TEXT NOT NULL
);


INSERT INTO programs (school_id, name, start_date, end_date, computers, located_in, computer_type) SELECT id, 'old session', '2012-09-01', '2013-06-01', 5, 'something', 'something' FROM schools;

ALTER TABLE classrooms ADD COLUMN program_id INTEGER REFERENCES programs (id);
UPDATE classrooms SET program_id = (SELECT id FROM programs WHERE programs.school_id = classrooms.school_id);
ALTER TABLE classrooms ALTER COLUMN program_id SET NOT NULL;

ALTER TABLE classrooms DROP COLUMN school_id;

ALTER TABLE districts ADD COLUMN active BOOLEAN NOT NULL DEFAULT true;
ALTER TABLE districts ALTER COLUMN active DROP DEFAULT;
ALTER TABLE districts ADD COLUMN join_date DATE NOT NULL DEFAULT now();
ALTER TABLE districts ALTER COLUMN join_date DROP DEFAULT;
ALTER TABLE districts DROP COLUMN contract_start_date;
ALTER TABLE districts DROP COLUMN contract_end_date;
ALTER TABLE districts DROP COLUMN passcode;
ALTER TABLE districts DROP COLUMN number_of_students;

ALTER TABLE districts RENAME COLUMN contact_first_name TO hha_contact_first_name;
ALTER TABLE districts RENAME COLUMN contact_last_name TO hha_contact_last_name;
ALTER TABLE districts RENAME COLUMN contact_email TO hha_contact_email;
ALTER TABLE districts RENAME COLUMN contact_work_phone TO hha_contact_phone;

ALTER TABLE schools ADD COLUMN active BOOLEAN NOT NULL DEFAULT true;
ALTER TABLE schools ALTER COLUMN active DROP DEFAULT;
ALTER TABLE schools ADD COLUMN join_date DATE NoT NULL DEFAULT now();
ALTER TABLE schools ALTER COLUMN join_date DROP DEFAULT;
ALTER TABLE schools DROP COLUMN passcode;



ALTER TABLE classrooms DROP COLUMN passcode;
ALTER TABLE classrooms DROP COLUMN contact_prefix;
ALTER TABLE classrooms DROP COLUMN contact_first_name;
ALTER TABLE classrooms DROP COLUMN contact_last_name;
ALTER TABLE classrooms DROP COLUMN contact_super_hero_name;
ALTER TABLE classrooms DROP COLUMN contact_email;
ALTER TABLE classrooms DROP COLUMN contact_work_phone;
ALTER TABLE classrooms DROP COLUMN contact_cell_phone;
ALTER TABLE classrooms DROP COLUMN grade_type;

INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(1, 'Student Workbooks: Section 1', 'There is no lesson plan for this week, instead, students will be writing about their SuperHERO character descriptions, Alter-Ego, reflecting on the Minion that hovers around them that they''d most like to change, and deciding what good work they''d like to do in the world now that they''ve committed to their Warrior Training!', 'Student Workbooks: Section 1, pens / pencils, SHAKTI Warriors, ''Back-stories'' (included in the SW Kit).');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(2, 'Heroes Journals', 'These Heroes Journals will be used on a continuous basis as an additional tool for student self-reflections. Incorporate time for students to sit quietly with their Heroes Journals. For example: students can write during the first 10 minutes of every SHAKTI Warriors session as other students are arriving; when a student is acting disruptive or disrespectful as a way for them to reflect about their behavior and ways to act more positively the next time; during specified Lesson Plan activities, etc.', 'Be sure that you have one spiral-bound notebook PER CHILD (or at least 10 stapled notebook pages for each child). A variety of arts & crafts supplies.');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(3, 'Writing Your Super hero', 'A key objective to the lesson, "Writing Your Superhero", is to support the development of student writing. It is important, then, for you to help them develop complete sentences throughout their writing process. A complete sentence has three parts: 1) It begins with a Capital letter; 2) There is an "end mark" at the end of every complete sentence (a period, question mark, exclamation point); 3) A complete sentence contains a NOUN (person, place, thing, idea), and a VERB (an action word). Make sure to have students (in groups and with your oversight) review and revise every sentence they write in preparation for entering this information in the SHAKTI Online Academy SUPERHERO / Avatar creation pages.', 'Chart paper (or white board), index cards (or paper), pens / pencils, and Student Workbooks: Section 1. ');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(4, 'Cut Out Chaos', 'It is important for you to walk from group to group as students identify various types of ''chaos'' they find in the magazines, and other periodicals, and help them summarize with each other the reasons WHY they chose different pictures as representations of ''chaos'', as well as ideas they have for solving these various types of ''chaos''.', 'Magazines, newspapers, scissors, glue, construction paper, and an open table or counter for students to use as a work station.');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(5, 'Escape from Camp Chaos', 'There are two parts to this lesson and two sets of prompting and reflective questions - be sure to read through the lesson prior to engaging with your students in order to be best prepared.', 'Student''s "Heroes Journals", pens / pencils, an open space to play the ‘Dodgeball’-type game, and foam balls (if available) - if not, then try to get other bouncing game balls that are NOT too hard.');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(6, 'Warrior Actions', 'This lesson will take at least two SHAKTI sessions to complete, then you should revisit this lesson in about two weeks in order to check in with students on their Warrior Actions progress.', 'You will need to go to the following website as a reference for students: http://www.choosemyplate.gov/physical-activity/calories-burn.html. You will need: calculators and graph paper (if possible), otherwise notebook paper, and math skills, pencils, and the Activity Chart, which is located in the lesson plan book, Warrior Actions, page 7.');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(7, 'Hero''s Recipe', 'This lesson is a great way for students to learn about the myplate guidelines for eating a balanced, healthy meal whether breakfast, lunch, dinner or snacks. Encourage them to prepare their recipe at home with their families and perhaps even bring a meal in for a SHAKTI Warriors: SuperHERO Feast!', 'You will need to go to the following website as a reference for students: http://recipefinder.nal.usda.gov. You will need paper, markers, pens / pencils, and a copy of the recipe template example, which is located in the lesson plan book, A Hero''s Recipe, page 4.');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(8, 'Turn That Tease Around', 'This lesson will take at least two SHAKTI sessions to complete with your students and will require some pre-planning and student discussion guidelines (refer to the CCCS for Speaking and Listening: the codes are located in the lesson plan book, Turn That Tease Around, page 74),  and support from you.  ', 'You will need to print and read the following two articles (website links below) prior to experiencing this lesson with your students: "Emotional Vocabulary" http://www.micheleborba.com/Pages/ArtBMI09.htm   "Teaching Kids How to Handle Teases and Put Downs" http://www.micheleborba.com/Pages/ArtNLM09.htm. You will also need: Index cards or sheets of notebook paper cut in half, pens / pencils, and one large piece of chart paper or one poster-board per group.');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(9, 'Unknotting Chaos', 'This is a fun activity for students but it is also a great way to use a physical activity to create discussions about strategies students can use in a group to solve problems, resolve conflict in a positive way, and ultimately, overcome ''chaos''.', 'No materials are required.');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(10, '"Rude-ster"… "Right-ster"', 'Prior to engaging in this lesson with your students, you will need to develop at least 10-15 ''rude sayings'' that you commonly hear your students saying to each other.', 'Index cards, pen / pencil.');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(11, 'My Hero''s Voice', 'This lesson will most likely take two, back-to-back SHAKTI sessions with your students in order to completely experience both parts of the lesson with your students. For the first part, students will be writing in their HEROES JOURNALS in preparation for part 2. ', 'An open space, a towel for each student or a large mat for students to sit on, HEROES JOURNALS, and a few empowerment phrases you wrote, prior to part 2 of this lesson, to use as examples for students when the write their own empowerment phrases.');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(12, 'Dear BYSTANDER… This is your UPSTANDER', 'It is very important that your students feel safe and comfortable discussing issues and feelings they have when it comes to bullying situations. At this point in the SHAKTI sessions, all of you should have a strong foundational knowledge base when it comes to dealing with things like bullying, AKA: ''Chaos''. Refer to the Emotional Vocabulary and the ABC-Listing charts that your students created when you engaged in "Turn that Tease Around" from lesson #7, from SHAKTI''s Top 13.', 'You will need to print the article from the following website link: http://www.micheleborba.com/blog/2011/02/23/mobilizing-bystanders-to-stop-bulling-6-teachable-skills-to-stop-a-bully/. Be sure to bring and display the "Six Bully B.U.S.T.E.R Strategies" from the website article. You will also need students to have their Heroes Journals, pens / pencils, one large piece of chart paper (or a white board), and markers.');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(13, '"Bully Schmully"', 'Take time to discuss with students the idea of "Balance of Power" noted in the lesson plan and be sure to use some of the examples offered in the Prompting Questions.', 'Large poster board (at least two pieces taped or stapled together horizontally), a general layout of the school and school campus, red, yellow and green circle stickers, pens, markers, crayons, pencils, and any additional arts and crafts supplies available.');
INSERT INTO lesson_plans (week, description, things_to_remember, preperation) VALUES(14, 'Cooling Down Our School', 'This lesson MUST be completed in two, back-to-back sessions. Do not tell students that they will be labeling the bullying ''hot spots'' when you are experiencing session 1 (part 1) of this lesson. This knowledge might limit students when they are creating the map of their school and school campus. Instead, tell students this information when they arrive to complete session 2 (part 2) when you are discussing Prompting Questions for session 2 (part 2) with them.', 'Schools will be ending this week; therefore, there is no specfic lesson preparation to note. ');

INSERT INTO web_activity_entries (week, academy_year, name, description) VALUES (3, 1, 'Online Student ENTRANCE Assessment', 'Once students have created their username and password, they must complete an Online Student ENTRANCE assessment. This assessment is organized into four sections: Hero''s Body, Mind, Heart, and Community. Each section includes 10 simple Yes / No / Maybe questions. Your role is to assist students as they answer these questions.');
INSERT INTO web_activity_entries (week, academy_year, name, description) VALUES (3, 1, 'JOIN the SHAKTI Warriors ONLINE Academy. ', 'Make sure that you printed the SHAKTI Student Roster prior to student arrival. To begin, tell every student to gather their Student Workbook: Section 1, a piece of paper and a pen / pencil. Then, organize each student to sit at their assigned computer. They will open the internet browser, and type: www.shaktiwarriors.com. Tell them to click on the ICON, ''JOIN NOW''. On the next page, they will be asked to type in their basic information, such as: Their name, age, etc. Once they do this, they must now create a SUPERHERO Name, and USER PASSWORD. Tell your students to write their superhero name and password on the piece of paper you had them bring to the computer area. Your role is to walk around to every student and write their SUPERHERO name, and password next to each of your student''s STUDENT ID number generated from your school.');
INSERT INTO web_activity_entries (week, academy_year, name, description) VALUES (4, 1, 'SuperHERO descriptions and Digital Avatars', 'Students should have their written SuperHERO descriptions from the "Writing Your SuperHERO" lesson plan. Today they will write these descriptions and character traits on their SHAKTI Online Academy webpage. As each student completes their online writing, your role is to read / review everything they type. Once you decide that their writing meets the criteria from the "Writing Your Superhero" lesson plan, tell them they are APPROVED for PUBLICATION! Students will also create their digital Avatar creations once you have approved their Online SuperHERO writing. ');
INSERT INTO web_activity_entries (week, academy_year, name, description) VALUES (5, 1, 'Earning Points for Hero''s Body, Mind, Heart & Community… Use the ECHO Board Too!', 'A. Students will need their Student Workbooks: Section 2, and begin entering their points Online from the Hero''s Body, Mind, Heart, and Community categories from their weekly point tracking. B. Students should also begin to read the Online articles and answer questions on the ECHO Board to earn additional points to move up the rank toward Superhero status.');
INSERT INTO web_activity_entries (week, academy_year, name, description) VALUES (6, 1, 'Continue Earning Points…And, Don''t Forget the ECHO Board!', 'A. Students will need their Student Workbooks: Section 2, and continue entering their points Online from the Hero''s Body, Mind, Heart, and Community categories from their weekly point tracking. B. Students should also continue to read the Online articles and answer questions on the ECHO Board to earn additional points toward.');
INSERT INTO web_activity_entries (week, academy_year, name, description) VALUES (8, 1, 'Student Online EXIT Assessment', 'During the last week of the SHAKTI Warriors program at your school, students must complete the Online Student EXIT assessment. This assessment is the same as the Online Student ENTRANCE assessment they completed during the first week of the program. Again, it is organized into four sections: Hero''s Body, Mind, Heart, and Community. Each section includes 10 simple Yes / No / Maybe questions. Your role is to assist students as they answer these questions.');

INSERT INTO web_activity_entries (week, academy_year, name, description) VALUES (3, 2, 'Online Student ENTRANCE Assessment', 'Once students have created their username and password, they must complete an Online Student ENTRANCE assessment. This assessment is organized into four sections: Hero''s Body, Mind, Heart, and Community. Each section includes 10 simple Yes / No / Maybe questions. Your role is to assist students as they answer these questions.');
INSERT INTO web_activity_entries (week, academy_year, name, description) VALUES (3, 2, 'Create the ORB Books.', 'A. Much like the Hero''s Journal, using a single-subject notebook, ''Second Year Academy Students'' will create an ORB Book in order to track the actions they need to take for each step in their ORB Quests. They should create an ORB Book that reflects their SuperHERO qualities, SuperHUMAN qualities, and any positive message they want to create on the front and / or back cover of their ORB Books. B. Once students have created their ORB Books, they must log-on to the website, type their Username (Superhero Name) and password, choose the ''Orb Tracker'' ORB, and click COMPLETE!');
INSERT INTO web_activity_entries (week, academy_year, name, description) VALUES (4, 2, 'ORBs', 'A. Students should choose their first ORB to complete now that they''ve completed their ORB Book. B. Remind students to really read about the ORB and the steps needed for completion before they choose to begin the ORB Quests.');
INSERT INTO web_activity_entries (week, academy_year, name, description) VALUES (5, 2, 'ORBs, Point Tracking, and the ECHO Board.', 'A. Students should use their ORB Books every time they go to their Second Year Online Academy webpage to enter each completed ORB Quest. B. Students should use their Student Workbook: Section 2 to enter all points earned from the Hero''s Body, Mind, Heart, and Community categories. C. Students should read the Online articles and answer questions from the ECHO Board to earn additional points.');
INSERT INTO web_activity_entries (week, academy_year, name, description) VALUES (8, 2, 'Student Online EXIT Assessment', 'During the last week of the SHAKTI Warriors program at your school, students must complete the Online Student EXIT assessment. This assessment is the same as the Online Student ENTRANCE assessment they completed during the first week of the program. Again, it is organized into four sections: Hero''s Body, Mind, Heart, and Community. Each section includes 10 simple Yes / No / Maybe questions. Your role is to assist students as they answer these questions.');

INSERT INTO weekly_reminders (week, description) VALUES(3, 'Once students have created their username and password for their webpage, students must complete the Online Student ENTRANCE Assessment.');
INSERT INTO weekly_reminders (week, description) VALUES(3, 'Allow time for student to write in their Heroes Journals. ');
INSERT INTO weekly_reminders (week, description) VALUES(3, 'Hand out Trading Cards for positive student actions.');
INSERT INTO weekly_reminders (week, description) VALUES(3, 'Use the "TIME-INs" for students to self-reflect, slow down with their breathing, etc. ');
INSERT INTO weekly_reminders (week, description) VALUES(4, 'Remind students to track their points at home everyday using the Point Tracking pages from their Student Workbook: Section 2.');
INSERT INTO weekly_reminders (week, description) VALUES(5, 'Remind students to get their parent / guardian to sign their completed point tracking pages from their Student Workbook: Section 2 at the end of every week.');
INSERT INTO weekly_reminders (week, description) VALUES(5, 'Tell students to periodically review their SuperHERO back-story and descriptions and edit to make their writing better, update their changing ideas about superHEROES, fix spelling errors, etc: Have students work in groups of two.');
INSERT INTO weekly_reminders (week, description) VALUES(5, 'Remind Second Year Academy Students to continue writing their ORB Quest steps in their ORB Books and use this information when they enter their ORB completions on their webpage.');
INSERT INTO weekly_reminders (week, description) VALUES(6, 'When experiencing lesson plans, remind students to include the work they are creating and goals they are monitoring when they enter their points earned Online.');
INSERT INTO weekly_reminders (week, description) VALUES(8, 'Some schools will be ending around week eight, while other schools will end later. Either way, during the last week of the SHAKTI Warriors program at your school, students must complete the Online Student EXIT Assessment.');

INSERT INTO as_time_permits_entries (week, description) VALUES(2, 'Use the Supplemental Materials in Section 3 of the Student Workbooks');
INSERT INTO as_time_permits_entries (week, description) VALUES(3, 'Introduce the Board Game');
INSERT INTO as_time_permits_entries (week, description) VALUES(5, 'Encourage students to read the comic books in reading groups');
INSERT INTO as_time_permits_entries (week, description) VALUES(5, 'Play the Board Game with students');
INSERT INTO as_time_permits_entries (week, description) VALUES(6, 'Allow students to play the various SHAKTI Games located in the Online Academy as ''Arcade''');
INSERT INTO as_time_permits_entries (week, description) VALUES(8, 'Explore other lessons in the Lesson Plan Book once students have experienced all of SHAKTI''s Top 13');

COMMIT;