BEGIN;

ALTER TABLE classrooms ADD COLUMN computers INTEGER;
UPDATE classrooms SET computers = (SELECT computers FROM programs WHERE programs.id = classrooms.program_id);
ALTER TABLE classrooms ALTER COLUMN computers SET NOT NULL;

ALTER TABLE classrooms ADD COLUMN computers_located_in CHARACTER VARYING;
UPDATE classrooms SET computers_located_in = (SELECT located_in FROM programs WHERE programs.id = classrooms.program_id);
ALTER TABLE classrooms ALTER COLUMN computers_located_in SET NOT NULL;

CREATE TABLE computer_types (
    name CHARACTER VARYING PRIMARY KEY
);

INSERT INTO computer_types VALUES('PC_DESKTOP'), ('PC_LAPTOP'), ('MAC_DESKTOP'), ('MAC_LAPTOP'), ('OTHER');

CREATE TABLE classroom_computer_types (
    classroom_id INTEGER NOT NULL REFERENCES classrooms (id),
    computer_type CHARACTER VARYING REFERENCES computer_types (name),
    other_value CHARACTER VARYING
);

CREATE TABLE classroom_days (
    classroom_id INTEGER NOT NULL REFERENCES classrooms (id),
    day CHARACTER VARYING NOT NULL,
    unique (classroom_id, day)
);

ALTER TABLE classrooms ADD COLUMN start_time TIME NOT NULL DEFAULT '8:00:00';
ALTER TABLE classrooms ALTER COLUMN start_time DROP DEFAULT;
ALTER TABLE classrooms ADD COLUMN end_time TIME NOT NULL DEFAULT '15:00:00';
ALTER TABLE classrooms ALTER COLUMN end_time DROP DEFAULT;
ALTER TABLE classrooms ADD COLUMN approx_number_minutes INTEGER NOT NULL DEFAULT 0;
ALTER TABLE classrooms ALTER COLUMN approx_number_minutes DROP DEFAULT;

ALTER TABLE programs DROP COLUMN computers;
ALTER TABLE programs DROP COLUMN located_in;
ALTER TABLE programs DROP COLUMN computer_type;

COMMIT;