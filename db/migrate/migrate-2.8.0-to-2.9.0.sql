BEGIN;

ALTER TABLE classrooms ADD COLUMN school_id INTEGER REFERENCES schools(id);
UPDATE classrooms set school_id = (SELECT school_id FROM programs WHERE programs.id = classrooms.program_id);
ALTER TABLE classrooms ALTER COLUMN school_id SET NOT NULL;

ALTER TABLE classrooms DROP COLUMN program_id;
DROP TABLE program_not_in_session_dates;
DROP TABLE programs;

CREATE TABLE sessions (
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING NOT NULL UNIQUE
);

INSERT INTO sessions (name) VALUES('2013-2014 School Year');

CREATE TABLE district_sessions (
    id SERIAL PRIMARY KEY,
    district_id INTEGER NOT NULL REFERENCES districts(id),
    session_id INTEGER NOT NULL REFERENCES sessions(id),
    start_date DATE NOT NULL,
    end_date DATE NOT NULL
);

INSERT INTO district_sessions (district_id, session_id, start_date, end_date) SELECT id, (SELECT id FROM sessions WHERE name = '2013-2014 School Year'), '2013-09-01', '2014-06-30' FROM districts;

ALTER TABLE schools ADD COLUMN district_session_id INTEGER REFERENCES district_sessions(id);
UPDATE schools SET district_session_id = (SELECT id FROM district_sessions WHERE district_sessions.district_id = schools.district_id);
ALTER TABLE schools ALTER COLUMN district_session_id SET NOT NULL;
ALTER TABLE schools DROP COLUMN district_id;

CREATE TABLE school_not_in_session_dates (
    id SERIAL PRIMARY KEY,
    school_id INTEGER NOT NULL REFERENCES schools(id),
    start_date DATE NOT NULL
);

CREATE TABLE classroom_groups (
    id SERIAL PRIMARY KEY,
    classroom_id INTEGER NOT NULL REFERENCES classrooms(id),
    name CHARACTER VARYING NOT NULL,
    start_date DATE NOT NULL,
    start_week INTEGER NOT NULL,
    current_week INTEGER NOT NULL,
    done BOOLEAN NOT NULL,
    updated BOOLEAN NOT NULL
);

INSERT INTO classroom_groups (classroom_id, name, start_date, start_week, current_week, done, updated) SELECT id, 'Group 1', '2013-09-01', 1, 1, false, false FROM classrooms WHERE active = true;

CREATE TABLE classroom_group_actions (
    name CHARACTER VARYING PRIMARY KEY
);

INSERT INTO classroom_group_actions (name) VALUES('YES'), ('NO'), ('DONE');

CREATE TABLE classroom_group_history_entries (
    id SERIAL PRIMARY KEY,
    classroom_group_id INTEGER NOT NULL REFERENCES classroom_groups(id),
    admin_user_id INTEGER NOT NULL REFERENCES admin_users(id),
    created TIMESTAMP NOT NULL,
    action CHARACTER VARYING NOT NULL REFERENCES classroom_group_actions(name)
);

ALTER TABLE schools DROP COLUMN contact_first_name;
ALTER TABLE schools DROP COLUMN contact_last_name;
ALTER TABLE schools DROP COLUMN contact_email;
ALTER TABLE schools DROP COLUMN contact_prefix;
ALTER TABLE schools DROP COLUMN contact_work_phone;
ALTER TABLE schools DROP COLUMN contact_super_hero_name;
ALTER TABLE schools DROP COLUMN contact_cell_phone;
ALTER TABLE schools DROP COLUMN contact_role;
ALTER TABLE schools DROP COLUMN organization_type;
ALTER TABLE schools DROP COLUMN number_of_students;
ALTER TABLE schools DROP COLUMN number_of_students_in_program;
ALTER TABLE schools DROP COLUMN interest;
ALTER TABLE schools DROP COLUMN sel_program;
ALTER TABLE schools DROP COLUMN internet_access;
ALTER TABLE schools ADD COLUMN start_date DATE NOT NULL DEFAULT '2013-09-01';
ALTER TABLE schools ALTER COLUMN start_date DROP DEFAULT;
ALTER TABLE schools ADD COLUMN end_date DATE NOT NULL DEFAULT '2014-06-30';
ALTER TABLE schools ALTER COLUMN end_date DROP DEFAULT;

