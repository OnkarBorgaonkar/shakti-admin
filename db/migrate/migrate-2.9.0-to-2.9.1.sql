BEGIN;

ALTER TABLE classrooms ADD COLUMN students INTEGER;
UPDATE classrooms SET students = 0;
ALTER TABLE classrooms ALTER COLUMN students SET NOT NULL;

COMMIT;
