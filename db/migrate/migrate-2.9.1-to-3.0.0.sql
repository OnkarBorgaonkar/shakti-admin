BEGIN;

CREATE TABLE programs(
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING NOT NULL
);

ALTER TABLE orbs ALTER COLUMN academy_year DROP NOT NULL;
ALTER TABLE orbs ADD COLUMN program_id INTEGER REFERENCES programs(id);

INSERT INTO programs (name)
VALUES ('Summer Of Learning 2014 - Elementary School');

INSERT INTO programs (name)
VALUES ('Summer Of Learning 2014 - Middle School');

/* Add Elementary school orbs */
INSERT INTO orbs(name, "order", active, points, program_id)
VALUES ('Science', 15, true, 0, 1);

INSERT INTO orbs(name, "order", active, points, program_id)
VALUES ('Technology', 16, true, 0, 1);

INSERT INTO orbs(name, "order", active, points, program_id)
VALUES ('Engineering', 17, true, 0, 1);

INSERT INTO orbs(name, "order", active, points, program_id)
VALUES ('Arts', 18, true, 0, 1);

INSERT INTO orbs(name, "order", active, points, program_id)
VALUES ('Mathematics', 19, true, 0, 1);

/* Add Middle school orbs */
INSERT INTO orbs(name, "order", active, points, program_id)
VALUES ('Science', 20, true, 0, 2);

INSERT INTO orbs(name, "order", active, points, program_id)
VALUES ('Technology', 21, true, 0, 2);

INSERT INTO orbs(name, "order", active, points, program_id)
VALUES ('Engineering', 22, true, 0, 2);

INSERT INTO orbs(name, "order", active, points, program_id)
VALUES ('Arts', 23, true, 0, 2);

INSERT INTO orbs(name, "order", active, points, program_id)
VALUES ('Mathematics', 24, true, 0, 2);


INSERT INTO orb_checkpoint_types(name)
VALUES('STORY_IMAGE');


/* Add Elementary Checkpoints */
INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 1), 'Who was Thomas Edison?', 'LINK', true, 1, 'http://www.fplsafetyworld.com/?ver=kkblue&utilid=fplforkids&id=16193', 300, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 1), 'Benjamin Franklin: Not Just a President', 'LINK', true, 2, 'https://www.youtube.com/watch?feature=player_detailpage&v=ig0vS-sDCns', 137, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 1), 'Olive Oil Presser', 'LINK', true, 3, 'https://www.youtube.com/watch?feature=player_detailpage&v=bYBeyU0klqY#t=0', 96, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 1), 'Folding Egg Video', 'LINK', true, 4, 'https://www.youtube.com/watch?feature=player_detailpage&v=xqGYg_czNwc#t=0', 195, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 1), 'Project', 'MULTIPLE_CHOICE', true, 5, '', 0, 0);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 1)), 'Turn Milk into Stone!', true, 1);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 1)), 'Mighty Morphin Milk', true, 2);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 1), 'Tell us what you did', 'STORY', true, 7, 0);



INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 1), 'Computer Technology of the Future', 'LINK', true, 1, 'https://www.youtube.com/watch?feature=player_embedded&v=-2Kn2HKCWqs', 202, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 1), 'Steve Jobs: A Tribute', 'LINK', true, 2, 'https://www.youtube.com/watch?feature=player_detailpage&v=dInYmYI7Q20#t=4', 105, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 1), '12 year-old, Thomas Suarez - An App Developer', 'LINK', true, 3, 'https://www.youtube.com/watch?feature=player_detailpage&v=Fkd9TWUtFm0', 280, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 1), 'A Spaghetti Bridge!', 'LINK', true, 4, 'https://www.youtube.com/watch?feature=player_detailpage&v=buhP8jcvGjA', 288, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 1), 'Project', 'MULTIPLE_CHOICE', true, 5, '', 0, 0);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 1)), 'Design a Ray Shield like Astronauts!', true, 1);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 1)), 'Design a Parachute!', true, 2);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 1), 'Tell us what you did', 'STORY', true, 7, 0);



INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 1), 'There are Many different Kinds of Engineering Jobs: Read about some that you''re interested in', 'LINK', true, 1, 'http://www.sciencekids.co.nz/sciencefacts/engineering/typesofengineeringjobs.html', 900, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 1), 'Kevin Do: 15 year-old engineer WOW''s M.I.T with his engineering!', 'LINK', true, 2, 'https://www.youtube.com/watch?feature=player_detailpage&v=XOLOLrUBRBY', 600, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 1), 'Drilling for Oil', 'LINK', true, 3, 'http://science.howstuffworks.com/30982-dirty-jobs-drilling-for-oil-video.htm', 330, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 1), 'How do Engineers Decide what they need to build?', 'LINK', true, 4, 'https://www.youtube.com/watch?feature=player_detailpage&v=BPZXgQwI8Ec', 162, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 1), 'Project', 'MULTIPLE_CHOICE', true, 5, '', 0, 0);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 1)), 'Cut a Watermelon with a Rubberband!', true, 1);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 1)), 'Paper Plate Illusions', true, 2);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 1), 'Tell us what you did', 'STORY', true, 7, 0);



INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 1), 'The Art of Video Games!', 'LINK', true, 1, 'http://www.youtube.com/watch?feature=player_embedded&v=e2QZGreZa-k#t=3', 140, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 1), 'Interviews with Video Game ARTISTS!', 'LINK', true, 2, 'https://www.youtube.com/watch?feature=player_embedded&list=PLBC8C4A4ECCE64C9B&v=L7XZhwKTqjo', 149, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 1), 'A virtual tour of Jobs that Use Art', 'LINK', true, 3, 'http://prezi.com/cnnzc6cwdbbw/art-careers-for-kids/', 180, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 1), 'How to Make a Bomb Glider and Eagle: Paper Planes that Really Fly!', 'LINK', true, 4, 'https://www.youtube.com/watch?feature=player_detailpage&v=Bmd3iAKA0Gw', 900, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 1), 'Project', 'MULTIPLE_CHOICE', true, 5, '', 0, 0);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 1)), 'Sneakers and Flowers', true, 1);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 1)), 'Love Potion Flower', true, 2);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 1), 'Tell us what you did', 'STORY', true, 7, 0);



INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 1), '7 cool jobs that require math', 'LINK', true, 1, 'http://www.dreambox.com/blog/7-dream-jobs-that-require-math', 900, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 1), 'Sir Isaac Newton: Math is how Science Talks!', 'LINK', true, 2, 'http://www.youtube.com/watch?feature=player_embedded&v=mn34mnnDnKU#t=10', 370, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 1), 'Math is Everywhere!', 'LINK', true, 3, 'https://www.youtube.com/watch?feature=player_detailpage&v=HtqlIVN9bh8', 300, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 1), 'Math in Movies: Donald Duck in Mathmagic Land! ', 'LINK', true, 4, 'https://www.youtube.com/watch?v=YVODhFLe0mw&feature=player_detailpage&list=PLDE4CE22F2FB1C14F', 470, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 1), 'Project', 'MULTIPLE_CHOICE', true, 5, '', 0, 0);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 1)), 'Ghost Bubbles', true, 1);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 1)), 'Leprechaun Snot!', true, 2);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 1), 'Tell us what you did', 'STORY', true, 7, 0);


/* Add Middle School Checkpoints */
INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 2), 'Steam Is Not just Hot Water Vapor', 'LINK', true, 1, 'https://www.youtube.com/watch?feature=player_detailpage&v=R9uvIhgVz04', 188, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 2), 'Sci-Fi Technologies that Already Exist: Matthew Santoro', 'LINK', true, 2, 'https://www.youtube.com/watch?feature=player_detailpage&v=aQ7pECmGDJk#t=1', 460, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 2), 'Santiago Gonzalez: A 14 year-old Programmer', 'LINK', true, 3, 'https://www.youtube.com/watch?feature=player_detailpage&v=DBXZWB_dNsw', 520, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 2), '10 Amazing Facts that will Blow Your MIND!', 'LINK', true, 4, 'https://www.youtube.com/watch?feature=player_detailpage&v=6Ni5HOdGtzM#t=1', 127, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 2), 'Project', 'MULTIPLE_CHOICE', true, 5, '', 0, 0);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 2)), 'Pumpkin Seeds!', true, 1);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 2)), 'Electric Play-Dough', true, 2);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Science' AND program_id = 2), 'Tell us what you did', 'STORY_IMAGE', true, 7, 0);



INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 2), '10 Future Technologies that Already Exist', 'LINK', true, 1, 'https://www.youtube.com/watch?feature=player_detailpage&v=4rOR-OheZvc', 358, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 2), 'Steve Jobs: Commencement Speech at Stanford University', 'LINK', true, 2, 'https://www.youtube.com/watch?feature=player_detailpage&v=D1R-jKKp3NA', 873, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 2), 'What is Nano-Technology?', 'LINK', true, 3, 'http://www.youtube.com/watch?feature=player_detailpage&v=xlYIex2TF5g', 900, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 2), 'Electic Motor Technology', 'LINK', true, 4, 'https://www.youtube.com/watch?feature=player_detailpage&v=ziWUmIUcR2k#t=2', 145, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 2), 'Project', 'MULTIPLE_CHOICE', true, 5, '', 0, 0);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 2)), 'Build a 4-Wheel Balloon Car!', true, 1);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 2)), 'Design your own Hovercraft!', true, 2);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 2), 'Tell us what you did', 'STORY_IMAGE', true, 7, 0);



INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 2), 'Nothing Funny about these Race Cars!', 'LINK', true, 1, 'https://www.asme.org/engineering-topics/articles/automotive/nothing-funny-about-these-cars', 480, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 2), 'Famous Automotive Engineers: A Day in the Life', 'LINK', true, 2, 'https://www.youtube.com/watch?feature=player_detailpage&v=r9SQ76Vn-YU', 132, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 2), 'Automotive Engineering Video', 'LINK', true, 3, 'https://www.youtube.com/watch?feature=player_detailpage&v=rPDy5Hc92tE', 146, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 2), 'Students at the College for Creative Studies: Video for Automotive Projects', 'LINK', true, 4, 'https://www.youtube.com/watch?feature=player_detailpage&v=XtpwuPzkmcg', 352, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 2), 'Project', 'MULTIPLE_CHOICE', true, 5, '', 0, 0);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 2)), 'Design a STEM Spinner!', true, 1);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 2)), 'Design an Automobile: It all begins with the Sketch!', true, 2);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Engineering' AND program_id = 2), 'Tell us what you did', 'STORY_IMAGE', true, 7, 0);



INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 2), 'Skills needed to be a Game Designer', 'LINK', true, 1, 'http://electronics.howstuffworks.com/video-game-designer1.htm/printable', 600, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 2), 'So, You Wanna be a Video Game Designer?', 'LINK', true, 2, 'https://www.youtube.com/watch?feature=player_detailpage&v=D0uMAd3zPSA', 416, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 2), 'Inside the Animation Studio', 'LINK', true, 3, 'https://www.youtube.com/watch?feature=player_detailpage&v=MdzjqOuO_Ig', 438, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 2), 'How to Draw an Green Lantern Before Animation', 'LINK', true, 4, 'https://www.youtube.com/watch?feature=player_detailpage&v=q7WHIdoLZ9M', 600, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 2), 'Project', 'MULTIPLE_CHOICE', true, 5, '', 0, 0);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 2)), 'Make a Cardboard Kaleidoscope!', true, 1);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 2)), 'Grow Taller INSTANTLY: Canned Food Stilts!', true, 2);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Arts' AND program_id = 2), 'Tell us what you did', 'STORY_IMAGE', true, 7, 0);



INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 2), 'Sir Isaac Newton: 3 Laws of Motion on a Bi-Cycle!', 'LINK', true, 1, 'http://www.youtube.com/watch?feature=player_detailpage&v=JGO_zDWmkvk', 212, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 2), 'The Beauty of Math!', 'LINK', true, 2, 'https://www.youtube.com/watch?feature=player_detailpage&v=SEiSloE1r-A#t=9', 540, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 2), 'Why Study Math? All the Careers that Need Math!', 'LINK', true, 3, 'https://www.youtube.com/watch?feature=player_detailpage&v=8QqkLrduFA0', 405, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 2), 'Math Errors in Movies and How they should be solved!', 'LINK', true, 4, 'https://www.youtube.com/watch?v=xfQxZTtbCTg&feature=player_detailpage&list=PLDE4CE22F2FB1C14F', 125, 0);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", url, seconds, points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 2), 'Project', 'MULTIPLE_CHOICE', true, 5, '', 0, 0);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 2)), 'Toilet Paper Tube Ping-Pong Launcher', true, 1);

INSERT INTO orb_checkpoint_options(orb_checkpoint_id, name, correct, "order")
VALUES ((SELECT id FROM orb_checkpoints WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 2)), 'The Reversing Arrow', true, 2);

INSERT INTO orb_checkpoints(description, orb_id, name, type, active, "order", points)
VALUES ('', (SELECT id FROM orbs WHERE name = 'Mathematics' AND program_id = 2), 'Tell us what you did', 'STORY_IMAGE', true, 7, 0);

ALTER TABLE user_story_responses ADD COLUMN content_type CHARACTER VARYING;
ALTER TABLE user_story_responses ADD COLUMN file_type CHARACTER VARYING;
ALTER TABLE user_story_responses ADD COLUMN file_name CHARACTER VARYING;

COMMIT;