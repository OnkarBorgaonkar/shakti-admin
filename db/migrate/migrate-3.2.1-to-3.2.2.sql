BEGIN;

UPDATE orbs SET description = '"One machine can do the work of fifty ordinary people. No machine can do the work of one extraordinary person." –Elbert Hubbard. <br/>We need YOU to make technology AMAZING!' WHERE name = 'Technology';

UPDATE
    orb_checkpoints
SET
    url = '/public/summerOfLearning/pdf/orbs/elem-math-1.pdf'
WHERE
    id = (
        SELECT
            orb_checkpoints.id
        FROM
            orb_checkpoints INNER JOIN orbs ON orb_checkpoints.orb_id = orbs.id
        WHERE
            orb_checkpoints."order" = 1
            AND orbs.name = 'Mathematics'
            AND orbs.program_id = 1
    )
;

UPDATE
    orb_checkpoints
SET
    url = '/public/summerOfLearning/pdf/orbs/elem-science-1.pdf'
WHERE
    id = (
        SELECT
            orb_checkpoints.id
        FROM
            orb_checkpoints INNER JOIN orbs ON orb_checkpoints.orb_id = orbs.id
        WHERE
            orb_checkpoints."order" = 1
            AND orbs.name = 'Science'
            AND orbs.program_id = 1
    )
;

UPDATE
    orb_checkpoints
SET
    url = '/public/summerOfLearning/pdf/orbs/middle-art-1.pdf'
WHERE
    id = (
        SELECT
            orb_checkpoints.id
        FROM
            orb_checkpoints INNER JOIN orbs ON orb_checkpoints.orb_id = orbs.id
        WHERE
            orb_checkpoints."order" = 1
            AND orbs.name = 'Arts'
            AND orbs.program_id = 2
    )
;

UPDATE orbs SET long_description =  'Math... It''s not just about the numbers in your textbook! Don’t believe us? Check out how math is with you when you ride your bi-cycle, when you watch movies, and even when you blow bubbles.' WHERE name = 'Mathematics';

COMMIT;