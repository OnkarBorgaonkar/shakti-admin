BEGIN;

alter table assessments add column active boolean;
update assessments set active = FALSE;
insert into assessments (name, active) values ('Hope, Engagement, Well-Being', TRUE);
CREATE UNIQUE INDEX one_active_assessment ON assessments (active) WHERE active;

create table assessment_question_categories (id serial primary key not null,  name varchar(120) not null, "order" integer not null, assessment_id integer not null, description varchar(240), type integer not null);

insert into assessment_question_categories (name, "order", assessment_id, description, type) values ('Hero''s Body', 1, 1, 'As a SHAKTI Warrior, what do you think about the following questions:', 1);
insert into assessment_question_categories (name, "order", assessment_id, description, type) values ('Hero''s Mind', 2, 1, 'As a SHAKTI Warrior, what do you think about the following questions:', 1);
insert into assessment_question_categories (name, "order", assessment_id, description, type) values ('Hero''s Heart', 3, 1, 'As a SHAKTI Warrior, what do you think about the following questions:', 1);
insert into assessment_question_categories (name, "order", assessment_id, description, type) values ('Hero''s Community', 4, 1, 'As a SHAKTI Warrior, what do you think about the following questions:', 1);
insert into assessment_question_categories (name, "order", assessment_id, description, type) values ('Hope', 1, 2, 'The ideas and energy you have for your future.', 3);
insert into assessment_question_categories (name, "order", assessment_id, description, type) values ('Engagement', 2, 2, 'The involvement and excitement you have for school.', 3);
insert into assessment_question_categories (name, "order", assessment_id, description, type) values ('Wellbeing', 3, 2, 'What you think about your life, health, safety, and your overall experiences today.', 2);

alter table assessment_questions drop constraint "assessment_questions_category_id_fkey";
alter table assessment_questions rename category_id to assessment_question_category_id;
alter table assessment_questions add constraint assessment_questions_categories_id_fkey FOREIGN KEY (assessment_question_category_id) references assessment_question_categories (id);

insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'Imagine a ladder with steps numbered from 1 at the bottom to 5 at the top. The top of the ladder represents the best possible life for you and the bottom of the ladder represents the worst possible life for you. On which step of the ladder would you say you feel you stand at this time?
', 1, 5);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'I know I will graduate from high school.', 2, 5);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'There is an adult in my life who cares about my future.', 3, 5);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'I can think of many ways to get good grades.', 4, 5);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'I energetically pursue my goals.', 5, 5);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'I can find lots of ways around any problem.', 6, 5);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'I know I will find a good job after I graduate.', 7, 5);

insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'I have a best friend at school.', 1, 6);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'I feel safe in this school.', 2, 6);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'My teachers make me feel my schoolwork is important.', 3, 6);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'At this school, I have the opportunity to do what I do best every day.', 4, 6);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'In the last seven days, I have received recognition or praise for doing good schoolwork.', 5, 6);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'My school is committed to building the strengths of each student.', 6, 6);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'I have at least one teacher who makes me excited about the future.', 7, 6);

insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'Were you treated with respect all day yesterday?', 1, 7);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'Did you smile or laugh a lot yesterday?', 2, 7);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'Did you learn or do something interesting yesterday?', 3, 7);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'Did you have enough energy to get things done yesterday?', 4, 7);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'Do you have health problems that keep you from doing any of the things other people your age normally
can do?', 5, 7);
insert into assessment_questions (assessment_id, question, "order", assessment_question_category_id) values (3, 'If you are in trouble, do you have family or friends you can count on to help whenever you need them?', 6, 7);

COMMIT;