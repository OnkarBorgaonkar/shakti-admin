
EXPLAIN ANALYZE
SELECT (
        		SELECT COUNT(*) 
        		FROM 
        			child_hero_tasks INNER JOIN tasks ON child_hero_tasks.task_id = tasks.id 
    			WHERE 
    				tasks.category = 'Hero''s Body' 
    				AND child_hero_tasks.child_hero_id = 4286
    				AND child_hero_tasks.completed_at >= '2011-10-03'
			)::real / (
				(
					SELECT COUNT (*) 
					FROM tasks 
					WHERE tasks.category = 'Hero''s Body'
				)*14
			) AS "body",
			(
        		SELECT COUNT(*) 
        		FROM 
        			child_hero_tasks INNER JOIN tasks ON child_hero_tasks.task_id = tasks.id 
    			WHERE 
    				tasks.category = 'Hero''s Mind' 
    				AND child_hero_tasks.child_hero_id = 4286
    				AND child_hero_tasks.completed_at >= '2011-10-03'
			)::real / (
				(
					SELECT COUNT (*) 
					FROM tasks 
					WHERE tasks.category = 'Hero''s Mind'
				)*14
			) AS "mind",
			(
        		SELECT COUNT(*) 
        		FROM 
        			child_hero_tasks INNER JOIN tasks ON child_hero_tasks.task_id = tasks.id 
    			WHERE 
    				tasks.category = 'Hero''s Heart' 
    				AND child_hero_tasks.child_hero_id = 4286
    				AND child_hero_tasks.completed_at >= '2011-10-03'
			)::real / (
				(
					SELECT COUNT (*) 
					FROM tasks 
					WHERE tasks.category = 'Hero''s Heart'
				)*14
			) AS "heart",
			(
        		SELECT COUNT(*) 
        		FROM 
        			child_hero_tasks INNER JOIN tasks ON child_hero_tasks.task_id = tasks.id 
    			WHERE 
    				tasks.category = 'Hero''s Community' 
    				AND child_hero_tasks.child_hero_id = 4286
    				AND child_hero_tasks.completed_at >= '2011-10-03'
			)::real / (
				(
					SELECT COUNT (*) 
					FROM tasks 
					WHERE tasks.category = 'Hero''s Community'
				)*14
			) AS "community"