BEGIN;

INSERT INTO orb_checkpoints (orb_id, name, description, "type", active, "order") VALUES (1, 'Story', 'This is a story checkpoint please type in a story', 'STORY', true, 1);
INSERT INTO orb_checkpoints (orb_id, name, description, "type", active, "order", answer) VALUES (1, 'Fill in the blank', 'This is a fill in the blank checkpoint the answer is "shakti"', 'FILL_IN_THE_BLANK', true, 2, 'shakti');
INSERT INTO orb_checkpoints (orb_id, name, description, "type", active, "order", url, seconds) VALUES (1, 'Link', 'This is a link checkpoint, please visit the link and read it', 'LINK', true, 3, 'http://www.google.com', 15);
INSERT INTO orb_checkpoints (orb_id, name, description, "type", active, "order") VALUES (1, 'Muliple Choice', 'This is a multiple choice checkpoint. The correct Answer is B', 'MULTIPLE_CHOICE', true, 4);

INSERT INTO orb_checkpoint_options (orb_checkpoint_id, name, correct, "order") VALUES((SELECT id FROM orb_checkpoints WHERE orb_id = 1 AND "order" = 4), 'wrong choice', false, 1);
INSERT INTO orb_checkpoint_options (orb_checkpoint_id, name, correct, "order") VALUES((SELECT id FROM orb_checkpoints WHERE orb_id = 1 AND "order" = 4), 'right choice', true, 2);
INSERT INTO orb_checkpoint_options (orb_checkpoint_id, name, correct, "order") VALUES((SELECT id FROM orb_checkpoints WHERE orb_id = 1 AND "order" = 4), 'wrong choice', false, 3);
INSERT INTO orb_checkpoint_options (orb_checkpoint_id, name, correct, "order") VALUES((SELECT id FROM orb_checkpoints WHERE orb_id = 1 AND "order" = 4), 'wrong choice', false, 4);

COMMIT;