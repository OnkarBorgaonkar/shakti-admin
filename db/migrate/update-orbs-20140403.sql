BEGIN;

CREATE TEMP TABLE temp_orbs ( id INTEGER UNIQUE );
COPY temp_orbs (id) FROM STDIN;
155
1
154
125
126
2
127
137
129
128
3
131
138
130
132
4
134
139
133
136
135
119
118
117
5
124
123
120
122
121
108
98
105
110
101
99
109
100
104
106
6
115
112
111
113
114
148
147
142
143
146
141
145
144
151
149
150
153
152
140
93
9
96
10
95
11
97
12
94
82
90
92
13
83
14
86
81
91
88
15
89
84
85
87
70
16
72
75
78
80
79
74
73
76
77
71
63
17
66
65
62
57
60
68
58
64
69
59
67
61
54
47
52
50
48
18
55
19
56
20
49
51
53
158
159
24
162
165
160
166
25
161
156
157
164
163
44
22
42
43
40
39
46
41
38
37
45
29
35
33
36
32
30
26
27
34
31
28
\.

DELETE FROM orb_checkpoint_options WHERE orb_checkpoint_id NOT IN (SELECT id FROM temp_orbs);
DELETE FROM user_story_responses WHERE orb_checkpoint_id NOT IN (SELECT id FROM temp_orbs);
DELETE FROM user_orb_checkpoints WHERE orb_checkpoint_id NOT IN (SELECT id FROM temp_orbs);
DELETE FROM orb_checkpoints WHERE id NOT IN (SELECT id FROM temp_orbs);

UPDATE orb_checkpoints SET description = 'The first thing you need to do is create your Orb Book.  You will use your Orb Book to keep track of your progress on the Orb Quests. You can use many different things for your Orb Book – including:  a blank notebook, a journal, or you can even use blank paper stapled together.   When you have selected what you are going to use for your Orb Book, type “Complete” in the space below. ' WHERE id = 155;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Decorate your Orb Book. Spend at least 15 minutes decorating the front page of your Orb Book. You must include the following on the front of your book (or the first page).</br><ol><li>ORB BOOK</li><li>Your Name</li><li>Your Teacher''s Name</li><li>Your Super Hero''s Name</li><li>Your Super Human and Hero Powers</li><li>The Minion that hovers around you </li></ol></br>When you have finished decorating your Orb Book type "Complete" in the space below.</div>' WHERE id = 1;
UPDATE orb_checkpoints SET description = 'Great start!   You have earned your ORB Book Orb.   Now you are ready to earn other Orbs!  Now type “I did it!” in the space below to earn your Orb!' WHERE id = 154;
UPDATE orb_checkpoints SET description = 'Draw a simple map of your community.  Include things like - location of your school, grocery stores, parks, restaurants, and fun places kids like to go. Make 3 copies of this map. Hold on to these to hand out later.  Now go draw your map.   When you are done, type “Complete” in the space provided.   ' WHERE id = 125;
UPDATE orb_checkpoints SET description = 'Now you are going to be introducing yourself to a new student in your school.   Pick someone you feel safe talking to and then tell us their first name.      ' WHERE id = 126;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">To get to know this person, ask him or her some questions. Make sure to write these questions and their answers in your Orb Book. Then you can tell us online.</br>Questions you can ask:</br><ol><li>Where are you from?</li><li>Do you have any brothers or sisters? If yes, how old are they and what are their names?</li><li>Are you new in town? If so, why did your family move here? If not, how long have you been here?</li><li>What is one thing you like about this town? (or school?)</li><li>What do you like to do for fun?</li></ol></br>Also, think of things that you would like to know about this person.</br>Tell us what you learned about them.</div>' WHERE id = 2;
UPDATE orb_checkpoints SET description = 'Give one of the maps you created to this student. Take a few moments and review the map with them. This will help them get familiar with fun places in your town.   Ask them if there other places they want  to include on this map.   Once you have completed this, type “Complete” in the space provided.  ' WHERE id = 127;
UPDATE orb_checkpoints SET description = 'Invite this person to sit with you at the lunch table at least twice this week.  Now spend the next few days asking them to join you for lunch.   When you have completed this step type "Complete" in the space provided. ' WHERE id = 137;
UPDATE orb_checkpoints SET description = 'Tell us what you liked about this new person you met.   Was there anything that both of you liked? Tell us about it. ' WHERE id = 129;
UPDATE orb_checkpoints SET description = 'MISSION TWO. Introduce yourself to another student in your school. This could be a new student or someone you have seen before but never met.   Pick someone you feel safe talking to and then tell us their first name.' WHERE id = 128;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">To get to know this second person, ask him / her some questions. Make sure to write these questions and answers in your Orb Book so you''ll remember them later when you tell us online.</br>Questions for you to ask:</br><ol>"<li>Where are you from?</li><li>Do you have any brothers or sisters? If yes, how old are they and what are their names?</li><li>Are you new to the area? If so, why did your family move here? If not, how long have you been here?</li><li>What is one thing you like about this town? (or school?)</li><li>What do you like to do for fun? What are some of your hobbies?</li></ol></br>Also, come up with a few of your own questions that you would like to know about this person.</br>Then tell us what you learned about them. </div>' WHERE id = 3;
UPDATE orb_checkpoints SET description = 'Give one of the maps you created to this student. Review the map with them.    Ask them what other places they would like to include on this map?   Once you have completed this, type “Complete” in the space provided.  ' WHERE id = 131;
UPDATE orb_checkpoints SET description = 'Invite this person to sit with you at the lunch table at least twice this week.  Now spend the next few days asking them to join you for lunch.   When  you have completed this step type "Complete" in the space provided. ' WHERE id = 138;
UPDATE orb_checkpoints SET description = 'Tell us what qualities you liked about this new person. Was there anything  that both of you liked?' WHERE id = 130;
UPDATE orb_checkpoints SET description = 'MISSION THREE. Introduce yourself to a student you know does not have many friends. Tell us their first name. ' WHERE id = 132;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">To to get to know them, ask this person some questions about themselves. Write these questions and answers in your Orb Book, so you can input your answers later.</br>Questions:</br><ol><li>Where are you from?</li><li>Do you have any brothers or sisters? If yes, how old are they and what are their names?</li><li>Are you new to town? If so, why did your family move here? If not, how long have you been here?</li><li>What is one thing you like about this town? (or school?)</li><li>What do you like to do for fun? What are some of your hobbies?</li></ol></br>Come up with a few of your own questions that you would like to know about this person.</br>Then tell us what you learned about them.</div>' WHERE id = 4;
UPDATE orb_checkpoints SET description = 'Give one of the maps you created to this student. Take a few moments and review the map with them.   Ask them if there other places they would put on the map.  Once you have completed this, type “Complete” in the space provided.  ' WHERE id = 134;
UPDATE orb_checkpoints SET description = 'Invite this person to sit with you at the lunch table at least twice this week.  Now spend the next few days asking them to join you for lunch.   When  you have completed this step type "Complete" in the space provided. ' WHERE id = 139;
UPDATE orb_checkpoints SET description = 'Tell us what qualities you liked about this person. Was there anything that both of you liked?  Do you think you could be friends?  Yes or No?  ' WHERE id = 133;
UPDATE orb_checkpoints SET description = 'After you have completed these 3 Ambassador missions,  tell us what you learned about being an Ambassador for your school.  Will you continue to be an Ambassador?' WHERE id = 136;
UPDATE orb_checkpoints SET description = 'Great Work! Continue to practice being an Ambassador for your school and encourage your friends to do the same. You have earned the Ambassador Orb! Now type “I did it!” in the space provided to earn your orb!' WHERE id = 135;
UPDATE orb_checkpoints SET description = 'Read the "How to find a Mentor" article.' WHERE id = 119;
UPDATE orb_checkpoints SET description = 'It''s time for you to pick a mentor who can help you with a school subject that is hard for you. Tell us which subject you want support with and why.' WHERE id = 118;
UPDATE orb_checkpoints SET description = 'In your Orb Book, write down the names of people you think would make good mentors for you.   Tell us their first names and why you picked them.' WHERE id = 117;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Then, ask the first person on your list if they would be your mentor.  Explain to them what subject you want help with. Let them know that you would like to meet with them once for 30 minutes each week.  You should plan to meet at school or in a place you feel safe.   If this person is not able to be your mentor, it''s ok. Just select the next person on your list.  Keep asking until you have found your mentor. Then, in the space provided, tell us your mentor''s name and what days of the week you plan to meet. (For example, we will meet on Monday''s, after school, from 3:00 to 3:30).</div>' WHERE id = 5;
UPDATE orb_checkpoints SET description = 'Week 1: Meet with your mentor for at least 15 minutes this week.   Once you have completed this checkpoint, type “Complete” in the space provided. ' WHERE id = 124;
UPDATE orb_checkpoints SET description = 'Week 2: Meet with your mentor for at least 15 minutes this week.   Once you have completed this checkpoint, type “Complete” in the space provided.' WHERE id = 123;
UPDATE orb_checkpoints SET description = 'Week 3: Meet with your mentor for at least 15 minutes this week.   Once you have completed this checkpoint, type “Complete” in the space provided.' WHERE id = 120;
UPDATE orb_checkpoints SET description = 'Tell us the best part about meeting with your mentor.   Did you find it helpful?  Did you learn more about your school subject?   Tell us what you think. ' WHERE id = 122;
UPDATE orb_checkpoints SET description = 'Congratulations. You have earned the Academic Mentor Orb.  Job well done!   Now it is up to you if you want to continue to meet with your mentor.    If they were helpful to you, we hope that you continue to meet with them.    Type “I did it!” in the space provided to earn your orb!' WHERE id = 121;
UPDATE orb_checkpoints SET description = 'In the space provide, tell us your definition of the word Hope.  ' WHERE id = 108;
UPDATE orb_checkpoints SET description = 'Write down ONE real-life example of someone who had HOPE. Or write about a situation where EVERYONE involved had HOPE. This could be either on the news, in person, at home, in school or something you read on the internet. Tell us about it.' WHERE id = 98;
UPDATE orb_checkpoints SET description = 'Write down ONE real-life example of someone who had NO HOPE. Or write about a situation whereEVERYONE involved had NO HOPE. This could be either on the news, in person, at home, in school or something you read on the internet. Tell us about it.' WHERE id = 105;
UPDATE orb_checkpoints SET description = 'Tell us about a time in your life that you were HOPEFUL.   For example... Making the a sports team,  getting a good grade on a test, looking forward to a present you were hoping to get, etc.    How did it feel to be HOPEFUL?' WHERE id = 110;
UPDATE orb_checkpoints SET description = 'Over the next few days,  you''re going to look for another real-life example of someone or a situation you''ve  heard about where people had HOPE. This could be either on the news, in person, at home, in school or something you read on the internet.   Now step away from this quest and go look for hope.    When you have heard of a story of HOPE, come back and tell us about it.' WHERE id = 101;
UPDATE orb_checkpoints SET description = 'Over the next few days,  you''re going to look for another real-life example of someone or a situation you heard about where people had NO HOPE. This could be either on the news, in person, at home, in school or something you read on the internet.   Now step away from this quest and go look for NO HOPE.    When you have heard of a story, come back and tell us about it.' WHERE id = 99;
UPDATE orb_checkpoints SET description = 'Tell us about a time in your life that you were NOT HOPEFUL.   For example...  you knew you were not getting a good grade,  you thought a friend was mad at you, you knew something was not going to work out the way you had hoped.   How did it feel to have NO HOPE?' WHERE id = 109;
UPDATE orb_checkpoints SET description = 'What do you think is better – to have HOPE or to have NO HOPE?  In the space provided, tell us what you think about this.' WHERE id = 100;
UPDATE orb_checkpoints SET description = 'Over the next week, you''re going to talk to as many friends, and family members as you can about this idea of HOPE. In your Orb Book, make notes about what they said.   Now step away from this quest and go talk to your friends and family.   When you have finished, come back and tell us what you learned.' WHERE id = 104;
UPDATE orb_checkpoints SET description = 'Congratulations. You have earned your Rope of Hope Orb.  Now, it is up to you continue to look for HOPE in the world.   AND, where you find NO HOPE… try to change that! Now type: "I did it!" to earn your orb.' WHERE id = 106;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Tell us - what do you think a Role Model is?  Also, tell us 3 of your favorite "famous" role models. Then, tell us 3 of your favorite role models that are "close to home". (These could be  family members, neighbors, school teachers, or others in your community. ) Then, write their names down in your Orb Book.</div>' WHERE id = 6;
UPDATE orb_checkpoints SET description = 'Read the "Your Role Model" article.' WHERE id = 115;
UPDATE orb_checkpoints SET description = 'After reading the article, look back at the role models choices you wrote in your Orb Book. Based on what you learned, would you still choose these people as your role models?  Explain why or why not. If you think you should choose different role models now, tell us who they are.' WHERE id = 112;
UPDATE orb_checkpoints SET description = 'Talk to as many friends and family members as you can over the next week.  Ask each of them to tell you who their role models are.  In the space below, tell us what they said. ' WHERE id = 111;
UPDATE orb_checkpoints SET description = 'Do you think you could be a positive role model?   Why?  or Why not?  Tell us what you think about this.' WHERE id = 113;
UPDATE orb_checkpoints SET description = 'Whew.  That was a lot of thinking and writing!  Great work. Remember to continue to look for new role models in your life and continue to be a role model to those around you. Now type "I did it!" in the space provided to earn your orb!' WHERE id = 114;
UPDATE orb_checkpoints SET description = 'To be safe you need to memorize your parents cell phone number and home address.  Enter it in your ORB Book.    Now step away from this orb and go tell your teacher/coach your parents phone number and your home address.  Once you have done this  type “Complete” in the space below.   ' WHERE id = 148;
UPDATE orb_checkpoints SET description = 'What is the difference between dialing 411 and 911.  Tell us what the difference is.' WHERE id = 147;
UPDATE orb_checkpoints SET description = 'Tell us why you should never answer the door if you don’t know the person on the other side?' WHERE id = 142;
UPDATE orb_checkpoints SET description = 'Read the article "What to do in Case of a Fire".  Make notes about what you read in your Orb Book.' WHERE id = 143;
UPDATE orb_checkpoints SET description = 'Talk with your family about what you read in the fire safety article.  Then conduct a fire safety drill in your house.   You''re going to do a fire drill in the evening with the lights off.    Now step away from this quest and go complete your fire drill.   Come back when you have finished and tell us what you learned.' WHERE id = 146;
UPDATE orb_checkpoints SET description = 'Which of these actions should you NEVER do?' WHERE id = 141;
UPDATE orb_checkpoints SET description = 'Read the article:  “Gun safety."   Remember to make notes about what you read in your Orb Book.     ' WHERE id = 145;
UPDATE orb_checkpoints SET description = 'What should you do if you see a gun?' WHERE id = 144;
UPDATE orb_checkpoints SET description = 'We have all heard about scary events going on in schools today.  Talk to your teacher / coach about what you should do if someone came to your school with a gun and was going to hurt people.   Now step away from this quest and go talk to your teacher /coach about this.  Come back when you have talked to them and tell us what they said you should do.   ' WHERE id = 151;
UPDATE orb_checkpoints SET description = 'Read the article:   "Safe Cyber-Surfing".   Remember to take notes about what you read in your Orb Book.' WHERE id = 149;
UPDATE orb_checkpoints SET description = 'In order to be safe online you should:  ' WHERE id = 150;
UPDATE orb_checkpoints SET description = 'Examples of personal information include: ' WHERE id = 153;
UPDATE orb_checkpoints SET description = 'Why should you NEVER, EVER, EVER play behind a parked car?   In the space provide, tell us what you think.' WHERE id = 152;
UPDATE orb_checkpoints SET description = 'Congratulations. You have earned your Safety Orb. Job well done. Now it is up to you to spread the word about SAFETY. Once finished, type: "I did it!" to earn your orb.' WHERE id = 140;
UPDATE orb_checkpoints SET description = 'Start a book club with at least three members doing SHAKTI Warriors. In the space provided, tell us the first names of those friends in your book club.' WHERE id = 93;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Set up a meeting with the other Word Warriors in your book club. Together, decide what topics interest all of you. Then, decide how long your think it will take all of you to read each book. For example: We choose books about Aliens and we think it will take two weeks to read each book. Choose the specific books you are going to read. Tell us the names of each book you are going to read.</div>' WHERE id = 9;
UPDATE orb_checkpoints SET description = 'Read Book 1. Once you have finished Book 1,  type “Complete” in the space provided.' WHERE id = 96;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">About book 1. In the space provided, tell us about book 1.</br><ul><li>What was the story about?</li><li>Was there a lesson that you learned from reading this book?</li><li>Did this book help you? Did this book scare you? Did this book make you laugh? etc.</li><li>Did you like this book?</li><li>Would you tell someone else to read this book? If yes, why? If not, why not?</li><li>What did you learn about the other Word Warriors in your book club after reading this book?</li></ul></div>' WHERE id = 10;
UPDATE orb_checkpoints SET description = 'Read Book 2. Once you have completed reading Book 2,  type "Complete" in the space provided.' WHERE id = 95;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">About book 2. In the space provided, tell us about book 2.</br><ul><li>What was the story really about?</li><li>Was there a lesson that you learned from reading this book?</li><li>Did this book help you? Did this book scare you? Did this book make you laugh? etc.</li><li>Did you like this book?</li><li>Would you tell someone else to read this book? If yes, why? If not, why not?</li><li>What did you learn about the other Word Warriors in your book club after reading this book?<li></ul></div>' WHERE id = 11;
UPDATE orb_checkpoints SET description = 'Read Book 3. Once you have completed reading Book 3,, type "Complete" in the space provided.' WHERE id = 97;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">About book 3. In the space provided, tell us about book 3.</br><ul><li>What was the story really about?</li><li>Was there a lesson that you learned from reading this book?</li><li>Did this book help you? Did this book scare you? Did this book make you laugh? etc.</li><li>Did you like this book? </li><li>Would you tell someone else to read this book? If yes, why? If not, why not?</li><li>What did you learn about the other Word Warriors in your book club after reading this book?</li><ul></div>' WHERE id = 12;
UPDATE orb_checkpoints SET description = 'Congratulations. You have earned your Word Warriors Orb. Great job! Remember, words, and stories open NEW worlds for us and shareing these new worlds with others makes the journey even BETTER! Now type:   "I did it!" to earn your orb.' WHERE id = 94;
UPDATE orb_checkpoints SET description = 'Re-read the KidsHealth Article on protecting the Earth.' WHERE id = 82;
UPDATE orb_checkpoints SET description = 'Print out the GREEN MACHINE checklist. Post this on your refrigerator at home.   If you are not able to print, please ask your teacher / coach to print a copy for you. ' WHERE id = 90;
UPDATE orb_checkpoints SET description = 'Read each item on the Green Machine checklist list 3 times. MEMORIZE them!. When you have done this, type "Complete" in the space provided.' WHERE id = 92;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Week 1: For 7 days, check off 3 things that you did from the GREEN MACHINE checklist. Do at least 3 things each day for all 7 days.</br>If you forget to do something one day - that''s ok. But you won''t be able to count that day in your quest.</br>When you have finished being "green" for 7 days, type "Complete" in the space provided.</div>' WHERE id = 13;
UPDATE orb_checkpoints SET description = 'Week 2: Pick up litter and throw it away everyday for one whole week. You can do this at school, on your walk to/from school, around your neighborhood.  You don''t have to pick up EVERY piece of litter, just do what you can.   Make sure to wash your hands after you throw the litter away. After 5 days, tell us how you think your actions helped.  ' WHERE id = 83;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Week 3: For 7 days, check off 3 things that you did from the GREEN MACHINE checklist. Do at least 3 things each day for all 7 days.</br>If you forget to do something one day - that''s ok. But you won''t be able to count that day in your quest.</br>When you have finished being "green" for 7 days, type "Complete" in the space provided.</div>' WHERE id = 14;
UPDATE orb_checkpoints SET description = 'Week 4: Pick up litter and throw it away everyday for one week.  You can do this at school, on your walk to/from school, around your neighborhood.   Make sure to wash your hands after you throw the litter away. After 5 days, tell us how you think your actions helped the Earth.' WHERE id = 86;
UPDATE orb_checkpoints SET description = 'Read the article (again) on "Energy Minions"' WHERE id = 81;
UPDATE orb_checkpoints SET description = 'Week 5: You are going to find and turn off 2 Energy Minions in your home. Tell us which ones you turned off and why. Remember to do this every night for one whole week (7 days).  Now step away from this quest and spend 7 days turning off energy minions.  When you have finished, type "Complete" in the space provided.' WHERE id = 91;
UPDATE orb_checkpoints SET description = 'Week 6:  Place recyclable items in recyclable bins at schools.  Examples of recyclable items include:  Paper, juice boxes, milk cartons, etc. Remember to do this every day for 5 days. When you have finished this, type "Complete" in the space provided.' WHERE id = 88;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Week 7: Reuse the same plastic water bottle for 2 days. Then throw it away. Repeat this step 3 times in one week.  When you have done this, type "Complete" in the space provided.</div>' WHERE id = 15;
UPDATE orb_checkpoints SET description = 'This week, do one extra thing that is not on the list, but you know will help keep the environment clean.   Tell us why you chose this extra GREEN quest action and also tell us what you did.' WHERE id = 89;
UPDATE orb_checkpoints SET description = 'In the space provided answer the following questions. Do you think it’s important for everyone to help keep the Earth clean?  Why or why not? What would happen if every child your age in America did something everyday to keep America clean? How might you teach others to take care of our Earth?' WHERE id = 84;
UPDATE orb_checkpoints SET description = 'Now go out and tell 2 people, how you feel after completing all of these GREEN quests for our Earth! In the space provided, tell us how they responded to you. Do you think your actions will make them choose to keep the Earth clean? Why or Why not? ' WHERE id = 85;
UPDATE orb_checkpoints SET description = 'Congratulations. You have earned your GREEN MACHINE Orb. Great job! Remember, EVERYONE is responsible for keeping the Earth clean.  We only have ONE Earth to share!  Keep practicing what you''ve learned. Now type: "I did it!" to earn your orb.' WHERE id = 87;
UPDATE orb_checkpoints SET description = 'Create a new section in your Orb Book and call it "The Thinker". When done, type "Complete" in the space provided.' WHERE id = 70;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Question 1: What do you think would happen if you saved half of all the money you received (as a gift, or your allowance, etc.) from now until when you graduated high school? How much money do you think you would have? (Take a guess.) What would you be able to buy with this money? In the space provided, tell us what you think.</div>' WHERE id = 16;
UPDATE orb_checkpoints SET description = 'How does NOT understanding something make you feel?    In the space provided, tell us what you think.' WHERE id = 72;
UPDATE orb_checkpoints SET description = 'What''s a better number 1 or 100?   In the space provided, tell us which number you picked and why.  ' WHERE id = 75;
UPDATE orb_checkpoints SET description = 'What might happen if there were no bullies in the world? In the space provided, tell us what you think.' WHERE id = 78;
UPDATE orb_checkpoints SET description = 'Think of the silliest way to eat ice cream. Now, in the space provided tell us what it is! ' WHERE id = 80;
UPDATE orb_checkpoints SET description = 'Why do you think it is hard for people to get along with each other sometimes? In the space provided, tell us what you think.' WHERE id = 79;
UPDATE orb_checkpoints SET description = 'Where does a summer breeze come from?   In the space provided, tell us what you think.   ' WHERE id = 74;
UPDATE orb_checkpoints SET description = 'What does it mean to be an American citizen?   In the space provided, tell us what you think.' WHERE id = 73;
UPDATE orb_checkpoints SET description = 'Can choosing to say ''no'', sometimes be a good thing?  In the space provided, give us an example and tell us what you think.     ' WHERE id = 76;
UPDATE orb_checkpoints SET description = 'Do you like the way your brain thinks?    Why? or Why Not?  In the space provided, tell us what you think.' WHERE id = 77;
UPDATE orb_checkpoints SET description = 'Congratulations. You have earned your THINKER Orb.   Great job! Remember, always use your mind because if YOU DON''T, someone else will do the thinking for you!. Now type: "I did it!" to earn your orb.' WHERE id = 71;
UPDATE orb_checkpoints SET description = 'Do 50 jumping jacks in one day. You don’t have to do them all at one time.   But complete them all by the end of the day.    Now step away from this quest and go do your jumping jacks. Once you have completed this quest type "Complete" in the space provided. Then wait for tomorrow to start your next fitness move. ' WHERE id = 63;
UPDATE orb_checkpoints SET description = 'Do 25 pushups.  (You can do them with your knees on the floor or using straight legs with your knees off the floor). Make sure you are using proper form. If you are unsure of your form, ask someone. You don''t have to do them all at one time.  But complete them all by the end of the day. Once you have completed this checkpoint, type "Complete" in the space provided. Then wait for the next day to start your next fitness move.' WHERE id = 17;
UPDATE orb_checkpoints SET description = 'Do 25 sit-ups.  You don’t have to do them all in one day, you can take a few days to finish.      Now step away from this quest and go do your situps. Once you have completed this task, type "Complete" in the space provided. Then wait for the next day to start your next fitness move.' WHERE id = 66;
UPDATE orb_checkpoints SET description = 'You''re going to run for 3 minutes. You can run in place inside or run outside.  But do not run outside alone.     Now step away from this quest and go run.  Once you have completed this task, type "Complete" in the space provided.  Then wait for the next day to start your next fitness move.' WHERE id = 65;
UPDATE orb_checkpoints SET description = 'Do 100 jumping jacks in one day. You don’t have to do them all at one time, but you must complete them all by the end of the day.   Now step away from this quest and go do your jumping jacks.   Once you have completed this quest, type "Complete" in the space provided. Then wait for tomorrow to start your next fitness move.' WHERE id = 62;
UPDATE orb_checkpoints SET description = 'Do 50 push-ups. (You can do them with your knees on the floor or straight legs with knees off the floor). You don’t have to do them all in one day, you can take a few days to finish.   Now step away from this quest and go to your pushups.  Once you have completed this checkpoint, type "Complete" in the space provided.  Then wait for the next day to start your next fitness move.' WHERE id = 57;
UPDATE orb_checkpoints SET description = 'Do 50 sit-ups. You don’t have to do them all in one day, you can take a few days to finish.  Now step away from this quest and go do your sit-ups.  Once you have completed this checkpoint, type "Complete" in the space provided. Then wait for the next day to start your next fitness move.' WHERE id = 60;
UPDATE orb_checkpoints SET description = 'Today, you''re going to run for 5 minutes. You can run in place inside or run outside. But do not run outside alone.   Now step away from this quest and go for a run.  Once you have completed this checkpoint, type "Complete" in the space provided. Then wait for tomorrow to start your next fitness move.' WHERE id = 68;
UPDATE orb_checkpoints SET description = 'You''re MAKING PROGRESS! Today, you''re going to do 200 jumping jacks in one day. You don’t have to do them all at one time, but you must complete them all by the end of the day. Now step away from this quest and go do your jumping jacks.  Once you have completed this checkpoint, type "Complete" in the space provided. Then wait for tomorrow to start your next fitness move.' WHERE id = 58;
UPDATE orb_checkpoints SET description = 'Don''t give up! Today, do 75 pushups. (You can do them with your knees on the floor or straight legs with knees off the floor). You don’t have to do them all in one day, you can take a few days to finish. Now step away from this quest and go do your pushups.   Once you have completed this checkpoint, type "Complete" in the space provided. Then wait for the next day to start your next fitness move.' WHERE id = 64;
UPDATE orb_checkpoints SET description = 'You''re very close to your GOAL! Today, you''re going to do 75 sit-ups. You don’t have to do them all in one day, you can take a few days to finish.  Now step away from this quest and go do your sit-ups.  Once you have completed this checkpoint, type "Complete" in the space provided. Then wait for the next day to start your next fitness move. ' WHERE id = 69;
UPDATE orb_checkpoints SET description = 'TODAY IS THE DAY! Your final goal will be to jog or run for 10 minutes without stopping! Make sure you time yourself. You can jog in place inside or jog outside. But do not run outside alone.  Now stop away from this quest and go for a jog.  Once you have completed this checkpoint, type "Complete" in the space provided. ' WHERE id = 59;
UPDATE orb_checkpoints SET description = 'In the space provided tell us how you feel now that you have completed all of these checkpoints.  Was this orb easy or difficult for you? Explain. Do you have more or less energy now? Does anything about your body feel or look different? Do you feel proud of yourself for completing "Get Your Move On" Orb?... You SHOULD!!!! ' WHERE id = 67;
UPDATE orb_checkpoints SET description = 'You did it! Congratulations. You have earned the "Get Your Move On" Orb. Great job!  Remember, having a healthy body for the rest of your life will help you feel strong, happy, and confident! Now type: "I did it!" to earn your orb.' WHERE id = 61;
UPDATE orb_checkpoints SET description = 'Tell us your definition of LEARNING.' WHERE id = 54;
UPDATE orb_checkpoints SET description = 'Tell us a quote that you found about LEARNING. Use the internet to search for quotes if you need to.' WHERE id = 47;
UPDATE orb_checkpoints SET description = 'Tell us which subject at school is the EASIEST for you to learn.  Why is this subject easy for you?  Tell us what you think.' WHERE id = 52;
UPDATE orb_checkpoints SET description = 'Tell us which subject at school is REALLY HARD  (a challenge) for you to learn.  BE HONEST.  Why do you think this subject is difficult for you?  Tell us what you think. ' WHERE id = 50;
UPDATE orb_checkpoints SET description = 'Question for you: If someone asked you to help them LEARN how to study better ... what would you tell them? In the space provided, tell us what you think.' WHERE id = 48;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">It''s time to learn how to LEARN.   Ask others to help you create LEARNING ACTIONS to help you learn. Talk to teachers, friends, parents.  Have them help you come up with LEARNING ACTIONS and write these down in your Orb Book.  (For example:  Studying an extra 30 minutes each night.  Or, looking up information on the internet. Or, going to the library to read a book that might help you. Or, writing down the questions you have about this subject and talking to your teacher about these questions.)   Create at least 3 actions that might help you LEARN this subject! When you''ve completed writing your LEARNING ACTIONS, type "Complete" in the space provided.</div>' WHERE id = 18;
UPDATE orb_checkpoints SET description = 'In the space provided, tell us your ACTION PLAN and who helped you create most of it.' WHERE id = 55;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Week 1.  Every day this week, do at least one LEARNING ACTIONS to help you get better at the subject in school that is hardest for you right now. Remember,  write down all your actions in your Orb Book so you''ll remember later when you have to tell us online! When you''ve completed doing your actions this week, type the word "Complete" in the space provided.</div>' WHERE id = 19;
UPDATE orb_checkpoints SET description = 'After ONE WEEK (7 days) ...Tell us EVERY ACTION you took to help you learn each day.   Reference your notes from your Orb Book. ' WHERE id = 56;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Week 2.   Every day this week, do at least two LEARNING ACTIONS to help you get better at the subject in school that is hardest for you right now. Remember,  write down all your actions in your Orb Book so you''ll remember later when you have to tell us online! When you''ve completed doing your actions this week, type the word "Complete" in the space provided.</div>' WHERE id = 20;
UPDATE orb_checkpoints SET description = 'After WEEK TWO, tell us EVERY ACTION you took during week two - to improve your learning.   Refer back to the notes in your Orb Book.' WHERE id = 49;
UPDATE orb_checkpoints SET description = 'After two weeks of doing your LEARNING ACTIONS, tell us what you learned. Is the subject getting easier for you to LEARN?  If so, explain!   If not, and the subject is still hard, tell us what extra actions you want to take to help you learn this subject better?' WHERE id = 51;
UPDATE orb_checkpoints SET description = 'Congratulations for being willing to LEARN! You have earned your LEARN This! Orb. It is never easy to admit that something is hard to LEARN. It takes a lot of courage! If you think that creating a Learn THIS! ACTION PLAN was helpful… then "spread the word" so others can LEARN ways to LEARN. Remember, learning never really ends and the best way to learn, is to practice! Now type: "I did it!" to earn your orb.' WHERE id = 53;
UPDATE orb_checkpoints SET description = 'Tell us YOUR definition of "Gratitude".   It''s the same thing as being "grateful".   What do you think it means?      ' WHERE id = 158;
UPDATE orb_checkpoints SET description = 'Now, look up the word "Gratitude" in a dictionary. Or, you can look it up on the internet. Tell us the definition you found. Does your definition of "Gratitude" match the definition you found?  Explain.' WHERE id = 159;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Look in newspapers, magazines, on television, on the Internet, etc., to find examples of people that talk about being "grateful". Do this for 2 days. Write these examples down in your Orb Book. (For example: I saw a story on the news about a family. The family lost their home in a storm. But, the mom said that she was just "grateful that her family survived!").Make sure you tell us WHERE your found this story and where the story took place (the setting of the story). When finished, type "Complete" in the space provided.</div>' WHERE id = 24;
UPDATE orb_checkpoints SET description = 'Tell us the two examples of "gratitude" (people being grateful) you found. Did seeing these examples of "gratitude" make you feel any different? Explain.' WHERE id = 162;
UPDATE orb_checkpoints SET description = 'Today you''re going to list 3 things you are grateful for today and write them down in your Orb Book.    Now step away from this quest and go fill out your Orb Book.   When you have done this, type Complete in the space provide. ' WHERE id = 165;
UPDATE orb_checkpoints SET description = 'Then tell us why you''re grateful for these 3 things. Do these 3 things help you in your life? Explain. ' WHERE id = 160;
UPDATE orb_checkpoints SET description = 'Now go write a list of 3 people that you are grateful that they are in your life.  Write their names in your Orb Book.    Write at least 2 things about each of these 3 people that you’re grateful for.     When finished, type "Complete" in the space provided.' WHERE id = 166;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Now it''s time to take HERO ACTION and create GRATITUDE CARDS!  For each of the 3 people you listed, create a GRATITUDE CARD for them.  You should not spend any money on this.  Use the arts and crafts supplies you have in your classroom or at home.   Include your name, the persons name, and why you are grateful for them!  Then step away from the computer and go give them their card.    Tell us how they responded?</div>' WHERE id = 25;
UPDATE orb_checkpoints SET description = 'Do you think you made them feel better when you gave them their card?  Tell us what you think.' WHERE id = 161;
UPDATE orb_checkpoints SET description = 'Think of 2 people in your school, community, or at home that you think might feel better if someone told them "thank you" for simply being them!   Make sure they are people you trust and feel safe with.  (for example:  your teacher, your principal, your friend).   Now, write their first names in your Orb Book. When finished, type “Complete” in the space provided.' WHERE id = 156;
UPDATE orb_checkpoints SET description = 'Make it your mission to tell each of these 2 people "THANK YOU" one time each day.  Do this for 3 days. Make sure you write when you did this for each person in your Orb Book. When finished, type "Complete" in the space provided.   Now step away from this quest and complete your task.   Come back to this quest in 3 days when you have completed it. ' WHERE id = 157;
UPDATE orb_checkpoints SET description = 'Tell us how each of these people responded to you when you said "THANK YOU".  How did you feel saying "THANK YOU"? Do you think that having an ‘Attitude of Gratitude’ changes the way you see your life?  Explain.' WHERE id = 164;
UPDATE orb_checkpoints SET description = 'CONGRATULATIONS! You have created gratitude in your life. And, you have also shared this feeling with others. You should be very proud of yourself! You deserve the Attitude of Gratitude Orb. Now type: "I did it!" to earn your orb.' WHERE id = 163;
UPDATE orb_checkpoints SET description = 'Make a list in your Orb Book of all those things that you are doing that you know are not good for you.  (For example:  eating too much junk food,  being lazy, not completing your home work on time, using bad works, etc..   Now step away from this quest and go make your list in your Orb Book.   Type ''Complete'' in the space provided when you have made your list. ' WHERE id = 44;
UPDATE orb_checkpoints SET description = '<div class="checkpointWrap">Now comes the hard part. Take a moment and review your list. Pick one item from your list that you know you should "give up" for 5 days because you''d be a stronger person if you did. Do not pick something just because it is easy. Pick something that will really challenge you! Are you ready to give this one thing up for 5 days? And we mean give it up completely. Tell us your answer Yes or No?</div>' WHERE id = 22;
UPDATE orb_checkpoints SET description = 'If No, cancel out of this orb now and pick another orb to try to earn. If yes, in the space provided, tell us what you are going to give up!' WHERE id = 42;
UPDATE orb_checkpoints SET description = 'Day 1: Give this activity up for the whole day. AND we mean the WHOLE day. Once you have given this thing up for one day - type "Gave it up!" in the space provided.   (Remember, CHAOS and his MINIONS want you to keep your BAD HABIT.   DO NOT listen to them!)  ' WHERE id = 43;
UPDATE orb_checkpoints SET description = 'Day 2: Give this activity up for the whole day. AND we mean the WHOLE day. Once you have given this thing up for the 2nd day - type "Gave it up!" in the space provided.    (Remember, CHAOS and his MINIONS want you to keep your BAD HABIT.   DO NOT listen to them!)  ' WHERE id = 40;
UPDATE orb_checkpoints SET description = 'Day 3: Give this activity up for the whole day. AND we mean the WHOLE day. Once you have given this thing up for the 3rd day - type "Gave it up!" in the space provided.     (Remember, CHAOS and his MINIONS want you to keep your BAD HABIT.   DO NOT listen to them!)  ' WHERE id = 39;
UPDATE orb_checkpoints SET description = 'Day 4: Checking in.   You have now "given up" something that is not good for you for 3 days.   That takes a lot of effort.  How are you feeling?   Can you make it 2 more days?   Tell us how you feel?' WHERE id = 46;
UPDATE orb_checkpoints SET description = 'Day 4: Keep going....only 2 more days!  You can do it.  Remember self-discipline is THE KEY to being a HERO.   Once you have given this thing up for the 4th whole day - type "Gave it up!" in the space provided.   DO NOT listen to the MINIONS!' WHERE id = 41;
UPDATE orb_checkpoints SET description = 'Day 5: FINAL DAY!  Don''t stop.    Give this activity up for th WHOLE day. AND we mean the WHOLE day. Once you have given this thing up for the 5th and final day - type "Gave it up!" in the space provided.' WHERE id = 38;
UPDATE orb_checkpoints SET description = 'Checking in..... giving up something you really enjoy is not easy.   Tell us how it felt to give up something that you really like.   What did you learn? What was the hardest part? Did it get easier or harder after a few days?  ' WHERE id = 37;
UPDATE orb_checkpoints SET description = 'Congratulations!  You have learned to give something up that you really enjoyed!  That is NEVER easy to do.   Now type ''I did it!'' to earn your "Give It Up" orb.  ' WHERE id = 45;
UPDATE orb_checkpoints SET description = 'Week 1. For week one, you''re going to surprise your parent(s) or guardian(s) by giving them a hug and telling them that you love them every day this week.   Now step away from this quest and perform your task.   At the end of the week, in the space provided, tell us what they said or did and how it made you feel.  ' WHERE id = 29;
UPDATE orb_checkpoints SET description = 'Print the Kid Asset Checklist.   If you are not able to print a copy - ask your teacher / coach to print you a copy. ' WHERE id = 35;
UPDATE orb_checkpoints SET description = 'Spend some time reviewing the Kid Assess Checklist.   Review each of the items on the checklist with them.   Take notes in your Orb Book about what questions you want to talk to your parents/guardian about.  When finished, type “Complete” in the space provided.' WHERE id = 33;
UPDATE orb_checkpoints SET description = 'Make time to talk to your parent(s) or guardian(s) about your thoughts on the Kid Asset Checklist.  Talk to them about the support you feel you need at home, at school, and in your neighborhood.  If you would feel more comfotable, practice having this conversation first with a friend or an adult (you feel safe with).    Then after you speak to your parent(s) / guardian(s) tell us what they said. Were they supportive?   What do you think about what they said?   Then wait a few days to continue to the next checkpoint.   ' WHERE id = 36;
UPDATE orb_checkpoints SET description = 'Your goal is to have a sit-down, family dinner this week.   Take time to help prepare the meal.  Or, you can help set the table.    During dinner ask your family members questions about themselves.   Questions like:  What did they want to be when they grew up?   What was their best childhood memory?   Who was their favorite friend, and why?   Did they have a favorite pet?      Remember, this is time for you to learn about them.  Write the answers in your Orb Book.  Now go talk to your family members and ask them these questions.     When finished, come back and tell us what they said. ' WHERE id = 32;
UPDATE orb_checkpoints SET description = 'Ask your parent(s) or guardian(s) to describe the family rules. Did you understand all of these rules? If not, ask them to help you understand these rules better. Do you understand what will happen if you don’t follow these rules? Write all this down in your Orb Book. Once completed, type “Complete” in the space provided.' WHERE id = 30;
UPDATE orb_checkpoints SET description = 'Tell us what your family rules are and what you think about them. Then wait a week to continue to the next checkpoint.' WHERE id = 26;
UPDATE orb_checkpoints SET description = 'This week, go somewhere together with your family members. This could be a trip to the grocery store. Or, a trip to a museum.  Or, a picnic in your backyard.  Pick anything that you can do together.  But it can’t be watching TV, playing video games, or anything with technology. Once you have done this, type “Complete” in the space provided.' WHERE id = 27;
UPDATE orb_checkpoints SET description = 'Sit down with your parent(s) or guardian(s) and come up with ONE healthy habit that you can do together for 7 days. For example: Eating one extra piece of fruit everyday, going for walks together at least three times per week for at least 30 minutes each time, etc. Even if you and your parent(s) or guardian(s) already have healthy habits that you do together, pick an extra one for this orb. Then tell us what healthy habit you chose and why.  ' WHERE id = 34;
UPDATE orb_checkpoints SET description = 'Now, work together with your parent(s) or guardian(s) on this healthy habit for seven days. Once you’re finished, type: “Complete” in the space provided. ' WHERE id = 31;
UPDATE orb_checkpoints SET description = 'Congratulations. You have earned your Home Sweet Home orb. Remember this is the only family you have. Do your best to try and understand them. Now type: "I did it!" to earn your orb.' WHERE id = 28;

COMMIT;
