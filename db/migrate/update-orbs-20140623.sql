BEGIN;

ALTER TABLE children ADD COLUMN city CHARACTER VARYING;

CREATE TABLE pilot_requests (
    id SERIAL PRIMARY KEY,
    first_name CHARACTER VARYING NOT NULL,
    last_name CHARACTER VARYING NOT NULL,
    email CHARACTER VARYING NOT NULL,
    phone CHARACTER VARYING NOT NULL,
    city CHARACTER VARYING NOT NULL,
    state CHARACTER VARYING NOT NULL,
    level CHARACTER VARYING
);

INSERT INTO orb_checkpoint_types VALUES ('VIDEO');

UPDATE orb_checkpoints SET "type" = 'VIDEO' WHERE "type" = 'LINK' AND url ~* 'youtube';

UPDATE
    orb_checkpoints
SET
    url = (
        SELECT
            'elementary-' || lower(orbs.name) || '-' || oc."order"
        FROM
            orbs INNER JOIN orb_checkpoints as oc ON orbs.id = oc.orb_id
        WHERE
            oc.id = orb_checkpoints.id

    )
WHERE
    orb_checkpoints.id IN (
        SELECT
            orb_checkpoints.id
        FROM
            orb_checkpoints INNER JOIN orbs ON orb_checkpoints.orb_id = orbs.id
        WHERE
            orbs.program_id = 1
            AND orb_checkpoints.type = 'VIDEO'
    )
;

UPDATE
    orb_checkpoints
SET
    url = (
        SELECT
            'middle-' || lower(orbs.name) || '-' || oc."order"
        FROM
            orbs INNER JOIN orb_checkpoints as oc ON orbs.id = oc.orb_id
        WHERE
            oc.id = orb_checkpoints.id

    )
WHERE
    orb_checkpoints.id IN (
        SELECT
            orb_checkpoints.id
        FROM
            orb_checkpoints INNER JOIN orbs ON orb_checkpoints.orb_id = orbs.id
        WHERE
            orbs.program_id = 2
            AND orb_checkpoints.type = 'VIDEO'
    )
;



COMMIT;