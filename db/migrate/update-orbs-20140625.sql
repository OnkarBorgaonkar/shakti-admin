BEGIN;

/* Middle School Art 1*/
UPDATE orb_checkpoints SET description = 'Video games are not just games... they are ART! This article will help you understand the creative world of art behind the game!' WHERE name = 'The Art of Video Games!';

/* Elementary Engineering 1 */
UPDATE orb_checkpoints SET url = 'http://www.engg-kids.com/WhatdoesanEngdo.html' WHERE name = 'There are Many different Kinds of Engineering Jobs: Read about some that you''re interested in';

/* Elementary Tech Project */
UPDATE orb_checkpoints SET description = 'Now it''s your turn to turn on your TECH. Mind! You''ll have two projects to choose from - You only need to complete ONE...  Go for it! <p>Ions in Space <a target="_blank" href="http://spaceplace.nasa.gov/ion-balloons/en/">Check it out!</a><br/>Watch Ions in Action! This project will show you how Ions are sticky little things! You''ll need some balloons and paper (flying paper that is!)</p><p>Design a Parachute! <a target="_blank" href="http://www.sciencekids.co.nz/experiments/freefall.html">Check it out!</a><br/>Now''s your chance to design and test a parachute! You''ll be reading and following directions that will take you around 20 minutes. You''ll need: A plastic bag or light material, Scissors, String & a small object to act as the weight, a little action figure would be perfect!</p>' WHERE name = 'Project' AND orb_id = (SELECT id FROM orbs WHERE name = 'Technology' AND program_id = 1);

COMMIT;