INSERT INTO user_points_entries(user_id, point_type, points, created, activity_id)
SELECT
    quiz_responses.user_id,
    'ECHO_BOARD',
    quiz_questions.points,
    quiz_answers.created_on,
    quiz_answers.id
FROM
    quiz_responses INNER JOIN quiz_answers ON quiz_responses.id = quiz_answers.quiz_response_id
    INNER JOIN quiz_questions ON quiz_answers.quiz_question_id = quiz_questions.id
;

INSERT INTO user_points_entries(user_id, point_type, points, created, activity_id)
SELECT
    library_responses.user_id,
    'LIBRARY_ARTICLE',
    library_articles.points,
    library_answers.created_on,
    library_answers.id
FROM
    library_responses INNER JOIN library_answers ON library_responses.id = library_answers.library_response_id
    INNER JOIN library_articles ON library_answers.library_article_id = library_articles.id
;

INSERT INTO user_points_entries(user_id, point_type, points, created, activity_id, category_id)
SELECT
    children.user_id,
    'DAILY_TRAINING',
    tasks.points,
    child_hero_tasks.completed_at,
    child_hero_tasks.id,
    categories.id
FROM
    children INNER JOIN child_heroes ON children.id = child_heroes.child_id
    INNER JOIN child_hero_tasks ON child_heroes.id = child_hero_tasks.child_hero_id
    INNER JOIN tasks ON child_hero_tasks.task_id = tasks.id
    INNER JOIN categories ON tasks.category = categories.name
WHERE
    child_hero_tasks.approved = 'true'
;


INSERT INTO user_points_entries(user_id, point_type, points, created, activity_id)
SELECT
    group_join_points.user_id,
    'RECRUITING',
    group_join_points.points,
    group_join_points.created_on,
    group_join_points.id
FROM
    group_join_points
;

INSERT INTO user_points_entries(user_id, point_type, points, created, activity_id)
SELECT
    user_orbs.user_id,
    'ORB',
    orbs.points,
    user_orbs.completed_date,
    user_orbs.id
FROM
    user_orbs INNER JOIN orbs ON user_orbs.orb_id = orbs.id
WHERE
    user_orbs.completed_date IS NOT NULL
;

INSERT INTO user_points_entries(user_id, point_type, points, created, activity_id)
SELECT
    user_orb_checkpoints.user_id,
    'ORB_CHECKPOINT',
    orb_checkpoints.points,
    user_orb_checkpoints.completed_at,
    user_orb_checkpoints.id
FROM
    user_orb_checkpoints INNER JOIN orb_checkpoints ON user_orb_checkpoints.orb_checkpoint_id = orb_checkpoints.id
;

UPDATE users SET total_points = (SELECT COALESCE(SUM(user_points_entries.points), 0) FROM user_points_entries WHERE user_points_entries.user_id = users.id);