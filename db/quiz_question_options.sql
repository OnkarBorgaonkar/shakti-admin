--
-- Name: quiz_question_options_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('quiz_question_options_id_seq', 1022, true);


--
-- Data for Name: quiz_question_options; Type: TABLE DATA; Schema: public; Owner: -
--

COPY quiz_question_options (id, quiz_question_id, option, correct, "order") FROM stdin;
607	191	TRUE	t	1
608	191	FALSE	f	2
609	192	After a few hours	f	1
610	192	After 100 years	f	2
611	192	After a few days	t	3
612	192	After a few weeks	f	4
613	193	A lot of water	t	1
614	193	A lot of sand	f	2
615	193	No water	f	3
616	193	No nutrients	f	4
617	194	Starvation	f	1
618	194	Dehydration	t	2
619	194	Playstation	f	3
620	194	Desperation	f	4
621	195	Drink less water	f	1
622	195	Eat more lemons	f	2
623	195	Eat less bananas	f	3
624	195	Drink more water	t	4
625	196	Try to starve themselves	f	1
626	196	Eat more	t	2
627	196	Eat less	f	3
628	196	Don't bother paying attention to how much they eat	f	4
629	197	Help our voice sound clear	f	1
630	197	Help us do math problems	f	2
631	197	Help our attitude be better	f	3
632	197	Help our bones be strong	t	4
633	198	Supposed to be nutritious	t	1
634	198	Supposed to be full of fat	f	2
635	198	Supposed to be full of sugar	f	3
636	198	Supposed to make you feel full so you can skip your next meal	f	4
637	199	Fall asleep	f	1
638	199	Grow 12 inches taller in a few months	f	2
639	199	Have quick energy	t	3
640	199	Dream more colorfully	f	4
641	200	TRUE	t	1
642	200	FALSE	f	2
643	201	Are food that should make you say "whoa" Should I eat that?	f	1
644	201	That can cause weight problems	f	2
645	201	ok to eat once-in-a-while	f	3
646	201	All of the above	t	4
647	202	TRUE	f	1
648	202	FALSE	t	2
649	203	Heavy syrup has more salt	f	1
650	203	Light syrup has more sugar	f	2
651	203	Heavy syrup has more sugar	t	3
652	203	Light syrup has more salt	f	4
653	204	Unhealthy food doesn't give us energy	t	1
654	204	Unhealthy food helps us feel our best	f	2
655	204	Unhealthy food is what our teachers wants us to eat	f	3
656	204	Healthy food is only for some people	f	4
657	205	Helps keep us from getting sick	f	1
658	205	Helps our bodies grow correctly	f	2
659	205	Gives us energy to do stuff	f	3
660	205	All of the above	t	4
661	206	Is important because our bodies need the right combination of nutrients to work	t	1
662	206	Tells us if we will like the taste	f	2
663	206	If on there so the package looks nicer	f	3
664	206	Doesn't really matter	f	4
665	207	TRUE	f	1
666	207	FALSE	f	2
667	208	Soda or energy drinks	f	2
668	208	Milk or water	t	2
669	208	Beer or wine	f	3
670	208	Coffee or tea	f	4
671	209	TRUE	f	1
672	209	FALSE	t	2
673	210	Make it easier to fall asleep	f	1
674	210	Is used to make the drink taste better	f	2
675	210	Can temporarily boost a persons energy	t	3
676	210	People need to drink to stay alive	f	4
677	211	A person can fit going to the movie into their schedule	f	1
678	211	A person eats well and exercises regularly	t	2
679	211	A person can fit a volley ball in their mouth	f	3
680	211	A person who does their homework	f	4
681	212	TRUE	f	1
682	212	FALSE	t	2
683	213	Bone	f	1
684	213	Skin	f	2
685	213	Love	f	3
686	213	Muscle	t	4
687	214	Staring at clouds moving	f	1
688	214	Doing push-ups	t	2
689	214	Doing math problems	f	3
690	214	Imagining riding a bike	f	4
691	215	TRUE	f	1
692	215	FALSE	t	2
693	216	Might not think clearly and have a hard time following directions	f	1
694	216	Might feel clumsy playing our favorite sport or instrument	f	2
695	216	Might not grow as well	f	3
696	216	All of the above	t	4
697	217	TRUE	t	1
698	217	FALSE	f	2
699	218	Watching a scary movie	f	1
700	218	Taking a warm bath or reading	t	2
701	218	Doing jumping Jacks just before jumping into bed	f	3
702	218	Having a drink with caffeine in it	f	4
703	219	Are your brains way of making sense of what happened during the day	f	1
704	219	Allow your brain to sort through the events of the day, storing what's important and getting rid of the junk	f	2
705	219	Are a clue to what you're worried about or thinking about	f	3
706	219	All of the above	t	4
707	220	TRUE	t	1
708	220	FALSE	f	2
709	221	It slows down your breathing	f	1
710	221	It helps stretch the body	f	2
711	221	It relaxes your mind	f	3
712	221	All of the above	t	4
713	222	Wear uncomfortable clothes and keep your shoes on	f	1
714	222	Pick a place with people moving around and talking loudly	f	2
715	222	Find a spot on the bare floor without any soft cushion or rug	f	3
716	222	None of the above	t	4
717	223	TRUE	f	1
718	223	FALSE	t	2
719	224	Hearing	f	1
720	224	Concentration	t	2
721	224	Smelling	f	4
722	224	Tasting	f	4
723	225	TRUE	t	1
724	225	FALSE	f	2
725	226	May feel their pants are looser	f	1
726	226	May feel as light a feather	f	2
727	226	May gain weight	t	3
728	226	May think they got skinnier	f	4
729	227	TRUE	f	1
730	227	FALSE	t	2
731	228	Blindness	f	1
732	228	Smoke coming out of your ears	f	2
733	228	Cancer and heart disease	t	3
734	228	Fresh breath	f	4
735	229	Smoking was invented in 1200 B.C.	f	1
736	229	1200 people die each day due to smoking	t	2
737	229	It's best to smoke at 12:00 each day	f	3
738	229	$12.00 is the price of a pack of cigarettes	f	4
739	230	Our body knows when it's being poisoned	t	1
740	230	Because coughing from smoking makes them look cool	f	2
741	230	Because they like throwing up	f	3
742	230	Because coughing and throwing up means something is healthy for you	f	4
743	231	TRUE	t	1
744	231	FALSE	f	2
745	232	TRUE	t	1
746	232	FALSE	f	2
751	234	Sponge	f	1
752	234	Prefrontal Cortex	f	2
753	234	Amygdala	t	3
754	234	Hippocampus	f	4
755	235	You would try new food before you decide if you like it.	t	1
756	235	You would refuse to try new foods because you know you would not like it.	f	2
757	235	Feed any food you don't like to the dog.	f	3
758	235	Taste new food but spit it out if you don't like it.	f	4
759	236	When you need to calm down after recess and start a math test?	f	1
760	236	When you miss the school bus and feel stressed out and not sure what to do?	f	2
761	236	When you are nervous because you think you've done something wrong?	f	3
762	236	All of the above?	t	4
769	239	Tasting	f	1
770	239	Seeing	f	2
771	239	Listening	t	3
772	239	Smelling	f	4
773	240	Are making our feet work too hard	f	1
774	240	Are helping our brain's health	t	2
775	240	Are making our feet happy	f	3
776	240	Are making our brain work too hard	f	4
777	241	You think that you are the only person that matters in the world	f	1
778	241	You think that other people who don’t agree with you are wrong.	f	2
779	241	You accept and understand people’s differences	t	3
780	241	You laugh who dress differently that you do.	f	4
781	242	TRUE	t	1
782	242	FALSE	f	2
783	243	A “C” is good but I know I can do better next time.	f	1
784	243	Exercise is hard but I know it is good for my body.	f	2
785	243	I don’t like eating vegetables but I know that they will help me grow strong	f	3
786	243	All of the above?	t	4
787	244	You are optimistic.	t	1
788	244	You are pessimistic.	f	2
789	244	You are artistic.	f	3
790	244	You are thirsty.	f	4
791	245	Handle stress better.	f	1
792	245	Have good health.	f	2
793	245	Learn new information easier.	f	3
794	245	All of the above?	t	4
795	246	TRUE	t	1
796	246	FALSE	f	2
797	247	Encouraging others.	f	1
798	247	Helping others.	f	2
799	247	Complimenting others.	f	3
800	247	All of the above?	t	4
801	248	Keep track of all the things you are grateful for.	t	1
802	248	Keep track of all the toys you want.	f	2
803	248	Keep track of what you want others to do for you.	f	3
804	248	A notepad to draw doodles on.	f	4
805	249	making yourself laugh over the memory of a joke you shared with a friend	f	1
806	249	remembering the smile on the face of someone that you helped	f	2
807	249	recalling a warm feeling of hug from a loving caring adult	f	3
808	249	all of the above?	t	4
809	250	TRUE	t	1
810	250	FALSE	f	2
818	252	All of the above.	t	4
819	253	TRUE	t	1
820	253	FALSE	f	2
821	254	Ignore it and look the other way.	f	1
822	254	Tell an adult.	t	2
823	254	Laugh about it on facebook.	f	3
824	254	Join in and bully the kid.	f	4
827	256	Rin Ten Ten	f	1
828	256	Ben Ten	f	2
829	256	Take Ten	t	3
830	256	All of the above. 	f	4
831	257	Get you to think before you act.	f	1
832	257	Get you to cool off before you do something you might be sorry about later.	f	2
833	257	Get you to realize there are better ways than violence for dealing with anger.	f	3
834	257	All of the above. 	t	4
835	258	Everyone	t	1
747	233	back part	f	1
749	233	Amygdala	f	3
750	233	Hippocampus	f	4
763	237	The opposite of a "time out".	f	1
765	237	Taking a moment to sit quietly and listen to yourself.	t	3
766	237	All of the above	f	4
767	238	It can help you to slow your heart beat down.	f	1
768	238	It can help you stay focused	f	2
811	251	Avoids a bully	f	1
812	251	Tries to help the Victim	t	2
813	251	Watches the bully	f	3
814	251	Befriends the bully	f	4
815	252	It's a cruel act done repeatedly	f	1
816	252	It's done on purpose	f	2
817	252	It's done to intimidate or humiliate	f	3
825	255	Step closer to the victim	f	1
826	255	Tell the bully that what they are doing is wrong and to stop it.	f	2
836	258	Superheroes	f	2
837	258	Kids	f	3
838	258	Adults	f	4
839	259	I am really sorry I hit you when I was mad. I won’t do it anymore.	f	1
840	259	I am sorry I hurt your feelings.	f	2
841	259	I was mad and I should not have called you a name. I am sorry.	f	3
842	259	All of the above. 	t	4
843	260	TRUE	t	1
844	260	FALSE	f	2
845	261	Physical (hitting, kicking, shoving, etc)	f	1
846	261	Verbal (name calling, teasing, threats, etc)	f	2
847	261	Emotional Bullying (spreading rumors, leaving someone out, sending a not nice text message)	f	3
848	261	All of the above. 	t	4
851	263	Money	f	1
852	263	Friends	f	2
853	263	Power	t	3
854	263	Satisfaction	f	4
855	264	Try to put yourself in their shoes.	f	1
856	264	Be patient and gentle when speaking with them.	f	2
857	264	Understand that this is not a choice for them. It is their way of life.	f	3
858	264	All of the above.	t	4
859	265	TRUE	t	1
860	265	FALSE	f	2
861	266	When a group of kids leave out other kids on purpose.	t	1
862	266	A group of friends who are invite others to join.	f	2
863	266	A group of people who try to make others happy.	f	3
864	266	A fun group to be a part of.	f	4
865	267	Find other friends.	f	1
866	267	Speak up and encourage them to welcome others.	f	2
867	267	Invite a friend in the clique to do something with you away from the clique.	f	3
868	267	All of the above. 	t	4
869	268	Keep your feelings to yourself.	f	1
870	268	Put your feelings into words.	t	2
871	268	Break something to get your frustration out.	f	3
872	268	Call someone a name to feel better.	f	4
873	269	TRUE	t	1
874	269	FALSE	f	2
875	270	The kid is sick and their body is hot.	f	1
876	270	The kid is always complaining about something.	f	2
877	270	The kid is a really strong athlete.	f	3
878	270	The kid behaves badly when feeling angry or frustrated.	t	4
879	271	Don’t talk to strangers online	f	1
880	271	Never agree to meet someone you met online in person	f	2
881	271	Tell a parent if a stranger contacts you online.	f	3
882	271	All of the above.	t	4
883	272	TRUE	f	1
884	272	FALSE	t	2
885	273	Your Birthday	f	1
886	273	Your email or home address	f	2
887	273	Your school name	f	3
888	273	None of the above	t	4
889	274	TRUE	t	1
890	274	FALSE	f	2
891	275	Cyberbullying isn’t hurtful... it’s just kidding.	f	1
892	275	It’s ok forward a picture if someone else forwarded it to you.	f	2
893	275	It’s wrong. And people can get seriously hurt. So just don’t do it.	t	3
894	275	It’s ok to tease someone online because it’s not the same as in person.	f	4
895	276	A	t	1
896	276	B	f	2
897	276	C	f	3
898	276	D	f	4
899	277	A	t	1
900	277	B	f	2
901	277	C	f	3
902	277	D	f	4
903	278	A	t	1
904	278	B	f	2
905	278	C	f	3
906	278	D	f	4
907	279	A	t	1
908	279	B	f	2
909	279	C	f	3
910	279	D	f	4
911	280	A	t	1
912	280	B	f	2
913	280	C	f	3
914	280	D	f	4
915	281	TRUE	t	1
916	281	FALSE	f	2
917	282	Your teachers.	f	1
918	282	Your parents.	f	2
919	282	You.	t	3
920	282	Your friends.	f	4
921	283	Bragging about how great you are.	f	1
922	283	Quietly knowing that you are worthy of being loved and accepted.	t	2
923	283	Thinking that you are smarter than everyone else.	f	3
924	283	Putting yourself in front of other people.	f	4
925	284	Helps you hold your head high and be proud of yourself.	f	1
926	284	Gives you courage to try new things.	f	2
927	284	Lets you respect yourself even when you make mistakes.	f	3
928	284	All of the above.	t	4
929	285	TRUE	t	1
930	285	FALSE	f	2
931	286	TRUE	t	1
932	286	FALSE	f	2
933	287	Energy Drinks	f	1
934	287	Having lots of friends	f	2
935	287	Having lots of confidence in yourself	t	3
936	287	Having lots of money	f	4
937	288	They want to be liked.	f	1
938	288	They want to fit in.	f	2
939	288	They worry that other kids will make fun of them if they don’t go along with the group.	f	3
940	288	All of the above.	t	4
941	289	Making your own choices.	f	1
942	289	Going along with other people’s choice.	t	2
943	289	Knowing that you have a choice.	f	3
944	289	Asking people what the right choice is. 	f	4
945	290	Quickly.	f	1
946	290	Confidently.	f	2
947	290	Proudly.	f	3
948	290	All of the above.	t	4
949	291	Reduce it.	t	1
950	291	Reuse it.	f	2
951	291	Recycle it.	f	3
952	291	All of the above.	f	4
953	292	Reduce it.	f	1
954	292	Reuse it.	f	2
955	292	Recycle it.	t	3
956	292	None of the above	f	4
957	293	Another name for a garden center.	f	1
958	293	An area on the side of the highway that is covered with grass.	f	2
959	293	A new video game that is out that teaches the importance of taking care of the world.	f	3
960	293	A big mountain of trash where all the garbage trucks go to dump their load.	t	4
961	294	Availability of food	f	1
962	294	Clean Water	f	2
963	294	Temperatures that support life	f	3
964	294	All of the above.	t	4
965	295	TRUE	f	1
966	295	FALSE	t	2
967	296	TRUE	f	1
968	296	FALSE	t	2
969	297	Getting a good job when they graduate from college.	f	1
970	297	Start saving energy and water in their own homes right away.	t	2
971	297	Reading about helping the planet but not actually doing anything	f	3
972	297	All of the above.	f	4
973	298	TRUE	t	1
974	298	FALSE	f	2
975	299	TRUE	f	1
976	299	FALSE	t	2
977	300	Run the washer and dryer when they are filled with clothes	f	1
978	300	Change your light bulbs to energy efficient ones.	f	2
979	300	Unplug energy minions	f	3
980	300	All of the above.	t	4
981	301	Turning the faucet off when you are brushing your teeth.	f	1
982	301	Take shorter showers	f	2
983	301	Stop using plastic bottled water	f	3
984	301	All of the above.	t	4
985	302	TRUE	t	1
986	302	FALSE	f	2
987	303	Because the food is grown local their are less chemicals in it to keep it fresh	f	1
988	303	You support your local community	f	2
989	303	The food are probably more fresh than store bought food	f	3
990	303	All of the above.	t	4
991	304	TRUE	f	1
992	304	FALSE	t	2
993	305	Keeping you healthy	f	1
994	305	Keeping the rivers healthy	f	2
995	305	Keeping the animals healthy	f	3
996	305	All of the above.	t	4
997	306	A	t	1
998	306	B	f	2
999	306	C	f	3
1000	306	D	f	4
1001	307	A	t	1
1002	307	B	f	2
1003	307	C	f	3
1004	307	D	f	4
1005	308	A	t	1
1006	308	B	f	2
1007	308	C	f	3
1008	308	D	f	4
1009	309	A	t	1
1010	309	B	f	2
1011	309	C	f	3
1012	309	D	f	4
1013	310	A	t	1
1014	310	B	f	2
1015	310	C	f	3
1016	310	D	f	4
748	233	Prefrontal Cortex	t	2
764	237	That you are back and ready to play a game.	f	2
1017	238	It can help you learn to control yourself.	f	3
1018	238	All of the above	t	4
1019	255	Ask the victim if they need help.	f	3
1020	255	All of the above	t	4
850	262	Have your friend beat the bully up first.	f	2
849	262	Avoid the bully.	t	1
1021	262	Call them names and make them afraid of you.	f	3
1022	262	All of the above.	f	4
\.


--
-- PostgreSQL database dump complete
--

