SELECT pg_catalog.setval('quiz_questions_id_seq', 310, true);


--
-- Data for Name: quiz_questions; Type: TABLE DATA; Schema: public; Owner: -
--

COPY quiz_questions (id, quiz_id, subject_id, points, question, "order", library_article_id) FROM stdin;
216	2	1	10	If we don't get enough sleep we	1	5
217	2	1	20	True or false: We dream in the R.E.M. (Rapid Eye Movement) stage of sleep	2	1
191	1	2	10	True or False? Any breakfast is better than no breakfast?	1	\N
192	1	2	20	Without water our bodies would stop working:	2	\N
193	1	2	30	Vegetables and fruits contain:	3	\N
194	1	2	40	When your body doesn't have enough water, it's called:	4	\N
195	1	2	50	If your pee is dark yellow you should:	5	\N
196	2	2	10	Kids who are active burn more calories so they need more calories. This means they should:	1	\N
197	2	2	20	Our bodies need calcium to:	2	\N
198	2	2	30	Snacks are:	3	\N
199	2	2	40	Eating an orange will help you:	4	\N
200	2	2	50	True or false: A snack of cheese and crackers makes you feel full longer than cookies.	5	\N
201	3	2	10	The list of "whoa" foods means these are foods that are:	1	\N
202	3	2	20	True of False? Frying foods is healthier than baking them in the oven.	2	\N
203	3	2	30	Fruit canned in "light" syrup is healthier for you than the fruit in "heavy" syrup because:	3	\N
204	3	2	40	If lunch is putting fuel in your tank, why would unhealthy food make us run out of gas?	4	\N
205	3	2	50	Eating a VARIETY of healthy foods:	5	\N
206	4	2	10	The information on food labels that lists the ingredients:	1	\N
207	4	2	20	True or false: Most nutrients are measured in grams (g), or milligrams (mg)	2	\N
208	4	2	30	The healthiest drink for kids is:	3	\N
209	4	2	40	True or false: Clear lemon lime sodas usually have caffeine	4	\N
210	4	2	50	Caffeine is a chemical added to drinks that:	5	\N
212	1	1	20	True or False: Kids should eat 3 servings of fruits and 2 servings of vegetables daily	2	\N
213	1	1	30	Our heart tissue is made of:	3	\N
214	1	1	40	Examples of exercises that strengthens your muscles are:	4	\N
215	1	1	50	True or false: Gymnastics doesn't help us get more flexible:	5	\N
218	2	1	30	A tip for falling asleep easily is:	3	\N
219	2	1	40	No one knows for sure why we dream but some scientists think dreams:	4	\N
220	2	1	50	Experts agree that most kids between the ages of 5 to 12 need between 10 and 11 hours of sleep each night.	5	\N
221	3	1	10	Yoga makes people feel good because:	1	\N
222	3	1	20	In order to do yoga properly you should:	2	\N
223	3	1	30	True or false:  While doing yoga you are supposed to feel pain:	3	\N
224	3	1	40	Girls who play sports do better in school because exercise improves your:	4	\N
225	3	1	50	True or false:  Girls who exercise are less likely to smoke:	5	\N
226	4	1	10	If a person eats more calories than their body uses then they:	1	\N
227	4	1	20	True or false: The word diet has 2 meanings. It means "an attempt to lose weight" and "a collection of food we never eat"	2	\N
228	4	1	30	Smoking and tobacco can cause:	3	\N
229	4	1	40	The smoking awareness red rubber bracelet has the number 1200 on it because:	4	\N
230	4	1	50	The reason people cough and sometimes even throw up when they first try smoking is because:	5	\N
231	1	5	10	Every time you perform an activity your brain creates a pathway to remembering.	1	\N
234	1	5	40	When we react to a stray ball flying towards us, the part of the brain to help us react quickly is the:	4	\N
235	1	5	50	Practicing mindful awareness at dinner time would mean:	5	\N
239	2	5	40	When we slow down to ignore distractions and purposely focus on only one sound in a loud room, we are practicing Mindful	4	\N
240	2	5	50	If we practice standing on one foot to improve our physical balance then we:	5	\N
241	3	5	10	Having a mindful perspective about something means:	1	\N
242	3	5	20	True or False: It’s ok for people to look, act and speak differently that you do.	2	\N
243	3	5	30	Which of these saying would likely come from an optimistic person.	3	\N
244	3	5	40	What does “seeing the glass of water half full” mean?	4	\N
245	3	5	50	Having an optimistic attitude can help us:	5	\N
246	4	5	10	True or False: Happiness is a choice. Even when you have very little you can choose to be happy.	1	\N
247	4	5	20	Examples of acts of kindness are:	2	\N
249	4	5	40	An example of appreciating happy experiences are:	4	\N
250	4	5	50	True or False: Creating mini-movies about happy memories in our past will help us stay happy in the present.	5	\N
253	1	4	30	True or False: Oftentimes bullies are just acting out what they see at home. They think that being angry, yelling and pushing people is a normal way to act.	3	\N
254	1	4	40	When you see someone belng bullied the best thing to do is:	4	\N
256	2	4	10	Ten deep breaths,  ten steps, ten minutes and ten laps around the playground are all examples of what skill for preventing violence?	1	\N
257	2	4	20	The purpose behind TAKE TEN is to:	2	\N
258	2	4	30	Take Ten is skill for preventing violence that is good for:	3	\N
259	2	4	40	Which of these are an example of an apology:	4	\N
233	1	5	30	The part of the brain that does the thinking and the decision making is the:	3	\N
236	2	5	10	When is a good time to take a "time in"?	1	\N
237	2	5	20	A "time in" means - 	2	\N
238	2	5	30	Why is it important to take a “time in” through out the day?	3	\N
248	4	5	30	A Hero's journal is used to:	3	\N
251	1	4	10	An "UPSTANDER" is someone who	1	\N
252	1	4	20	Which of these describes a bullying situation	2	\N
255	1	4	50	Which of the following is something you could do if you feel safe in helping a victim of bullying?	5	\N
260	2	4	50	True or False: Admitting when you are wrong and saying “I am sorry” is one of the best things you can learn to do.	5	\N
261	3	4	10	Which of these is a form of bullying:	1	\N
263	3	4	30	The main goal of bullying is to gain:	3	\N
264	3	4	40	When you see a child who has special need you should:	4	\N
265	3	4	50	True or False: Kids with special needs are every bit as important as you are.	5	\N
266	4	4	10	A clique is:	1	\N
267	4	4	20	If clicks are upsetting you what can you do?	2	\N
268	4	4	30	When you are angry it is important that you:	3	\N
269	4	4	40	True or False: Putting your energy into physical activity like karate or wrestling - is a good way to deal with your anger.	4	\N
270	4	4	50	When people say that a kid “has a temper” they mean:	5	\N
271	1	3	10	Which of these are rules for being safe on the internet?	1	\N
272	1	3	20	True or False:  It’s ok to enter contest or join clubs if you are friends are doing it too.	2	\N
273	1	3	30	Which of the following information is ok to share with friends you just met on the internet.	3	\N
274	1	3	40	True or False: Anything you post online can stay online forever - even if you delete it.	4	\N
275	1	3	50	When it comes to cyber-bullying, which of these statements are true:	5	\N
276	2	3	10	Place holder  Identity Sidekick question 1	1	\N
277	2	3	20	Place holder Identity Sidekick question 2	2	\N
278	2	3	30	Place holder Identity Sidekick question 3	3	\N
279	2	3	40	Place holder Identity Sidekick question 4	4	\N
280	2	3	50	Place holder Identity Sidekick question 5	5	\N
281	3	3	10	True or False: We are all superheroes in training.	1	\N
282	3	3	20	Who gets to decide who you are going to be in the world.	2	\N
283	3	3	30	Self esteem means:	3	\N
284	3	3	40	Good self-esteem is important because it:	4	\N
285	3	3	50	True or False: When you respect yourself adults and other kids usually respect you to.	5	\N
286	4	3	10	True or False: Bullies have no real power.	1	\N
287	4	3	20	Which of these gives you real power:	2	\N
288	4	3	30	Kids give in to peer pressure because:	3	\N
289	4	3	40	Peer pressure is	4	\N
290	4	3	50	What’s the best way to walk away from peer pressure.	5	\N
291	1	6	10	Turning the lights off when you are not using them is an example of:	1	\N
292	1	6	20	Drinking tap water instead of buying bottled water is an example of:	2	\N
293	1	6	30	A landfill is:	3	\N
294	1	6	40	A warming planet directly effects	4	\N
295	1	6	50	True or False: People should not be concerned about what is happening to a squirrel’s world because their world is much different than the world that people live in.	5	\N
296	2	6	10	True or False: Kids have no power to change the world.	1	\N
297	2	6	20	What’s the quickest way kids can start saving the planet is to:	2	\N
298	2	6	30	True or False: Everything that turns on/off, spins, moves, lights, heats up or cools down uses energy.	3	\N
299	2	6	40	True or False: People do not need to be concerned about how much energy they use because there is an endless supply available.	4	\N
300	2	6	50	Ways you can help your family save on energy costs include:	5	\N
301	3	6	10	Ways you can conserve water include:	1	\N
302	3	6	20	True or False: The world is running out of clean water.	2	\N
303	3	6	30	The benefits of buying food at a local farmers market are:	3	\N
304	3	6	40	True or False: If you can’t understand the ingredients on a food label then the food is probably very good for you.	4	\N
305	3	6	50	Organic foods help the world by:	5	\N
306	4	6	10	Placeholder for Green Superhero question 1	1	\N
307	4	6	20	Placeholder for Green Superhero question 2	2	\N
308	4	6	30	Placeholder for Green Superhero question 3	3	\N
309	4	6	40	Placeholder for Green Superhero question 4	4	\N
310	4	6	50	Placeholder for Green Superhero question 5	5	\N
232	1	5	20	Mindful awareness and mindful behavior means that you are becoming more aware of your thoughts and your actions	2	\N
262	3	4	20	The best way to handle being bullied is to:	2	\N
211	1	1	10	Being FIT means:	1	26
\.


--
-- PostgreSQL database dump complete
--

