--
-- Name: quizzes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('quizzes_id_seq', 4, true);


--
-- Data for Name: quizzes; Type: TABLE DATA; Schema: public; Owner: -
--

COPY quizzes (id, rank_id) FROM stdin;
1	1
2	2
3	3
4	4
\.


--
-- PostgreSQL database dump complete
--

