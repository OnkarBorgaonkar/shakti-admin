INSERT INTO child_hero_ranks (lower_bound, name) SELECT 0, 'Recruit';
INSERT INTO child_hero_ranks (lower_bound, name) SELECT 6000, 'Sidekick';
INSERT INTO child_hero_ranks (lower_bound, name) SELECT 12000, 'Hero';
INSERT INTO child_hero_ranks (lower_bound, name) SELECT 20000, 'Superhero';
