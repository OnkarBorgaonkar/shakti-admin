DROP TABLE roles CASCADE;
DROP TABLE users CASCADE;
DROP TABLE guardians CASCADE;
DROP TABLE superheroes CASCADE;
DROP TABLE children CASCADE;
DROP TABLE child_heroes CASCADE;
DROP TABLE child_hero_ranks CASCADE;
DROP TABLE human_powers CASCADE;
DROP TABLE super_powers CASCADE;
DROP TABLE child_hero_human_powers CASCADE;
DROP TABLE child_hero_super_powers CASCADE;
DROP TABLE weekly_messages CASCADE;
DROP TABLE tasks CASCADE;
DROP TABLE child_hero_tasks CASCADE;
DROP TABLE categories CASCADE;
DROP TABLE task_messages CASCADE;

DROP TABLE group_requests CASCADE;
DROP TABLE group_invitations CASCADE;
DROP TABLE users_groups CASCADE;
DROP TABLE group_goals CASCADE;
DROP TABLE groups CASCADE;
DROP TABLE group_types CASCADE;

-- "child," "guardian," "administrator" ...
CREATE TABLE roles(
    name CHARACTER VARYING(128) PRIMARY KEY
);

CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    email CHARACTER VARYING(128) UNIQUE,
    login CHARACTER VARYING(128) NOT NULL UNIQUE,
    password CHARACTER VARYING(128) NOT NULL,
    salt CHARACTER VARYING(128) NOT NULL,
    role CHARACTER VARYING(128) REFERENCES roles(name),
    file_type CHARACTER VARYING,
    content_type CHARACTER VARYING
);


--
-- well
--
CREATE TABLE group_types (
    name CHARACTER VARYING PRIMARY KEY
);

CREATE TABLE groups(
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(128) NOT NULL UNIQUE,
    tag CHARACTER VARYING,
    leader_id INTEGER NOT NULL REFERENCES users (id),
    type_name CHARACTER VARYING NOT NULL REFERENCES group_types (name),
    backstory TEXT,
    location CHARACTER VARYING,
    color CHARACTER VARYING(64) NOT NULL,
    created_on DATE NOT NULL DEFAULT now(),
    file_type CHARACTER VARYING,
    content_type CHARACTER VARYING,
    tag CHARACTER VARYING    
);


CREATE TABLE group_goals(
    id SERIAL PRIMARY KEY,
    group_id INTEGER NOT NULL REFERENCES groups(id) UNIQUE,
    name CHARACTER VARYING NOT NULL,
    points INTEGER NOT NULL,
    reward CHARACTER VARYING NOT NULL,
    created_on DATE NOT NULL DEFAULT now(),
    starts_on DATE NOT NULL,
    ends_on DATE NOT NULL
);

CREATE TABLE user_groups(
    user_id INTEGER NOT NULL REFERENCES users(id),
    group_id INTEGER NOT NULL REFERENCES groups(id),
    UNIQUE (user_id, group_id)
);

CREATE TABLE group_messages(
    id SERIAL PRIMARY KEY,
    group_id INTEGER NOT NULL REFERENCES groups(id),
    sender_id INTEGER NOT NULL REFERENCES users(id),
    message TEXT NOT NULL,
    sent_on TIMESTAMP NOT NULL
);

-- invitation to join a group
CREATE TABLE group_invitations(
    id SERIAL PRIMARY KEY,
    group_id INTEGER NOT NULL REFERENCES groups(id),
    sender_id INTEGER NOT NULL REFERENCES users(id),
    recipient_id INTEGER NOT NULL REFERENCES users(id),
    message TEXT NOT NULL,
    sent_on DATE NOT NULL DEFAULT now()
);

-- request to join a group
CREATE TABLE group_requests(
    id SERIAL PRIMARY KEY,
    group_id INTEGER NOT NULL REFERENCES groups(id),
    sender_id INTEGER NOT NULL REFERENCES users(id),
    message TEXT NOT NULL,
    sent_on DATE NOT NULL DEFAULT now()
);

CREATE TABLE group_join_points(
	id SERIAL PRIMARY KEY,
	group_id INTEGER REFERENCES groups(id),
	user_id INTEGER NOT NULL REFERENCES users(id),	
	points INTEGER NOT NULL,
	created_on TIMESTAMP DEFAULT NOW()
);

CREATE TABLE guardians(
    id SERIAL PRIMARY KEY,
    email CHARACTER VARYING(128),
    invalid_email BOOLEAN,
    notified_on DATE,
    notification_count INTEGER,
    approved_on DATE
);

CREATE TABLE superheroes(
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(128) NOT NULL
);

CREATE TABLE children(
    id SERIAL PRIMARY KEY,
    user_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
    guardian_id INTEGER REFERENCES guardians(id),
    recruited_by INTEGER REFERENCES children(id),
    first_name CHARACTER VARYING(64),
    gender CHARACTER(1),
    year CHARACTER(4),
    state CHARACTER(2),
    ethnicity CHARACTER VARYING(64),
    superhero_id INTEGER REFERENCES superheroes(id), -- The child's mentor
    created_at TIMESTAMP DEFAULT now()
);

-- The child_heroes table may have a number of other attributes.
CREATE TABLE child_heroes(
    id SERIAL PRIMARY KEY,
    child_id INTEGER REFERENCES children(id) ON DELETE CASCADE,
    height_feet SMALLINT,
    height_inches SMALLINT,
    weight SMALLINT,
    ethnicity CHARACTER VARYING(64),
    eye_color CHARACTER VARYING(64),
    hair_length CHARACTER VARYING(64),
    hair_type CHARACTER VARYING(64),
    hair_special_features CHARACTER VARYING(128),
    body_type CHARACTER VARYING(64),
    body_special_features CHARACTER VARYING(128),
    super_hero_outfit TEXT,
    backstory TEXT,
    plan_to_do_good TEXT,
    hero_machine_string TEXT,
    color CHARACTER VARYING
);

-- Which level the child hero has attained — e.g. Recruit, Sidekick, Hero, Super Hero
CREATE TABLE child_hero_ranks(
    lower_bound INTEGER NOT NULL, -- Lower bound in points to reach this rank.
    name CHARACTER VARYING(64)
);

CREATE TABLE human_powers(
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(128) NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE super_powers(
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING(128) NOT NULL,
    description TEXT NOT NULL
);

CREATE TABLE child_hero_human_powers(
    child_hero_id INTEGER NOT NULL REFERENCES child_heroes(id),
    human_power_id INTEGER NOT NULL REFERENCES human_powers(id)
);

CREATE TABLE child_hero_super_powers(
    child_hero_id INTEGER NOT NULL REFERENCES child_heroes(id),
    super_power_id INTEGER NOT NULL REFERENCES super_powers(id)
);

-- Children are automatically sent certain messages after they've been a
-- member a fixed amount of time.
CREATE TABLE weekly_messages(
    id SERIAL PRIMARY KEY,
    week INTEGER NOT NULL, -- Which week of membership the message should be sent during (0-indexed)
    superhero_id INTEGER REFERENCES superheroes(id), -- The superhero the message is from
    message TEXT NOT NULL
);

-- The child hero's tasks belong to categories, e.g. "Hero's Mind," "Hero's Body," "Extra Credit"
CREATE TABLE categories(
    name CHARACTER VARYING(128) PRIMARY KEY,
    "order" INTEGER NOT NULL
);

CREATE TABLE tasks(
    id SERIAL PRIMARY KEY,
    child_hero_id INTEGER REFERENCES child_heroes(id), -- If a task is assigned to a specific child by a guardian.
    name CHARACTER VARYING(128),
    description TEXT,
    category CHARACTER VARYING(64) REFERENCES categories(name),
    points INTEGER NOT NULL -- points for completing the task
);

CREATE TABLE child_hero_custom_tasks (
	id SERIAL PRIMARY KEY,
	child_hero_id INTEGER NOT NULL REFERENCES child_heroes(id),
	task_id INTEGER NOT NULL REFERENCES tasks(id),
	name CHARACTER VARYING NOT NULL
);

CREATE TABLE child_hero_tasks(
    id SERIAL PRIMARY KEY,
    child_hero_id INTEGER NOT NULL REFERENCES child_heroes(id),
    task_id INTEGER REFERENCES tasks(id),
    completed_at DATE NOT NULL DEFAULT now(),
    approved BOOLEAN NOT NULL
);



CREATE TABLE task_messages(
    id SERIAL PRIMARY KEY,
    task_id INTEGER REFERENCES tasks(id),
    superhero_id INTEGER REFERENCES superheroes(id), -- Who the message is from
    lower_bound INTEGER NOT NULL, -- Message should be sent if child's performance is in this range.
    message TEXT
);

--
-- Subjects
--

CREATE TABLE subjects (
    id SERIAL PRIMARY KEY,
    name CHARACTER VARYING UNIQUE,
    "order" INTEGER NOT NULL
);

INSERT INTO subjects (name, "order") VALUES ('Fitness', 0);
INSERT INTO subjects (name, "order") VALUES ('Nutrition', 1);
INSERT INTO subjects (name, "order") VALUES ('Cyber Safety', 2);
INSERT INTO subjects (name, "order") VALUES ('Non Violence', 3);
INSERT INTO subjects (name, "order") VALUES ('MindUp', 4);
INSERT INTO subjects (name, "order") VALUES ('Environmental Sustainability', 5);

--
-- Ranks
--

ALTER TABLE child_hero_ranks ADD COLUMN id SERIAL PRIMARY KEY;

--
-- Assessments
--

CREATE TABLE assessments (
    id SERIAL PRIMARY KEY,
    rank_id INTEGER NOT NULL REFERENCES child_hero_ranks (id),
    message TEXT NOT NULL
);

CREATE TABLE assessment_questions (
    id SERIAL PRIMARY KEY,
    assessment_id INTEGER NOT NULL REFERENCES assessments (id),
    category_id INTEGER NOT NULL REFERENCES categories (id),
    question CHARACTER VARYING NOT NULL,
    "order" INTEGER NOT NULL,
    UNIQUE (assessment_id, category_id, question)
);

CREATE TABLE assessment_responses (
    id SERIAL PRIMARY KEY,
    assessment_id INTEGER NOT NULL REFERENCES assessments (id),
    user_id INTEGER NOT NULL REFERENCES users (id),
    completed_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE assessment_answers (
    id SERIAL PRIMARY KEY,
    assessment_response_id INTEGER NOT NULL REFERENCES assessment_responses (id),
    assessment_question_id INTEGER NOT NULL REFERENCES assessment_questions (id),
    answer INTEGER NOT NULL,
    UNIQUE (assessment_response_id, assessment_question_id)
);

--
--Articles
--

CREATE TABLE libraries (
    id SERIAL PRIMARY KEY,
    rank_id INTEGER NOT NULL REFERENCES child_hero_ranks(id)
);

CREATE TABLE library_article_types (
    article_type CHARACTER VARYING NOT NULL UNIQUE
);

CREATE TABLE library_articles (
    id SERIAL PRIMARY KEY,
    library_id INTEGER NOT NULL REFERENCES libraries(id),
    subject_id INTEGER NOT NULL REFERENCES subjects(id),
    article_type CHARACTER VARYING NOT NULL REFERENCES library_article_types(article_type),
    name CHARACTER VARYING NOT NULL,
    points INTEGER NOT NULL,
    "order" INTEGER NOT NULL,
    url CHARACTER VARYING,
    pages REAL NOT NULL
);

CREATE TABLE library_responses (
    id SERIAL PRIMARY KEY,
    library_id INTEGER NOT NULL REFERENCES libraries(id),
    user_id INTEGER NOT NULL REFERENCES users(id),
    completed_at TIMESTAMP
);

CREATE TABLE library_answers (
    id SERIAL PRIMARY KEY,
    library_response_id INTEGER NOT NULL REFERENCES library_responses(id),
    library_article_id INTEGER NOT NULL REFERENCES library_articles(id),
    created_on TIMESTAMP DEFAULT NOW(),
    completed_on TIMESTAMP
);

--
-- Quiz
--

CREATE TABLE quizzes (
    id SERIAL PRIMARY KEY,
    rank_id INTEGER NOT NULL REFERENCES child_hero_ranks (id)
);

CREATE TABLE quiz_questions (
    id SERIAL PRIMARY KEY,
    quiz_id INTEGER NOT NULL REFERENCES quizzes (id),
    subject_id INTEGER NOT NULL REFERENCES subjects (id),
    library_article_id INTEGER REFERENCES library_articles(id),
    points INTEGER NOT NULL,
    question CHARACTER VARYING NOT NULL,
    library_article_id INTEGER REFERENCES library_articles (id),
    "order" INTEGER NOT NULL,
    UNIQUE (quiz_id, subject_id, question)
);

CREATE TABLE quiz_question_options (
    id SERIAL PRIMARY KEY,
    quiz_question_id INTEGER NOT NULL REFERENCES quiz_questions (id),
    option CHARACTER VARYING NOT NULL,
    correct BOOLEAN NOT NULL,
    "order" INTEGER NOT NULL,
    UNIQUE (quiz_question_id, option)
);

CREATE UNIQUE INDEX quiz_question_options_idx ON quiz_question_options (quiz_question_id) WHERE correct = TRUE;

CREATE TABLE quiz_responses (
    id SERIAL PRIMARY KEY,
    quiz_id INTEGER NOT NULL REFERENCES quizzes (id),
    user_id INTEGER NOT NULL REFERENCES users (id),
    completed_at TIMESTAMP,
    UNIQUE (quiz_id, user_id)
);

CREATE TABLE quiz_answers (
    id SERIAL PRIMARY KEY,
    quiz_response_id INTEGER NOT NULL REFERENCES quiz_responses (id),
    quiz_question_id INTEGER NOT NULL REFERENCES quiz_questions (id),
    quiz_question_option_id INTEGER NOT NULL REFERENCES quiz_question_options (id),
    created_on TIMESTAMP NOT NULL DEFAULT NOW(),
    UNIQUE(quiz_response_id, quiz_question_id, Quiz_question_option_id)
);

--
-- Competition
--

CREATE TABLE competitions (
	id SERIAL PRIMARY KEY,
	name CHARACTER VARYING NOT NULL,
	description CHARACTER VARYING NOT NULL,
	passcode CHARACTER VARYING NOT NULL,
	start_date DATE NOT NULL,
	end_date DATE NOT NULL
);

CREATE TABLE districts (
	id SERIAL PRIMARY KEY,
	contact_prefix CHARACTER VARYING NOT NULL,
	contact_first_name CHARACTER VARYING NOT NULL,
	contact_last_name CHARACTER VARYING NOT NULL,
	contact_super_hero_name CHARACTER VARYING NOT NULL,
	contact_email CHARACTER VARYING NOT NULL,
	contact_phone CHARACTER VARYiNG NOT NULL,
	contract_users INTEGER NOT NULL,
	contract_start_date DATE NOT NULL,
	contract_length INTEGER NOT NULL,
	passcode NOT NULL UNIQUE
);

CREATE TABLE schools (
	id SERIAL PRIMARY KEY,
	district_id INTEGER NOT NULL REFERENCES districts(id),
	name CHARACTER VARYING NOT NULL,	
	contact_prefix CHARACTER VARYING NOT NULL,
	contact_first_name CHARACTER VARYING NOT NULL,
	contact_last_name CHARACTER VARYING NOT NULL,
	contact_super_hero_name CHARACTER VARYING NOT NULL,
	contact_email CHARACTER VARYING NOT NULL,
	contact_phone CHARACTER VARYiNG NOT NULL,
	address CHARACTER VARYING NOT NULL,
	city CHARACTER VARYING NOT NULL,
	state CHARACTER VARYING NOT NULL,
	zip CHARACTER VARYING NOT NULL,
	latitude REAL,
	longitude REAL	
);

CREATE TABLE classroom (
	id SERIAL PRIMARY KEY,
	school_id INTEGER NOT NULL REFERENCES schools(id),
	contact_prefix CHARACTER VARYING NOT NULL,
	contact_first_name CHARACTER VARYING NOT NULL,
	contact_last_name CHARACTER VARYING NOT NULL,
	contact_super_hero_name CHARACTER VARYING NOT NULL,
	contact_email CHARACTER VARYING NOT NULL,
	contact_phone CHARACTER VARYiNG NOT NULL,
	grade_type CHARACTER VARYING NOT NULL REFRENCES grade_types(name),
	active BOOLEAN NOT NULL,
	passcode NOT NULL UNIQUE
);

CREATE TABLE competition_teams (
	competition_id INTEGER NOT NULL REFERENCES competitions(id),
	group_id INTEGER NOT NULL REFERENCES groups(id),
	UNIQUE (competition_id, group_id)
);

CREATE TABLE classroom_information (
	id SERIAL PRIMARY KEY,
	school_id INTEGER NOT NULL REFERENCES schools(id),
	group_id INTEGER NOT NULL UNIQUE REFERENCES groups(id),
	grade CHARACTER VARYING NOT NULL,
	coach_first_name CHARACTER VARYING NOT NULL,
	coach_last_name CHARACTER VARYING NOT NULL,
	coach_email CHARACTER VARYING NOT NULL,
	coach_phone CHARACTER VARYING NOT NULL
);

CREATE TABLE competition_school_standings (
	id SERIAL PRIMARY KEY
	competition_id INTEGER NOT NULL REFERENCES competitions(id),
	school_id INTEGER NOT NULL REFERENCES schools(id), 
	average_points REAL NOT NULL, 
	total_points REAL NOT NULL, 
	created TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE competition_user_standings (
	id SERIAL PRIMARY KEY,
	user_id INTEGER NOT NULL REFERENCES users(id),
	competition_id INTEGER NOT NULL REFERENCES competitions(id),	
	school_id INTEGER NOT NULL REFERENCES schools(id),
	total_points REAL NOT NULL,
	rank_name CHARACTER VARYING NOT NULL,
	school_name CHARACTER VARYING NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT now()
);

CREATE TABLE competition_user_category_standings (
	id SERIAL PRIMARY KEY,
	competition_id INTEGER NOT NULL REFERENCES competitions(id),
	user_id INTEGER NOT NULL REFERENCES users(id), 
	category_id INTEGER NOT NULL REFERENCES categories(id),
	total_points REAL NOT NULL, 
	rank_name CHARACTER VARYING NOT NULL,
	school_name CHARACTER VARYING NOT NULL,
	created TIMESTAMP NOT NULL DEFAULT now()
);















