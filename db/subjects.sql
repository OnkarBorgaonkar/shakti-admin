INSERT INTO subjects (name, display_name, "order") VALUES ('Fitness', 'Body Works', 0);
INSERT INTO subjects (name, display_name, "order") VALUES ('Nutrition', 'Energy Source', 1);
INSERT INTO subjects (name, display_name, "order") VALUES ('Non Violence', 'Peace Out'2);
INSERT INTO subjects (name, display_name, "order") VALUES ('Identity', 'Alter Ego', 3);
INSERT INTO subjects (name, display_name, "order") VALUES ('Mind - Brain', 'Mind Works', 4);
INSERT INTO subjects (name, display_name, "order") VALUES ('Environmental Sustainability', 'Green Machine', 5);
