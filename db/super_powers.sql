INSERT INTO super_powers (name, description) SELECT 'Forcefield', 'A powerful forcefield protects your body';
INSERT INTO super_powers (name, description) SELECT 'Genius', 'Genius level intellect';
INSERT INTO super_powers (name, description) SELECT 'Fire', 'Transforms to fire and controls fire';
INSERT INTO super_powers (name, description) SELECT 'Indestructible', 'Skin made of hard steel';