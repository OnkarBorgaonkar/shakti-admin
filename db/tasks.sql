--hero's body
INSERT INTO tasks (name, category, points) VALUES('Exercised for 20 minutes?','Hero''s Body','10');
INSERT INTO tasks (name, category, points) VALUES('Ate 3-5 helpings of fruits and vegetables?','Hero''s Body','10');
INSERT INTO tasks (name, category, points) VALUES('Ate junk food less than twice today?','Hero''s Body','10');
INSERT INTO tasks (name, category, points) VALUES('Went to bed on time?','Hero''s Body','10');

--hero's mind
INSERT INTO tasks (name, category, points) VALUES('Made it to school on-time?','Hero''s Mind','10');
INSERT INTO tasks (name, category, points) VALUES('Completed your homework on-time?','Hero''s Mind','10');
INSERT INTO tasks (name, category, points) VALUES('Took a �time-in� 2 - 3 times today?','Hero''s Mind','10');
INSERT INTO tasks (name, category, points) VALUES('Limited TV and video games to one hour?','Hero''s Mind','10');
INSERT INTO tasks (name, category, points) VALUES('Told the truth all day today?','Hero''s Mind','10');

--hero's heart
INSERT INTO tasks (name, category, points) VALUES('Were kind to those around you?','Hero''s Heart','10');
INSERT INTO tasks (name, category, points) VALUES('Wrote in your journal today?','Hero''s Heart','10');
INSERT INTO tasks (name, category, points) VALUES('Showed compassion for someone less fortunate than you?','Hero''s Heart','10');
INSERT INTO tasks (name, category, points) VALUES('Did a nice deed for someone today? ','Hero''s Heart','10');

--hero's community
INSERT INTO tasks (name, category, points) VALUES('Completed your chores?','Hero''s Community','10');
INSERT INTO tasks (name, category, points) VALUES('Reported to an adult about a bully or cyber-bully?','Hero''s Community','10');
INSERT INTO tasks (name, category, points) VALUES('Supported your teammates?','Hero''s Community','10');

--exra credit
INSERT INTO tasks(name, category, points) VALUES('Had your parents read to you this week', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Took a long walk  (or went outside) with your family ', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Helped out with the grocery planning/shopping', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Improved your report card!!!!  ', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Completed a book', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Completed an art project', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Won the Warriors in Action Game', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Joined a team (Shakti Warriors team, sport, dance, etc.) ', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Baby sit your brother/sister', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Raked you or your neighbors leaves', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Shoveled you or your neighbors snow', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Mowed you or your neighbors lawn', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Washed the car', 'Extra Credit', '10');
INSERT INTO tasks(name, category, points) VALUES('Helped out with the laundry', 'Extra Credit', '10');



