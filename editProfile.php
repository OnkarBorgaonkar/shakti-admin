<?php
	require_once("globals.php");
	require_once("Authenticator.php");

	$pageTitle = "Profile";
	$currentYear = date("Y");
	$child = $user->getChild();
	include("editProfile.phtml");
?>
