<?php
	require_once("globals.php");
	require_once("Authenticator.php");
	$pageTitle = "Superhero";
	
	$childHeroId = $user->getChildHeroId();

/** Human Powers **/	
	$humanPowerService = new HumanPowerService();
	$humanPowers = $humanPowerService->find();

	$childHeroHumanPowers = $humanPowerService->findForChildHeroId($childHeroId);

	$humanPower1 = array_pop($childHeroHumanPowers);
	$humanPower1 = $humanPower1->id;
	$humanPower2 = array_pop($childHeroHumanPowers);
	$humanPower2 = $humanPower2->id;
	$humanPower3 = array_pop($childHeroHumanPowers);
	$humanPower3 = $humanPower3->id;
/** End Human Powers **/

/** Super Powers **/	
	$superPowerService = new SuperPowerService();
	$superHeroPowers = $superPowerService->find();
	
	$childHeroSuperPowers = $superPowerService->findForChildHeroId($childHeroId);
	
	$superHeroPower1 = array_pop($childHeroSuperPowers);
	$superHeroPower1 = $superHeroPower1->id;
	$superHeroPower2 = array_pop($childHeroSuperPowers);
	$superHeroPower2 = $superHeroPower2->id;
/** End Super Powers **/
	
	include("editSuperHero.phtml");
?>