<?php
	require_once("globals.php");
	require_once("Authenticator.php");
	$pageTitle = "Extra";	
	
	// Get the string representation of the date of the Sunday of the current week.
	$baseDate = Date("Y-m-d", strtotime("last Sunday"));

	$taskService = new TaskService();
	$tasksByCategories["Extra Credit"] = $taskService->findByCategoryName("Extra Credit");

	include("extraCredit.phtml");
?>