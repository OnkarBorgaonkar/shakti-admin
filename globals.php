<?php

require_once("config/Config.php");
Config::init();


require_once("Inflector.php");

function __autoload($className)
{
    require_once($className . ".php");
}
/**
* The call to session_start() must be after all of the required classes have been loaded via require_once above.
*/
session_start();

if (!isset($_SESSION['user']) && isset($_COOKIE['bd-bucket']))
	setcookie ("bd-bucket", "", time() - 3600, "/", ".shaktiwarriors.com", false, false);


?>

