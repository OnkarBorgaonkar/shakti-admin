<?php
    require_once("globals.php");
    $pageTitle = "Group Info";

    $groupService = new GroupService();
    $group = array_pop($groupService->find("id =" .  $_REQUEST['groupId']));

    $groupMembers = $group->getMembers();

    $totalPoints = $group->getTotalPoints();

    $groupSize = sizeof($groupMembers);
    $groupLeader = $group->getLeader();

    $user = $_SESSION['user'];

    if($user != null){
        $isMember = $group->isMember($user);
    }

    $goal = $group->getGoal();

    $groupRequestService = new GroupRequestService();
    $requests = $groupRequestService->find("group_id = $group->id");
    
    //$competitions = $group->getCompetitions();

    include("groupDetails.phtml");
?>
