<?php
    require_once("globals.php");
    require_once("Authenticator.php");

    $userService = new UserService();
    $requestUserId = $_GET['userId'];
    $requestUser = array_pop($userService->find("id = $requestUserId"));

    if($requestUser == null){
        $message = htmlspecialchars("Member Does not Exist");
        $url = $_SERVER['HTTP_REFERER'] . "&systemMessage=$message";
        header("location: $url");
    }

    $groups = $user->getLeaderTeams();

    if (sizeof($groups) == 0){
        $message = htmlspecialchars("You have no Groups to Invite to");
        $url = $_SERVER['HTTP_REFERER'] . "&systemMessage=$message";
        header("location: $url");
    }

    $pageTitle = "Invite";
    include("groupInvite.phtml");
?>
