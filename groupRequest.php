<?php
    require_once("globals.php");
    require_once("Authenticator.php");

    $groupId = $_GET['groupId'];
    
    $groupService = new GroupService();
    $group = array_pop($groupService->find("id = $groupId"));
    
    if ($group->typeName == "CLASS_ROOM" && $groupService->isUserInAClassroom($user->id)) {
        $humanReadableMessage = "You Already Belong to a Classroom Team";
        $systemMessage = urlencode($humanReadableMessage);
    
        header("location: /groupDetails.php?groupId=$group->id&systemMessage=$systemMessage");
        die();
    }

    $pageTitle = "Request";
    include("groupRequest.phtml");
?>
