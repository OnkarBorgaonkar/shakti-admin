module.exports = function (grunt) {
    grunt.option("admin_less", 'public/admin/styles/less/');
    grunt.option("sol_less", 'public/summerOfLearning/styles/less/');
    grunt.option("admin_outputCss", 'public/admin/styles/css/');
    grunt.option("sol_outputCss", 'public/summerOfLearning/dist/css/');
    grunt.option("admin_templates", 'app/views/admin/');
    grunt.option("sol_templates", 'app/views/summerOfLearning/');

    grunt.option("sol_js", 'public/summerOfLearning/js');
    grunt.option("output_sol_js", 'public/summerOfLearning/dist/js');

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'), // the package file to use

        less: {
            admin_development: {
                options: {
                    sourceMap: true,
                    compress: false,
                    cleancss: false
                },
                files: {
                    "<%= grunt.option(\'admin_outputCss\') %>/main.css" : "<%= grunt.option(\'admin_less\') %>/main.less"
                }
            },
            sol_development: {
                options: {
                    sourceMap: true,
                    compress: false,
                    cleancss: false
                },
                files: {
                    "<%= grunt.option(\'sol_outputCss\') %>/main.css" : "<%= grunt.option(\'sol_less\') %>/main.less"
                }
            },
            admin_production: {
                options: {
                    outputSourceFiles: false,
                    sourceMap: false,
                    cleancss: true,
                    compress: true
                },
                files: {
                    "<%= grunt.option(\'admin_outputCss\') %>/main.css": "<%= grunt.option(\'admin_less\') %>/main.less"
                }
            },
            sol_production: {
                options: {
                    outputSourceFiles: false,
                    sourceMap: false,
                    cleancss: false,
                    compress: true
                },
                files: {
                    "<%= grunt.option(\'sol_outputCss\') %>/main.css": "<%= grunt.option(\'sol_less\') %>/main.less"
                }
            }
        },
        watch: {
            admin_css: {
                files: ['<%= grunt.option(\'admin_less\') %>/**/*.less'],
                tasks: ['less:admin_development']
            },
            sol_css: {
                options: {
                    spawn: true
                },
                files: ['<%= grunt.option(\'sol_less\') %>/**/*.less'],
                tasks: ['less:sol_development']
            },
            sol_js: {
                options: {
                    spawn: true
                },
                files: ['<%= grunt.option(\'sol_js\') %>/**/*.js'],
                tasks: ['jshint', 'uglify:application']
            }
        },
        jshint: {
            options: {
                sub: true,
                es3: true,
                curly: true,
                expr: true,
                eqnull: true,
                globals: {
                    jQuery: true
                }
            },
            all: ['<%= grunt.option(\'sol_js\') %>/application/**/*.js']
        },
        uglify: {
            options: {
                beautify: false,
                compress: true, // turn off for speed
                sourceMap: false
            },
            vendor: {
                options : {
                    compress : false
                },
                files: {
                    '<%= grunt.option(\'output_sol_js\') %>/vendor.min.js': [
                        '<%= grunt.option(\'sol_js\') %>/vendor/*.js',
                        '<%= grunt.option(\'sol_js\') %>/vendor/**/*.js' ]
                }
            },
            application: {
                files: {
                    '<%= grunt.option(\'output_sol_js\') %>/application.min.js': [
                        '<%= grunt.option(\'sol_js\') %>/application/ShaktiSOL.js',
                        '<%= grunt.option(\'sol_js\') %>/application/Application.js',
                        '<%= grunt.option(\'sol_js\') %>/application/page/GlobalPage.js',
                        '<%= grunt.option(\'sol_js\') %>/application/page/**/*.js',
                        '<%= grunt.option(\'sol_js\') %>/application/widget/**/*.js' ]
                }
            }

        },
        addAssets: {
            admin_css: {
                options: {
                    base: '/<%= grunt.option(\'admin_outputCss\') %>/'
                },
                files: {
                    '<%= grunt.option(\'admin_templates\') %>/include/cssFiles.phtml': ['<%= grunt.option(\'admin_outputCss\') %>/**.css']
                }
            },
            sol_css: {
                options: {
                    base: '/<%= grunt.option(\'sol_outputCss\') %>/'
                },
                files: {
                    '<%= grunt.option(\'sol_templates\') %>/include/cssFiles.phtml': ['<%= grunt.option(\'sol_outputCss\') %>/**.css']
                }
            },
            sol_js: {
                options: {
                    type: 'js',
                    base: '/<%= grunt.option(\'output_sol_js\') %>/'
                },
                files: {
                    '<%= grunt.option(\'sol_templates\') %>/include/js.phtml': [
                        '<%= grunt.option(\'output_sol_js\') %>/vendor.min.js',
                        '<%= grunt.option(\'output_sol_js\') %>/application.min.js']
                }
            }
        }
    });


    grunt.loadNpmTasks('grunt-addassets');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');

    //by default
    grunt.registerTask('default', ['less:admin_development', 'less:sol_development', 'jshint', 'uglify', 'addAssets']);
    grunt.registerTask('admin', ['less:admin_development', 'addAssets:admin_css']);
    grunt.registerTask('sol', ['less:sol_development', 'jshint', 'uglify', 'addAssets:sol_css']);

    //by watch
    grunt.registerTask('server', ['default', 'watch']);
    grunt.registerTask('serveradmin', ['admin', 'watch:admin_css']);
    grunt.registerTask('serversol', ['sol', 'watch']);


    //build
    grunt.registerTask('deploy', ['less:admin_production', 'less:sol_production', 'jshint', 'uglify', 'addAssets']);

};
