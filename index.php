<?php
	require_once("globals.php");
	$pageTitle = "Home";
	
	// For the hero machine link
	$heroMachineSystemMessage = urlencode("After you join the Academy you can create your Hero Machine Avatar!");
	
	include("index.phtml");
?>