<?php
    require_once("globals.php");
    require_once("Authenticator.php");
    require_once("isGroupLeader.php");
   
    $competitionService = new CompetitionService();
    $today = date("Y-m-d");
    $competitions = $competitionService->find("end_date > '$today' ORDER BY name");
    
    $pageTitle = "Join Competition";
    include("joinCompetition.phtml");
?>