<?php
	require_once("../globals.php");
	if (!isset($_SESSION['manager'])) {
        require_once("Authenticator.php");
	}
	else {
	    $role = isset($_SESSION['manager']['role']) ? $_SESSION['manager']['role'] : null;
	    if ($role == null || ($role != 'district' && $role != 'school' && $role != 'classroom')) {
	        $systemMessage = urlencode("You do not have access to do this.");
	        header("location: /index.php?systemMessage=$systemMessage");
	    }
	}
    
    $userClassroomService = new UserClassroomService();
    $classroomService = new ClassroomService();
    
    $success = true;
    $message = "";
    
    $userClassroom = new UserClassroom();
    $userClassroom->setAll($_POST);
    
    $classroom = $classroomService->getClassroomByUserId($userClassroom->userId);
    if ($classroom != null) {
        $message = "This user already belongs to the classroom \"$classroom->name\"";
        $success = false;
    }
    else {         
        try {
            $userClassroomService->save($userClassroom);
        }
        catch(PDOException $e) {
            $message = "Something went wrong while saving to the database.";
            $success = false;
        }
    }
    
    $json = array (
    	"success" => $success,
    	"message" => $message
    );
    
    echo json_encode($json);
    die;
	
?>