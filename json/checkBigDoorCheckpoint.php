<?php
    require_once("../globals.php");
    
    $bigDoorCheckpointId = $_REQUEST['bigDoorCheckpointId'];
    
    $bigDoorCheckpointService = new BigDoorCheckpointService();
    $bigDoorCheckpoint = array_pop($bigDoorCheckpointService->find("big_door_checkpoint_id = $bigDoorCheckpointId"));
    
    $found = false;
    $description = "";
    if ($bigDoorCheckpoint != null) {
        $found = true;
        $description = $bigDoorCheckpoint->description;
    }
    
    $json = array (
        "found" => $found,
        "description" => $description
    );
    
    echo json_encode($json);    
?>