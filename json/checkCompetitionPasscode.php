<?php
    require_once("../globals.php");
    
    $passcode = $_GET['passcode'];
    
    if ($passcode == null) {
        $json = array(
                "valid" => false,
                "message" => "No Passcode Supplied"
        );
    
        echo json_encode($json);
        die();
    }
    
    $competitionService = new CompetitionService();
    $competition = array_pop($competitionService->find("passcode = '$passcode'"));
    
    if ($competition == null) {
        $json = array(
            "valid" => false,
            "message" => "Invalid Passcode"
        );
        
        echo json_encode($json);
        die();
    }
    
    if ($competition->getStatus() == "Completed") {
        $json = array(
                "valid" => false,
                "message" => "This Competition has already ended"
        );
    
        echo json_encode($json);
        die();
    }
    
    $json = array (
        "valid" => true,
        "competitionId" => $competition->id
    );
    
    echo json_encode($json);    
?>