<?php
    require_once("../globals.php");

    $username = trim(strtolower($_REQUEST['user']['login']));

    $sql = "lower(login) = '$username'";
    if (isset($_REQUEST['id']))
        $sql .= " AND id != {$_REQUEST['id']}";

    $userService = new UserService();
    $existingUser = array_pop($userService->find($sql));

    if ($existingUser == null) {
        echo json_encode(true);
    }
    else {
        echo  json_encode("Sorry, that super hero name is already taken.  But it's ok...try adding a number on the end like Bright Wind32, or Sonic Power4.");
    }
?>