<?php
    require_once("../globals.php");

    $state = $_REQUEST['state'];
    $city = strtolower($_REQUEST['term']);

    $locationService = new LocationService();
    $sql = "
        SELECT DISTINCT ON (city)
            city
        FROM
            locations
        WHERE
            state_abbr = '$state'
            AND lower(city) ~* '$city'
    ";
    $locations = $locationService->findBySql($sql);
    echo json_encode($locations);
?>