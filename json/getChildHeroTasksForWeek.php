<?php
	require_once("../globals.php");
	require_once("Authenticator.php");
	
	$date = $_GET['baseDate'];	
	$childHeroId = $user->getChildHeroId();
	
	$childHeroTaskService = new ChildHeroTaskService();
	print(json_encode($childHeroTaskService->findForChildHeroAndWeek($childHeroId, $date)));
?>