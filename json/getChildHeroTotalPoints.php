<?php
/**
* This script doesn't return JSON, but a simple number e.g. "1432".
*/
    require_once("../globals.php");
    require_once("Authenticator.php");

    print number_format($user->getTotalPoints());
?>
