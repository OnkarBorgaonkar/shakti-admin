<?php
    /**
    * Note: due to requirements of the jquery.autocomplete.js plugin, this file doesn't
    * return JSON, but flat text.
    *
    * This file is used by recruits.php to build an autocompletable list of users from which the
    * logged in user can select his recruiter.
    */
    require_once("../globals.php");

    $userService = new UserService();
    foreach($userService->find("login ilike '%${_REQUEST['q']}%'") as $user)
        echo "$user->login\n";
?>
