<?php
	require_once("../globals.php");
	require_once("Authenticator.php");
	$weekStartDate = $_POST['weekStartDate'];	
	$childHeroId = $user->getChildHeroId();
	
	$childHeroTaskService = new ChildHeroTaskService();
	$tasksForWeek = ($childHeroTaskService->findForChildHeroAndWeek($childHeroId, $weekStartDate));

	$sum = 0;
	foreach ($tasksForWeek as $task)
	{
		if ($task->approved)
			$sum += $task->points;
	}
	print $sum;
?>