<?php
    require_once("../globals.php");
    require_once("Authenticator.php");

    $libraryAnswers = $user->getCurrentLibraryAnswers();

    echo json_encode($libraryAnswers);
?>
