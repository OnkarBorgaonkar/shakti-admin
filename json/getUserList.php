<?php
    require_once("../globals.php");

    $term = $_GET['term'];

    $userService = new UserService();
    $sql = "
        SELECT
            users.*,
            children.first_name,
            children.student_id
        FROM
            users INNER JOIN children ON users.id = children.user_id
        WHERE
            lower(users.login) ~* lower('$term')
    ";
    $users = $userService->findBySql($sql);


    foreach ($users as $user) {
        unset($user->password);
        unset($user->salt);
        $user->rankName = $user->getRankName();
        $user->avatar = $user->getAvatarImage();
    }
    echo json_encode($users);
?>