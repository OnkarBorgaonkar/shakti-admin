<?php
    require_once("../globals.php");

    $text = $_REQUEST['text'];
    $webPurifyService = new WebPurifyService();
    $result = $webPurifyService->checkForProfanity($text);
    
    $json = array("result" => $result);
    echo json_encode($json);
?>