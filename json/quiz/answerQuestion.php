<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $quizResponse = $user->getCurrentQuizResponse();

    $quizQuestionOptionId = $_REQUEST['quizQuestionOptionId'];

    $quizAnswer = new QuizAnswer();
    $quizAnswer->quizQuestionId = $_REQUEST['quizQuestionId'];
    $quizAnswer->quizResponseId = $quizResponse->id;

    $quizQuestionOptionService = new QuizQuestionOptionService();
    $option = array_pop($quizQuestionOptionService->find("id = $quizQuestionOptionId"));

    if ($option != null && $option->correct){
        $quizAnswerService = new QuizAnswerService();

        try{
            $quizAnswerService->save($quizAnswer);

            $quizQuestionService = new QuizQuestionService();
            $quizQuestion = array_pop($quizQuestionService->find("id = $quizAnswer->quizQuestionId"));

            $userPointsEntry = new UserPointsEntry();
            $userPointsEntry->pointType = "ECHO_BOARD";
            $userPointsEntry->points = $quizQuestion->points;
            $userPointsEntry->activityId = $quizAnswer->id;

            $userPointsEntryService = new UserPointsEntryService();
            $userPointsEntryService->saveEntry($userPointsEntry, $user);

            $user->recalculateRank();

            echo "correct";
        }
        catch(PDOException $e){
            echo $e->getMessage();
        }
        $quizQuestionService = new QuizQuestionService();
        $quizQuestions = $quizQuestionService->getNotAnsweredQuestions($quizResponse);

        if (sizeof($quizQuestions) == 0){
            $now = new DateTime();
            $quizResponse->completedAt = $now->format("Y-m-d H:i:s");

            $quizResponseService = new QuizResponseService();
            $quizResponseService->save($quizResponse);
            $user->recalculateRank();
        }
    }
    else{
        echo "wrong";
    }

?>
