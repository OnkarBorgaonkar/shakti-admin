<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $quizResponse = $user->getCurrentQuizResponse();

    if($quizResponse != null){
        $quizQuestionService = new QuizQuestionService();
        $quizQuestions = $quizQuestionService->getAnsweredQuestions($quizResponse);

        print_r(json_encode($quizQuestions));
    }
?>
