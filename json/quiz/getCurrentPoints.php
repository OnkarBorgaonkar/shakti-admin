<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $quizResponse = $user->getCurrentQuizResponse();

    $points = 0;

    if($quizResponse != null){
        $points = $quizResponse->getCurrentPoints();
    }

    $json = array("points" => $points);

    echo json_encode($json);

?>
