<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");

    $quiz = $user->getCurrentQuiz();

    if ($quiz != null){
        $subjectService = new SubjectService();
        $subjects = $subjectService->find('true ORDER BY "order"');

        $quizQuestionService = new QuizQuestionService();
        $quizQuestionOptionService = new QuizQuestionOptionService();

        foreach ($subjects as $subject){
            $quizQuestions = $quizQuestionService->find("quiz_id = $quiz->id and subject_id = $subject->id ORDER BY \"order\"");

            foreach ($quizQuestions as $quizQuestion){
                $options = $quizQuestionOptionService->find("quiz_question_id = $quizQuestion->id ORDER BY \"order\"");
                $quizQuestion->options = $options;
                $quizQuestion->articleLink = Config::$baseUrl . "/library.php";
                if ($quizQuestion->libraryArticleId != null){
                    $quizQuestion->articleLink = Config::$baseUrl . "/app/actions/doReadArticle.php?articleId=$quizQuestion->libraryArticleId";
                 }
            }
            $subject->quizQuestions = $quizQuestions;
        }

        $quiz->subjects = $subjects;
        print_r(json_encode($quiz));
    }

?>
