<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    $articleId = $_REQUEST['articleId'];
    
    $libraryArticleService = new LibraryArticleService();
    $article = array_pop($libraryArticleService->find("id = $articleId"));
    
    $libraryResponseService = new LibraryResponseService();
    $libraryResponse = array_pop($libraryResponseService->find("user_id = $user->id AND library_id = $article->libraryId"));
    if ($libraryResponse == null) {
        $json = array ("hasReadArticle" => false);
        echo json_encode($json);
        die();
    }
    
    $libraryAnswerService = new LibraryAnswerService();
    $libraryAnswer = array_pop($libraryAnswerService->find("library_response_id = $libraryResponse->id AND library_article_id = $article->id"));
    if($libraryAnswer == null) {
        $json = array ("hasReadArticle" => false);
        echo json_encode($json);
        die();
    }
    else {
        $json = array ("hasReadArticle" => true);
        echo json_encode($json);
        die();
    }
    
?>