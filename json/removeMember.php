<?php
	require_once("../globals.php");
	require_once("../globals.php");
	if (!isset($_SESSION['manager'])) {
        require_once("Authenticator.php");
	}
	else {
	    $role = isset($_SESSION['manager']['role']) ? $_SESSION['manager']['role'] : null;
	    if ($role == null || ($role != 'district' && $role != 'school' && $role != 'classroom')) {
	        $systemMessage = urlencode("You do not have access to do this.");
	        header("location: /index.php?systemMessage=$systemMessage");
	    }
	}
    
    $success = true;
    
    $userClassroom = new UserClassroom();
    $userClassroom->setAll($_POST);
    
    $userClassroomService = new UserClassroomService();   
    try {
        $userClassroomService->deleteWhere("classroom_id = $userClassroom->classroomId AND user_id = $userClassroom->userId");
    }
    catch(PDOException $e) {
        $success = false;
    }
    
    $json = array (
    	"success" => $success,
    );
    echo json_encode($json);
    die;
	
?>