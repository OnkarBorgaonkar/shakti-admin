<?php
if (isset($_SESSION['admin'])) {
    $admin = $_SESSION['admin'];

    if ($admin->role != "admin") {
        $sessionService = new SessionService();
        $session = $sessionService->getCurrentSessionForUser($admin);
        if ($session == null) {
            $_SESSION['errorMessage'] = "There is no active session for you right now.";
            $_SESSION['redirect'] = $_SERVER['REQUEST_URI'];
            header("location: /admin/login.php");
            die();
        }
    }
}
else if (!$BYPASS_AUTH) {
    $_SESSION['errorMessage'] = "You must be logged in to access this. Please log in below.";
    $_SESSION['redirect'] = $_SERVER['REQUEST_URI'];
    header("location: /admin/login.php");
    die();
}
?>