<?php
if (isset($_SESSION['user']))
	$user = $_SESSION['user'];
else
{
	$systemMessage = urlencode("You must join now or login to view the requested page.");
	header("location: /index.php?systemMessage=$systemMessage");
}
?>