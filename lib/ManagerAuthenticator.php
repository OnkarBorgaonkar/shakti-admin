<?php
    if (!isset($_SESSION['manager'])) {
        $systemMessage = urlencode("You are not logged in as a manager");
        header("location: /index.php?systemMessage=$systemMessage");    
    }
    
    $role = $_SESSION['manager']['role'];
    $request = $_SERVER['SCRIPT_NAME'];
    
    if (strpos($request, "/district") != false && $role != "district") {
        $systemMessage = urlencode("You do not have proper access to view this");
        header("location: /index.php?systemMessage=$systemMessage");        
    }
    else if (strpos($request, "/school") != false && ($role != "district" && $role != 'school')) {
        $systemMessage = urlencode("You do not have proper access to view this");
        header("location: /index.php?systemMessage=$systemMessage");        
    }
    else if (strpos($request, "/classroom") != false && ($role != "district" && $role != 'school' && $role != 'classroom')) {
        $systemMessage = urlencode("You do not have proper access to view this");
        header("location: /index.php?systemMessage=$systemMessage");        
    }
    

?>