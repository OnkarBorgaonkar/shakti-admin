<?php
    $groupId = $_REQUEST['groupId'];

    $groupService = new GroupService();
    $group = array_pop($groupService->find("id = $groupId"));

    if($group == null){
        $humanReadableMessage = "Group Not Found";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /index.php?systemMessage=$systemMessage");
        die();
    }

    if($group->leaderId != $user->id){
        $humanReadableMessage = "You Are Not the Leader of the Group";
        $systemMessage = urlencode($humanReadableMessage);
        header("location: /groupDetails.php?groupId=$group->id&systemMessage=$systemMessage");
        die();
    }
?>
