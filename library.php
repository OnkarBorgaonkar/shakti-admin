<?php
    require_once("globals.php");
    require_once("Authenticator.php");

    $pageTitle = "Library";

    $libraryArticleService = new LibraryArticleService();

    $articleSubjects = $user->getCurrentLibraryData();

    $totalPoints = 0;
    
    $articleId = isset($_GET['articleId']) ? $_GET['articleId'] : null;

    require_once 'library.phtml';
?>
