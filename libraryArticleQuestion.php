<?php
    require_once("globals.php");
    require_once("Authenticator.php");
    
    $pageTitle = "Library";
    
    $libraryArticleId = $_REQUEST['articleId'];
    $quizQuestionService = new QuizQuestionService();
    $quizQuestion = array_pop($quizQuestionService->find("library_article_id = $libraryArticleId ORDER BY RANDOM()"));
    
    $quizQuestionOptionService = new QuizQuestionOptionService();
    $quizQuestionOptions = $quizQuestionOptionService->find("quiz_question_id = $quizQuestion->id");

    require_once 'libraryArticleQuestion.phtml';
?>
