<?php    
    require_once("../../globals.php");
    require_once("ManagerAuthenticator.php");
    
    if (!isset($_GET['classroomId']) && $role == 'classroom') {
        $systemMessage = urlencode("You do not have access to do this.");
        header("location: /index.php?systemMessage=$systemMessage");
        die();
    }
    
    $classroom = new Classroom();
    $classroom->schoolId = isset($_GET['schoolId']) ? $_GET['schoolId'] : null;
    
    $gradeTypes = Array();
    
    if(isset($_REQUEST['classroomId'])){
        $classroomId = $_REQUEST['classroomId'];
        $classroomService = new ClassroomService();
        $result = array_pop($classroomService->find("id = $classroomId"));
    
        if ($result != null) {
            $classroom = $result;
            $gradeTypes = $classroom->getGradeTypes();
        }
    }
    
    $pageTitle = "Edit Classrooms";
    include("manager/classroom/editClassroom.phtml");
    
?>