<?php
    require_once("../../globals.php");
    require_once("ManagerAuthenticator.php");

    $classroomId = $_GET['classroomId'];
    $classroomServce = new ClassroomService();
    $classroom = array_pop($classroomServce->find("id = $classroomId"));
    
    $students = $classroom->getMembers();
    
    $pageTitle = "Manage Classrooms";
    include("manager/classroom/index.phtml");
?>