<?php
    require_once("../../globals.php");
    require_once("ManagerAuthenticator.php");
    
    $classroomId = $_GET['classroomId'];
    $classroomService = new ClassroomService();
    $classroom = array_pop($classroomService->find("id = $classroomId"));
    
    $userService = new UserService();
    $nonMembers = $userService->find("id NOT IN (SELECT user_id FROM user_classrooms WHERE classroom_id = $classroomId) ORDER BY login");
    
    $members = $classroom->getMembers();

    $pageTitle = "Remove a Member";



    include("manager/classroom/manageStudents.phtml");

?>
