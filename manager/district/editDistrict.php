<?php
    require_once("../../globals.php");
    require_once("ManagerAuthenticator.php");   
    
    $districtId = $_REQUEST['districtId'];
    $districtService = new DistrictService();
    $district = array_pop($districtService->find("id = $districtId"));   
    
    $pageTitle = "Edit District";
    include("manager/district/editDistrict.phtml");    
?>