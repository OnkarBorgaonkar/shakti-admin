<?php
    require_once("../../globals.php");
    require_once("ManagerAuthenticator.php");

    $districtId = $_GET['districtId'];
    $districtServce = new DistrictService();
    $district = array_pop($districtServce->find("id = $districtId"));
    
    $schools = $district->getSchools();
    
    $pageTitle = "Manage District";
    include("manager/district/index.phtml");
?>