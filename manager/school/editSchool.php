<?php 
    require_once("../../globals.php");
    require_once("ManagerAuthenticator.php");
    
    if (!isset($_GET['schoolId']) && $role == 'school') {
        $systemMessage = urlencode("You do not have access to do this.");
        header("location: /index.php?systemMessage=$systemMessage");
        die();
    }
    
    $school = new School();
    $school->districtId = isset($_GET['districtId']) ? $_GET['districtId'] : null;
    
    if(isset($_REQUEST['schoolId'])){        
        $schoolId = $_REQUEST['schoolId'];
        $schoolService = new SchoolService();
        $result = array_pop($schoolService->find("id = $schoolId"));
    
        if ($result != null)
            $school = $result;
    }
    
    $pageTitle = "Edit Schools";
    include("manager/school/editSchool.phtml");
?>