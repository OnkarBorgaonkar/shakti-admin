<?php
    require_once("../../globals.php");
    require_once("ManagerAuthenticator.php");

    $schoolId = $_GET['schoolId'];
    $schoolServce = new SchoolService();
    $school = array_pop($schoolServce->find("id = $schoolId"));
    
    $classrooms = $school->getClassrooms();
    
    $pageTitle = "Manage Schools";
    include("manager/school/index.phtml");
?>