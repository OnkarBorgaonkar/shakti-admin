<?php
    require_once("../../globals.php");
    require_once("Authenticator.php");
    
    if ($user->role != 'admin')
        header("location: /welcome.php");
    
    $schoolId = $_GET['schoolId'];
    
    $competitionService = new CompetitionService();
    $competitions = $competitionService->find("TRUE");
        
    $pageTitle = "Join Competition";    
    include("manager/school/joinCompetition.phtml");    
?>