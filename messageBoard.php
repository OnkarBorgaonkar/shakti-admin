<?php
    require_once("globals.php");
    require_once("Authenticator.php");

    $pageTitle = "Message Board";

    $groupId = $_REQUEST['groupId'];
    $groupService = new GroupService();
    $group = array_pop($groupService->find("id = $groupId"));
    if (!$group->isMember($user)){
        $message = htmlspecialchars("You must be a part of the group to visit the message board.");
        header("location: /groupDetails.php?groupId=$groupId&systemMessage=$message");
    }
    $messages = $group->getMessages();

    include("messageBoard.phtml");


?>


