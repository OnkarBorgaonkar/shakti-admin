<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/**
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://brattoyou/yourdomain/min/builder/
 *
 * See http://brattoyou/code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
    //old javascript files
    "shakti-js" => array(
        "//public/js/jquery-1.5.2.min.js",
        "//public/js/jquery.jcarousel.min.js",
        "//public/js/jquery.carouFredSel.js",
        "//public/js/jquery.cookie.js",
        "//public/js/jquery.validate.min.js",
        "//public/js/additional-methods.min.js",
        "//public/js/jquery-ui-1.8.11.custom.min.js",
        "//public/js/jquery.ui.core.js",
        "//public/js/jquery.ui.draggable.js",
        "//public/js/jquery.ui.droppable.js",
        "//public/js/jquery.ui.sortable.js",
        "//public/js/pageReady.js",
        "//public/js/swfobject.js",
        "//public/js/shaktiUtilities.js",
        "//public/js/fancybox/jquery.fancybox-1.3.4.js",
        "//public/js/jquery.carouFredSel-6.1.0-packed.js",
        "//public/js/jquery.form.js",
        "//public/js/jQueryRotate.2.1.js",
        "//public/js/jquery.tooltip.js",
        "//public/js/vendor/jquery/plugins/jquery.fastLiveFilter.js",
        "//public/js/vendor/jquery/plugins/jquery.tinysort.js"
    ),
    //core framework
    "application-js-core" => array(
        "//public/js/application/Core.js",
        "//public/js/application/Application.js",
        "//public/js/application/page/DefaultPage.js",
        "//public/js/application/page/ApplicationPage.js",
        "//public/js/application/page/HomePage.js",
        "//public/js/application/page/training/BaseTrainingPage.js",
        "//public/js/application/page/training/EditProfilePage.js",
        "//public/js/application/page/training/OrbPage.js",
        "//public/js/application/page/training/WelcomePage2.js",
        "//public/js/application/widget/popup/InformationPopUp.js",
        "//public/js/application/page/admin/classroom/manageStudents.js"
    ),
    //global css files
    "application-css" => array(
        "//public/css/global.css",
        "//public/css/jquery-ui-1.8.13.custom.css",
        "//public/css/forms.css",
        "//public/js/fancybox/jquery.fancybox-1.3.4.css",
        "//public/css/jquery.tooltip.css"
    ),



    //Shakti Admin
    //core framework
    "shaktiAdmin-js" => array(
        //VENDOR
        "//public/admin/js/vendor/jquery-1.10.1.js",
        //"//public/admin/js/vendor/jquery-1.9.0.js",
        
        "//public/admin/js/vendor/jquery-ui-1.10.3.custom.js",
        "//public/admin/js/vendor/jquery.form.js",
        "//public/admin/js/vendor/jquery.validate.js",
        "//public/admin/js/vendor/jquery.validate.additional-methods.js",

        //Bootstrap3
        "//public/admin/js/vendor/bootstrap3/bootstrap.js",

        //Application
        "//public/admin/js/application/ShaktiAdmin.js",
        "//public/admin/js/application/Application.js",
        "//public/admin/js/application/page/GlobalPage.js",
        "//public/admin/js/application/page/home/HomePage.js",
        "//public/admin/js/application/page/district/DistrictFormPage.js",
        "//public/admin/js/application/page/district/DistrictSessionFormPage.js",
        "//public/admin/js/application/page/district/DistrictOverviewPage.js",
        "//public/admin/js/application/page/school/SchoolFormPage.js",
        "//public/admin/js/application/page/program/ProgramOverviewPage.js",
        "//public/admin/js/application/page/program/ProgramFormPage.js",
        "//public/admin/js/application/page/classroom/ClassroomFormPage.js",
        "//public/admin/js/application/page/classroom/ClassroomOverviewPage.js",
        "//public/admin/js/application/page/classroom/ManageStudentsPage.js",
        "//public/admin/js/application/page/competition/CompetitionOverviewPage.js",
        "//public/admin/js/application/page/competition/CompetitionFormPage.js",
        "//public/admin/js/application/page/session/SessionFormPage.js",
        "//public/admin/js/application/page/contact/ContactUsPage.js",
        "//public/admin/js/application/page/stateReport/StateReportPage.js",
        "//public/admin/js/application/page/training/TrainingVideosPage.js"
    )
);
