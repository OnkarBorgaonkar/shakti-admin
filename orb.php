<?php
    require_once("globals.php");
    require_once("Authenticator.php");

    $id = $_GET['id'];

    $userOrbService = new UserOrbService();

    $orbService = new OrbService();
    $orb = array_pop($orbService->find("id = $id"));

    $lockOrb = true;
    if ($orb->name == 'Orb Tracker') {
        $lockOrb = false;
    }
    else {
        $sql = "
            SELECT
                user_orbs.*
            FROM
                user_orbs INNER JOIN orbs ON user_orbs.orb_id = orbs.id
            WHERE
                orbs.name = 'Orb Tracker' AND user_orbs.user_id = $user->id
                AND user_orbs.completed_date IS NOT NULL
        ";
        $orbTrackerCompeted = array_pop($userOrbService->findBySql($sql));
        if ($orbTrackerCompeted != null)
            $lockOrb = false;
        else
            $lockOrb = true;
    }

    $checkpoints = $orb->getCheckpointsForUser($user->id);

    $startingCheckpoint = -1;
    foreach ($checkpoints as $checkpoint) {
        $startingCheckpoint++;
        if (!$checkpoint->completed)
            break;
    }

    $userOrb = array_pop($userOrbService->find("user_id = $user->id AND orb_id = $orb->id"));

    include("orb.phtml");
?>
