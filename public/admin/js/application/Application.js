shaktiAdmin.Application = function(options) {
    var base = this;
    jQuery.extend(base, options);
    base.onBeforeReady();
    jQuery(document).ready(function() {
        base.ready = true;
        base.onReady();
    });
};

shaktiAdmin.Application.prototype = {

    //properties
    ready: false,
    widths: {
        desktop: 980,
        tablet: 900,
        mobile: 600
    },
    //events
    onBeforeReady: jQuery.noop,
    onReady: jQuery.noop,

    //methods
    decimalToPercent: function(rating) {
        return rating / 5 * 100 + '%';
    },

    deleteCookie: function(name) {
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    },

    getCookieValue: function(name) {
        var pieces = document.cookie.split('; ');
        for (var i = 0; i < pieces.length; i++) {
            var parts = pieces[i].split('=');
            if (parts[0] == name)
                return parts[1];
        }
        return null;
    },

    getParamValue: function(name) {
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?#&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec(window.location.href);
        if (results == null) {
            return "";
        }
        else {
            return results[1];
        }
    },

    isNumber: function (testString) {
        return /^\d+$/.test(testString);
    },

    isReady: function() {
        return this.ready === true;
    },

    replaceURLWithHTMLLinks : function(text) {
        if (text == null)
            return;

        var exp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig;
        return text.replace(exp, '<a href="$1" class="newWindow">$1</a>');
    },

    setCookie: function(name, value, expires) {
        if (!expires instanceof Date)
            expires = null;

        var cookie = escape(name) + '=' + escape(value);

        if (expires != null)
            cookie += '; expires=' + expires.toUTCString();

        document.cookie = cookie;
    },

    trackEvent: function(category, action, opt_label, opt_value) {
        if (opt_label == undefined)
            opt_label = "";

        if (opt_value == undefined)
            opt_value = "";

        try {
            _gaq.push(['_trackEvent', category, action, opt_label, opt_value]);
        } catch(err) {
            try {
                console.warn("Event tracking failed: " + action + " " + opt_label);
            } catch(err) {}
        }
    },

    trackPage: function(page) {
        try {
            _gaq.push(['_trackPageview', page]);
        } catch(err) {
            try {
                console.warn("Pageview tracking failed: " + page);
            } catch(err) {}
        }
    }

};