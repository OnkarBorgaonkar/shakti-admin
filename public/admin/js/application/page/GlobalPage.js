shaktiAdmin.page.GlobalPage = shaktiAdmin.extend(shaktiAdmin.Application, {

    //properties
    validationConfig: {
        errorClass: 'has-error',
        validClass: 'has-success',
        highlight : function(element, errorClass, validClass) {
            $($(element).parents('.form-group')[0]).addClass(errorClass).removeClass(validClass);
        },
        unhighlight : function(element, errorClass, validClass) {
            $($(element).parents('.input-wrap')[0]).removeClass(errorClass).addClass(validClass);
        }
    },
    //events

    /**
     * Event handler for when you just want to run code and dont want to wait for document ready.
     */
    onBeforeReady: function() {
        var self = this;

        $(".clickableRow").click(function(e){
            console.log(e, e.target);
            if (e.target.nodeName == "TD") {
                window.document.location = $(this).attr("href");
            }
        });
        if($('input').hasClass('weekpicker') && $('input').hasClass('datepicker')){
            self.bindWeekPicker($(".weekpicker"));
        }else if($('input').hasClass('datepicker')){
            self.bindDatePicker($(".datepicker"));
        }

        // $(".default-form").validate();
    },

    /**
     * Event handler for when you need to wait for document ready.
     */
    onReady: function() {
        var self = this;
        
        $("#emailform").validate({
            rules: {
                "managers[]": {
                    required: true,
                    minlength: 1
                }
            },
            messages: {
                "managers[]": "Please select someone to send the email to."
            },
            submitHandler: function(form) {
                $("#emailform").ajaxSubmit({
                    success: function() {
                        $("#emailSentMessage").show();
                        setTimeout(function() {
                            $("#email-modal").modal('hide');
                            $("#emailSentMessage").hide();
                        }, 1500);
                    }
                });
            }
        });
        
        $(".sendEmail").click(function() {
            var $this = $(this);
            var email = $this.data("email");
            var schoolEmails = $this.data("email-schools");
            var classroomEmails = $this.data("email-classrooms");
            
            var $emailForm = $("#emailform");
            
            $emailForm.find("input[name=adminEmail]").val(email);
            $emailForm.find("input[name=schoolEmails]").val(schoolEmails);
            $emailForm.find("input[name=classroomEmails]").val(classroomEmails);
            
            var $adminCheckbox = $emailForm.find("#to-email-admin input");
            var $schoolCheckbox = $emailForm.find("#to-email-school input");
            var $classroomCheckbox = $emailForm.find("#to-email-classroom input");
            
            $adminCheckbox.prop("checked", true);
            $schoolCheckbox.prop("checked", false);
            $classroomCheckbox.prop("checked", false);

            if (schoolEmails === undefined && classroomEmails === undefined) {
                $emailForm.find("#to-email-admin").removeClass("checkbox");
                $emailForm.find("#to-email-admin input").attr("disabled", true);
                $emailForm.find("#to-email-school").addClass("hidden");
                $emailForm.find("#to-email-classroom").addClass("hidden");
                
                $emailForm.find("#to-email-admin").html(email);
            } else if (schoolEmails === undefined) {
                $emailForm.find("#to-email-admin").addClass("checkbox");
                $emailForm.find("#to-email-admin input").attr("disabled", false);
                $emailForm.find("#to-email-school").addClass("hidden");
                $emailForm.find("#to-email-classroom").removeClass("hidden");
                
                $emailForm.find("#to-email-admin span").html("School Manager");
            } else {                
                $emailForm.find("#to-email-admin").addClass("checkbox");
                $emailForm.find("#to-email-admin input").attr("disabled", false);
                $emailForm.find("#to-email-school").removeClass("hidden");
                $emailForm.find("#to-email-classroom").removeClass("hidden");
                
                $emailForm.find("#to-email-admin span").html("District Manager");
            }
            // Default the to value to the admin email
            $emailForm.find("input[name=to]").val(email);
            
            $emailForm.find("input[name=subject]").val("");
            $emailForm.find("textarea").val("");
        });
        
        $(".toEmails input[type=checkbox]").on("change", function() {
            var value = this.value;
            
            var $emailForm = $("#emailform");
            var $adminCheckbox = $emailForm.find("#to-email-admin input");
            var $schoolCheckbox = $emailForm.find("#to-email-school input");
            var $classroomCheckbox = $emailForm.find("#to-email-classroom input");
            
            var adminChecked = $adminCheckbox.prop("checked");
            var schoolChecked = $schoolCheckbox.prop("checked");
            var classroomChecked = $classroomCheckbox.prop("checked");
            
            var email = $emailForm.find("input[name=adminEmail]").val();
            var schoolEmails = $emailForm.find("input[name=schoolEmails]").val();
            var classroomEmails = $emailForm.find("input[name=classroomEmails]").val();
            
            var emailsToSend = "";
            
            if (adminChecked) {
                emailsToSend = email + ", ";
            }
            
            if (schoolChecked) {
                emailsToSend += schoolEmails + ", ";
            }
            
            if (classroomChecked) {
                emailsToSend += classroomEmails + ", ";
            }
            
            // Remove the last ", " from string of emails
            emailsToSend = emailsToSend.slice(0, -2);
            
            $emailForm.find("input[name=to]").val(emailsToSend);
        });


        $('#passwordModal').on('shown.bs.modal', function (e) {
            self.bindValidation();

        });
    },
    bindValidation: function(){
        var self = this;

        var theForm = $('#passwordForm');
        if(theForm.length > 0){
            var options = $.extend({
                rules: {
                    confirmPassword: {
                        equalTo: "#password"
                    }
                },
                messages: {
                    confirmPassword: "Passwords do not match"
                },
                submitHandler: function(form) {
                    $(form).ajaxSubmit({
                        success: function() {
                            $("#passwordChangeMessage").show();
                            setTimeout(function() {
                                // Clear password inputs
                                $("#passwordModal").find("input[type=password]").val('');
                                $("#passwordModal").modal("hide");
                                $("#passwordChangeMessage").hide();
                            }, 1500);
                        }
                    });
                }
            }, self.validationConfig);


            
            $('#passwordForm').validate(options);
        }else{
            if(typeof self.validationBindFail === 'undefined'){
                self.validationBindFail = 0;
            }
            else{
                self.validationBindFail++;
            }

            if(self.validationBindFail <= 15){
                setTimeout(function(){
                    console.log('Bind validation fail, will try ' + (15-self.validationBindFail) + ' more time(s)');
                    self.bindValidation();
                },500);
            }
            else{
                self.displayError('Something went wrong. Please try again.');
                $('#passwordModal').modal('hide')
            }
        }
    },


    displayError: function(message,opts){

        var settings = $.extend({
            type: 'danger',
            dismissable: true
        },opts);

        var messageHtml = '';
            messageHtml += '<div class="alert alert-' + settings.type+' '+ (settings.dismissable === true ? 'alert-dismissable' :'') +'">';
            messageHtml +=     (settings.dismissable === true ? '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' :'');
            messageHtml +=     message;
            messageHtml += '</div>';

        $('#displayMessages').append(messageHtml);
    },
    bindDatePicker: function($target, beforeShow){
        if(beforeShow == undefined){
            beforeShow = function(){};
        }

        $target.datepicker({
            constrainInput: true,
            dateFormat: "mm/dd/yy"
        });
    },

    bindWeekPicker: function($target){

        $target.datepicker({
            constrainInput: true,
            dateFormat: "mm/dd/yy",
            firstDay: 1,
            beforeShowDay: function(date) {
                var selectedDate = null;

                //if and try are to avoid issues with empty fields or bad data
                if($(this).val() != ''){
                    try{
                        selectedDate = $(this).datepicker("getDate");
                    }catch(e){
                        console.log('error')
                    }
                }

                //if we get a date set
                if(selectedDate!=null){
                    var endOfTheWeek = (selectedDate.getDate() + 6);//find the end of the week of that day
                    var lastDayOfWeek = new Date(selectedDate);//set last day of week
                    lastDayOfWeek.setDate(endOfTheWeek);//to a new date

                    var date1 = selectedDate;//make two dates for a range
                    var date2 = lastDayOfWeek;//to compare to return a class for days in the range

                    return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "ui-state-active" : ""];
                }
                else{
                    return [true, ""];//fail safe
                }
            },
            onSelect: function(dateText, inst) {
                //set date selected
                var selectedDate = new Date(dateText);
                //set date object to work with
                var firstDayOfWeek = new Date(dateText);
                //take the day of the month and subtract the day of the week from it to select the first day of a week
                var adjustDayBy = (selectedDate.getDate() - selectedDate.getDay()+1);
                //update the date
                firstDayOfWeek.setDate(adjustDayBy)
                //update the ui
                $(this).datepicker( "setDate", firstDayOfWeek );
            },
            beforeShow: function(el,inst){
                var newSettings = {};

                if($(el).attr('data-getmindatefrominputid')){
                    var startDate = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $('#'+$(el).attr('data-getmindatefrominputid')).val());

                    $.extend(newSettings,{
                        minDate:startDate
                    });
                }

                if($(el).attr('data-getmaxdatefrominputid')){
                    var endDate = $.datepicker.parseDate($.datepicker._defaults.dateFormat, $('#'+$(el).attr('data-getmaxdatefrominputid')).val());

                    $.extend(newSettings,{
                        maxDate:endDate
                    });
                }

                return newSettings;
            }
        });
    },

    validateForm: function($selector, config) {
        //this method is not working in IE9 down, something is preventing jquery objects saved to variables from working.
        var self = this;
        return $selector.validate($.extend({}, self.validationConfig, config));
    }
});
