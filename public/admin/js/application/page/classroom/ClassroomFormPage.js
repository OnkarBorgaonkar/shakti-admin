shaktiAdmin.page.classroom.ClassroomFormPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    redirectUrl: null,
    //events
    onBeforeReady: function() {
        shaktiAdmin.page.classroom.ClassroomFormPage.superclass.onBeforeReady.call(this);
        var self = this;

        $('.default-form').validate({
            errorClass: 'has-error',
            validClass: 'has-success',
            ignore: "",
            rules: {
                "classroom[approxNumberMinutes]": {
                    min: 1
                }
            },
            messages: {
                "classroom[approxNumberMinutes]": {
                    min: "Please specify when you will be working on SHAKTI Warriors above"
                }
            },
            highlight : function(element, errorClass, validClass) {
                $($(element).parents('.form-group')[0]).addClass(errorClass).removeClass(validClass);
            },
            unhighlight : function(element, errorClass, validClass) {
                $($(element).parents('.input-wrap')[0]).removeClass(errorClass).addClass(validClass);
            },
            submitHandler: function() {
                $('.default-form').ajaxSubmit({
                    dataType: "json",
                    success: function(json) {
                        console.log(json);
                        if (json.success) {
                            window.location = self.redirectUrl;
                        }
                        else {
                            $("#classroomWarning").html(json.systemMessage).show();
                            $("html, body").animate({scrollTop: 0}, "slow");
                        }
                    }
                });
            }
        });

        $('#otherOption').click(function() {
            if($(this).prop("checked")) {
                $("#otherValue").addClass("required");
                $("#otherField").slideDown();
            }
            else {
                $("#otherValue").removeClass("required");
                $("#otherField").slideUp();
            }
        });

        $(".cancel").click(function() {
            window.location = self.redirectUrl;
            return false;
        });

        $(".updateApproximateTime").change(function() {
            var days = $("#dayCheckboxes input:checked").length

            var valuestart = $("select[name='classroom[startTime]']").val();
            var valuestop = $("select[name='classroom[endTime]']").val();

            var startTime = new Date("01/01/2007 " + valuestart);
            var startMinutes = startTime.getHours() * 60 + startTime.getMinutes();

            var endTime = new Date("01/01/2007 " + valuestop);
            var endMinutes = endTime.getHours() * 60 + endTime.getMinutes();

            var minuteDiff = endMinutes - startMinutes;
            minuteDiff * = days;

            minuteDiff = minuteDiff > 0 ? minuteDiff : 0;

            $("#approximateTime").val(minuteDiff);
        });

        $("#approximateTime").keypress(function() { return false;});
    },

    onReady: function() {
        shaktiAdmin.page.classroom.ClassroomFormPage.superclass.onReady.call(this);
    }

});