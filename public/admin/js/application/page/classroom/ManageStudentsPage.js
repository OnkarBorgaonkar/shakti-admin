shaktiAdmin.page.classroom.ManageStudentsPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    notSaved: false,

    //events
    onBeforeReady: function() {
        var self = this;
        shaktiAdmin.page.classroom.ManageStudentsPage.superclass.onBeforeReady.call(this);

        window.onbeforeunload = function() {
            if (self.notSaved) {
                return "You have unsaved data. Are you sure you would like to leave?";
            }
        };

        $("#students").on('click', '.delete', function() {
           $(this).parents("tr").remove();
           self.notSaved = true;
           return false;
        });

        $(".submit").click(function() {
            self.notSaved = false;
        });

        $("#studentSearchInput").autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/json/getUserList.php",
                    data: {term: request.term},
                    dataType: "json",
                    success: function(json) {
                        response($.map(json, function(student) {
                            return {
                                label: student['login'] + ' - ' + student['firstName'] + ' ',
                                login: student['login'],
                                first: student['firstName'],
                                value: student['id'],
                                sid: student['studentId'],
                                avatar: student['avatar'],
                                points: student['totalPoints'],
                                rank: student['rankName']
                            };
                        }));
                    }
                });
            },
            select: function(event, ui) {
                var id = ui.item.value;
                var login = ui.item.login;
                var name = ui.item.first;
                var studentId = ui.item.sid;
                var points = ui.item.points;
                var rank = ui.item.rank;

                var studentCount = $('input[name="ids[]"][value="'+id+'"]').length;

                if (studentCount > 0 ) {
                    $("#studentWarning").text(name + " is already in the classroom").slideDown();
                    setTimeout(function() {
                        $("#studentWarning").slideUp();
                    }, 5000);
                }
                else {

                    if (studentId === null)
                        studentId = ""

                    var html = '' +
                    '<tr>' +
                    '<input type="hidden" name="ids[]" value="' + id + '"/>' +
                    '<td>' + login + ' - <a data-target="#passwordModal" data-toggle="modal" href="/admin/changePassword.php?userId='+ id +'">Change Password</a></td>' +
                    '<td>' + name + '</td>' +
                    '<td>' + points + '</td>' +
                    '<td>' + rank + '</td>' +
                    '<td><input type="text" name="studentIds[]" value="' + studentId + '"/></td>' +
                    '<td><a href="#" class="delete"><i><!-- Delete Symbol --></i></a></td>' +
                    '</tr>';

                    $("#students").prepend(html);

                    $("#studentSearchInput").val("");

                    self.notSaved = true;
                }

                return false;
            },
            focus: function (event, ui) {
                $("#studentSearchInput").val(ui.item.label);
                return false;
            },
            minLength: 2
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append('<a>' + '<img src="' +item.avatar + '" style="width: 25px; height: auto;"/> ' + item.label + '</a>')
            .appendTo(ul);
        };


    },

    onReady: function() {
        var self = this;
        shaktiAdmin.page.classroom.ManageStudentsPage.superclass.onReady.call(this);
    }
});