shaktiAdmin.page.competition.CompetitionFormPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    redirectUrl: null,
    //events
    onBeforeReady: function() {
        shaktiAdmin.page.competition.CompetitionFormPage.superclass.onBeforeReady.call(this);
        var self = this;
        $('.default-form').validate({
            errorClass: 'has-error',
            validClass: 'has-success',
            highlight : function(element, errorClass, validClass) {
                $($(element).parents('.form-group')[0]).addClass(errorClass).removeClass(validClass);
            },
            unhighlight : function(element, errorClass, validClass) {
                $($(element).parents('.input-wrap')[0]).removeClass(errorClass).addClass(validClass);
            },
            submitHandler: function() {
                $('.default-form').ajaxSubmit({
                    dataType: "json",
                    success: function(json) {
                        console.log(json);
                        if (json.success) {
                            window.location = self.redirectUrl;
                        }
                        else {
                            $("#districtWarning").html(json.systemMessage).show();
                            $("html, body").animate({scrollTop: 0}, "slow");
                        }
                    }
                });
            }
        });

        $(".cancel").click(function() {
            window.location = self.redirectUrl;
            return false;
        });
    },



    onReady: function() {
        shaktiAdmin.page.competition.CompetitionFormPage.superclass.onReady.call(this);
    }

});