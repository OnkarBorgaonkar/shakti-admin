shaktiAdmin.page.competition.CompetitionOverviewPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    //events
    onBeforeReady: function() {
        shaktiAdmin.page.competition.CompetitionOverviewPage.superclass.onBeforeReady.call(this);
        console.log('district overview page');

    },

    onReady: function() {
        shaktiAdmin.page.competition.CompetitionOverviewPage.superclass.onReady.call(this);
    }

});
