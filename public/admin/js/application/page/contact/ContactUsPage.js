shaktiAdmin.page.contact.ContactUsPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    redirectUrl: null,
    //events
    onBeforeReady: function() {
        shaktiAdmin.page.contact.ContactUsPage.superclass.onBeforeReady.call(this);
        var self = this;
        $('#contactForm').validate({
            submitHandler: function() {
                $('#contactForm').ajaxSubmit({
                    dataType: "json",
                    success: function(json) {
                        console.log(json);
                        if (json.success) {
                            $("#successMessage").html(json.systemMessage).show();
                            $("#warningMessage").html(json.systemMessage).hide();
                        }
                        else {
                            $("#warningMessage").html(json.systemMessage).show();
                            $("#successMessage").html(json.systemMessage).hide();
                        }
                        $("html, body").animate({scrollTop: 0}, "slow");
                    }
                });
            }
        });
    },

    onReady: function() {
        shaktiAdmin.page.contact.ContactUsPage.superclass.onReady.call(this);
    }

});
