shaktiAdmin.page.district.DistrictOverviewPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    //events
    onBeforeReady: function() {
        shaktiAdmin.page.district.DistrictOverviewPage.superclass.onBeforeReady.call(this);
        console.log('district overview page');
        
    },
    
    onReady: function() {
        shaktiAdmin.page.district.DistrictOverviewPage.superclass.onReady.call(this);
    }

});
