shaktiAdmin.page.district.DistrictSessionFormPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    redirectUrl: null,
    //events
    onBeforeReady: function() {
        shaktiAdmin.page.district.DistrictSessionFormPage.superclass.onBeforeReady.call(this);
        var self = this;
        $('.default-form').validate({
            errorClass: 'has-error',
            validClass: 'has-success',
            highlight : function(element, errorClass, validClass) {
                $($(element).parents('.form-group')[0]).addClass(errorClass).removeClass(validClass);
            },
            unhighlight : function(element, errorClass, validClass) {
                $($(element).parents('.input-wrap')[0]).removeClass(errorClass).addClass(validClass);
            },
            submitHandler: function() {
                $('.default-form').ajaxSubmit({
                    dataType: "json",
                    success: function(json) {
                        console.log(json);
                        if (json.success) {
                            window.location = self.redirectUrl;
                        }
                        else {
                            $("#sessionWarning").html(json.systemMessage).show();
                            $("html, body").animate({scrollTop: 0}, "slow");
                        }
                    }
                });
            }
        });

        $(".cancel").click(function() {
            window.location = self.redirectUrl;
            return false;
        });

        $("body").on('click',".datepicker + .calendar", function(e) {
            var $input = $(this).siblings(".datepicker");

            $input.focus();
            return false;
        });
    },



    onReady: function() {
        shaktiAdmin.page.district.DistrictFormPage.superclass.onReady.call(this);
    }

});