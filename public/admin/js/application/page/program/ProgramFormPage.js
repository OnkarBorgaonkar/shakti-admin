shaktiAdmin.page.program.ProgramFormPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    redirectUrl: null,
    //events
    onBeforeReady: function() {
        shaktiAdmin.page.program.ProgramFormPage.superclass.onBeforeReady.call(this);
        var self = this;

        $('.default-form').validate({
            errorClass: 'has-error',
            validClass: 'has-success',
            highlight : function(element, errorClass, validClass) {
                $($(element).parents('.form-group')[0]).addClass(errorClass).removeClass(validClass);
            },
            unhighlight : function(element, errorClass, validClass) {
                $($(element).parents('.input-wrap')[0]).removeClass(errorClass).addClass(validClass);
            },
            submitHandler: function() {
                $('.default-form').ajaxSubmit({
                    dataType: "json",
                    success: function(json) {
                        console.log(json);
                        if (json.success) {
                            window.location = self.redirectUrl;
                        }
                        else {
                            $("#programWarning").html(json.systemMessage).show();
                            $("html, body").animate({scrollTop: 0}, "slow");
                        }
                    }
                });
            }
        });

        $(".cancel").click(function() {
            window.location = self.redirectUrl;
            return false;
        });

//        self.bindWeekPicker($('.weekpicker'));

        $("#addNotInSessionWeek").click(function() {
            var html = '' +
            '<div class="form-group date">' +
            '<label class="control-label" for="inputDatesOut">Week not in session</label>' +
            '<div class="controls">' +
            '<input class="datepicker weekpicker" type="text" name="weeksNotInSession[]" data-getmindatefrominputid="inputProgramStart" data-getmaxdatefrominputid="inputProgramEnd">' +
            '<a class="calendar" href="#"><i class="glyphicon glyphicon-calendar"></i></a>' +
            '</div>' +
            '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' +
            '</div>'; +

            $("#notInSession").append(html);

            var $newDatePicker = $('#notInSession .date:last input.weekpicker');

            self.bindWeekPicker($newDatePicker);

            return false;
        });

        $("body").on('click',".controls .datepicker + .calendar", function(e) {
            var $input = $(this).siblings(".datepicker");

            $input.focus();
            return false;
        });
    },



    onReady: function() {
        shaktiAdmin.page.program.ProgramFormPage.superclass.onReady.call(this);
    }

});
