shaktiAdmin.page.program.ProgramOverviewPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    //events
    onBeforeReady: function() {
        shaktiAdmin.page.program.ProgramOverviewPage.superclass.onBeforeReady.call(this);
        console.log('program overview page');
        
    },
    
    onReady: function() {
        shaktiAdmin.page.program.ProgramOverviewPage.superclass.onReady.call(this);
    }

});