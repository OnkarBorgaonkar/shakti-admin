shaktiAdmin.page.school.SchoolFormPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    redirectUrl: null,
    //events
    onBeforeReady: function() {
        shaktiAdmin.page.school.SchoolFormPage.superclass.onBeforeReady.call(this);
        var self = this;

        var options = $.extend({
            submitHandler: function() {
                $('.default-form').ajaxSubmit({
                    dataType: "json",
                    success: function(json) {
                        console.log(json);
                        if (json.success) {
                            window.location = self.redirectUrl;
                        }
                        else {
                            $("#schoolWarning").html(json.systemMessage).show();
                            $("html, body").animate({scrollTop: 0}, "slow");
                        }
                    }
                });
            }
        }, self.validationConfig);

        $(".default-form").validate(options);
        

        $(".cancel").click(function() {
            window.location = self.redirectUrl;
            return false;
        });

        $("#addNotInSessionWeek").click(function() {
            var html = '' +
            '<div class="form-group date">' +
            '<label class="control-label" for="inputDatesOut">Week not in session</label>' +
            '<div class="controls">' +
            '<input class="datepicker weekpicker" type="text" name="weeksNotInSession[]" data-getmindatefrominputid="inputSchoolStartDate" data-getmaxdatefrominputid="inputSchoolEndDate">' +
            '<a class="calendar" href="#"><i class="glyphicon glyphicon-calendar"></i></a>' +
            '</div>' +
            '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>' +
            '</div>'; +

            $("#notInSession").append(html);

            var $newDatePicker = $('#notInSession .date:last input.weekpicker');

            self.bindWeekPicker($newDatePicker);

            return false;
        });

        $("body").on('click',".controls .datepicker + .calendar", function(e) {
            var $input = $(this).siblings(".datepicker");

            $input.focus();
            return false;
        });

    },



    onReady: function() {
        shaktiAdmin.page.school.SchoolFormPage.superclass.onReady.call(this);
    }

});