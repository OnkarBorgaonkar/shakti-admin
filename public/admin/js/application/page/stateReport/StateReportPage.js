shaktiAdmin.page.stateReport.StateReportPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    //events
    onBeforeReady: function() {
        shaktiAdmin.page.stateReport.StateReportPage.superclass.onBeforeReady.call(this);

        var self = this;

        $(".default-form").validate();

    },

    onReady: function() {
        shaktiAdmin.page.stateReport.StateReportPage.superclass.onReady.call(this);

    }

});