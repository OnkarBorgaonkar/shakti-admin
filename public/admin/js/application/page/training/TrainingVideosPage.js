shaktiAdmin.page.training.TrainingVideosPage = shaktiAdmin.extend(shaktiAdmin.page.GlobalPage, {

    //events
    onBeforeReady: function() {
        shaktiAdmin.page.training.TrainingVideosPage.superclass.onBeforeReady.call(this);
        var self = this;
    },

    onReady: function() {
        shaktiAdmin.page.training.TrainingVideosPage.superclass.onReady.call(this);

        $(".youtubeLink").click(function() {
            var youtubeId = $(this).data("youtube");
            var title = $(this).data("title");

            $("#videoPlayer .modal-title").html(title);
            var html = '<iframe width="538" height="404" allowtransparency="true" src="http://www.youtube.com/embed/' + youtubeId + '?rel=0&wmode=transparent&autoplay=1" frameborder="0"></iframe>';
            $("#videoPlayer .videoWrapper").html(html);
        });
    }

});