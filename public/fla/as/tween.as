﻿ //let the coder know tween code is in the movie
trace('Applying tween.as to : ' + this);
/* Tween Code */
import mx.transitions.Tween;
import mx.transitions.easing. *;
_global.doTween = function (toTween, tweenType, endValue, duration, easeType, moreToDo)
{
	//trace("in do Tween" + toTween);
	function doEase (easeType)
	{
		switch (easeType)
		{
			case "easeOut" :
			return (mx.transitions.easing.Regular.easeOut);
			break;
			case "easeIn" :
			return (mx.transitions.easing.Regular.easeIn);
			break;
			case "easeInOut" :
			return (mx.transitions.easing.Regular.easeInOut);
			break;
			case "strongEaseInOut" :
			return (mx.transitions.easing.Strong.easeInOut);
			break;
			case "elastic" :
			return (mx.transitions.easing.Elastic.easeOut);
			break;
			case "None" :
			return (mx.transitions.easing.None.easeNone);
			break;
			default :
			return (mx.transitions.easing.None.easeNone);
			break;
		}
	}
	toTween.codeTween = new mx.transitions.Tween (toTween, tweenType, doEase (easeType) , eval (toTween + "." + tweenType) , endValue, duration, true);
	if (moreToDo != false)
	{
		toTween.codeTween.onMotionFinished = function ()
		{
			toTween = toTween + '1';
			//trace ("detected " + toTween.doNext);
			//trace(moreToDo + " = more to do")
			moreToDo();
		};
	}
	/*
	if (moreToDo != false)
	{

	toTween.codeTween.onMotionFinished = function ()
	{
	var ass = "masive";
	var hole = 1;
	var ploop = ass+hole;
	trace(ploop + " = ploop");
	var string1 = toTween;
	var string2 = Number(moreToDo);
	var other = toTween + moreToDo;
	newName = eval(string1 + string2);
	trace ("detected " + toTween.doNext);
	trace (toTween + " = toTween");
	trace (moreToDo + " = moreToDo");
	trace(other + " = other")
	trace (newName + " = newName");
	other.doNext();
	}
	}
	*/
	/*
	if (moreToDo == true) {
	toTween.codeTween.onMotionFinished = function() {
	trace("detected"+toTween.doNext);
	toTween.doNext();
	};
	}
	*/
};
/* So thats the Tween Code */
