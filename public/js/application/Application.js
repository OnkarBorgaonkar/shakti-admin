application.Application = function(options) {
    var base = this;
    jQuery.extend(base, options);
    base.initApp();
};

application.Application.prototype = {

    //properties
    isLoggedIn: false,

    //core
    initApp: function() {
        this.init();

        jQuery(document).ready(function() {
            $(".newWindow").click(function() {
                window.open($(this).attr("href"));
                return false;
            });
        });


    },

    init: jQuery.noop

};