application.page.ApplicationPage = Core.extend(application.page.DefaultPage, {

    init : function() {
        application.page.ApplicationPage.superclass.init.call(this);
        var self = this;

        $("#applicationForm").validate({
            rules: {
                confirm: {
                    equalTo: "#password"
                },
                "user[login]": {
                    remote: "json/checkUserName.php"
                }
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            window.location = "/welcome.php?firstVisit=true";
                        }
                        else {
                            $(".submitButton").attr('disabled',false).removeClass('submitting');
                            $("#systemMessage").show().html(data.systemMessage);
                            $("html, body").animate({scrollTop: 0}, "slow");
                        }
                    }
                });
                $(".submitButton").attr('disabled',true).addClass('submitting');
            }
        });


        $(document).ready(function() {

        });
    }
});
