application.page.DefaultPage = Core.extend(application.Application, {

    init : function() {
        application.page.DefaultPage.superclass.init.call(this);
        var self = this;

        self.docReady();


    },
    docReady: function(){
        var self = this;
        $(document).ready(function() {
            //
            $("#leaderboardLink").click(function() {
                $.fancybox({
                    'onStart'    : function(){
                        $("#fancybox-close").css('left', '475px');
                        $("#fancybox-close").css('right', 'auto');
                        $("#fancybox-close").css('top', '133px');
                    },
                    'href'        : "#competitionList"
                });
                return false;
            });
            //
            $("#challengeVideo").click(function() {
                $.fancybox({
                    'padding'        : 0,
                    'autoScale'        : false,
                    'transitionIn'    : 'none',
                    'transitionOut'    : 'none',
                    'title'            : this.title,
                    'width'            : 1100,
                    'height'        : 800,
                    'href'            : '/app/views/youtubeVideo.phtml',
                    'type'            : 'iframe',
                    'onStart'        : function () {
                        $("#fancybox-close").css('right', '261px');
                        $("#fancybox-close").css('left', 'auto');
                        $("#fancybox-close").css('top', '30px');
                        $("#fancybox-overlay").css('background','none');
                    }
                });
                return false;
            });

            //TODO: why is this everyplace? Make better
            $(".shakti-form").validate({
                rules: {
                    "child[firstName]": "required",
                    name: "required",
                    tag: "required",
                    typeName: "required",
                    "child[gender]": "required",
                    "child[year]": "required",
                    "user[login]": "required",
                    "child[superheroId]": "required",
                    confirm: "required",
                    "user[password]": "required",
                    "child[ethnicity]": "required",
                    "superPowers[superHeroPower1]": "required",
                    "superPowers[superHeroPower2]": "required",
                    "humanPowers[humanPower1]": "required",
                    "humanPowers[humanPower2]": "required",
                    "humanPowers[humanPower3]": "required",
                    points: "required",
                    startsOn: "required",
                    endsOn: "required",
                    reward: "required",
                    memberId: "required",
                    color:"required",
                    address: "required",
                    city: "required",
                    state: "required",
                    zip: "required"
                },
                messages: {
                    name: "This field is required."
                }
            });

        });
    },

    showDescriptionPopup: function(description, closeBigdoor, onComplete) {
        closeBigdoor = (typeof closeBigdoor === "undefined") ? false : closeBigdoor;
        onComplete = (typeof onComplete === "undefined") ? function(){} : onComplete;

        //insert html to dom before calling fancybox to allow fancybox to size and posistion properly
        $("#bigDoorExtendedDescription").html(description);
        $.fancybox({
            'padding'      : 0,
            'autoScale'     : true,
            'scolling'      : 'no',
            'transitionIn'  : 'none',
            'transitionOut' : 'none',
            'centerOnScroll':true,
            'width':700,
            'href'          : '#bigdoor-description',
            'onStart'       : function () {
                $("#fancybox-close").css('right', '114px');
                $("#fancybox-close").css('left', 'auto');
                $("#fancybox-close").css('top', '70px');
                $("#fancybox-overlay").css('background','none');
             },

             'onComplete' : function() {
                $('#fancybox-close').css({
                    right: '99px',
                    top: '60px'
                });
                $('#fancybox-wrap').width(700);
                if (closeBigdoor) {
                    $('#fancybox-content').addClass("closeBigdoor");
                }
                //call custom on complete function
                onComplete();
              }
        });
    }
});
