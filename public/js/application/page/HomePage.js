application.page.HomePage = Core.extend(application.page.DefaultPage, {

    init : function() {
        application.page.HomePage.superclass.init.call(this);
        var self = this;

        $(document).ready(function() {
            //game on the homepage
            $("#powerUp").click(function() {
                $.fancybox({
                    'padding'        : 0,
                    'autoScale'        : false,
                    'width'            : 816,
                    'height'        : 650,
                    'href'            : "public/swf/widget_pu.swf",
                    'onStart'        : function () {
                        $("#fancybox-close").css('right', '86px');
                        $("#fancybox-close").css('left', 'auto');
                        $("#fancybox-close").css('top', '86px');
                        $("#fancybox-overlay").css('background','none');
                    },
                    'type'            : 'swf',
                    'swf'            : {
                         'wmode'        : 'transparent',
                        'allowfullscreen'    : 'true'
                    }
                });
                return false;
            });

            //game on the homepage
            $("#anatomy").click(function() {
                $.fancybox({
                    href: "/public/swf/widget_aofl.swf",
                    'onStart'        : function () {
                        $("#fancybox-close").css('left', '285px');
                        $("#fancybox-close").css('right', 'auto');
                        $("#fancybox-close").css('top', '0px');
                        $("#fancybox-overlay").css('background','none');
                    },
                    'width': 300
                });
                return false;
            });

            $("#scalesEnergy").click(function() {
                $.fancybox({
                    href: "/public/swf/widget_soe.swf",
                    'onStart'        : function () {
                        $("#fancybox-close").css('left', '295px');
                        $("#fancybox-close").css('right', 'auto');
                        $("#fancybox-close").css('top', '-5px');
                        $("#fancybox-overlay").css('background','none');
                        },
                    'width': 310
                });
                return false;
            });
            $("#shaktiProgram").click(function() {
                var html = $("#ShaktiProgramContent").html();
                new application.widget.popup.InformationPopUp({description: html});
                return false;
            });
        });

    }
});
