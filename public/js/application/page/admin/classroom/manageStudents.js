application.page.admin.manageStudents = Core.extend(application.page.training.BaseTrainingPage, {

    classroomId: null,

    init : function() {
        application.page.admin.manageStudents.superclass.init.call(this);
        var self = this;

        $(document).ready(function() {
            $(".nonMember").draggable({
                appendTo: 'body',
                helper: 'clone'
            });

            $(".member").draggable({
                appendTo: 'body',
                helper: 'clone'
            });

            $("#members").droppable({
                accept: '.nonMember',
                drop: function(event, ui) {
                    var member = ui.draggable;
                    var memberList = this;
                    self.addMember(member, memberList);
                }
            });

            $("#nonMembers").droppable({
                accept: '.member',
                drop: function(event, ui) {
                    var member = ui.draggable;
                    var memberList = this;
                    self.removeMember(member, memberList);
                }
            });

            self.listFilterInit();


            $("#clear").click(function() {
                $('#search').val('').keyup();
            });

            self.sortUnorderedList("#nonMembers");
            self.sortUnorderedList("#members");

        });

    },

    listFilterInit: function(){
        $('#search').fastLiveFilter($('#nonMembers'));
    },
    addMember: function(member, memberList) {
        var memberId = member.attr("title"),
            self = this;

        $.ajax({
            async: true,
            type: 'POST',
            data: {userId: memberId, classroomId: self.classroomId},
            dataType: 'json',
            url: '/json/addMember.php',
            success: function(json) {
                if (json.success == true) {
                    member.appendTo(memberList);
                    member.addClass("member").removeClass("nonMember");
                    self.sortUnorderedList("#members");
                }
                else {
                    alert(json.message);
                }

            }
        });
        self.listFilterInit();
    },

    removeMember: function(member, memberList) {
        var memberId = member.attr("title"),
        self = this;

        member.append('<small> saving...</small>');
        member.appendTo(memberList);
        member.addClass("nonMember").removeClass("member");

        $.ajax({
            async: true,
            type: 'POST',
            data: {userId: memberId, classroomId: self.classroomId},
            dataType: 'json',
            url: '/json/removeMember.php',
            success: function(json) {
                if (json.success == true) {
                    member.find('small').remove();
                    self.sortUnorderedList("#nonMembers");
                }
                else {
                    member.appendTo($("#nonMembers"));
                }
            }
        });
        self.listFilterInit();
    },

    sortUnorderedList: function(ul, sortDescending) {

        $(ul + ' li').tsort();

        /*
        var mylist = $(ul);
        var listitems = mylist.children('li').get();
        listitems.sort(function(a, b) {
           var compA = $(a).text().toUpperCase();
           var compB = $(b).text().toUpperCase();
           return (compA < compB) ? -1 : (compA > compB) ? 1 : 0;
        })
        $.each(listitems, function(idx, itm) { mylist.append(itm); });
        */
    }

});
