application.page.training.BaseTrainingPage = Core.extend(application.page.DefaultPage, {
    init : function() {
        application.page.training.BaseTrainingPage.superclass.init.call(this);
        var self = this;

        self.initializeCalendar();
    },
    initializeCalendar: function(){
        var self = this;

        // If there's a calendarContainer on this page (i.e. an internal, post-login page)
        if ($("#calendarContainer")[0]) {
            var minDate = new Date();
            calendar = new YAHOO.widget.Calendar("calendar","calendarContainer");
            calendar.cfg.setProperty("mindate", application.USER.calendarDate);
            calendar.cfg.setProperty("maxdate", new Date());
            calendar.render();
            calendar.selectEvent.subscribe(calendarChanged, calendar, true);
        }
    }
});
