application.page.training.EditProfilePage = Core.extend(application.page.DefaultPage, {
    init : function() {
        application.page.training.EditProfilePage.superclass.init.call(this);
        var self = this;

        $(".editForm").validate({
            rules: {
                "user[login]": {
                    remote: {
                        url: "json/checkUserName.php",
                        data: {
                            id: function() {
                                return $("input[name='user[id]']").val();
                            }
                        }
                    }
                }
            }
        });

    }
});
