application.page.training.OrbPage = Core.extend(application.page.training.BaseTrainingPage, {

    slideNumber: 1,
    hasUnsavedChanges: false,
    startingCheckpoint: 0,

    init : function() {
        application.page.training.OrbPage.superclass.init.call(this);
        var self = this;

        var hasFocus;
        var timer;
        var elapsedTime;
        var limitReached;
        var timerStarted;
        var timeLeft = 90;

        $(document).ready(function() {
            hasFocus = true;

            $(window).blur(function() {
                hasFocus = false;
            });
            $(window).focus(function() {
                hasFocus = true;
            });

            $('.orbCheckpointsWrap').hide();

            $('.startOrb').click(function(){
                var orbId = $(this).data("id");
                self.updateUserOrb(orbId, "started");
                $('.orbLanding').hide();
                $('.orbCheckpointsWrap').show();
                self.initSlider();
                self.updateStatus();
            });

            $('.finishButton').click(function() {
                var orbId = $(this).data("id");
                self.updateUserOrb(orbId, "completed");
            });

            $('.storySubmit').click(function () {
                $('.loader').addClass('showLoading');
                $('.errorMessage').html('<div class="errorMessage" style="display: none;"></div>');
            });
            
            $('.orbWrap').keydown(function(event){
			    if (event.keyCode == 9) {
			        event.preventDefault();
			    }
			});
			
            $('.orbWrap').keydown(function(event){
			    if (event.shiftKey && event.keyCode == 9) {
			        event.preventDefault();
			    }
			});
			
			$(".checkpoints :input, .checkpoints a").attr("tabindex", "-1");
        });


        function updateTimer() {

            if (!hasFocus) {
                elapsedTime++;

                timeLeft = timeLimit - elapsedTime;

                if (!limitReached && elapsedTime >= timeLimit) {
                    clearInterval(timer);
                    timer = null;
                    limitReached = true;
                    $("#timer").hide();
                    $("#timer").removeClass('showTimer');
                    $('.linkCheckpoint .linkForm .inputContainer').html('<input type="submit" value="Submit" class="linkSubmit" />');

                } else if (!timerStarted){
                    timerStarted = true;
                }

                var minutes = Math.floor(timeLeft / 60);
                var seconds = timeLeft % 60;

                var secString = "" + seconds;
                if (seconds < 10)
                    secString = "0" + secString;

                $("#timer").html(minutes + ":" + secString);
            }
        }

        function stopTimer() {
            clearInterval(timer);
            timer = null;
            $("#timer").html("0:00");
        }

        $('.checkpoint .link').click(function() {
            if (timer == null) {
                timeLimit = parseInt($(this).data("seconds"));
                elapsedTime = 0;
                limitReached = false;
                timer = setInterval(updateTimer, 1000);
                $('.linkSubmit').hide();
            }
        });

        $('form').each(function(){
            $(this).validate({
                errorContainer: '.errorMessage',
                submitHandler: function(form) {
                    $("form input[type=submit]").attr("disabled", "disabled");
                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: function(json) {
                            if (json.success) {
                                $(".errorMessage").html("");
                                $('.loader').removeClass('showLoading');
                                var numberOfSlides = $('.checkpoints .orbSlide').length - 1;
                                $(".checkpoints").trigger("currentPosition", function(pos) {
                                    if (numberOfSlides == pos) {
                                        self.showLast();
                                    } else {
                                        self.nextSlide();
                                        $('.checkpoints').trigger("updateSizes");
                                    }
                                });
                            }
                            else {
                                $(".errorMessage").html(json.systemMessage);
                                $('.loader').removeClass('showLoading');
                                $(".errorMessage").addClass("showError");
                                $("form input[type=submit]").removeAttr("disabled");
                            }
                        }
                    });
                },
                invalidHandler: function(form){
                    $('.loader').removeClass('showLoading');
                }
            });
        });

        $(".link").click(function() {
            $(this).hide().siblings(".linkForm").show();
            $("#timer").addClass('showTimer');
        });
    },

    buildPagination: function(){

        $(".checkpoints").trigger("currentPosition", function(pos) {
            console.log(pos);
        });

        $('.checkpoints .orbSlide').each(function(i){
            //$('.checkpointsHeader .status').append('<a href="#item'+i+'" class="pagSphere caroufredsel checkpointPag'+i+'"></a>');
        });

    },

    initSlider: function(){
        var self = this;

        self.buildPagination();

        $('.checkpoints').carouFredSel({
            auto: false,
            responsive: true,
            height: 'auto',
            infinite: false,
            circular: false,
            onCreate: function( data ) {
                $(".checkpoints").trigger("slideTo",self.startingCheckpoint);
            },
            scroll        : {
                onBefore : function( data ) {
                    self.updateStatus();
                },
                onAfter : function( data ) {
                    $("form input[type=submit]").removeAttr("disabled");
                }
            }
        });

       $('.checkpointsHeader .status a').click(function (){
            var navIndex = $('.checkpointsHeader .status').index($(this));
        });


        self.getStep(self.slideNumber);

    },

    updateStatus: function (){
        var self = this;
        var numberOfSlides = $('.checkpoints .orbSlide').length;
        $(".checkpoints").trigger("currentPosition", function( pos ) {
            $('.checkpointsHeader .checkpointStatusHeader').html('<div class="checkpointHeaderImg' + (pos+1) + '">(of '+ numberOfSlides +')</div>');
            $('.orbWrap').addClass('step'+(pos+1)+'');
        });

    },
    getStep: function(page){
        var self = this;
        var numberOfSteps = $('.checkpoints .orbSlide').length;
        var pos = $(".checkpoints").triggerHandler("currentPosition");
    },
    showLast: function(page){
        var self = this;
        $(".checkpoints").trigger("destroy");
        $(".checkpoints").hide();
        $(".checkpointsHeader").hide();
        $(".orbClosing").css("display","block");

    },

    nextSlide: function(){
        $('.checkpoints').trigger("updateSizes");
        $('.checkpoints').trigger('next');
    },

    prevSlide: function(){
        var self = this;
        if(self.slideNumber > 1){
            self.slideNumber--;
            self.getStep(self.slideNumber);
        }else{
            //some kind of error?
        }
    },
    updateUserOrb: function(orbId, status) {
        $.ajax({
            url: "/app/actions/doUpdateUserOrb.php",
            data: {orbId: orbId, status: status},
            type: "post",
            dataType: "json",
            success: function(json) {
                if (status == "completed") {
                    parent.$.fancybox.close();
                }
            }
        });
    }
});
