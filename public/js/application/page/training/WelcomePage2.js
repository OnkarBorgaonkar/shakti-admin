application.page.training.WelcomePage2 = Core.extend(application.page.training.BaseTrainingPage, {

    takeAssessment: false,
    heroMachineString: "",

    init : function() {
        application.page.training.WelcomePage2.superclass.init.call(this);
        var self = this;

        $(document).ready(function() {
            $('.orbContent .orbsPane').mouseenter(function (){
                var $orbParent = $(this).parent('.orbContent');
                var $orbImg = $orbParent.find('.orbs');

                $orbParent.stop().animate({
                    width: "112px"
                }).css({"overflow":"visible"});
                $orbImg.attr('src', $orbImg.attr('data-active-src'));
                $orbParent.addClass('hover');

                $('.shaktiOrbsDarkener').addClass('hover');
            });
            $('.orbContent .orbsPane').mouseleave(function (){
                var $orbContent = $(this).parent();
                var $orbParent = $(this).parent('.orbContent');
                var $orbImg = $orbParent.find('.orbs');
                var offWidth = $orbContent.attr('data-orig-width');

                $orbParent.stop().animate({
                    width: offWidth*1
                }).css({"overflow":"visible"});
                $orbImg.attr('src', $orbImg.attr('data-original-src'));
                $orbParent.removeClass('hover');

                $('.shaktiOrbsDarkener').removeClass('hover');
            });

			 $('#Welcome').keydown(function(event){
			    if (event.keyCode == 9) {
			        event.preventDefault();
			    }
			});
			
            $("#fancybox-close").click(function() {
                var checkpoint = $("#fancybox-content").hasClass("closeBigdoor");
                if (checkpoint) {
                    $("#fancybox-content").removeClass("closeBigdoor");
                    $(".bd-minimize-button").click();
                }
            });

            $(".orbContent").click(function() {
//                var title = $(this).attr("data-title");
//                var description =$(this).attr("data-long-description");
//                var questCompleted = $(this).attr("data-quest-completed");
//                var bigDoorQuestId = $(this).attr("data-big-door-quest-id");
//                var orbImage = $(this).find(".orbs").attr("data-active-src");
//
//                $("#orb-popup-title").html(title);
//                $("#orb-popup-description").html(description);
//                $("#orb-popup-image").attr("src", orbImage);
//
//                if (questCompleted) {
//                    $("#orb-completed-content").show();
//                    $("#orb-popup-link").hide();
//                }
//                else {
//                    $("#orb-completed-content").hide();
//                    $("#orb-popup-link").show().attr("data-orb-id", bigDoorQuestId);
//                }
//
//                var html = $("#orb-description-popup").html();
//                new application.widget.popup.InformationPopUp({description: html});
//                return false;
            });

            $(".orbPopUp").fancybox({
                'type': 'iframe',
                'autoScale' : false,
                'width': 800,
                'height' : 900,
                'onClosed' : function() {
                    window.location.reload();
                }
            });
            

            $("#orb-popup-link").live("click", function() {
                if(BDM.app.current().providers.quests._fetch_incomplete_called==false) {
                    BDM.app.current().providers.quests.lazy_fetch_incomplete();
                }

                var bigDoorQuestId = $(this).attr("data-orb-id");
                var qst = BDM.first(BDM.app.current().providers.quests.selectable(), function(quest){ return (quest.id()===Number(bigDoorQuestId))});
                qst.activate();

                $.fancybox.close();
                return false;
            });


            $("#orb-info").click(function() {
                new application.widget.popup.InformationPopUp({
                    description: $("#orbs-popup-text").html(),
                    onComplete: self.orbInfoCarousal,
                    kapowBG: false
                });
                return false;
            });


            $("#heros-corner-info").click(function() {
                new application.widget.popup.InformationPopUp({description: $("#heros-popup-text").html()});
                return false;
            });


            $('#teamSlider').jcarousel({
                visible: 2,
                scroll: 1,
                auto: 3,
                itemFallbackDimension: "156",
                wrap: "last",
                buttonNextHTML: "<div>&nbsp;</div>",
                buttonPrevHTML: "<div>&nbsp;</div>"
            });
            $('#extraSlider').jcarousel({
                visible: 1,
                scroll: 1,
                auto: 0,
                wrap: "last",
                width: "300px",
                itemFallbackDimension: "300",
                buttonNextHTML: "<div id='extraLeft'>&nbsp;</div>",
                buttonPrevHTML: "<div id='extraRight'>&nbsp;</div>",
                namespace: 'extraSlider'
            });
            $('.heroImage').tooltip({
                delay: 0,
                showURL: false,
                bodyHandler: function() {
                    return $("<img/>").attr("src", this.src);
                }
            });


            if (self.takeAssessment) {
                    new application.widget.popup.InformationPopUp({
                        description: $("#takeAssessment-popup-text").html(),
                        kapowBG: true
                    });
                    return false;
            }

            $('.readArticle').tooltip({ showURL: false, positionLeft: true });

            if ("" == self.heroMachineString)
                $(".heroMachine").toggle();
        });
    },

    orbInfoCarousal: function() {
        $("#bigDoorExtendedDescription #orbsHowTo").carouFredSel({
            auto : false,
            height:320,
            width: 410,
            prev : '#bigDoorExtendedDescription #backHowTo',
            next : '#bigDoorExtendedDescription #nextHowTo',
            items       : {
                visible     : 1,
                width       : 410
            }
        });
    }

});
