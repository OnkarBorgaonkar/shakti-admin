application.widget.popup.InformationPopUp = function(config) {
    var base = this;
    jQuery.extend(base, config);
    base.init();
};

application.widget.popup.InformationPopUp.prototype = {

    //properties
    kapowBG: false,
    description: null,
    closeBigDoor: false,
    onComplete: function(){},
    //core
    init: function() {
        var self = this;

        //insert html to dom before calling fancybox to allow fancybox to size and posistion properly
        $("#bigDoorExtendedDescription").html(self.description);
        $.fancybox({
            'padding'      : 0,
            'autoScale'     : true,
            'scolling'      : 'no',
            'transitionIn'  : 'none',
            'transitionOut' : 'none',
            'centerOnScroll':true,
            'width':700,
            'href'          : '#bigdoor-description',
            'onStart'       : function () {
            	
            	if(self.kapowBG == true){
            		$('body').addClass('fancybox-kapow');
            	}
             },
             'onComplete' : function() {
                $('#fancybox-close').css({
                    right: '99px',
                    top: '60px'
                });
                $('#fancybox-wrap').width(700);
                if (self.closeBigdoor) {
                    $('#fancybox-content').addClass("closeBigdoor");
                }
                //call custom on complete function
                self.onComplete();
             },
             'onClosed': function(){
             	$('body').removeClass('fancybox-kapow');
             }
        });

    }

    //methods

};
