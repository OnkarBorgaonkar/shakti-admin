// Standard JQuery document.ready handling caused problems, perhaps only in IE.
$(window).load(function()
{
    $(".shakti-form label input, .shakti-form label textarea, .shakti-form label select").focus(function(){
        $(this).parent('label').addClass("focus");
    }).blur(function(){
        $(this).parent('label').removeClass("focus");
    });

});