/**
* Take a string representation of a date in the format 2007-04-03 and return
* a javascript Date object for the represented date.
*/
function getDateObjectForString(dateString)
{
    var parts = dateString.split(/[- :]/);

    // Note: the "parts[1] - 1" below because Date() expects months to
    // be zero-indexed.
    return new Date(parts[0], parts[1] - 1, parts[2]);
}

function updateTotalPoints()
{
    $(".totalPoints").load("/json/getChildHeroTotalPoints.php");
}

// This function is used if we're on a page for which the calendar serves no purpose
// and is present purely as eye candy. For Track your Points and Extra Credit, this
// function is redefined usefully in an in-page script tag.
function calendarChanged()
{
}

function initializeCalendar()
{
    // If there's a calendarContainer on this page (i.e. an internal, post-login page)
    if ($("#calendarContainer")[0])
    {
        calendar = new YAHOO.widget.Calendar("calendar","calendarContainer");
        calendar.cfg.setProperty("maxdate", new Date());
        calendar.render();
        calendar.selectEvent.subscribe(calendarChanged, calendar, true);
    }
}

function popUpWindow (webUrl, topPosition, leftPosition, windowWidth, windowHeight, showScroll)
{

    window.open(webUrl,'popUp','top='+topPosition+',left='+leftPosition+',width='+windowWidth+',height='+windowHeight+',scrollbars='+showScroll+',resizable=yes');
};
/**
* This function is called by the nav.swf to login
*/
function login(login, password)
{
    document.loginForm.login.value = login;
    document.loginForm.password.value = password;
    document.loginForm.submit();
}
/*************************************************/
function getQueryStringVariable(name)
{
    var matches = window.location.href.match(/\?(.*)$/);
    if (!matches)
        return false;
    matches = matches.pop().split("&");
    for (var i=0; i < matches.length; i++)
    {
        var pair = matches[i].split("=");
        if (name == pair[0])
            return pair[1];
    }
}
/**
* Set a form's fields based on values from a JSON object.
* If the form has fields with names like child[firstName], child[gender] (often used when a single form edits
* many domain objects, then the third parameter can specify the array name.)
*
* @param DOMElement form the Form object
* @param Object    jsonObject the JSON object
* @fieldArrayName string the name of the "array" used in array-style notation.
*/
function setFormFields(form, jsonObject, fieldArrayName)
{
    var elements = form.elements;
    for(key in jsonObject)
    {
        if (fieldArrayName)
            fieldName = fieldArrayName + "[" + key + "]";
        else
            fieldName = key;
        var input = elements[fieldName];
        if (null != input)
            switch(input.type)
            {
                case "checkbox":
                    if ("t" == jsonObject[key])
                        input.checked = "true";
                    break;
                default:
                    // We must test jsonObject[key] to see if it's null; if it is,
                    // Internet Explorer will output a literal string "null".
                    if (jsonObject[key])
                        input.value = jsonObject[key];
            }
    }
}

////////////////////////////////////////////////////////////////////////////////
//
// AutoRollover
// Copyright (C) 2004 Homeboyz Interactive, Inc.
// License: BSD
// $Id: autoRollover.js,v 1.5 2004/04/13 15:51:45 tduffey Exp $
//
////////////////////////////////////////////////////////////////////////////////
//
// Automatically turn images with class "autoRollover" into
// rollover images where the hover-state image is filename_hover.ext,
// e.g. if you have:
//
//    <img src="images/foo.gif" class="autoRollover" />
//
// then there must be an image called "images/foo_hover.gif"
//
// The script does not check if the hover image exists!
//
////////////////////////////////////////////////////////////////////////////////

function autoRollover()
{
    if (document.getElementById)
    {
        var searchRegex = /autoRollover/i;
        var images = document.getElementsByTagName("img");

        for (var i = 0; i < images.length; i++)
        {
            // If image has class "autoRollover"
            if (-1 != images[i].className.search(searchRegex))
            {
                var src = images[i].src;
                var lastDotPosition = src.lastIndexOf(".");
                var srcExt = src.substring(lastDotPosition, src.length);

                var hoverSrc = src.substring(0, lastDotPosition) + "_hover" + srcExt;

                images[i].hImg = new Image();
                images[i].hImg.src = hoverSrc;

                images[i].oImg = new Image();
                images[i].oImg.src = images[i].src;

                images[i].onmouseover = function()
                {
                    this.src = this.hImg.src;
                }

                images[i].onmouseout = function()
                {
                    this.src = this.oImg.src;
                }
            }
        }
    }
}

function showDescriptionPopup(description, closeBigdoor, onComplete) {
    closeBigdoor = (typeof closeBigdoor === "undefined") ? false : closeBigdoor;
    onComplete = (typeof onComplete === "undefined") ? function(){} : onComplete;

    //insert html to dom before calling fancybox to allow fancybox to size and posistion properly
    $("#bigDoorExtendedDescription").html(description);
    $.fancybox({
        'padding'      : 0,
        'autoScale'     : true,
        'scolling'      : 'no',
        'transitionIn'  : 'none',
        'transitionOut' : 'none',
        'centerOnScroll':true,
        'width':700,
        'href'          : '#bigdoor-description',
        'onStart'       : function () {
            $("#fancybox-close").css('right', '114px');
            $("#fancybox-close").css('left', 'auto');
            $("#fancybox-close").css('top', '70px');
            $("#fancybox-overlay").css('background','none');
         },

         'onComplete' : function() {
            $('#fancybox-close').css({
                right: '99px',
                top: '60px'
            });
            $('#fancybox-wrap').width(700);
            if (closeBigdoor) {
                $('#fancybox-content').addClass("closeBigdoor");
            }
            //call custom on complete function
            onComplete();
          }
    });
}

if (typeof window.addEventListener != "undefined")
    window.addEventListener("load", autoRollover, false);
else if (typeof document.addEventListener != "undefined")
    document.addEventListener("load", autoRollover, false);
else if (typeof window.attachEvent != "undefined")
    window.attachEvent("onload", autoRollover);
else
{
    if (typeof window.onload == "function")
    {
        window.currentOnload = window.onload;

        window.onload = function()
        {
            window.currentOnload();
            autoRollover();
        }
    }
    else
        window.onload = autoRollover;
}
