shaktiSOL.Application = function(options) {
    var base = this;
    jQuery.extend(base, options);
    base.onBeforeReady();
    jQuery(document).ready(function() {
        base.ready = true;
        base.onReady();
    });

    jQuery(window).load(function(){
        base.onLoad();
    });
};

shaktiSOL.Application.prototype = {

    //properties
    ready: false,
    widths: {
        desktop: 980,
        tablet: 900,
        mobile: 600
    },
    //events
    onBeforeReady: jQuery.noop,
    onReady: jQuery.noop,
    onLoad: jQuery.noop,

    //methods
    decimalToPercent: function(rating) {
        return rating / 5 * 100 + '%';
    },

    deleteCookie: function(name) {
        document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    },

    getCookieValue: function(name) {
        var pieces = document.cookie.split('; ');
        for (var i = 0; i < pieces.length; i++) {
            var parts = pieces[i].split('=');
            if (parts[0] == name){
                return parts[1];
            }
        }
        return null;
    },

    getHashValue: function(name) {
        name = name.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
        var regexS = "[\\?#&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec(document.location.hash);
        if (results === null) {
            return "";
        }
        else {
            return results[1];
        }
    },

    isNumber: function (testString) {
        return /^\d+$/.test(testString);
    },

    isReady: function() {
        return this.ready === true;
    }
};