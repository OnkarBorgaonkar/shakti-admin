shaktiSOL = {

    uid: 0,

    extend : function(){
        var io = function(o){
            for(var m in o){
                this[m] = o[m];
            }
        };
        var oc = Object.prototype.constructor;

        return function(sb, sp, overrides){
            if(typeof sp == 'object'){
                overrides = sp;
                sp = sb;
                sb = overrides.constructor != oc ? overrides.constructor : function(){sp.apply(this, arguments);};
            }
            var F = function(){},
                sbp,
                spp = sp.prototype;

            F.prototype = spp;
            sbp = sb.prototype = new F();
            sbp.constructor=sb;
            sb.superclass=spp;
            if(spp.constructor == oc){
                spp.constructor=sp;
            }
            sb.override = function(o){
                shaktiSOL.override(sb, o);
            };
            sbp.superclass = sbp.supr = (function(){
                return spp;
            });
            sbp.override = io;
            shaktiSOL.override(sb, overrides);
            sb.extend = function(o){return shaktiSOL.extend(sb, o);};
            return sb;
        };
    }(),

    override : function(origclass, overrides){
        if(overrides){
            var p = origclass.prototype;
            jQuery.extend(p, overrides);
        }
    },

    namespace : function(){
        var o, d;
        for (var i = 0; i < arguments.length; i++) {
            v = arguments[i];
            d = v.split(".");
            o = window[d[0]] = window[d[0]] || {};
            for (var dd = 0; dd < d.slice(1).length; dd++) {
                v2 = d.slice(1)[dd];
                o = o[v2] = o[v2] || {};
            }
        }
        return o;
    },

    getId: function() {
        return this.uid++;
    },

    getURLParam: function(strParamName) {
        var strReturn = "";
        var strHref = window.location.href;
        if ( strHref.indexOf("?") > -1 ) {
            var strQueryString = strHref.substr(strHref.indexOf("?")).toLowerCase();
            var aQueryString = strQueryString.split("&");
            for ( var iParam = 0; iParam < aQueryString.length; iParam++ ) {
                  if (aQueryString[iParam].indexOf(strParamName.toLowerCase() + "=") > -1 ) {
                    var aParam = aQueryString[iParam].split("=");
                    strReturn = aParam[1];
                    break;
                  }
            }
          }
          return unescape(strReturn);
    }
};

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function(str) {
        return this.indexOf(str) === 0;
    };
}

if (typeof window.console == "undefined") {
    console = {};
    console.log = console.warn = console.error = function(a) {};
}

shaktiSOL.namespace(
    "shaktiSOL",
    "shaktiSOL.page",
    "shaktiSOL.page.home",
    "shaktiSOL.page.register",
    "shaktiSOL.page.share",
    "shaktiSOL.page.training",
    "shaktiSOL.page.parallaxTest",
    "shaktiSOL.page.pilotRegister"
);