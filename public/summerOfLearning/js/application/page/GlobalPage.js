shaktiSOL.page.GlobalPage = shaktiSOL.extend(shaktiSOL.Application, {

    burst: {
        animClasses: ['burst','burst2'],
        odds: 10,//higher happens less
        timeout: 15 //in seconds
    },

    //properties
    validationConfig: {
        errorClass: 'has-error',
        validClass: 'has-success',
        highlight : function(element, errorClass, validClass) {
            $($(element).parents('.form-group')[0]).addClass(errorClass).removeClass(validClass);
        },
        unhighlight : function(element, errorClass, validClass) {
            $($(element).parents('.input-wrap')[0]).removeClass(errorClass).addClass(validClass);
        }
    },
    //events

    /**
     * Event handler for when you just want to run code and dont want to wait for document ready.
     */
    onBeforeReady: function() {

    },

    /**
     * Event handler for when you need to wait for document ready.
     */
    onReady: function() {

        //header
        $('#loginNow').on('click', function() {
            $(this).slideUp();
            $('.home.title').slideUp();
            $('.loginForm').slideDown();
        });

        //placeholders in login form
        $('input, textarea').placeholder();
    },

    randomNumber: function(within){
        return Math.floor((Math.random() * within));
    },
    isMobile: {
        self: this,
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (this.Android() || this.BlackBerry() || this.iOS() || this.Opera() || this.Windows());
        }
    },
    initOrbAnims: function(){
        var self = this;
        self.$orbFlares = $('.flareAndBall');
        self.playOrbAnim();

    },
    playOrbAnim: function(){
        var self = this;
        var random =  self.randomNumber(10);

        //if( random < 8 ){
            var randomNumber = self.randomNumber(self.$orbFlares.length+1);

            var animOrb = self.$orbFlares[randomNumber];

            var animClass = self.burst.animClasses[self.randomNumber(self.burst.animClasses.length)];

            for(var classesCleaner = self.burst.animClasses.length; classesCleaner > 0; classesCleaner--){
                $(animOrb).removeClass(self.burst.animClasses[classesCleaner]);
            }

            $(animOrb).toggleClass(animClass);

       // }

        setTimeout(function(){
            self.playOrbAnim();
        }, 10*1000);
    }
});
