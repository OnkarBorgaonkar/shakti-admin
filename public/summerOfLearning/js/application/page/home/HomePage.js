shaktiSOL.page.home.HomePage = shaktiSOL.extend(shaktiSOL.page.GlobalPage, {
    heroContainerHeight: $('.homeHero').height(),
    cloudSpeed: 240,//seconds
    //events
    onBeforeReady: function() {
        shaktiSOL.page.home.HomePage.superclass.onBeforeReady.call(this);

    },

    onReady: function() {
        shaktiSOL.page.home.HomePage.superclass.onReady.call(this);

        var self = this;

        if(! self.isMobile.any()){
            $(window).stellar({
                hideDistantElements: false
            });
        }

        var $header = $('#headerContainer');
        var $position = $header.position().top;
        // var $move = $position - $header.height() - $(window).scrollTop();
        self.heroContainerHeight = $('#homeHero').height();
        $(window).resize(function(){
            self.heroContainerHeight = $('#homeHero').height();
        });


        $(window).scroll( function() {
            self.heroContainerHeight = $('#homeHero').height() + $('#homeHeaderLogo').height();

            var marginHeight = $header.height();
            if ($(document).scrollTop() > self.heroContainerHeight) {
                $($header).addClass('fixed');
                $(".introContainer").css({
                    "margin-top": marginHeight + "px"
                });
            } else {
                if ($($header).hasClass('fixed')) {
                    $($header).removeClass("fixed");
                    $(".introContainer").css({
                        "margin-top": 0
                    });
                }
            }
        });

        $("#pilot").fancybox({
            fitToView:true,
            width:'95%',
            autoSize:true,
            autoScale:true,
            closeClick:false,
            maxWidth:800,
            autoResize:true
        });

        $('.member').hover(function(){
            var video = $(this).find('video');
            video.get(0).play();
        },function(){
            var video = $(this).find('video');
            video.get(0).pause();
        });


        this.initOrbAnims();
//      this.heroClouds();

    },
    heroClouds: function(){
        var self = this;

        $('#homeHero').stop().animate({
            backgroundPositionX: '-2150px'
          },
          self.cloudSpeed*1000,
          'linear',
          function(){
              $(this).css({
                  backgroundPositionX: '0px'
              });
              self.heroClouds();
          });
    },
    onLoad: function(){

        shaktiSOL.page.home.HomePage.superclass.onLoad.call(this);

        $('html').addClass('load');

            $('.videoBg').each(function(){
                var videoName = $(this).attr('data-bg');

                var videoHtml = '';
                videoHtml += '<video loop>';
                videoHtml += '<source src="/public/summerOfLearning/movies/warriorCards/' + videoName + '.webm" type="video/webm; codecs=vp8,vorbis" />';
                videoHtml += '<source src="/public/summerOfLearning/movies/warriorCards/' + videoName + '.ogv" type="video/ogg; codecs=theora,vorbis" />';
                videoHtml += '<source src="/public/summerOfLearning/movies/warriorCards/' + videoName + '.mp4" type="video/mp4">';
                videoHtml += '<img src="/public/summerOfLearning/movies/warriorCards/' + videoName + '.jpg" alt="member" />';
                videoHtml += '</video>';

                $(this).html(videoHtml);
            });
    }

});
