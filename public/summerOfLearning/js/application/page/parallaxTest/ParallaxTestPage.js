shaktiSOL.page.parallaxTest.ParallaxTestPage = shaktiSOL.extend(shaktiSOL.page.GlobalPage, {

    //events
    onBeforeReady: function() {
        shaktiSOL.page.parallaxTest.ParallaxTestPage.superclass.onBeforeReady.call(this);
    },

    onReady: function() {
        shaktiSOL.page.parallaxTest.ParallaxTestPage.superclass.onReady.call(this);

        $(window).stellar({
        });

    }

});