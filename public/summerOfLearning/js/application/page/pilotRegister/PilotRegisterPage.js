shaktiSOL.page.pilotRegister.PilotRegisterPage = shaktiSOL.extend(shaktiSOL.page.GlobalPage, {

    //events
    onBeforeReady: function() {
        shaktiSOL.page.pilotRegister.PilotRegisterPage.superclass.onBeforeReady.call(this);
    },

    onReady: function() {
        shaktiSOL.page.pilotRegister.PilotRegisterPage.superclass.onReady.call(this);
        var self = this;
        $("#pilotRegisterForm").validate({
            rules: {
                email: {
                    email: true
                },
                phone: {
                    phoneUS: true
                }
            },
            messages: {
                email: "Please enter a valid email.",
                phone: "Please enter a valid phone number."
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            console.log('success');
                            $("#pilotRegisterForm").fadeOut();//hide form
                            $("#thankYou").delay(300).fadeIn();//show success
                            setTimeout(self.closeForm, 5000);//close form
                        } else {
                            console.log('data failure');
                        }
                    },
                    error: function(e) {
                        console.log("Error: ",e);
                    }
                });
                // $("#submit").attr('disabled',true).addClass('submitting');
            }
        });

        //input selector highlights
        var inputSelector = $(".inputContainer");

        $(inputSelector).children().on('focus', function() {
            $(this).parent().css('background', 'white');
            $(this).parent().css('border', '2px solid #242222');
        });
        $(inputSelector).children().on('blur', function() {
            if ($(this).hasClass("valid")) {
                $(this).parent().css('border', '2px solid #41743a');
            } else if ($(this).hasClass("error")) {
                $(this).parent().css('border', '2px solid #fc121c');
            } else {
                $(this).parent().css('background-position', '0 0');
                $(this).parent().parent().css('background-position', 'right -440px');
            }
        });

        $('#close').click(function() {
            parent.$.fancybox.close();
        });

        $("#city").autocomplete({
            source: function(request, response) {
                var state = $("#state").val();
                $.ajax({
                    url: "/json/citySearch.php",
                    data: {term: request.term, state: state},
                    dataType: "json",
                    success: function(json) {
                        response($.map(json, function(city) {
                            return {
                                label: city.city,
                                value: city.city
                            };
                        }));
                    }
                });
            },
            focus: function (event, ui) {
                $("#city").val(ui.item.label);
                return false;
            },
            minLength: 2
        });
    },
    closeForm: function(){
        parent.$.fancybox.close();
    }
});