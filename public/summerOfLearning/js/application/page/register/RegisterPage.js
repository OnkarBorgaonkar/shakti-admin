shaktiSOL.page.register.RegisterPage = shaktiSOL.extend(shaktiSOL.page.GlobalPage, {

    //events
    onBeforeReady: function() {
        shaktiSOL.page.register.RegisterPage.superclass.onBeforeReady.call(this);

        $("#applicationForm").validate({
            rules: {
                confirm: {
                    equalTo: "#password"
                },
                "user[login]": {
                    remote: "/json/checkUserName.php"
                }
            },
            messages: {
                confirm: "Password does not match."
            },
            submitHandler: function(form) {
                $(form).ajaxSubmit({
                    dataType: "json",
                    success: function(data) {
                        if (data.success) {
                            window.location = "/summerOfLearning/training.php?firstVisit=true";
                        } else {
                            $("#continue").attr('disabled', false).removeClass('submitting');
                            $("#systemMessage").show().html(data.systemMessage);
                            $("html, body").animate({scrollTop: 0}, "slow");
                        }
                    },
                    error: function(e) {
                        console.log("Error: ",e);
                    }
                });
                $("#continue").attr('disabled',true).addClass('submitting');
            }
        });

        //submit form
        $("#continue").on('click', function(e) {
            $('#applicationForm').submit();
            e.preventDefault();
        });

        //input selector highlights
        var inputSelector = $(".inputContainer");

        //TODO: set a variable for the background-position x-pos and y-pos
        $(inputSelector).children().on('focus', function() {
            $(this).parent().css('background', 'white');
            $(this).parent().css('border', '2px solid #242222');
        });
        $(inputSelector).children().on('blur', function() {
            if ($(this).hasClass("valid")) {
                $(this).parent().css('border', '2px solid #41743a');
            } else if ($(this).hasClass("error")) {
                $(this).parent().css('border', '2px solid #fc121c');
            } else {
                $(this).parent().css('background-position', '0 0');
                $(this).parent().parent().css('background-position', 'right -440px');
            }
        });

        //royal slider
        $(".royalSlider").royalSlider({
            //addActiveClass: true,
            autoScaleSlider: false,
            // autoSliderWidth: '800',
            autoHeight: true,
            autoPlay: {
                enabled: false
            },
            addActiveClass: true,
            navigateByClick: false,
            fadeinLoadedSlide: true,
            sliderDrag: false,
            keyboardNavEnabled: false,
            sliderTouch: false,
            slidesSpacing: 0,
            usePreloader: true,
            transitionType: 'move',
            transitionSpeed: '500',
            loop: 'false'
        });

        //royal slider button
        var slider = $(".royalSlider").data('royalSlider');

        // $("#next").on('click', function() {
        //     slider.next();
        // });

        $("#next").on('click', function() {
            var slideInputs = $('#slide1').find(".required");

            slideInputs.validate();
            if (slideInputs.valid()) {
                slider.next();
            }
        });

        $("#city").autocomplete({
            source: function(request, response) {
                var state = $("#state").val();
                $.ajax({
                    url: "/json/citySearch.php",
                    data: {term: request.term, state: state},
                    dataType: "json",
                    success: function(json) {
                        response($.map(json, function(city) {
                            return {
                                label: city.city,
                                value: city.city
                            };
                        }));
                    }
                });
            },
            focus: function (event, ui) {
                $("#city").val(ui.item.label);
                return false;
            },
            minLength: 2
        });

    },

    onReady: function() {
        shaktiSOL.page.register.RegisterPage.superclass.onReady.call(this);
    }

});
