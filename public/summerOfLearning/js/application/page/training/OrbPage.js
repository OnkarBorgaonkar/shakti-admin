shaktiSOL.page.training.OrbPage = shaktiSOL.extend(shaktiSOL.page.GlobalPage, {

    slideNumber: 1,
    hasUnsavedChanges: false,
    startingCheckpoint: 0,
    hasFocus: false,
    timer: null,
    elapsedTime: 0,
    limitReached: false,
    timerStarted: false,
    timeLeft: 90,
    videoPath: 'http://videos.shaktiwarriors.com/steam-2014/',
    video: {
        autoPlay: true,
        loadTimeout: 400
    },

    onBeforeReady : function() {
        shaktiSOL.page.training.OrbPage.superclass.onBeforeReady.call(this);
        var self = this;

        $('.checkpoint .link').click(function() {
            if (self.timer == null) {
                self.timeLimit = parseInt($(this).data("seconds"), 10);
                self.elapsedTime = 0;
                self.limitReached = false;
                self.timer = setInterval(function(){
                    self.updateTimer();
                }, 1000);
                $('.linkSubmit').hide();
            }
        });

        $('form').each(function(){
            $(this).validate({
                errorContainer: '.errorMessage',
                submitHandler: function(form) {
                    $("form input[type=submit]").attr("disabled", "disabled");
                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: function(json) {
                            if (json.success) {
                                $(".errorMessage").html("");
                                $('.loader').removeClass('showLoading');
                                var numberOfSlides = $('.checkpoints .orbSlide').length - 1;
                                $(".checkpoints").trigger("currentPosition", function(pos) {
                                    if (numberOfSlides == pos) {
                                        self.showLast();
                                    } else {
                                        self.nextSlide();
                                        $('.checkpoints').trigger("updateSizes");
                                    }
                                });
                            }
                            else {
                                $(".errorMessage").html(json.systemMessage);
                                $('.loader').removeClass('showLoading');
                                $(".errorMessage").addClass("showError");
                                $("form input[type=submit]").removeAttr("disabled");
                            }
                        },
                        complete: function(jqXHR, status){
                            console.log(jqXHR, status);
                        },
                        error: function(jqXHR, textStatus, errorThrown){
                            console.log(jqXHR, textStatus, errorThrown);
                        }
                    });
                },
                invalidHandler: function(){
                    $('.loader').removeClass('showLoading');
                }
            });
        });

        $(".link").click(function() {
            $(this).hide().siblings(".linkForm").show();
            $("#timer").addClass('showTimer');
        });
    },

    onReady: function() {
        shaktiSOL.page.training.OrbPage.superclass.onReady.call(this);

        var self = this;

        self.hasFocus = true;

        $(window).blur(function() {
            self.hasFocus = false;
        });
        $(window).focus(function() {
            self.hasFocus = true;
        });

        $('.orbCheckpointsWrap').hide();

        $('.startOrb').click(function(){
            window.parent.$.fancybox.update();
            var orbId = $(this).data("id");
            self.updateUserOrb(orbId, "started");
            $('.orbLanding').hide();
            $('.orbCheckpointsWrap').show();
            self.initSlider();
            self.updateStatus();
        });

        $('#closeButton').click(function() {
            var orbId = $(this).data("id");
            self.updateUserOrb(orbId, "completed");
        });

        $('.storySubmit').click(function () {
            $('.loader').addClass('showLoading');
            $('.errorMessage').html('<div class="errorMessage" style="display: none;"></div>');
        });

        $('.orbWrap').keydown(function(event){
            // Prevent tab/shift-tab
            if (event.keyCode == 9) {
                event.preventDefault();
            }
        });

        $(".checkpoints :input, .checkpoints a").attr("tabindex", "-1");

        //hides slider on video player if not on specific hosts
        var host = window.location.hostname;
        console.log(host);

        if (host == 'test.shaktiwarriors.com' || host == 'dev.shaktiwarriors.com' || host == 'staging.shaktiwarriors.com') {
            $('html').addClass('testing');
        }

    },

    updateTimer: function () {
        var self = this;

        if (!self.hasFocus) {
            self.elapsedTime++;

            self.timeLeft = self.timeLimit - self.elapsedTime;

            if (!self.limitReached && self.elapsedTime >= self.timeLimit) {
                clearInterval(self.timer);
                self.timer = null;
                self.limitReached = true;
                $("#timer").hide();
                $("#timer").removeClass('showTimer');
                $('.linkCheckpoint .linkForm .inputContainer').html('<input type="submit" value="Click To Earn Your Badge!" class="linkSubmit button large" />');

            } else if (!self.timerStarted){
                self.timerStarted = true;
            }

            var minutes = Math.floor(self.timeLeft / 60);
            var seconds = self.timeLeft % 60;

            var secString = "" + secondsf;
            if (seconds < 10) {
                secString = "0" + secString;
            }

            $("#timer").html(minutes + ":" + secString);
        }
    },

    stopTimer: function () {
        var self = this;
        clearInterval(self.timer);
        self.timer = null;
        $("#timer").html("0:00");
    },

    initSlider: function(){
        var self = this;

        $('.checkpoints').carouFredSel({
            auto: false,
            responsive: true,
            height: 'variable',
            infinite: false,
            circular: false,
            onCreate: function() {
                $(".checkpoints").trigger("slideTo",self.startingCheckpoint);

                if(self.startingCheckpoint === 0){
                    console.log('First slide');
                    self.killVideos();
                    setTimeout(function(){
                        self.initVideo();
                    }, self.video.loadTimeout);
                }

            },
            scroll: {
                onBefore : function() {
                    self.updateStatus();
                    self.killVideos();
                },
                onAfter : function(data) {
                    $("form input[type=submit]").removeAttr("disabled");
                    window.parent.$.fancybox.update();

                    //wait a bit before adding the video for IOS
                    setTimeout(function(){
                        self.initVideo();
                    }, self.video.loadTimeout);
                }
            }
        });

        //self.initVideo();
    },

    updateStatus: function (){
        var numberOfSlides = $('.checkpoints .orbSlide').length;
        $(".checkpoints").trigger("currentPosition", function( pos ) {
            $('.checkpointsHeader .checkpointStatusHeader').html('Quest ' + (pos+1) + ' of ' + numberOfSlides);
            $('.orbWrap').addClass('step'+(pos+1)+'');
        });

    },
    showLast: function(){
        $(".checkpoints").trigger("destroy");
        $(".checkpoints").hide();
        $(".checkpointsHeader").hide();
        $(".orbClosing").css("display","block");

    },

    nextSlide: function(){
        $('.checkpoints').trigger("updateSizes");
        $('.checkpoints').trigger('next');
    },

    prevSlide: function(){
        var self = this;
        if(self.slideNumber > 1){
            self.slideNumber--;
        }else{
            //some kind of error?
        }
    },
    killVideos: function(){
        var self = this;
        //clean up old videos
        var oldVideos = $('.checkpoints').find('.activeVideo');
        if(oldVideos.length > 0){
            $.each(oldVideos ,function(){
                //set old video videojs object
                var currentPlayer = videojs(self.getVideoId($(this).attr('data-video')));
                //clean it up
                currentPlayer.dispose();
                //get the post image
                var img = '<img src="' + $(this).attr('data-poster') + '" class="posterImage" style="width: 100%;" />';
                //load in the post image again
                $(this).removeClass('activeVideo').html(img);
            });
        }
    },
    updateUserOrb: function(orbId, status) {
        $.ajax({
            url: "/app/actions/doUpdateUserOrb.php",
            data: {orbId: orbId, status: status},
            type: "post",
            dataType: "json",
            success: function(json) {
                if (status == "completed") {
                    parent.$.fancybox.close();
                }
            }
        });
    },
    initVideo: function(items){
        var self = this;


        //add new videos
        // ask carouFredSel what carousel items are visible
        var carouselItems = $('.checkpoints').trigger('currentVisible', function(items){
            //items is a object with all visible items in the carousel
            $.each(items, function(index, value){
                var $videoPlayers = $(value).find('.videoPlayer');
                if($videoPlayers.length > 0){

                    if(! $videoPlayers.hasClass('activeVideo')){
                        var videoName       = self.getVideoName($videoPlayers),
                            videoPoster     = self.getVideoPoster($videoPlayers),
                            videoPlayerHtml = self.getVideoHtml(videoName, videoPoster),
                            videoId         = self.getVideoId(videoName);


                        $videoPlayers.addClass('activeVideo').html(videoPlayerHtml);


                        // if(self.isMobile.any()){

                            var videoPlayer = videojs(videoId, {}, function(){
                                var thePlayer = this;

                                this.on('play', function(){
                                     this.posterImage.hide();//show the poster again
                                     this.bigPlayButton.hide();//
                                });

                                this.on('ended',function(){
                                    self.completedRequirement();//trigger completed requirement event

                                    //TODO: the following has issues in IE, come back if time
                                    // this.posterImage.show();//show the poster again
                                    // this.bigPlayButton.show();//
                                    this.currentTime(0);//
                                    this.pause();
                                });

                            });
                        // }
                    }

                }
            });
        });
    },
    completedRequirement: function(){
        var self = this,
            slideForm = self.getActiveSlides().find('.linkForm');

        slideForm.find('.inputContainer').html('<input type="submit" value="Click To Earn Your Badge!" class="linkSubmit button large" />');
        slideForm.show();

        // $('.checkpoints').trigger('updateSizes');
    },
    getActiveSlides: function(){
        var activeSlides;
        $('.checkpoints').trigger('currentVisible', function(items){
            activeSlides = items;
        });
        return activeSlides;
    },
    getVideoName: function($videoItem){
        return $videoItem.attr('data-video');
    },
    getVideoId: function(video){
        return 'video_'+video;
    },
    getVideoPoster: function($videoItem){
        var posterImage = $($videoItem).attr('data-poster');
        return posterImage;
    },
    getStageWidth: function(){
        var width = $('.orbWrapper ').width();
        return width;
    },
    getVideoHtml: function(video, poster){
        var self = this;
        var videoPath = self.videoPath + video;
        var videoHtml = '';
            videoHtml += '<video id="'+ self.getVideoId(video) +'" class="video-js vjs-default-skin" controls preload="auto" width="'+ self.getStageWidth() +'" height="300" poster="'+ poster +'">';
            videoHtml += '<source src="'+ videoPath +'.mp4" type="video/mp4" />';
            videoHtml += '<source src="'+ videoPath +'.webmhd.webm" type="video/webm" />';
            videoHtml += '<source src="'+ videoPath +'.ogv" type="video/ogg" />';
            videoHtml += '<p class="vjs-no-js">To view this video please enable JavaScript, and install <a href="http://get.adobe.com/flashplayer/" target="_blank">Adobe Flash Player</a></p>';
            videoHtml += '</video>';
        return videoHtml;
    }
});
