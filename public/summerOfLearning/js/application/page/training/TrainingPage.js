shaktiSOL.page.training.TrainingPage = shaktiSOL.extend(shaktiSOL.page.GlobalPage, {

    //events
    onBeforeReady: function() {
        shaktiSOL.page.training.TrainingPage.superclass.onBeforeReady.call(this);
    },

    

    onReady: function() {
        shaktiSOL.page.training.TrainingPage.superclass.onReady.call(this);
        
        $(".orbPopUp").fancybox({
            'type': 'iframe',
            // 'maxWidth': 700,
            // 'minHeight': 400,
            // height: '80%',
            // fitToView: true,

            maxWidth    : 700,
            fitToView   : false,
            width       : '70%',
            height      : '95%',
            autoSize    : false,
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none',
            padding: 0,
            'wrapCSS': 'orbContainer',
            'afterClose' : function() {
                window.location.reload();
            }
        });

        //progress bars
        $(".power").each(function() {
            var self = this;
            var width = $(self).attr('data-percent');
            var widthInt = parseInt(width, 10);

            $(self).css('width', widthInt + '% ');

            if (widthInt == 100) {
                var backgroundPosition = $(self).css('background-position').split(" ");
                    xPosition = backgroundPosition[0];
                    yPosition = backgroundPosition[1];

                    console.log(xPosition);
                    console.log(yPosition);
                
                $(self).css('background-position', '-30px ' + yPosition);                
            }
            
        });

        $(".member").fancybox({
            'type': 'iframe',
            'autoSize': 'true'
        });

        this.initOrbAnims();
    }
});

