<?php
    require_once("globals.php");
    $pageTitle = "Meet";
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;

    $groupService = new GroupService();

    $category = "group";

    $search = isset($_GET['search']) ? $_GET['search'] : null;

    $orderBy = isset($_GET['orderBy']) ? $_GET['orderBy'] : 'points';

    $orderDirection = "DESC";

    if($orderBy != 'points'){
        $orderDirection = 'ASC';
    }

    $groups = $groupService->searchGroups($search, $orderBy, $orderDirection, 12, $page * 12);

    include("searchGroup.phtml");
?>
