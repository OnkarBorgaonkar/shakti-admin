<?php
    require_once("globals.php");
    $pageTitle = "Meet";
    $page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 0;
    $prevPage = $page - 1;
    $nextPage = $page + 1;

    $userService = new UserService();

    $category = "user";

    $search = isset($_GET['search']) ? $_GET['search'] : null;

    $orderBy = isset($_GET['orderBy']) ? $_GET['orderBy'] : 'points';
    
    $view = isset($_GET['view']) ? $_GET['view'] : 'card';

    $orderDirection = "DESC";

    if($orderBy != 'points'){
        $orderDirection = 'ASC';
    }

    $members = $userService->searchUsers($search, $orderBy, $orderDirection, 12, $page * 12);

    include("searchUser.phtml");
?>
