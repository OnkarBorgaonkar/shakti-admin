<?php
    require_once("../globals.php");
    require_once("Authenticator.php");

    $id = $_GET['id'];

    $userOrbService = new UserOrbService();

    $orbService = new OrbService();
    $orb = array_pop($orbService->find("id = $id"));

    $checkpoints = $orb->getCheckpointsForUser($user->id);

    $startingCheckpoint = -1;
    foreach ($checkpoints as $checkpoint) {
        $startingCheckpoint++;
        if (!$checkpoint->completed)
            break;
    }

    $userOrb = array_pop($userOrbService->find("user_id = $user->id AND orb_id = $orb->id"));

    include("summerOfLearning/orb.phtml");
?>
