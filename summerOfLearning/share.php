<?php
require_once("../globals.php");
$pageTitle = "Share - Summer of Learning";

$userService = new UserService();
$user = array_pop($userService->find("id = {$_GET['userId']}"));

if ($user == null) {
    $message = urlencode("User not found");
    header("location: /summerOfLearning/?systemMessage=$message");
    die();
}


$child = $user->getChild();

$studentYear = $child->year;

$currentYear = date("Y");


// Program Id is hard-coded because there are only two existing programs at this time.
if ($currentYear - $studentYear < 11) {
    $programId = 1;
    // echo "Elementary School";
} else {
    $programId = 2;
    // echo "Middle School";
}

$orbs = $user->getOrbsByProgramId($programId);

$totalCheckpoints = 0;
$totalCheckpointsCompleted = 0;

foreach ($orbs as $orb) {
    $userCheckpoints = $orb->getCheckpointsForUser($user->id);

    foreach ($userCheckpoints as $checkpoint) {
        $totalCheckpoints ++;
        if ($checkpoint->completed) {
            $totalCheckpointsCompleted++;
        }
    }
    
    $percentCompleted = 0;
    if ($totalCheckpoints > 0) {
        $percentCompleted = floor($totalCheckpointsCompleted / $totalCheckpoints * 100);
    }
}


include("summerOfLearning/share.phtml");
?>