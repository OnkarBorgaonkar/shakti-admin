<?php
	require_once("globals.php");
	require_once("Authenticator.php");
	
	$pageTitle = "Track";
	
	// Get the string representation of the date of the Sunday of the current week.
	$baseDate = Date("Y-m-d", strtotime("last Sunday"));
	
	function getCurrentDayClassString($baseDate, $dayOffset)
	{
		$today = date("Y-m-d");
		$dateOfInterest = date("Y-m-d", strtotime($baseDate . "+$dayOffset days"));

		if ($dateOfInterest == $today)
			return "currentDay";
	}

	$taskService = new TaskService();
	$tasksByCategories = $taskService->findAllByCategories();
	
	$customTasks = $taskService->find("category = 'Custom'");
	include("trackYourPoints.phtml");	
?>