<?php
    require_once("globals.php");
    $pageTitle = "View";

    $userService = new UserService();
    $user = array_pop($userService->find("id =" .  $_REQUEST['userId']));

    $login = $user->login;
    $points = $user->getTotalPoints();
    $rank = $user->getRankName();

    $childHeroService = new ChildHeroService();
    $childHero = $childHeroService->findByUserId($user->id);
    extract(get_object_vars($childHero));
    print("<!--");
    print_r($childHero);
    print("-->");

    $heroMachineString = urlencode($heroMachineString);

    $nextRank = $user->getNextRank();
    if ($nextRank != null){

        $nextRankPoints = $nextRank->lowerBound;
        $nextRankPointDelta = $nextRankPoints - $points;

        $nextRankPercentage = $points / $nextRankPoints * 100;
    }
    else{
        $nextRankPercentage = 100;
    }
    include("viewWarrior.phtml");
?>
