<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html>
<head>
	<title>Shakti Warriors - Online Academy for Kids</title>
	<!-- Training kids to be a superheroes, equipped to defend against Chaos in their homes, schools and communities.-->
	<meta name="description" content="Training kids to be a superheroes, equipped to defend against Chaos in their homes, schools and communities " />
	<meta name="keywords" content="shakti warriors, superheroes, children, kids, healthy lifestyle, shakti, solan, vibrato, maye, kaku, sable, echo, training program, online community, shakti, healthy heroes, boys, girls, academy " /> 
	<meta http-equiv="Content-Type" content="text/html; charset=windows-1252" />
	<meta name="language" content="English" />
	<META NAME="ROBOTS" CONTENT="index, follow" />
	<META NAME="REVISIT-AFTER" CONTENT="1 Week" />
	<link href="styleiia.css" rel="stylesheet" type="text/css" />
	<!-- All above here old code. Below is TBD addition -->
	<link rel="stylesheet" type="text/css" href="css_rebuild/global.css" />
	<script type="text/javascript" src="js_rebuild/autoRollover.js"></script>
	<script type="text/javascript" src="js_rebuild/swfobject.js"></script>
	<script src="js_rebuild/prototype.js" type="text/javascript"></script>
	<script src="js_rebuild/scriptaculous.js" type="text/javascript"></script>
	<script type="text/javascript" src="js_rebuild/stripVariabels.js"></script>
		<script type="text/javascript" src="js_rebuild/writeIn.js"></script>
	<script type="text/javascript" src="js_rebuild/logIn.js"></script>
</head>
<body id="webStore">
		<div id="mainBody">
			<div id="navigation"><img src="images_rebuild/fake/navigation.jpg" alt="fake nav" width="762" height="120" /></div>
			<script type="text/javascript">
				// <![CDATA[
				var so = new SWFObject("flash_rebuild/nav.swf", "nav", "762", "120", "8.0.23", "#000000", true);
				so.addVariable("youAreHereLocation", $A(document.getElementsByTagName('body')).pop().id);
				so.addVariable("msg", getURLParam('msg'));
				so.write("navigation");
				// ]]>
			</script>
			<form name="LoginForm" action="https://www.shaktiwarriors.com/login_processing.asp"	method="post" onSubmit="return validateLogin()" style="display:none;">
				<input name="AlterEgo" type="password" />
				<input name="Password" type="password" />
			</form>
			<!-- Open main content -->
			<div class="fullLength comicBook">
				<img src="images_rebuild/webstore/comicSub_hdr.gif" alt="The Shakti Warriors Comic Subscription - Limited Edition - original Series" width="258" height="66" />
				<p>Order now and receive the original series of the Shakti Warriors comic books which introduce you to each of the Warriors and their back stories. Comic books will be mailed once a month for the next four months.  <strong>Order includes a Shakti Warrior Training Poster!</strong></p>
				<div class="order">
					<h1>Our Price: $16.95</h1>
					<p>allow 3-4 weeks for the first delivery.  You must be over 18 to purchase items from this webstore. There are no refunds for ths item.</p>
					 <input size="2" maxlength="2" name="qty_5" value="" />
				     <input type="image" src="images_rebuild/webstore/addToCart.gif" alt="Add to cart" class="submit" />
					<br /><br />
				</div>
			</div>
			
			<!--comment out table until later use
			
			<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td class="stackedShopping bgSet1">
					<img src="images_rebuild/webstore/gameBoard_hdr.gif" alt="The Shakti Warriors Game Board" width="346" height="21" class="hdr" />
					<div>
						<div class="productImage">
							<img src="images_rebuild/webstore/products/gameBoard_thumb.jpg" alt="product shot" width="87" height="87" />	
							<a href="#">click to enlarge</a>
						</div>
						<div class="productContent">
							<p>Play the Shakti Warriors Board Game and learn grat suggestions on the right path.  Order now and receive the original series of the Shakti Warriors comic books which introduce you to each of the Warriors and their back stories.</p>
							<div class="order">
								<h1>Our Price: $89.95</h1>
								<p>allow 3-4 weeks for the first delivery.</p>
								<input size="2" maxlength="2" name="qty_5" value="" />
								<input type="image" src="images_rebuild/webstore/addToCart.gif" alt="Add to cart" class="submit" />
								<div class="clear"></div>
							</div>
						</div>	
					</div>
				</td>
				<td class="stackedShopping bgSet1">
					<img src="images_rebuild/webstore/poster_hdr.gif" alt="The Shakti Warriors Posters" width="304" height="21" class="hdr" />
					<div>
						<div class="productImage">
							<img src="images_rebuild/webstore/products/gameBoard_thumb.jpg" alt="product shot" width="87" height="87" />	
							<a href="#">click to enlarge</a>
						</div>
						<div class="productContent">
							<p>Play the Shakti Warriors Board Game and learn grat suggestions on the right path.  Order now and receive the original series of the Shakti Warriors comic books which introduce you to each of the Warriors and their back stories.</p>
							<div class="order">
								<h1>Our Price: $89.95</h1>
								<p>allow 3-4 weeks for the first delivery.</p>
								<input size="2" maxlength="2" name="qty_5" value="" />
								<input type="image" src="images_rebuild/webstore/addToCart.gif" alt="Add to cart" class="submit" />
								<div class="clear"></div>
							</div>
						</div>	
					</div>
				</td>
			</tr>
			</table>

end commented out table until used later-->
			
			<div class="fullLength badge">
				<img src="images_rebuild/webstore/hdr_badgeSeries.gif" alt="The Shakti Warriors Comic Subscription - Limited Edition - original Series" width="345" height="25" />
				<p>This item is available for members of our Academy. If you have not already joined log in at the top of this page or <a href="http://www.shaktiwarriors.com/application.php" class="quickLink">click here to join!</a></p>
				<p>Each time you move up a rank� we will send you your new badge. </p>
				<div class="order">
					<h1>Our Price: $15.95</h1>
					<p>allow 3-4 weeks for the first delivery.  You must be over 18 to purchase items from this webstore. There are no refunds for ths item.</p>
					 <input size="2" maxlength="2" name="qty_5" value="" />
				     <input type="image" src="images_rebuild/webstore/addToCart.gif" alt="Add to cart" class="submit" />
					<br /><br />
				</div>
			</div>
			
			<table cellpadding="0" cellspacing="0" border="0">
			<tr>
				<!--<td class="stackedShopping bgSet2">
					<img src="images_rebuild/webstore/hdr_workBook.gif" alt="The Shakti Warriors Game Board" width="346" height="21" class="hdr" />
					<div>
						<div class="productImage">
							<img src="images_rebuild/webstore/products/workBook_thumb.jpg" alt="product shot" width="87" height="87" />	
							<a href="#">click to enlarge</a>
						</div>
						<div class="productContent">
							<p>
							Keep track of your Shakti Warrior Points, llearn how to be a good warrior, all in this really cool workbook.  You can pop in more details here.</p>
							<div class="order">
								<h1>Our Price: $89.95</h1>
								<p>allow 3-4 weeks for the first delivery.</p>
								<input size="2" maxlength="2" name="qty_5" value="" />
								<input type="image" src="images_rebuild/webstore/addToCart.gif" alt="Add to cart" class="submit" />
								<div class="clear"></div>
							</div>
						</div>	
					</div>
				</td>-->
				<td><img src="images_rebuild/webstore/cafePressLinkLeft.jpg" /></td>
				<td><a href="http://www.cafepress.com/shaktiwarriors" ><img src="images_rebuild/webstore/cafePressLink.jpg" alt="Visit Our Cafe Press Store!" /></a></td>
			</tr>
			</table>
			
			<!-- Close main content -->
			<!-- Open footer -->
			<div id="footer">
				<div id="footerNav">
					<a href="index.php">Headquarters</a>
					<a href="chaos.php">The Bad Guys</a>
					<a href="mission_recruits.php">Your Mission</a>
					<a href="training_program.php">Training Program</a>
					<a href="contact.php">Contact Us</a>
					<a href="privacy_policy.php">Privacy Policy</a>
					<a href="terms_conditions.php">Terms &amp; Conditions</a>
					<!-- <a href="">About Us</a> -->
				</div>
				<div id="utilNav">
					
				</div>
				<div id="footerCopy">
					<div class="left">&#169; copyright and &trade; trademark 2005, 2006 Healthy Heroes, L.L.C.</div>
					<div class="right">created by TechByDesign</div>
				</div>
			</div>
			<!-- Close footer -->
		</div>
	</body>
</html>
