<?php
    require_once("globals.php");
    require_once("Authenticator.php");

    if(!isset($_SESSION['welcomeCount'])){
        $_SESSION['welcomeCount'] = 1;
    }
    else{
        $_SESSION['welcomeCount']++;
    }

    $pageTitle = "Welcome";
    $login = $user->login;
    $points = $user->getTotalPoints();

    $rank = $user->getRankName();

    $nextRankInfo = $user->getNextRankInfo();

    $takeAssessment = $user->needToTakeAssessment() && $_SESSION['welcomeCount'] > 1;

    $child = $user->getChild();
    $mentorId = empty($child->superheroId) ? 1 : $child->superheroId;
    $superheroService = new SuperheroService();
    $superhero = array_pop($superheroService->find("id=$mentorId"));

    $mentorName = $superhero->name;

    $childHeroService = new ChildHeroService();
    $childHero = $childHeroService->findByUserId($user->id);

    $heroMachineString = $childHero->heroMachineString;
    $heroMachineString = urlencode($heroMachineString);

    $childHeroId = $childHero->id;

    $groups = $user->getTeams();

    $groupInviteService = new GroupInvitationService();
    $invites = $groupInviteService->find("recipient_id = $user->id");

    $requests = $user->getGroupRequestsForGroups();

    $heroCornerInviteService = new HeroCornerInviteService();
    $heroCornerInvites = $heroCornerInviteService->find("recipient_id = $user->id");

    $gaugeData = $user->getGaugeData();

    $nextRank = $user->getNextRank();
    if ($nextRank != null){

        $nextRankPoints = $nextRank->lowerBound;
        $nextRankPointDelta = $nextRankPoints - $points;

        $nextRankPercentage = $points / $nextRankPoints * 100;
    }
    else{
        $nextRankPercentage = 100;
    }

    include("welcome1.phtml");
?>
