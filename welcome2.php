<?php
    require_once("globals.php");
    require_once("Authenticator.php");


    if(!isset($_SESSION['welcomeCount'])){
        $_SESSION['welcomeCount'] = 1;
    }
    else{
        $_SESSION['welcomeCount']++;
    }

    $pageTitle = "Welcome";
    $login = $user->login;
    $points = $user->getTotalPoints();

    $rank = $user->getRankName();

    $nextRankInfo = $user->getNextRankInfo();

    $takeAssessment = $user->needToTakeAssessment();

    $child = $user->getChild();
    $mentorId = empty($child->superheroId) ? 1 : $child->superheroId;
    $superheroService = new SuperheroService();
    $superhero = array_pop($superheroService->find("id=$mentorId"));

    $mentorName = $superhero->name;

    $childHeroService = new ChildHeroService();
    $childHero = $childHeroService->findByUserId($user->id);

    $heroMachineString = $childHero->heroMachineString;
    $heroMachineString = urlencode($heroMachineString);

    $childHeroId = $childHero->id;

    $groups = $user->getTeams();

    $groupInviteService = new GroupInvitationService();
    $invites = $groupInviteService->find("recipient_id = $user->id");

    $requests = $user->getGroupRequestsForGroups();

    $heroCornerInviteService = new HeroCornerInviteService();
    $sql = "
        SELECT
            hero_corner_invites.*,
            users.login
        FROM
            hero_corner_invites INNER JOIN users ON hero_corner_invites.sender_id = users.id
        WHERE
            recipient_id = $user->id
    ";
    $heroCornerInvites = $heroCornerInviteService->findBySql($sql);

    //$assessmentData = $user->getAssessmentData();

    $gaugeData = $user->getGaugeData();

    $nextRank = $user->getNextRank();
    if ($nextRank != null){

        $nextRankPoints = $nextRank->lowerBound;
        $nextRankPointDelta = $nextRankPoints - $points;

        $nextRankPercentage = $points / $nextRankPoints * 100;
    }
    else{
        $nextRankPercentage = 100;
    }

    $level = $user->getCurrentLevel();
    $bgId = $user->backgroundImage;

    $heroesInCorner = $user->getHeroesInCorner();
    $cornerPoints = 0;
    foreach ($heroesInCorner as $cornerHero) {
        $cornerPoints += $cornerHero->getTotalPoints();
    }

    $readArticles = $user->getReadArticles();
    $totalArticlePoints = 0;
    foreach ($readArticles as $readArticle)
        $totalArticlePoints += $readArticle->points;

    $orbs = $user->getOrbs();

    include("welcome2.phtml");
?>
